---
title: Download proxy
description: |-
  Un outil pour permettre le téléchargement de fichiers de confiance bloqués par un pare-feu, en redirigeant ou en transformant le flux.
author: chop

aliases:
    - /outils/download-proxy/
    - /blogue/2020/04/download-proxy-retour/
    - /blogue/2021/01/nouvelles-transformations-download-proxy/

tipping: true
---

Lorsque l'on travaille dans l'informatique en entreprise, il n'est pas inhabituel d'avoir besoin d'un outil ou d'une ressource que l'on ne peut pas télécharger.
Le plus souvent, ceci est dû à des règles de sécurité.
Il existe parfois un processus pour demander ces fichiers, mais il implique généralement de remplir des formulaires, puis d'attendre plusieurs heures ou jours que ceux-ci soient traités.

Ceci a pour résultat que de nombreux développeurs créent leur propre solution pour contourner ces restrictions : un script « _quick and dirty_ » posé sur un serveur obscur, un tunnel SSL pour les plus chanceux et les plus propres…
Comme souvent, lorsqu'il y a un besoin commun, il y a un moyen de mutualiser.
C'est l'objectif de ce projet : fournir un outil basé sur les meilleures idées que des collègues ont mises en œuvre pour contourner un pare-feu d'entreprise récalcitrant.

<div class="notice">

Les pare-feux existent pour une raison : la sécurité.
Ce (la sécurité) devrait être une préoccupation primordiale, aussi bien au niveau de l'individu que de l'entreprise.
J'ai conçu cet outil pour aider les développeurs à travailler, mais elle ne devrait être utilisée qu'en dernier recours.

Dans le cas où _vous utilisez_ cet outil, veillez à rester prudent et soyez certain·es de faire confiance aux fichiers que vous téléchargez.
Je ne cautionne aucune forme d'utilisation de cet outil à des fins illégales.
**Vous endossez seul·e la responsabilité de votre usage du Download Proxy.**

</div>


## Trop long, pas lu

**Si vous avez besoin de télécharger un fichier de confiance mais que le pare-feu de votre entreprise ne vous le permet pas, vous pouvez utiliser l'outil hébergé [ici](https://proxy.keyboardplaying.org).**


## Liens utiles

* **Outil en ligne** : https://proxy.keyboardplaying.org (peut prendre quelques minutes à démarrer, soyez patient·es)
* **Swagger UI** : https://proxy.keyboardplaying.org/q/swagger-ui/
* **GitLab** : https://gitlab.com/keyboardplaying/download-proxy


## Les transformations

Le Download Proxy fonctionne en transformant le fichier afin qu'il échappe à la règle de détection du pare-feu.
Ci-dessous, vous trouverez la description des transformations disponibles dans la version courante.


### Identité (redirection)

**Depuis** : 2.0

**Ce qu'elle fait** :
Le fichier n'est pas modifié, mais le pare-feu détectera qu'il est téléchargé depuis le domaine qui héberge le proxy, au lieu de son domaine d'origine.

**Quand l'utiliser** :
Cette transformation est la plus efficace lorsque la règle qui bloque le téléchargement du fichier est liée à son origine plutôt qu'au fichier lui-même.

**Comment extraire le fichier d'origine** :
Puisque le fichier d'origine n'est pas modifié, aucune action n'est nécessaire.


### Encodage en base64

**Depuis** : 2.0

**Ce qu'elle fait** :
Le fichier téléchargé est un fichier texte dont le contenu est la représentation du téléchargement original encodé en base64.

**Quand l'utiliser** :
Pour contourner un pare-feu qui utilise une détection de type de fichier (il ne verra qu'un fichier texte).
Avec des pare-feux qui contrôlent le contenu du fichier, cela peut entraîner des analyses de plusieurs dizaines de minutes.

**Comment extraire le fichier d'origine** :
Vous pouvez décoder le fichier téléchargé.
Sur la plupart des systèmes d'exploitation, une ligne de commande suffit.

```shell
# Sous Linux ou dans Git Bash
base64 -d my-file.ext.b64 > my-file.ext

# Sous Windows
certutil -decode file.ext.b64 file.ext
```


### Compression

**Depuis** : 1.0, 2.1[^fn-zip-20]

**Ce qu'elle fait** :
Le fichier original est compressé sous forme de fichier zip.

**Quand l'utiliser** :
Pour contourner un pare-feu qui utilise une détection de type de fichier.
Cependant, de nos jours, la plupart des pare-feux inspecte le contenu des zips, donc cela pourrait ne pas suffire.

**Comment extraire le fichier d'origine** :
Décompressez le fichier téléchargé pour en extraire votre téléchargement.

[^fn-zip-20]: Cette transformation a disparu un temps lors du passage en 2.0, mais a été réintroduite dès la 2.1.


### Déguisement de JPEG

**Depuis** : 2.1

**Ce qu'elle fait** :
Les [nombres magiques][magic-numbers] des JPEG sont ajoutés au fichier téléchargé.

**Quand l'utiliser** :
Pour contourner un pare-feu qui utilise une détection de type de ficher (il ne verra qu'une image).

**Comment extraire le fichier d'origine** :
Vous devez supprimer les trois premiers et les deux derniers octets du fichier que vous avez téléchargé, et le renommer pour rétablir la bonne extension.
Sous Linux ou dans Git Bash, vous pouvez le faire avec la commande suivante :

```shell
sed 's/^\xFF\xD8\xFF//g' file.ext.jpg | sed 's/\xFF\xD9$//' > file.ext
```


### Compression et concaténation avec un JPEG

**Depuis** : 2.1

**Ce qu'elle fait** :
Le fichier original est compressé et juxtaposé avec une image.
Le pare-feu et votre ordinateur verront une image, mais certains logiciels seront capables d'en extraire le contenu compressé.[^fn-htg-zip-into-picture]

**Quand l'utiliser** :
Pour contourner un pare-feu qui utilise une détection de type de fichier (il ne verra qu'une image), si annuler le déguisement de JPEG est trop compliqué.

**Comment extraire le fichier d'origine** :
Sous Windows, utilisez [7-Zip][^fn-7zip] ou [WinRAR].
Sous Linux, la commande `unzip` fonctionne.
**Ne modifiez pas l'extension du fichier !**

[^fn-htg-zip-into-picture]: Cette transformation est inspirée [d'un billet sur How-To Geek][htg-zip-img].
[^fn-7zip]: Dans 7-Zip par exemple, ne cherchez pas le menu clic droit > _Extraire_. À la place, ouvrez l'image dans le gestionnaire de fichiers de 7-Zip.

[magic-numbers]: https://fr.wikipedia.org/wiki/Nombre_magique_(programmation)#Indicateur_de_format
[htg-zip-img]: https://www.howtogeek.com/119365/how-to-hide-zip-files-inside-a-picture-without-any-extra-software/
[7-Zip]: https://www.7-zip.org/
[WinRAR]: https://www.rarlab.com/download.htm
