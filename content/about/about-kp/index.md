---
title: About the website
slug: about-keyboard-playing
description: |-
    Keyboard Playing's a personal website that was imagined as a place where multiple contributors could share.
    This page will give you a peek into its history and ideas to support us.
scripts: [ lazy ]
---

## Who Created this Site?

I did.
Me, {{< author "chop" >}}.
I'm a ~~developer~~ software creator.
As many people in this field, I wanted a place to experiment and share what I discovered.

I had blogs before, but never had my own domain.
This changed when I knew I would be using Bootstrap for a mission.
I decided to build my own website with it and make it public.
That's when I went to a registrar.



## Why the Name?

"Keyboard Playing" may seem abstract.
Many developers use their name or pseudo as their domain, but I hoped for a more collaborative space.
I was creating something, but hoping that others might contribute.

I didn't want it to be exclusively software-related either.
I'm not an artist myself, but I love music, videogame and I'd been dreaming of writing for years.
And I already had an obsession with keyboards.

Music, programming, writing…
Pianos, computers, typewriters.
There, you got it, we all play keyboards in our own way.



## Support Is Welcome {#support}

### Money?

This website is not here to make money.
I don't sell your data (I actually [don't collect any]({{< relref path="/about/privacy-policy" >}})).
There are no ads on this website, [nor any tracker]({{< relref path="/blog/2021/02-01-better-social-share-buttons" >}}).

It's time freely and gladly given.
You can tip if you like, that'll help pay for the hosting and domains.
[Click here][tip-url] if you're interested.
The amount is free.

If you'd rather buy something and still help, you can also [buy our book][carrefour-bookelis].
All profits will be reinvested in future projects from [the community][tw-commu].

{{< figure src="/img/carrefour/cover-v2-small.jpg" link="/img/carrefour/cover-v2.jpg" title="_Crossroads of the imaginations_" caption="Our book's cover" >}}


### Other Forms of Support

There are more efficient forms of support than money.

Did you like something that's been shared here?
Knowing that things are appreciated or useful, _that_ is a real motivation.

So, don't hesitate to react, comment.
Tell us how to make things better, any kind of constructive feedback is positive.

Publicity is welcome too: if you deem it worthy, don't hesitate to share.
Or just [send us a message][tw-kp].

Thank you, have a nice time on the website.


[tip-url]: {{< param "tipLink" >}}
[carrefour-bookelis]: https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html
[tw-commu]: https://twitter.com/commudustylo
[tw-kp]: https://twitter.com/KeyboardPlaying
