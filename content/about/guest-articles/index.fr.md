---
title: Publier votre article sur Keyboard Playing
slug: articles-invites
description: |-
    Si vous souhaitez publier un article sur ce site, voici ce qu'il faut savoir.
---


Nous avons été contacté quelques fois pour savoir si Keyboard Playing (KP) accepterait des articles invités.
La réponse courte est bien entendu « Oui ! »
Voici une foire aux questions au sujet de ce que vous devriez savoir.


## KP accepte-t-il des articles invités ?

Oui !
Nous avons toujours imaginé Keyboard Playing davantage comme une communauté que comme un blog individuel.
Si vous souhaitez faire entendre votre voix et votre avis ici, continuez votre lecture !


## Sur quels sujets pourrais-je écrire ?

[C'est un développeur qui a créé Keyboard Playing][about-kp].
Il ne souhaitait cependant pas restreindre son site à sa seule activité.
C'est pourquoi il est parti de son amour des claviers : _tout sujet lié aux claviers et ce qu'ils permettent de faire est le bienvenu_.

Cela couvre une multitude de choses : la création logicielle, l'écriture, la musique, peut-être même le jeu vidéo ?
Les sujets se sont même parfois étendus à des professions autour de ces thématiques premières.
Nous avons par exemple déjà partagés [des conseils sur comment encadrer et travailler avec des développeurs][tag-management].

Si vous hésitez sur l'appartenance de votre sujet à cette liste, contactez-nous et nous déciderons ensemble.


## Dans quelles langues devrais-je écrire ?

Vous avez pu remarquer que le site est publié dans trois langues : français, anglais et espéranto.
Vous n'avez pas besoin d'écrire dans ces trois idiomes pour être publié ici.
Choisissez celui que vous préférez — ou celui qui s'avère le mieux adapté à votre article.

Nous pourrons peut-être vous proposer une traduction vers les autres langues, si c'est pertinent et que nous pouvons y allouer le temps nécessaire.
Bien entendu, vous pourrez décider si cela vous semble acceptable ou non.


## Mon article serait-il altéré ?

Pas sans votre permission.
Nous pourrons offrir une relecture, mais elle sera fournie à titre de proposition.
Vous pourrez accepter ou refuser tout ou partie de nos suggestions.

Nous nous réservons le droit d'ajouter un paragraphe d'introduction ou de conclusion.
Dans ce cas, la mise en forme sera faite de sorte à ne pas permettre de confusion avec votre travail.


## Comment serait protégée ma propriété intellectuelle ?

En premier lieu, votre nom d'auteur sera affiché en haut de l'article, et nous pouvons ajouter un lien vers une page de votre choix.
Toutes les pages du site sont protégées par une licence (voir le pied de page), mais nous pouvons spécifier une licence Creative Commons de votre choix pour votre article, si vous souhaitez par exemple ajouter une clause « non commercial » ou permettre un usage public de votre création (CC0).


## Serais-je payé ?

Malheureusement, non.
Nous pensons que tout travail mérite compensation, mais nous n'avons pas les moyens de vous payer.

[KP reste avant tout un projet personnel][about-kp].
Nous n'affichons aucune publicité et n'avons aucune source de revenus.
Nous avons, en revanche, des frais (hébergement, nom de domaine…).
Notre bilan étant déjà négatif, nous ne pouvons nous permettre d'y ajouter davantage de dépenses.

Nous en sommes sincèrement désolés.


## Et au sujet des histoires ?

Les [histoires][stories] sont une part importante de cette version du site.
La plupart d'entre elles sont relativement courtes : des nouvelles, voire des micronouvelles.
Peut-être accueilleront-elles plus tard d'autres formes, plus longues, épistolaires ou interactives.

Les réponses fournies sur cette page pour les articles restent valide, à l'exception des sujets : laissez votre imagination libre de vagabonder !


## Je suis intéressé(e). Que se passe-t-il maintenant ?

Commencez par <a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>nous envoyer un email</a> !
Nous discuterons ensemble de votre idée.

Si nous sommes tous d'accord pour poursuivre, nous utiliserons une version de test du site pour vous montrer à quoi ressemblerait votre travail, puis nous planifierons ensemble sa publication.


[about-kp]: {{< relref path="/about/about-kp" >}}
[tag-management]: {{< relref path="/tags/management" >}}
[stories]: {{< relref path="/stories" >}}
