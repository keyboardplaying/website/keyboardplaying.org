---
title: Cookies Policy
slug: cookies-policy
description: |-
  This Cookies Policy explains what cookies are and certifies that we don't use them.
cover:
  src: mae-mu-kID9sxbJ3BQ-unsplash.jpg
  alt: Brown cookies in a round bowl.
  by: Mae Mu
  link: https://unsplash.com/photos/kID9sxbJ3BQ
  authorLink: https://unsplash.com/@picoftasty
  license: Unsplash
---

This Cookies Policy explains how Keyboard Playing ("**we**," "**us**," or "**our**") uses cookies to recognize you when you visit our websites at https://keyboardplaying.org ("**Websites**").
It explains what these technologies are, as well as your rights to control our use of them.


## Summary for Humans

Cookies are small data files that are placed on your device when you visit a website.
We don't use them.

Since this is an essential cookie for the correct operation of our Website, there is no Cookie Consent Manager.


## What are cookies?

Cookies are small data files that are placed on your computer or mobile device when you visit a website.
Cookies are widely used by website owners in order to make their websites work, or to work more efficiently, as well as to provide reporting information.

Cookies set by the website owner (in this case, Keyboard Playing) are called "first-party cookies."
Cookies set by parties other than the website owner are called "third-party cookies."
Third-party cookies enable third-party features or functionality to be provided on or through the website (e.g. advertising, interactive content and analytics).
The parties that set these third-party cookies can recognize your computer both when it visits the website in question and also when it visits certain other websites.


## Do we use cookies?

We do not use cookies, nor any other technology that stores information on your device.


## How can you control cookies?

You may set or amend your web browser controls to accept or refuse cookies.
As the means by which you can refuse cookies through your web browser controls vary from browser to browser, you should visit your browser's help menu for more information.


## Do we serve targeted advertising?

We do not serve any advertising and don't share your browsing information with any third party.


## How often will we update this Cookie Policy?

We may update this Cookie Policy from time to time in order to reflect, for example, changes to the cookies we use or for other operational, legal or regulatory reasons.
Please therefore revisit this Cookie Policy regularly to stay informed about our use of cookies and related technologies.


## Where can you get further information?

If you have any questions about our use of cookies or other technologies, please email us at
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.
