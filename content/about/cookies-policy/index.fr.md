---
title: Politique de cookies
slug: politique-cookies
description: |-
  Cette politique de cookies explique ce que sont les cookies et acte notre non-utilisation de cette technologie.
cover:
  src: mae-mu-kID9sxbJ3BQ-unsplash.jpg
  alt: Des cookies dans un bol rond.
  by: Mae Mu
  link: https://unsplash.com/photos/kID9sxbJ3BQ
  authorLink: https://unsplash.com/@picoftasty
  license: Unsplash
---

Cette politique de cookies explique comment Keyboard Playing (« **nous** », « **notre** » ou « **nos** ») utilise les cookies pour vous reconnaître lorsque vous visitez notre site Web https://keyboardplaying.fr (« **Site** »).
Elle explique ce que sont ces technologies, ainsi que les droits de contrôle que vous avez sur notre utilisation de celles-ci.


## Résumé pour les humains

Les cookies sont de petits fichiers de données qui sont placés sur votre appareil lorsque vous visitez un site Web.
Nous ne les utilisons pas.

Puisqu'il s'agit d'un cookie essentiel au bon fonctionnement de ce Site Web, nous n'offrons pas de gestionnaire de consentement aux cookies.


## Que sont les cookies ?

Les cookies sont de petits fichiers de données qui sont placés sur votre ordinateur ou appareil mobile lorsque vous visitez un site Web.
Les cookies sont largement utilisés par les propriétaires de sites Web afin de faire fonctionner leurs sites Web, ou de travailler plus efficacement, ainsi que pour fournir des informations de rapport.

Les cookies définis par le propriétaire du site Web (dans ce cas, le jeu au clavier) sont appelés « cookies propriétaires ».
Les cookies définis par des parties autres que le propriétaire du site Web sont appelés « cookies tiers ».
Les cookies tiers permettent aux fonctionnalités ou fonctionnalités tierces d'être fournies sur ou via le site Web (comme par exemple la publicité, le contenu interactif et l'analyse).
Les parties qui définissent ces cookies tiers peuvent reconnaître votre ordinateur à la fois lorsqu'il visite le site Web en question et également lorsqu'il visite certains autres sites Web.


## Utilisons-nous des cookies ?

Nous n'utilisons pas de cookies, ni aucune autre technologie permettant de stocker des informations sur votre appareil.


## Comment pouvez-vous contrôler les cookies ?

Vous pouvez définir ou modifier les contrôles de votre navigateur Web pour accepter ou refuser les cookies.
Si vous choisissez de le faire, vous pouvez toujours utiliser nos sites Web bien que votre accès à certaines fonctionnalités et zones de nos sites Web puisse être restreint.
Étant donné que les moyens par lesquels vous pouvez refuser les cookies via les contrôles de votre navigateur Web varient d'un navigateur à l'autre, vous devez consulter le menu d'aide de votre navigateur pour plus d'informations.


## Servons-nous de la publicité ciblée ?

Nous ne diffusons aucune publicité et ne partageons vos informations de navigation avec aucun tiers.


## À quelle fréquence mettrons-nous à jour cette politique de cookies ?

Nous pouvons mettre à jour cette politique en matière de cookies de temps à autre afin de refléter, par exemple, les modifications apportées aux cookies que nous utilisons ou pour d'autres raisons opérationnelles, légales ou réglementaires.
Veuillez donc revoir régulièrement cette politique en matière de cookies pour rester informé de notre utilisation des cookies et des technologies associées.


## Où pouvez-vous obtenir davantage d'informations ?

Si vous avez des questions sur notre utilisation des cookies ou d'autres technologies, contactez-nous à l'adresse
<a href='&#109;ailto&#58;&#99;o%&#54;Et%61&#99;%74&#64;%6Beyboa&#114;&#100;&#112;l&#37;&#54;&#49;yi&#110;g%2&#69;org'>c&#111;&#110;tac&#116;&#64;&#107;eyb&#111;&#97;&#114;d&#112;lay&#105;ng&#46;&#111;rg</a>.
