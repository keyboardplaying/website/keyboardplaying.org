---
date: 2020-02-14T13:44:33+01:00
title: Use editorconfig for Your Formatting Configuration
slug: editorconfig-formatting-configuration
description: |-
  Sharing a formatting configuration is often difficult, but editorconfig makes it easier.
author: chop
categories: [ software-creation ]
tags: [ programming ]
keywords: [ editorconfig, formatting, code formatting, collaboration, tool, space trimming, end of line ]

references:
- id: editorconfig
  name: EditorConfig
  url: https://editorconfig.org
  lang: en
---

Today's Valentine's day, the best day in the year to tell you how I fell in love with [editorconfig].

<!--more-->

## How We Met

It was a day of 2017, I was fooling around with [JHipster][jhipster].
I created a new project and it called a bunch of its friends to the party.
I already knew most of them, and I liked most of those.
One of them, however, was totally unknown to me: a file named `.editorconfig`.

Of course, since I had no idea what it was, I took a mouthful of [my own medicine][know-the-tech] and investigated it to know who it was and what it did in life.
And it was a wonderful, wonderful solution to a problem I'd met quite a few times.



## The Problem editorconfig Addresses

In the past, I've met several cases when a team wanted to share IDE settings with their source code.
In almost all those occurrences, the main purpose was to share a formatting configuration.
Yet, they often shared much more than that[^fn-project-config-eclipse].
Too often, **it included other settings, which should remain computer- or user-specific**.
This becomes hell to manage when a developer commits those, breaking the build for other developers, who will correct and likely commit them again, continuing for as long as the project lives.

[^fn-project-config-eclipse]: I find this especially true with Eclipse, which is still the most widespread IDE in my experience, though VS Code may soon win the throne.

Furthermore, even sharing an IDE's configuration is not bulletproof, especially in this day and age.
Every developer has an acquired taste for an editing platform or another.
Most "old school"[^fn-not-that-old] Java developers stick to Eclipse while newer generations seem to prefer VS Code, and a few in between are fervent fans of IntelliJ IDEA.
But **getting all these editors to format code the same way requires hours of fine-tuning their configuration**, especially when it comes down to code indentation and supposing it's even achievable.
That's when you get whole classes versioned because a developer formatted it with VS Code while another uses Eclipse...

[^fn-not-that-old]: They rarely are _that_ old.

I'm of the opinion that an IDE configuration should not be part of a project, but the way code is formatted may be considered a characteristic of it.
As such, it may be versioned along the source code.
That's what we're talking about here: **editorconfig provides a simple, shareable code formatting configuration**.



## editorconfig's Solution

### The Bird's-Eye View

[editorconfig] allows you to specify several formatting settings in a single file---or more if you prefer it so.

Once this file is ready, your IDE[^fn-ide-plugin] or text editor will apply this configuration as you edit your files.
Let's take a closer look at an example.

[^fn-ide-plugin]: In many cases, the IDE will rely on a plugin, but many IDE also include this plugin by default.


### editorconfig Through an Example

Below is an extract of the `.editorconfig` file I use when working on this website.

```ini
root = true

[*]
# Use 4 spaces for indentation
indent_style = space
indent_size = 4
# Establish some standards
charset = utf-8
end_of_line = lf
insert_final_newline = true
trim_trailing_whitespace = true

[*.md]
trim_trailing_whitespace = false

[*.{htm,html}]
indent_size = 2

# Don't add new lines at the end of some partials/shortcodes
# They mess with the non-compressed rendering of the website
[layouts/{shortcodes,partials/{utils,images}}/*.html]
insert_final_newline = false
```

As you can see, the file is structured in blocks.
For each one, you have one pattern to describe the files it applies to and then the settings that apply.


#### File path patterns

The pattern is basically a filepath glob, much like the one you would use in a `.gitignore` file.
And, much like with the `.gitignore`, you can match all files with a matching name or use more specific rules, relying on a more complete path.

You can use `*` as a wild card and `{pattern1, pattern2}` to match several distinct patterns.
Actually, you almost have all the power of regex with a lighter syntax.

| Pattern        | Matched sequence                               |
| -------------- | ---------------------------------------------- |
| `*`            | Any string of characters, except `/`           |
| `**`           | Any string of characters                       |
| `?`            | Any single character                           |
| `[list]`       | Any single character in _list_                 |
| `[!list]`      | Any single character not in _list_             |
| `{s1,s2,s3}`   | Any of the strings given (separated by commas) |
| `{num1..num2}` | Any integer number between _num1_ and _num2_   |

In my configuration, for instance, the first block `*` matches all files, so that block is my default configuration.
You can see patterns that match HTML and Markdown files.

The most interesting pattern is this one: `layouts/{shortcodes,partials/{utils,images}}/*.html`.
It uses both the wild card and alternating pattern, and even nests an list within a list.
It matches the following paths:
- `layouts/shortcodes/*.html`
- `layouts/partials/utils/*.html`
- `layouts/partials/images/*.html`

This example demonstrates the richness of this pattern system as it can be quite precise.


#### Formatting settings

The available settings are rather self-explanatory.

| Setting                    | Description                                                      | Possible values     |
| -------------------------- | ---------------------------------------------------------------- | ------------------- |
| `indent_style`             | The type of indentation you prefer                               | `tab`, `space`      |
| `indent_size`              | The size of indentations                                         | An integer          |
| `charset`                  | The charset to use                                               | [A charset ID](https://github.com/editorconfig/editorconfig/wiki/Character-Set-Support) |
| `end_of_line`              | The character you use for end of line                            | `lf`, ` cr`, `crlf` |
| `insert_final_newline`     | Whether an empty new line should be added at the end of the file | `true`, `false`     |
| `trim_trailing_whitespace` | Whether or not trailing spaces on each line should be removed    | `true`, `false`     |


#### Regarding the Resolution Mechanism

There is one line in the file I have not written about yet: `root = true`.
This indicates that this `.editorconfig` is the root.

When working, editorconfig will look for `.editorconfig` in the current directory, then merge with the one in the parent directory, then merge with...
This goes on until it meets the root `.editorconfig`.

As far as I'm concerned, I suggest using only one config file, and set it as root.
Much like `.gitignore`, you can indeed apply have a file in each directory, but doing so makes it difficult to get a consolidated view of all the rules and settings applied, while the patterns are sufficiently flexible to use a single file.



## The Limitations

So that's too good to be true, right?
There's _got_ to be some limitations.

Actually, I don't see much that could be blocking.
Firstly, you need a plugin, but today's editors are basically---or literally, for some of them---are platforms for plugins, so I'm not sure that's truly a problem.

Another limitation is that it handles text styling but not all of the styling aspects of code.
For instance, it won't automatically add braces on your single-statement `if`s and `for`s, and it won't either control the order of your Java imports (which is not the same in Eclipse and IDEA).
**editorconfig is mainly about indenting, charset and end of lines**.



## Configuration and Plugin Details

All details are available on [editorconfig's page][editorconfig], especially:
- the configuration details;
- the editors and IDEs supporting it by default;
- links to plugins for other editors, and for Ant and Maven.

Also check [the wiki](https://github.com/editorconfig/editorconfig/wiki) for [an exhaustive list of properties](https://github.com/editorconfig/editorconfig/wiki/EditorConfig-Properties) and more.

I hope you'll enjoy!


{{% references %}}

[jhipster]: https://jhipster.tech
[know-the-tech]: {{< relref path="/blog/2019/12-27-know-technology-not-framework" >}}
