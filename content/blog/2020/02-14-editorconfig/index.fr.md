---
date: 2020-02-14T13:44:33+01:00
title: editorconfig, le parfait compagnon pour le formatage de votre code
slug: editorconfig-formatage-code
description: |-
  Partager sa configuration de formatage est souvent difficile, mais editorconfig est là pour vous faciliter la tâche.

author: chop
categories: [ software-creation ]
tags: [ programming ]
keywords: [ editorconfig, formatage, formatage de code, collaboration, outil, nettoyage d'espaces, fin de ligne ]

references:
- id: editorconfig
  name: EditorConfig
  url: https://editorconfig.org
  lang: en
---

Pour la Saint-Valentin, j'avais envie de vous raconter comment je suis tombé amoureux d'[editorconfig].

<!--more-->

## Notre rencontre

C'était en 2017.
Je m'amusais avec [JHipster][jhipster].
Un jour, j'ai créé un projet et il a appelé plusieurs de ses amis à la fête.
Je connaissais déjà la plupart d'entre eux, et je les appréciais pour la plupart.
Un inconnu attira toutefois mon attention : un petit fichier nommé `.editorconfig`.

Bien entendu, ne sachant pas de quoi il s'agissait, j'ai suivi [mes propres conseils][know-the-tech] et ai effectué quelques recherches : qui était-il, quel était son but dans la vie ?
Et il s'avéra être une solution merveilleuse à un problème que j'avais déjà rencontré plusieurs fois.



## Le problème qu'adresse editorconfig

Par le passé, j'ai plusieurs fois rencontré des équipes partageant les paramètres de leur EDI avec le code source.
Pratiquement à chaque fois, c'était le formatage du code qui leur importait réellement.
Ils partageaient pourtant souvent bien plus que cela[^fn-project-config-eclipse].
Bien trop souvent, **cela incluait d'autres paramètres, qui auraient dû rester spécifiques à un poste ou un utilisateur**.
C'est ainsi que naissent les enfers : un développeur versionne ses paramètres, cassant la compilation de ses collègues, qui corrigeront et pousseront leurs propres changements, et ainsi de suite jusqu'à la fin du projet...

[^fn-project-config-eclipse]: Je trouve ceci particulièrement vrai d'Eclipse, qui reste l'EDI Java le plus répandu autour de moi, bien que VS Code soit en passe de le détrôner.

Outre le problème de périmètre, le partage de configuration n'est pas la solution miracle, en particulier à notre époque.
Les éditeurs se multiplient et les goûts des développeurs sont variés.
Il semble de plus en plus difficile de contraindre tout le monde à utiliser les mêmes outils : les plus « anciens[^fn-not-that-old] » des développeurs Java semblent continuer à préférer Eclipse tandis que les générations les plus jeunes glissent vers VS Code, tandis que quelques marginaux restent des fanatiques inconditionnels d'IntelliJ IDEA.
**Faire en sorte que tous ces éditeurs formatent le code de la même façon représente des heures à ajuster leurs configurations respectives**, en particulier concernant l'indentation et en supposant que ce soit même possible.
C'est ainsi que des classes entières sont versionnées parce qu'un développeur l'a formatée avec VS Code, puis écrasée par celui qui préfère Eclipse...

[^fn-not-that-old]: Ils ne sont généralement pas _si_ vieux.

Je suis d'avis que la configuration de l'EDI ne devrait pas faire partie du projet, mais la façon dont le code doit être mis en forme en est une caractéristique.
En tant que tel, il peut être légitime de versionner cette information avec le code source.
Voici de quoi nous parlons ici : **editorconfig est un moyen simple de partager la configuration de formatage du code**.



## La solution d'editorconfig

### La vue macroscopique

[editorconfig] vous permet de spécifier plusieurs options de mise en forme au sein d'un unique fichier --- ou plusieurs si vous le préférez.

Une fois ce fichier écrit, votre EDI[^fn-ide-plug-in] ou votre éditeur de texte appliquera cette configuration lorsque vous éditerez ou sauvegarderez vos fichiers.
Regardons cela de plus près avec un exemple.

[^fn-ide-plug-in]: La plupart du temps, l'EDI s'appuie sur un plug-in, mais plusieurs EDI intègrent le plug-in en question par défaut.


### editorconfig à travers un exemple

Ci-dessous, vous trouverez le fichier `.editorconfig` de ce site web.

```ini
root = true

[*]
# Use 4 spaces for indentation
indent_style = space
indent_size = 4
# Establish some standards
charset = utf-8
end_of_line = lf
insert_final_newline = true
trim_trailing_whitespace = true

[*.md]
trim_trailing_whitespace = false

[*.{htm,html}]
indent_size = 2

# Don't add new lines at the end of some partials/shortcodes
# They mess with the non-compressed rendering of the website
[layouts/{shortcodes,partials/{utils,images}}/*.html]
insert_final_newline = false
```

Comme vous pouvez le voir, le fichier est structuré en blocs.
Pour chacun, on a un identifiant décrivant les fichiers auxquels ce bloc s'applique, suivi des réglages en question.


#### Masques de chemins

Le masque est similaire à un « glob », comme ceux que l'on utilise dans un fichier `.gitignore`.
De même, comme avec `.gitignore`, vous pouvez utiliser un masque n'utilisant que le nom du fichier ou créer des règles plus spécifiques s'appuyant sur le chemin.

On a bien entendu le caractère de remplacement `*`, mais on peut également faire une liste de valeurs avec `{pattern1, pattern2}`.
En fait, on n'est pas loin de pouvoir faire des regex, mais avec une syntaxe différente.

| Masque         | Valeur détectée                                                     |
| -------------- | ------------------------------------------------------------------- |
| `*`            | Toute chaîne de caractères à l'exception de `/`                     |
| `**`           | Toute chaîne de caractères                                          |
| `?`            | Tout caractère                                                      |
| `[list]`       | Tout caractère dans _list_                                          |
| `[!list]`      | Tout caractère absent de _list_                                     |
| `{s1,s2,s3}`   | N'importe laquelle des chaînes fournies (séparées par des virgules) |
| `{num1..num2}` | Tout entier entre _num1_ et _num2_                                  |

Dans ma configuration, par exemple, le premier bloc `*` s'applique à tous les fichiers, ce qui en fait ma configuration par défaut.
Vous pouvez voir des masques pour les fichiers HTML et Markdown.

Le masque le plus intéressant cependant est le suivant : `layouts/{shortcodes,partials/{utils,images}}/*.html`.
Il utilise à la fois le caractère de remplacement général et des listes de valeurs, dont certaines sont imbriquées.
Il détecte les chemins suivants :
- `layouts/shortcodes/*.html`
- `layouts/partials/utils/*.html`
- `layouts/partials/images/*.html`

Cet exemple montre la richesse et la flexibilité de ce système de masques, qui peut être assez précis.


#### Les paramètres de formatage

Les paramètres disponibles sont plutôt explicites.

| Paramètre                  | Description                                                      | Valeurs possibles   |
| -------------------------- | ---------------------------------------------------------------- | ------------------- |
| `indent_style`             | Le type d'indentation préférée                                   | `tab`, `space`      |
| `indent_size`              | La taille d'indentation                                          | Un entier           |
| `charset`                  | Le charset à utiliser                                            | [A charset ID](https://github.com/editorconfig/editorconfig/wiki/Character-Set-Support) |
| `end_of_line`              | Le format de fin de ligne                                        | `lf`, ` cr`, `crlf` |
| `insert_final_newline`     | `true` pour ajouter une nouvelle ligne en fin de fichier         | `true`, `false`     |
| `trim_trailing_whitespace` | `true` pour supprimer les espaces en fin de ligne                | `true`, `false`     |


#### Le mécanisme de résolution

Il reste une ligne dans le fichier dont je n'ai pas encore parlé : `root = true`.
Elle indique que cet `.editorconfig` est la racine de ma configuration.

Quand il s'exécute, editorconfig cherche un `.editorconfig` dans le répertoire courant, puis le fusionne avec celui du répertoire parent, puis avec celui...
Ce mécanisme continue jusqu'à trouver l'`.editorconfig` racine.

En ce qui me concerne, je vous suggère de n'utiliser qu'un seul fichier à la base de votre projet.
Tout comme `.gitignore`, même si vous pouvez techniquement disposer d'une configuration par répertoire, une telle approche nuira à la lisibilité et vous empêchera d'avoir une vue consolidée des règles appliquées.
Je conseille d'utiliser des masques plus ou moins précis à la place.



## Les limites

Alors, c'est trop beau pour être vrai, hein ?
_Forcément_, j'ai planqué un truc sous le tapis !

En réalité, pas tant que ça.
Je ne vois rien qui soit bloquant en tout cas.
Le premier « piège » est le besoin d'utiliser un plug-in, mais les grands éditeurs aujourd'hui sont --- parfois littéralement --- des plateformes à plug-ins, donc je ne suis pas certain que ce soit un vrai problème.

Une autre restriction est qu'**editorconfig est parfait pour gérer les indentations, l'encodage et les fins de ligne**, mais il ne comprend pas votre code.
Par exemple, il ne va pas automatiquement ajouter les accolades manquantes à vos `if`s et `for`s, et il n'a aucun contrôle sur l'ordre des imports Java non plus (ordre qui diffère entre Eclipse et IDEA).



## Plus d'infos sur la configuration et les plug-ins

Tous les détails sont disponibles sur [la page d'editorconfig][editorconfig], en particulier :
- les détails de configuration ;
- les éditeurs et EDI le supportant par défaut ;
- les liens vers les plug-ins pour les autres éditeurs, ainsi que pour Ant et Maven.

Je vous invite également à parcourir [le wiki](https://github.com/editorconfig/editorconfig/wiki) pour notamment voir [une liste exhaustive des propriétés supportées](https://github.com/editorconfig/editorconfig/wiki/EditorConfig-Properties) et bien plus.

En espérant que cela vous plaise !


{{% references %}}

[jhipster]: https://jhipster.tech
[know-the-tech]: {{< relref path="/blog/2019/12-27-know-technology-not-framework" >}}
