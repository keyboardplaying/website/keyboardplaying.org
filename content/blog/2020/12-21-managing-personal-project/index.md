---
date: 2020-12-21T22:37:42+01:00
title: Managing a personal project
subtitle: Some takeaways
slug: managing-personal-project
description: |-
  It is common for developers to work on personal projects outside work, but have you ever tried to manage one such project?
  I did.
  Here's what I take away.
cover:
  #banner: false
  src: gantt-2020-08-30.svg
  socialSrc: gantt-2020-08-30.jpg
  alt: A Gantt diagram representing the state of the project at the end of August
  by: chop
  license: CC BY-NC 4.0
author: chop
categories: [ projects ]
tags: [ management ]
keywords: [ carrefour des imaginaires, project management, writing, gantt, time, burn out, collaboration ]
---


As a developer, I've had my share of side projects but, being the lone wolf I am, I always worked alone on those.
This year, the [creative community I'm part of][commudustylo] decided on achieving something that would require some work.
I thought I had a quite clear vision and decided to manage this project.

Here's what I take away from it.

<!--more-->

## The project

Before anything else, let's agree on the vocabulary: I've already used "personal project" and "side project."
They're the same to me, at least in the context of this post: a project you do on your spare time, with little hope to earn money from it.
This is more than a definition, these are drivers to keep in mind when managing this kind of project, but I'll come back to it later.


### The scope

<aside><p>We wanted to self-publish a book for Christmas.</p></aside>

So, what was _this_ project?
Quite simply, we wanted to self-publish a book we could give to our loved ones for Christmas.
This book was to be built around our [bimonthly challenges] and include illustrations from the drawers that make our community a bit different than most.

The main tasks were easy to see:
- Create some new content, especially allow those who hadn't taken all challenges to add new contributions.
- Proofread everything. Several times. By different people.
- Create an illustration for each challenge and create a cover.
- Self-publish and maybe promote the book if we decided to make it publicly available.

The timeframe could have been comfortable, but as it turns out, it was not excessive either: we started in June and our objective was to finish by the beginning of November, so that we would have spare time for unpredicted issues.


### Spoilers

First, we _did_ succeed in creating the book.
Almost all of us have received it just one week before Christmas, though one of us had a technical issue when ordering and won't have it in time.
Though we've been satisfied with [Bookelis] for most of the process, customer service is clearly not their strength.

The other question you may already have by this point: will the book be publicly available?
Yes, it will be.
For a price that we think is reasonable.
We will share the link during the upcoming week.
Any benefit we make from selling it will be saved for future projects by our community.


## The takeaways

These are not lessons.
Some of these I already knew from professional experience, or I did as an attempt to avoid mistakes I've seen.

I'm not a project manager, and this experience confirms it's not the role I'm the best at or enjoy the most.
Still, I work quite closely with them in my daily life, and I must manage people, so I had some experience and vision to draw upon.


### Give your team a vision {#vision}

I worked on a project that almost [burned me out][exhaustion] some time ago.
One thing I retain from it: we didn't know where we were going, nor where we wanted to go.
Everything seemed to radically change far too often.

You can't predict everything that will happen on a project from its start, but people need to know the main idea.
Give them a clear main direction and stick to it the best you can.
The details will change, there will be corrections, but all those on the boat need to know where they want to go, and put all their efforts in going there together.

In this case, when the project started, I took some time to describe our main objectives (have a book with our names on it to give on Christmas), some optional stretch goals (make it publicly available) and submit it to the vote.

The next thing I did was list all the tasks I thought of and establish a Gantt diagram that included them all and finished near our target date.
Believe it or not, several people on the team expressed some positive feeling at seeing it.
It gave everyone an idea of what was to be done, and when.

<aside><p>Give your team something to aspire to.</p></aside>

More than a mere direction, give your team a vision, something to aspire to.
We knew, instinctively, that being able to hold a physical book in our hands, something we made, and offering it to people we love, would be something we'd be proud of.
That kept us going.


### Don't underestimate the required time

At work, I'm often asked to estimate the time for a task.
After ten years of doing it, I'm still often too optimistic.

<aside><p>Even with a comfortable margin, we overshot.</p></aside>

This time, I used more precautions.
I allocated two whole months to proofreading existing content.
The same two months would allow new contributions that would be proofread, too.
Overall, all content should have been written and corrected over the course of three months, by the beginning of September.

And yet, I still found some mistakes and did some software-assisted proofreading when I built the first body for the book, by the _end_ of September.
Even with a large margin, we overshot.

Our Gantt diagram was regularly updated to take into account the unforeseen tasks and the ones that took longer than expected.
On my first diagram, I thought the cover would be illustrated by the end of August.
In reality, it was one of the last things we finished.

![Gantt diagram, June 9, 2020](gantt-2020-06-09.svg)

![Gantt diagram, July 25, 2020](gantt-2020-07-25.svg)

![Gantt diagram, September 13, 2020](gantt-2020-09-13.svg)

There are causes to it.
Time in a personal project is not as manageable as time in a professional project.


#### The time allocated to the project is a complicated thing

A professional project has allocated time, when people working on it must give it their full attention (yes, I'm talking about your work time).
When working on my lone side projects, it's (relatively) easy to reserve dedicated time slots over the week, for instance while commuting.
But when you're part of a group working on a side project, things are different.

<aside><p>You will get only a portion of each project member’s spare time.</p></aside>

Each person working on it is committing to spend some of their spare time to it, but "spare time" is a fleeting notion.
A young, single person may have a lot of it, but mainly, people have constraints, they deserve time with their families, they need to sleep...
To me, it meant putting all my other side projects on pause, which explains why this website has remained so calm since June.

Production is not linear when working on a side project.
When the stars align, everyone will be available and committed.
Most of the time, only some members work at their fullest, while others have to deal with personal issues you don't even imagine.

Don't think you're above it!
Even if you're the most impatient to see the project done, you'll have your bad times, too.
Mine was October and November, when we suddenly had an ambitious objective with a short deadline at work.
My spare time was reduced to the bare minimum, and I was so tired by November that I had no more energy to work on any kind of project.
I just needed rest.

Yes, you must manage your time and your energy.
That's not an easy thing to do.


#### Spare time may be reallocated

This paragraph is closely related to the previous one.
I highlighted that we can only get a portion of spare time for each project member, but those members may tire from working seemingly endlessly on the same repetitive, boring topics.

<aside><p>People will have new ideas they’ll want to try, but not for your project.</p></aside>

Imagine for instance that you've been proofreading the same short stories over and over for four weeks, while a new idea has been gnawing at you, like learning to draw with the latest tools you gifted yourself.
It's a natural behavior, we need novelty.

This implies you need to find incentives to keep people motivated, but this is not the professional world: you're not an angry manager with power over your employees, and their work will not earn them anything.
You need something else, and for us, that was the [vision](#vision) of our success.

It didn't prevent us from dabbing in other projects (I tried [the Lemontober and the Writober][creatober], for instance).
But overall, the project did move on.


### "Control freaks" are bad managers

Yeah, I'm talking about myself.
I'm not sure I'm a control freak, but I like things done the way I imagined it, or better, but I think it's a common flaw to think that you already thought what was best.

At work, I've been asked to delegate for years but I often prefer to do things myself.
I'm only now beginning to succeed at it.

A manager who wants everything done their way can become bad in two ways: either they will micromanage every task, or they'll do it themselves.

In the professional world, when a manager is too intrusive, you either endure it or try to make things run smoother with them.
You _have_ to work with them anyway.

On a side project, if you don't want to work with someone and they don't take the hint, well...
You'll let go of the side project and reallocate your spare time to something more agreeable.

Don't try to micromanage anyway.
Trust the [vision](#vision) and your teammates.

As for the case where the manager assigns themselves all the work they want to do, that was me.
It could have gone wrong in any number of ways, but it didn't, and that's my last takeaway.


### Ask for forgiveness, not permission

Yeah, it's been overused, but probably because it's not entirely wrong.

I already spoke about availability, motivation and so on.
I was motivated, I knew where I wanted to go and I understood that not everybody could dedicate more time.

So, some things I took upon myself to do.
That it had to be done was not in question.
_How_ it would be, however, had not been discussed.

I had the choice of either talking it out with everybody, assuming everybody would be available, or make a first version and submit it to the group for further enhancements.
Some propositions were immediately accepted.
For others, we dropped them altogether and started from scratch.
I remember a cover proposition I had designed over the course of a whole afternoon that was not to the taste of others (and they were right, what we have now is much better).
Another one was a draft, but it gave ideas and started exchanges that led to the cover as it is now.

Just remember, when you take an initiative before consulting your teammates, you must be ready for your proposition to be refused and your work to be in vain.


### Trust your teammates

I could go on, but this post's getting long, so one final piece of advice: trust your teammates.
There's a reason to build a team: you go faster alone, you go farther together.
They will have ideas and skills you won't.
The exchange will make things better.

You chose to be part of a team, play collective.


[commudustylo]: https://twitter.com/commudustylo
[bimonthly challenges]: /stories/inspirations/bimonthly/
[Bookelis]: https://bookelis.com/
[exhaustion]: /stories/exhaustion/
[creatober]: https://twitter.com/chopAuteur/status/1322834283115982848
