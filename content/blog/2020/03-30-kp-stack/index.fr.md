---
date: 2020-03-30T21:47:22+02:00
title: La pile technologique de Keyboard Playing
subtitle: J'espère que le courant passe bien avec le statique.
slug: pile-technologique-keyboard-playing
description: |-
  Keyboard Playing est construit autour de Hugo, un générateur de site statique écrit en Go, et NPM pour rattacher les pièces.
cover:
  src: hello-i-m-nik-RHcvA5zYoVg-unsplash.jpg
  alt: Des briques Lego dans l'herbe
  by: Hello I'm Nik
  link: https://unsplash.com/photos/RHcvA5zYoVg
  authorLink: https://unsplash.com/@helloimnik
  license: Unsplash
author: chop
categories: [ software-creation ]
tags: [ sustainable-it, open-source, programming, web-dev, golang ]
keywords: [ site statique, générateur de site statique, hugo, webpack, rollup, npm, staticman, gitlab, intégration continue, CI, déploiement continu, CD, matomo, vie privée, numérique responsable, empreinte, markdown, commonmark, asciidoctor, analytics ]
---

La semaine dernière, {{< author "tony" >}} m'a demandé la pile technologique derrière mon site.
C'est un sujet sur lequel je voulais écrire lorsque le site serait stable, mais je continue de le bricoler.
En tant que tel, il est loin d'être terminé et j'ai encore plein d'idées, mais parlons-en néanmoins.

<!--more-->

Lorsque j'ai commencé à refaire le site, je me suis fixé pour objectif de le rendre aussi statique que possible.
Puis j'ai découvert [Hugo], qui m'a grandement aidé à atteindre ce but.



## La pile pour les impatients

Si vous ne voulez pas vous farcir toute l'histoire qui m'a mené à cette pile, voici le résumé :

- [Hugo] génère des pages statiques :
  - Il transforme des fichiers Markdown en pages.
  - Il génère plusieurs tailles de chaque image affichée.
  - Il transpile mon SCSS en feuilles de style, tout en appliquant les règles [PostCSS] que j'ai programmées.
- [Rollup] rassemble mes fragments de JavaScript en un tout cohérent.
- [NPM] pilote tout ça.
- [Staticman] transforme les commentaires postés en commits.
- [GitLab CI/CD] automatise la construction et le déploiement de tout ça.
- [Matomo] sera utilisé pour des statistiques respectueuses de la vie privée.



## De la table de dessin...

Si vous avez un peu plus de temps à me consacrer, vous vous demandez peut-être comment j'en suis venu à ces choix.
Je suis profondément convaincu qu'il n'y a pas de panacée quand on construit une pile, et se contenter de répéter nos habitudes ne donne pas une solution optimale.
Avant de choisir la dernière techno à la mode, j'avais besoin de savoir ce que je construisais.


### Spécifications fonctionnelles

<aside><p>Je voulais une plateforme de publication internationalisée.</p></aside>

En gros, je voulais **une plateforme pour publier**.
Des billets, des nouvelles, des projets informatiques...
Mes expériences passées avec Dotclear et WordPress m'ont inspiré d'autres spécifications.
Tout d'abord, je voulais pouvoir **publier à la fois en anglais et en français**, en liant les pages équivalentes.
J'apprécie aussi les **taxonomies**, bien qu'elles puissent sembler gadget.
Enfin, la possibilité de **commenter** est indispensable.

Il est également devenu commun de voir [Github Pages] comme blogue.
J'aime l'idée que n'importe qui puisse **proposer une correction ou amélioration**.

Enfin, j'adorerais avoir quelques **statistiques** pour savoir ce qui intéresse les lecteurs.
Si possible, **en évitant les cookies et autres technologies d'espionnage**.


### Spécifications non fonctionnelles

Ce n'est pas tout de vouloir des fonctionnalités, on a d'autres contraintes.

<aside><p>Je souhaitais une plateforme responsable : statique, sobre et open source.</p></aside>

Puisque je parle de [**numérique responsable** et d'écoconception][tag-sustainable], il n'était que juste (et intéressant) d'appliquer les bonnes pratiques à mon projet et démontrer qu'un peu de réflexion en amont peut avoir un impact tout en laissant la solution utilisable.

J'ai parlé d'habitudes.
Si j'avais demandé conseil avec les spécifications ci-dessus, on m'aurait certainement dit de prendre un WordPress.
Cependant, la plupart des CMS calculent la page _chaque fois_ qu'un lecteur y accède, alors qu'il y a peu de chance que cette page ait changé depuis la dernière fois qu'elle a été vue.
C'est pourquoi je préférais **déployer un contenu statique** si possible.

J'avais aussi envie de sobriété : **utiliser aussi peu de ressources externes que possible**, pas de scripts externes, feuilles de style...
En autres termes, je voulais réduire le nombre de requêtes HTTP nécessaires à l'affichage d'une page.

Un dernier souhait de ma part était de m'appuyer autant que possible sur des solutions **open source**.


### Pour résumer

* Plateforme de publication avec internationalisation et taxonomies
* Commentaires
* Propositions d'améliorations
* Statistiques respectueuses de la vie privée
* Statique
* Sobre / économe en bande passante
* Open source



## ... À une pile technologique

### Le choix d'un générateur statique

À la fois [Github Pages] et [GitLab Pages] font ce que je veux : créer des pages statiques sur base de données structurées et d'un modèle de mon choix.
Par le passé, j'ai regardé comment le premier fonctionnait et ai regardé [Jekyll]
À cette époque cependant, je n'ai pas trouvé de guide de « démarrage rapide » et ai rapidement abandonné.

<aside><p>Hugo est un générateur statique écrit en Go, extrêmement rapide.</p></aside>

Pour Keyboard Playing, j'ai rapidement parcouru les moteurs de recherche et [Hugo] a vite montré le bout de son nez.
Je l'ai rapidement sélectionné sans trop comparer : il s'intègre avec d'autres outils qui m'intéressaient et il est écrit en Go.
J'ai un collègue / mentor qui a tendance à se focaliser sur tout ce qui est construit en [Go][tag-go], principalement parce que sa mascotte et ses multiples variantes l'amusent.
Ça a un peu déteint sur moi, et je n'en ai pas encore été déçu.

{{< figure src="gopher-hero.svg" alt="La mascotte de Go avec une cape et un masque de super-héros." caption="La version d'Hugo de la mascotte de Go" width="350" >}}

Hugo supporte les taxonomies et la plupart des fonctionnalités habituelles d'un blogue.
Il transforme des fichiers [Markdown] --- en respectant la spécification [CommonMark] --- en pages web statiques.

Il est flexible : bien que vous puissiez choisir parmi des dizaines de thèmes, vous pouvez créer le vôtre et le personnaliser aussi loin que votre imagination vous le permettra.
Je l'ai fait afin d'être certain de n'inclure que ce dont j'ai besoin (sobriété).
Par exemple, les images de couverture sont ma création (okay, pas _si_ sobre...).

Hugo est extrêmement rapide.
J'ai compilé le site juste pour avoir un chiffre : il m'a fallu moins de 3 secondes sur un petit portable.
Cela inclut la génération des feuilles de style, ainsi que production de plusieurs tailles de chaque image --- mais j'y reviendrai plus bas.
J'ai tenté --- et réussi --- d'utiliser [AsciiDoctor] à la place de Markdown, mais les performances ont pris un tel coup que j'ai abandonné.
Ça prouve toutefois que Markdown n'est pas une obligation.

Hugo gère l'internationalisation assez élégamment.
Il génère un site par langue --- et le serveur de dev a une URL par langue, ce qui permet de faire des tests assez réalistes.

Ensuite, je peux utiliser une plateforme open source, comme [GitLab], pour héberger mes sources.
Elles sont disponibles publiquement et n'importe qui peut proposer une modification, tandis que [GitLab CI/CD] me décharge de toute la partie construction et déploiement.

* Plateforme de publication avec internationalisation et taxonomies : OK
* Propositions d'améliorations : OK
* Statique : OK



### La gestion des commentaires

Puisque tout est généré à partir de fichiers statiques, il n'y a aucune base de données où sauver les commentaires.
Ce qui s'en approche le plus est [mon repo GitLab][kp-repo].

Bien entendu, je pourrais me baser sur [Disqus], mais cela ne serait pas vraiment sobre.
Disqus charge plusieurs scripts.
En outre, il permet de « convertir l'engagement en revenu », ce qui implique un service de publicité et certainement du tracking...
Cela va à l'encontre du respect de la vie privée également.

J'ai imaginé créer un service qui pourrait prendre les données d'un commentaire et en faire une _merge request_ sur le repo.
L'idée me semblait intéressante, mais je n'ai pas eu l'opportunité de m'y coller puisque [Staticman] existait déjà pour ça.

* Commentaires : OK


### La génération des scripts et feuilles de style

Cela fait quelques années que je connais [Webpack].
Il vous permet d'écrire vos scripts et styles dans des fichiers unitaires logiques et de les assembler ensuite en un tout cohérent.
Cela apporte de la clarté et la séparation des préoccupations à votre développement.

{{< figure src="webpack-bundler.mini.png" link="webpack-bundler.png" caption="Le principe de l'assemblage de ressources tel qu'exposé sur le site de Webpack" >}}

Dès le départ, je savais vouloir me passer des libs et frameworks qui font gagner du temps.
J'adore [Bootstrap] et [Foundation], mais les utiliser a souvent pour conséquence beaucoup de pollution et code inutilisé, sauf à être très attentif.
Je voulais également un bundler pour optimiser mon code, le rendre portable et le minifier.

<aside><p>J'ai recréé tous mes styles et scripts de zéro sans grand framework moderne.</p></aside>

Heureusement pour moi, [Netlify] a publié [Victor Hugo], un modèle pour aider à créer un site avec Hugo et Webpack, en utilisant [NPM] comme pilote.
C'était un superbe point de départ.
J'ai dû me livrer à quelques adaptations (Victor Hugo n'a pas été conçu pour des sites multilingues), mais il m'a fait gagner du temps et m'a permis d'en apprendre encore un peu sur NPM.

Au final, Webpack a laissé sa place à [Rollup].
Quelques tests m'ont montré que [ce changement faisait économiser 7 ko sur les scripts générés][tw-rollup-vs-webpack].
Ceci a à son tour conduit plus récemment à confier la génération des styles à Hugo, car il y a quelques problèmes de gestion des CSS produits à travers plusieurs bundles.

* Sobre / économe en bande passante : OK


### L'optimisation de la bande passante et du processeur

Dans ma tentative d'approche responsable, j'ai tenté d'optimiser la bande passante, parce que ça vous fait des économies sur la facture et parce que [c'est bon pour la planète][post-footprint].

* **Les icônes sont assemblées en un [SVG sprite][csstricks-svg-sprites] unique**.
IE ne supporte pas les sprites externes, mais j'ai privilégié la capacité du navigateur à mettre les icônes en cache d'une page à l'autre.
Une page doit être compréhensible sans icône de toute façon.
* **Les images sont disponibles en plusieurs tailles et chargées à la demande**.
La préparation des images _responsive_ m'a pris du temps[^fn-responsive-img], mais je suis plutôt satisfait du résulta.
* **Les ressources sont gzippées avant d'être servies** plutôt qu'à la volée.
J'ai lu que la compression n'était pas gourmande en processeur, mais cela me semblait mieux.

[^fn-responsive-img]: J'ai d'ailleurs découvert un bug et l'ai corrigé pour ce billet.

* Sobre / économe en bande passante : OK



### L'analyse des statistiques

Lorsque vous interrogez un serveur, les requêtes sont enregistrées.
J'ai songé que ces logs seraient une solution effective et non intrusive pour analyser les visites.
Il se trouve que [Matomo] (ex-Piwiq) y a songé également et [offre cette possibilité][matomo-log-analytics].
Matomo est open source et je peux l'héberger chez moi, ce qui me laisse le contrôle des données.

L'import des logs anonymise les IP, me laissant une idée générale de la géographie tout en m'empêchant de pister un visiteur particulier.
C'est parfait pour moi !
Je dois encore me créer un Docker compose pour analyser les logs que je sauvegarde, et [j'accepte de l'aide sur le sujet][kp-repo-issue-matomo].

* Statistiques respectueuses de la vie privée : OK



## Le résultat

Vous le parcourez.
Je ne suis pas un artiste, vous pouvez penser que ce n'est pas beau.
Je ne serai pas (trop) vexé.

Et, comme vous l'aurez remarqué, aucune solution propriétaire dans cette pile !

* Open source : OK

Si vous souhaitez en savoir davantage sur une des briques ou un des sujets abordés, laissez un commentaire !


[Hugo]: https://gohugo.io/
[PostCSS]: https://postcss.org/
[Rollup]: https://rollupjs.org/
[Staticman]: https://staticman.net/
[NPM]: https://www.npmjs.com/
[GitLab CI/CD]: https://docs.gitlab.com/ee/ci/
[Nginx]: https://www.nginx.com/
[Matomo]: https://matomo.org
[Github Pages]: https://pages.github.com/
[GitLab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Jekyll]: https://jekyllrb.com/
[Markdown]: https://daringfireball.net/projects/markdown/
[CommonMark]: https://commonmark.org/
[AsciiDoctor]: https://asciidoctor.org/
[GitLab]: https://gitlab.com/
[Disqus]: https://disqus.com/
[Webpack]: https://webpack.js.org/
[Bootstrap]: https://getbootstrap.com/
[Foundation]: https://get.foundation/sites
[Netlify]: https://www.netlify.com
[Victor Hugo]: https://www.netlify.com/blog/2016/09/21/a-step-by-step-guide-victor-hugo-on-netlify/

[kp-repo]: https://gitlab.com/keyboardplaying/website/keyboardplaying.org/
[kp-repo-issue-matomo]: https://gitlab.com/keyboardplaying/website/keyboardplaying.org/-/issues/73
[tw-rollup-vs-webpack]: https://twitter.com/KeyboardPlaying/status/1230748142682791943
[csstricks-svg-sprites]: https://css-tricks.com/svg-sprites-use-better-icon-fonts/
[matomo-log-analytics]: https://matomo.org/log-analytics/

[tag-sustainable]: {{< relref path="/tags/sustainable-it" >}}
[tag-go]: {{< relref path="/tags/golang" >}}
[post-footprint]: {{< relref path="/blog/2020/01-intro-sustainable-it/01-footprint-of-digital" >}}
