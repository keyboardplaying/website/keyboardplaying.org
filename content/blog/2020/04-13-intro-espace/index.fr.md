---
date: 2020-04-13T10:29:05+02:00
title: Les espaces en français
#subtitle: J'espère que le courant passe bien avec le statique.
slug: espaces-francais
description: |-
  Ceux qui sont un peu versés dans la typographie française savent qu'il n'y a pas qu'une espace.
  Et non, le féminin n'est pas une coquille.
author: chop
categories: [ writing ]
tags: [ typography ]
keywords: [ espace, typographie, conventions typographiques, étymologie, fonte, ponctuation, unicode, raccourci clavier ]

references:
- id: tlfi
  name: Trésor de la Langue Française informatisé
  url: https://www.cntrl.fr/
  lang: fr
- id: regles-typo
  name: Règles de l'écriture typographique du français (à l'usage des personnes qui exercent une activité sur Mac ou PC)
  title: true
  url: https://www.adverbum.fr/atelier-perrousseaux/yves-perrousseaux/regles-de-l-ecriture-typographique-du-francais_2phys0z2dfg1t74fa40a6qonk4.html
  lang: fr
  author:
    name: Yves Perrousseaux
- id: wiki
  name: Wikipédia
  url: https://fr.wikipedia.org
  lang: fr

---

En préparant un autre billet sur des règles typographiques, je me suis rendu compte que j'allais devoir parler d'espaces.
En typographie, tout particulièrement en français, les espaces sont de différents types et je me suis dit que :
- il sera compliqué de définir ces différents espaces au sein de billets qui seront parfois déjà eux-mêmes velus ;
- comme dans le code, c'est bien de factoriser et de renvoyer à un unique endroit pour retrouver les informations communes.

Voici donc un billet introductif aux espaces en français.

<!--more-->

## Note d'étymologie

Lorsque l'on parle de typographie, les mots que l'on utilise se réfèrent souvent à l'imprimerie.
Par exemple, la [fonte](https://fr.wikipedia.org/wiki/Fonte_de_caract%C3%A8res) désigne à l'origine les caractères mobiles utilisés pour imprimer.

<aside><p>
En typographie, « espace » est féminin.
</p></aside>

L'espace est un de ces caractères.
Le substantif féminin « espace » désigne la « petite lame de métal qu'on emploie pour séparer les mots » ([TLFi, article _espace_, subst. fém.][tlfi-espace]) et, par métonymie, le « blanc qui résulte de l'emploi de cette lame » ([_Ibid._][tlfi-espace]).
Et oui, voilà un peu de culture confiture à étaler à lors de votre prochain apéro Skype ou WhatsApp : lorsque l'on parle du caractère imprimé, « espace » est féminin.



## Les différentes espaces

### Une distinction selon leur taille

<aside><p>
L'espace fine est moins large que l'espace habituelle entre deux mots, ou plus simplement espace mots.
</p></aside>

Nous n'irons pas plus loin ici dans le monde de l'imprimerie, aussi fascinant le trouvé-je.
Revenons dans un univers plus moderne, où nous numérisons tout.
Dans cet univers, il n'y a que deux largeurs d'espace dont tenir compte ait un sens :
- L'**espace mots**[^fn-espace-mots], ou intermot, correspond à l'espace que vous entrez en appuyant sur la barre d'espace de votre clavier.
C'est généralement elle que l'on désigne lorsque l'on parle d'« espace » sans fournir davantage de précision.
- L'**espace fine** est moins large que sa consœur (certaines sources annoncent qu'elle équivaut à la moitié d'une espace mots) et est utilisée dans la ponctuation.

[^fn-espace-mots]: Contrairement à une orthographe courante, le _Lexique_ de l'Imprimerie nationale utilise le terme d'« espace mots ». Le trait d'union n'est pas nécessaire, car le mot « espace » ne change pas de sens, comme c'est le cas dans « espace-temps ». « Mots » est quant à lui au pluriel car une espace est généralement encadrée de deux mots.

Reste bien entendu le cas de l'**espace justifiante**, qui a la particularité dans l'univers numérique d'être une espace à largeur variable.
La plupart du temps, elle mesure au moins autant qu'une espace mots, mais certains logiciels peuvent également réduire la taille des espaces.
Je ne l'aborderai pas davantage ici.


### Les insécables

Pour les régions francophones, il faut compter deux espaces supplémentaires : l'espace mots et l'espace fine **insécables**.
Par défaut, une espace est un point de rupture accepté d'une ligne, mais il existe certains cas, notamment dans la ponctuation, où l'espace doit impérativement être préservée.
Dans ce cas, on utilise une espace insécable.



## Comment saisir ces espaces

Si saisir une espace mot est facile, les autres espaces sont moins accessibles.
Je suis d'ailleurs preneur de toute méthode qui semblerait universelle sur un OS, ou tout au moins indépendante du logiciel utilisé.[^fn-input-method]

[^fn-input-method]: Pour écrire ce site, n'ayant pas trouvé de meilleure solution sous Linux, je me contente de copier-coller les caractères spéciaux depuis [le fichier README de mon dépôt](https://gitlab.com/keyboardplaying/website/keyboardplaying.org/#typography).

| Type                    | Aperçu | Unicode | HTML                                      | Autre méthode (Windows) |
| ----------------------- | ------ | ------- | ----------------------------------------- | ----------------------- |
| Espace mots (sécable)   | `< >`  | U+0020  | `&#x20;`                                  | Barre d'espace          |
| Espace fine (sécable)   | `< >`  | U+2009  | `&thinsp;` (_**thin sp**ace_ en anglais)  |                         |
| Espace (mots) insécable | `< >`  | U+00A0  | `&nbsp;` ou `&#160;`                      | <kbd>Alt</kbd> + <kbd>0</kbd><kbd>1</kbd><kbd>6</kbd><kbd>0</kbd> ou <kbd>Alt</kbd> + <kbd>2</kbd><kbd>5</kbd><kbd>5</kbd> |
| Espace fine insécable   | `< >`  | U+202F  | `&#8239;`                                 |                         |

Petite note dans le cas où vous ne parviendriez pas à saisir les espaces fines :

> Si votre logiciel ne vous permet pas de réaliser l'espace fine, **mieux vaut mettre une espace-mot** (qui sera donc plus large que la fine) **que pas d'espace du tout** : pour une meilleure lisibilité et le confort de lecture.
> 
> {{< ref-cite "regles-typo" >}}



## Espaces et ponctuation

Je pense devoir préciser un point concernant ce paragraphe : je parle ici des conventions typographiques en France.
Les conventions typographiques sont effectivement définies nationalement et d'autres pays francophones peuvent appliquer des règles différentes et je vous laisserai alors vous renseigner --- n'hésitez pas à m'informer des différences par commentaire.

| Avant le signe | Signe de ponctuation | Après le signe |
| -------------: | :------------------: | :------------- |
| pas d'espace | <strong>.</strong><br>point | espace mots |
| espace fine insécable | <strong>?</strong><br>point d'interrogation | espace mots |
| espace fine insécable | <strong>!</strong><br>point d'exclamation | espace mots |
| pas d'espace | <strong>,</strong><br>virgule | espace mots |
| espace fine insécable | <strong>;</strong><br>point-virgule | espace mots |
| espace mots insécable | <strong>:</strong><br>deux-points | espace mots |
| pas d'espace | <strong>’</strong><br>apostrophe | pas d'espace |
| pas d'espace | <strong>-</strong><br>trait d'union | pas d'espace |
| pas d'espace<br><small>ou espace mots dans certaines utilisations</small> | <strong>…</strong><br>points de suspension | espace mots |
| espace mots<br><small>sauf en début d'alinéa</small> | <strong>«</strong><br>guillemet ouvrant | espace mots insécable |
| espace mots insécable | <strong>»</strong><br>guillemet fermant | espace mots |
| espace mots | <strong>(</strong> ou <strong>[</strong><br>parenthèse ou crochet ouvrants | pas d'espace |
| pas d'espace | <strong>)</strong> ou <strong>]</strong><br>parenthèse ou crochet fermants | espace mots |
| espace fine | <strong>–</strong> ou <strong>—</strong><br>tiret ou tiret long | espace fine |
| espace fine | <strong>/</strong><br>barre de fraction ou slash | espace fine |

Nota : L'apostrophe et le trait d'union ne sont pas des signes de ponctuation, mais des signes grammaticaux.
Il apparaissait toutefois intéressant de les faire figurer dans ce tableau.

## Sources

Pour cet article, je me suis inspiré de connaissances accumulées au fil des ans sur différentes sources, ainsi que sur :
- le [_Trésor de la Langue Française informatisé_ (TLFi)](https://www.cnrtl.fr/) ;
- le livre [_Règles de l'écriture typographique du français (à l'usage des personnes qui exercent une activité sur Mac ou PC)_](https://www.adverbum.fr/atelier-perrousseaux/yves-perrousseaux/regles-de-l-ecriture-typographique-du-francais_2phys0z2dfg1t74fa40a6qonk4.html) d'Yves Perrousseaux ;


{{% references %}}
[tlfi-espace]: https://www.cnrtl.fr/definition/espace
