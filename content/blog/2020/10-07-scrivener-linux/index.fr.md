---
date: 2020-10-06T07:21:32+02:00
title: Installer Scrivener 3 sous Linux
subtitle: Un peu de vin pour démarrer ?
slug: installer-scrivener-3-linux
description: |-
  Scrivener ne fournit pas de support pour Linux.
  Pourtant, il reste un espoir.
cover:
  banner: false
  src: scrivener-kubuntu.jpg
  alt: L'écran de démarrage de Scrivener 3
  by: chop
  license: CC BY-NC 4.0
author: chop
categories: [ writing, software ]
tags: [ linux ]
keywords: [ wine, scrivener, winetricks ]

references:
- id: pletcher-scrivener3-linux
  name: "Update: Scrivener 3 on Linux"
  title: true
  url: https://writeside.com/2020/08/24/update-scrivener-3-on-linux/
  date: 08/2020, mis à jour 01/2021
  lang: en
  author:
    name: Thomas Pletcher
---

Tandis que je me prépare pour ma première tentative au [NaNoWriMo], je sais que j'aurai besoin d'un logiciel pour m'organiser.
J'ai utilisé [Scrivener] par le passé et je sais que c'est parfaitement adapté pour mon usage.
Mon seul problème : j'utilise Linux en déplacement, pour lequel Scrivener n'offre pas de support.

Mais ce n'est plus un problème, ainsi que [l'a révélé Thomas Pletcher][pletcher-scrivener3-linux] : les dernières betas de [Scrivener 3] fonctionnent avec [Wine].
Voilà comment.


<!--more-->

**Mise à jour du 18 décembre 2024** : [Le guide de Ryan Dewalt en 9 étapes](https://rdewalt.substack.com/p/installing-scrivener-on-linux) semble désormais plus à jour que cette page.
N'hésitez pas à vous y référer. Merci à Brazz pour l'information.

**Mise à jour du 25 novembre 2022** : Brazz a partagé quelques informations suite à son installation récente, le guide a été mis à jour.
Il a à nouveau été mis à l'épreuve avec Scrivener 3.1.2 sur Kubuntu 22.04.

**Mise à jour du 15 janvier 2022** : Alwerto a trouvé une solution concernant le problème des touches mortes. Les sections _problèmes connus_ et _Résoudre les problèmes_ ont été mises à jour en conséquence.

**Mise à jour du 12 août 2021** : J'ai ajouté un avertissement sur les problèmes connus, afin que vous puissiez décider s'ils sont gênants pour vous avant de vous lancer dans l'installation.

**Mise à jour du 17 juillet 2021** : Cette page a été mise à jour pour Scrivener 3.0.1 pour Windows.
Cette procédure est fonctionnelle pour mon poste, mais n'hésitez pas à partager des pistes d'amélioration.



## Avant de commencer : les problèmes connus

Qu'il soit bien clair que je fournis ce guide en l'état : il fonctionne sur un portable Kubuntu 22.04, mais je ne suis pas en mesure de fournir un support au cas par cas.
Des adaptations pourront être à prévoir pour votre propre installation, mais je _ne peux pas_ fournir de consultations individuelles.

Bien que Scrivener fonctionne sous Linux, tout n'est pas parfait et il reste de petits imprévus.
Vous pourrez peut-être vous en contenter, mais je pense que vous préférerez savoir à quoi vous attendre.


### Un glisser-déposer un peu collant

En premier lieu, j'ai remarqué un petit problème avec le drag and drop : à chaque fois que vous déposez un élément, il reste en transparence avec votre souris, comme s'il glissait toujours, alors qu'il est bien là où vous l'avez déposé.
C'est un problème cosmétique avant tout : le déplacement est bien effectué.
Pour chasser cet embêtant fantôme, il suffit de presser la touche <kbd>Échap</kbd> de votre clavier.


## Faire fonctionner Scrivener sous Linux

### Installation de Wine

Pour faire tourner Scrivener sous Linux, nous utiliserons [Wine], qui offre une couche de compatibilité pour exécuter des programmes Windows sur des systèmes POSIX (pour nous : « Linux »).

**Note** : Ce tutoriel est écrit pour des personnes qui ne connaissent pas Wine et ne l'utiliseront probablement que pour Scrivener.
Si vous avez une connaissance plus avancée, vous voudrez peut-être utiliser un préfixe dédié.
Faites, je vous en prie. Je n'en parlerai pas ici.

Maintenant, pour faire court :

1. Nous aurons besoin de Wine, [Winetricks] et winbind.
Si ceux-ci sont disponibles dans les repos de votre distribution (c'est le cas pour Ubuntu), les installer se fera via la commande : `sudo apt install wine winetricks winbind`

2. L'installeur de Wine du dépôt Ubuntu a un petit défaut : il ne crée pas le lien symbolique qui permet de lancer l'application.
Pas grave, on va le faire à la main : `sudo ln -s /usr/share/doc/wine/examples/wine.desktop /usr/share/applications/`


### Configuration de Wine

Maintenant que Wine est installé, il va falloir ajouter les composants Windows dont Scrivener aura besoin.

1. Facultatif : `winetricks corefonts` installera les polices par défaut Windows (p. ex. Times New Roman).
Vous les avez peut-être déjà installées d'une autre façon (p. ex. `sudo apt install ttf-mscorefonts-installer`), en quel cas cette étape serait redondante.

2. `winetricks win10` préparera une architecture Windows 10.

3. `winetricks dotnet48` installera .NET 4.8 (Scrivener a besoin de .NET 4.6 ou plus). Cochez la case « Redémarrer maintenant » quand on vous le proposera.
Pas d'inquiétude, cela ne fera pas redémarrer votre machine.

4. `winetricks speechsdk`: le SpeechSDK n'est pas noté parmi les prérequis pour Scrivener, mais je n'ai pas pu l'exécuter avant de l'installer.\
Lorsqu'une fenêtre vous demandera _User Name_ et _Organization_, vous pourrez laisser ces champs vides.


### Installation de Scrivener

1. Il vous faut télécharger Scrivener 3 (à l'heure de l'écriture de ces lignes, vous pouvez télécharger la dernière version [ici][scrivener-download]).

2. Si vous tentez d'exécuter le fichier téléchargé en cliquant dessus, vous obtiendrez une erreur fatale avec le message « _called Tcl_Close on channel with refCount > 0_ »
Ouvrez plutôt un terminal et naviguez vers le répertoire où vous avez téléchargé le fichier d'installation (p.ex. `cd ~/Downloads`), puis exécutez la commande `wine Scrivener-installer.exe`.\
À la fin de l'installation, si vous cochez _Create a desktop shortcut_, vous verrez des messages d'erreur concernant l'icône.
De ce que j'ai pu voir, cela n'affecte pas la possibilité de lancer Scrivener.

C'est tout !
À la fin de l'installation, Wine devrait avoir créé un raccourci pour Scrivener dans votre liste d'applications.
Vous devriez pouvoir le démarre en cliquant dessus.
N'hésitez pas à partager d'autres astuces dans les commentaires.

3. Facultatif : il est possible de changer les icônes de Scrivener dans votre lanceur d'applications.
Pour cela, il suffit de trouver les fichiers `.desktop` correspondants (`Scrivener.desktop` et `Uninstall Scrivener.desktop` sous `~/.local/share/applications/wine/Programs/Scrivener 3`) et de modifier la ligne `Icon=`.[^fn-desktop-files]\
Les icônes `icon.ico` et `scriv.ico` sont disponibles sous `~/.wine/dosdevices/c:/Program Files/Scrivener3/resources`.\
Un exemple de ligne modifiée serait par conséquent :
`Icon=/home/myuser/.wine/dosdevices/c:/Program Files/Scrivener3/resources/icon.ico`.

[^fn-desktop-files]: Il n'est pas recommandé de modifier directement ces fichiers.
Pour faire proprement, vous devriez en créer de nouveaux puis utiliser la commande `desktop-file-install`.
Cependant, je n'ai pas envie de faire un tutorial dans un tutorial.\
Vous pouvez modifier directement ces fichiers.
Pour que les changements soient pris en compte, il vous faudra simplement vous déconnecter de votre session, vous reconnecter et attendre quelques minutes.

{{<figure src="icon.png" title="icon.ico">}}
{{<figure src="scriv.png" title="scriv.ico">}}

4. Si, lorsque vous lancez Scrivener, vous obtenez un avertissement indiquant que son fonctionnement n'est pas optimal sous Windows 7, suivez les étapes suivantes :
  1. Fermez Scrivener.
  2. Ouvrez _Wine configuration_ (ou saisissez la commande `winecfg`).
  3. En bas de l'onglet _Applications_, sélectionnez _Windows 10_.
  4. Fermez l'outil de configuration et relancez Scrivener.


## Résoudre les problèmes

Voici les problèmes partagés par nos lecteurs et les solutions que nous avons pu trouver.


### Un message d'erreur apparaît à l'exécution de l'installeur

Merci à [OOzyPal] de m'avoir incité à rechercher ce point.

Symptômes
: En exécutant Scrivener-installer.exe, un message d'erreur apparaît et indique :
: > called Tcl_Close on channel with refCount > 0.

Solution
: Il semblerait que cette erreur ne se produise que lorsque l'on exécute l'installeur via l'interface graphique.
Utiliser la ligne de commande semble résoudre le souci : `wine Scrivener-installer.exe`.


### L'activation de la license échoue

Merci à [qbit] pour avoir partagé à la fois le problème et sa solution.

Symptômes
: qbit pouvait exécuter Scrivener mais pas activer la license.
Il obtenait à la place le message suivant :
: > There was a problem activating Scrivener. Please try again in a moment, or restart your computer and ensure it is connected to the Internet, then try again. […] Object reference not set to an instance of an object.
: À l'aide de la commande `wine ping google.com`, il a confirmé que le problème ne provenait pas de l'accès à Internet.

Cause
: Le problème semble lié à l'installation .NET 4.8.

Solution
: qbit a résolu son souci en réinstallant .NET via Winetricks, via la commande : `winetricks --force dotnet48`.


### Les touches mortes ne fonctionnent pas correctement

Merci à [Alwerto] pour avoir trouvé une solution à ce problème qui me bloquait.

Symptômes
: Certaines [touches mortes][wiki-touches-mortes] ne fonctionnent pas correctement.
: Par exemple, la première combinaison <kbd>^</kbd> puis <kbd>e</kbd> donnera bien le `ê` attendu, mais toutes les combinaisons suivantes imprimeront `ê` également, comme si le logiciel restait bloqué sur la première combinaison.
: De temps à autre, une nouvelle combinaison remplace la première, sans que j'ai pu trouver une action permettant de forcer ce rafraîchissement.

Cause
: Pas complètement compris, mais cela ne semble pas lié spécifiquement à Wine ou Kubuntu, puisqu'[un utilisateur a reproduit ce comportement dans Linux Mint avec Cinnamon][winehq-forum-kbd-mapping].

Solution
: Alwerto a suivi [la solution proposée][winehq-forum-kbd-mapping] et cela a résolu le problème chez lui.
: L'idée est de passer le mode de saisie à Ibus. Sur mon poste sous KDE, la commande `im-config -n ibus` a fait l'office. Je vous laisserai chercher la procédure à suivre pour votre distribution.


### Quand rien ne fonctionne…

[Marie] a suivi la procédure décrite ici, mais cela n'a pas fonctionné et elle a souhaité tout désinstaller.
Voici la solution que j'ai suggérée :

1. `sudo apt purge wine winetricks winbind` retire Wine et tout les logiciels qui y sont liés.
2. `rm -rf ~/.wine` supprime le répertoire où Wine a créé son image d'une machine Windows.


{{% references %}}
[NaNoWriMo]: https://nanowrimo.org/
[Scrivener]: https://www.literatureandlatte.com/scrivener/overview
[Scrivener 3]: https://www.literatureandlatte.com/scrivener-3-for-windows-update
[scrivener-download]: https://www.literatureandlatte.com/scrivener/download
[Wine]: https://www.winehq.org/
[Winetricks]: https://wiki.winehq.org/Winetricks
[wiki-touches-mortes]: https://fr.wikipedia.org/wiki/Touche_morte
[winehq-forum-kbd-mapping]: https://forum.winehq.org/viewtopic.php?t=29209#p119201

[OOzyPal]: {{< ref path="/blog/2020/10-07-scrivener-linux" lang="en" >}}#1b81f9e0-e394-11eb-ba33-f7737f1b8e3b
[qbit]: {{< ref path="/blog/2020/10-07-scrivener-linux" lang="en" >}}#2484e0a0-e922-11eb-be49-af0855a433cf
[Marie]: {{< relref path="/blog/2020/10-07-scrivener-linux" >}}#14cb3d20-4905-11eb-af45-ef5ddb6294fc
[Alwerto]: {{< relref path="/blog/2020/10-07-scrivener-linux" lang="en" >}}#4686ba60-74b5-11ec-a447-21c0d47b28cc
