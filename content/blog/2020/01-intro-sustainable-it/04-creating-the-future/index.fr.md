---
date: 2020-02-03T22:36:30+01:00
title: Le futur est entre nos mains
#subtitle: No funny subtitle found
slug: intro-numerique-responsable/future-entre-mains
description: |-
  Ce que nous créons aujourd'hui définit ce que sera demain.
  Un soupçon de pensée ne gâcherait certainement pas notre plaisr créatif, tout en apportant un goût de sagesse.
cover:
  src: 16793434025_47a6a2e87b_k.jpg
  alt: Une photos mettant en scène un savant fou et son cobaye en figurines Lego.
  by: Yohanes Sanjaya
  link: https://www.flickr.com/photos/valiantize/16793434025
  authorLink: https://www.flickr.com/photos/valiantize/
  license: CC BY 2.0
author: chop
categories: [ software, software-creation ]
tags: [ sustainable-it ]
keywords: [ numérique responsable, intelligence articielle, IA, éthique, dilemme du tramway, moral machine, neuralink ]
series: [ intro-sustainable-it ]

references:
- id: china-social-ranking
  name: China ranks citizens with a social credit system - here's what you can do wrong and how you can be punished
  title: true
  url: https://www.independent.co.uk/life-style/gadgets-and-tech/china-social-credit-system-punishments-rewards-explained-a8297486.html
  date: 05/2018
  lang: en
  author:
    name: Alexandra Ma, Independent
- id: china-vs-black-mirror
  name: No, China isn't Black Mirror – social credit scores are more complex and sinister than that
  title: true
  url: https://www.newstatesman.com/world/asia/2018/04/no-china-isn-t-black-mirror-social-credit-scores-are-more-complex-and-sinister
  date: 04/2018
  lang: en
  author:
    name: Ed Jefferson, NewsStateman
- id: theverge-mazurenko
  name: Speak, Memory — When her best friend died, she rebuilt him using artificial intelligence
  tile: true
  url: https://www.theverge.com/a/luka-artificial-intelligence-memorial-roman-mazurenko-bot
  lang: en
  author:
    name: Casey Newton, The Verge
- id: verily-snapchat-filters
  name: Why I’m Sick of Snapchat’s Photoshopping and Sexualizing Lenses
  title: true
  url: https://verilymag.com/2016/06/snapchat-lenses-social-media-beauty-photoshop
  date: 06/2016
  lang: en
  author:
    name: Krizia Liquido, Verily
- id: snapchat-dysmorphia
  name: "'Snapchat Dysmorphia': Seeking Selfie Perfection"
  title: true
  url: https://www.webmd.com/beauty/news/20180810/snapchat-dysmorphia-seeking-selfie-perfection
  date: 08/2018
  lang: en
  author:
    name: Cameren Rogers, WebMD
- id: wiki-tay-ia
  name: Tay (intelligence artificielle)
  title: true
  url: https://fr.wikipedia.org/wiki/Tay_(intelligence_artificielle)
  lang: fr
  author:
    name: Wikipédia
- id: wiki-bot-zo
  name: Zo (bot)
  title: true
  url: https://en.wikipedia.org/wiki/Zo_(bot)
  lang: en
  author:
    name: Wikipedia
- id: zo-worse-than-tay
  name: Microsoft’s politically correct chatbot is even worse than its racist one
  title: true
  url: https://qz.com/1340990/microsofts-politically-correct-chat-bot-is-even-worse-than-its-racist-one/
  date: 07/2018
  lang: en
  author:
    name: Chloe Rose Stuart-Ulin, Quartz
- id: wiki-trolley-problem
  name: Trolley problem
  title: true
  url: https://en.wikipedia.org/wiki/Trolley_problem
  lang: en
  author:
    name: Wikipedia
- id: mmm
  name: Moral Machine
  url: http://moralmachine.mit.edu/
  lang: en
  author:
    name: MIT
- id: wiki-ai-open-letter
  name: Lettre ouverte sur l'intelligence artificielle
  title: true
  url: https://fr.wikipedia.org/wiki/Lettre_ouverte_sur_l%27intelligence_artificielle
  lang: fr
  author:
    name: Wikipédia
- id: ai-montreal
  name: Déclaration de Montréal pour un développement responsable de l'IA
  title: true
  url: https://www.declarationmontreal-iaresponsable.com/
  lang: fr
  author:
    name: Université de Montréal
- id: cnn-neuralink
  name: Elon Musk is making implants to link the brain with a smartphone
  title: true
  url: https://edition.cnn.com/2019/07/17/tech/elon-musk-neuralink-brain-implant/index.html
  date: 07/2019
  lang: en
  author:
    name: Michael Scaturro, CNN News
---

Pour conclure cette série sur le numérique responsable, je voulais partager quelques pensées autour des impacts de la création logicielle que nous avons tendance à négliger.
Nous avons tous vu des films dans lesquels un savant fou crée quelque chose magnifique à ses yeux, jusqu'au moment où cela lui échappe et menace de changer tout ce que nous connaissons.

> Nous sommes tous des savants fous, et la vie et notre labo.
> Nous expérimentons tous pour trouver un moyen de vivre, de résoudre des problèmes, de repousser la folie et le chaos.
> <cite>David Cronenberg</cite>

Tout ceci s'applique particulièrement bien dans le secteur de la création logicielle : nous innovons, créons de nouvelles technologies pour des milliers ou millions de gens.
Si nous manquons de prudence, nos créations pourraient changer la société entière, mais nécessairement comme nous l'aurions imaginé.

<!--more-->

## Le futur est déjà là

Connaissez-vous la série [_Black Mirror_][black-mirror] ?
C'est une de mes préférées.
Pour ceux qui ne l'ont jamais vue, elle se présente sous forme d'anthologie.
Chaque épisode se déroule dans un univers légèrement différent, souvent dans un futur pas si lointain et montre comment une technologie ou un aspect de notre société pourrait dérailler.
J'adore que ce « divertissement » fasse réfléchir à l'impact d'une technologie, ce qu'elle pourrait devenir, bien que ses créateurs n'aient certainement jamais envisagé une telle utilisation de près ou de loin.
Malgré tout, la réalité semble rattraper ces dystopies.

Je vais éviter de divulgâcher, mais deux épisodes me semblent d'excellents exemples.
[_Chute libre_ (2016)][black-mirror-nosedive] montre une société où chacun note chacun, comme on note un restaurant ou un service.
À première vue, cela pourrait sembler idyllique : chacun est contraint de se comporter agréablement --- bien que la question de la liberté d'expression puisse se poser ---, mais on en vient à réaliser que des restrictions existent pour ceux dont la note n'est pas assez élevée.
Cela ressemble à [la note sociale en Chine (2018)][china-social-ranking] --- bien que les Chinois soient notés [par des entités étatiques ou privées][china-vs-black-mirror].

L'autre épisode est [_Bientôt de retour_ (2013)][black-mirror-brb].
Ash, un utilisateur intensif des réseaux sociaux, est tué dans un accident de voiture.
Sa compagne est dévastée, mais elle découvre un service qui lui permet de communiquer avec une IA imitant Ash, en se basant sur ses nombreuses publications.
Cela me fait penser à l'[histoire d'Eugenia Kuyda][theverge-mazurenko] : quand son meilleur ami est mort (2015), elle a créé une IA qu'elle a nourrie avec tous les SMS qu'elle a pu récupérer de lui et a ainsi pu réaliser son souhait de lui parler une dernière fois.

Pourquoi vous présentè-je ces exemples ?
Parce que nous avons écrit/lu/vu des dystopies depuis des décennies, mais nous sommes à une époque où la dystopie peut devenir réalité si nous n'y prêtons pas attention.
La société de demain sera très certainement influencée par ce que nous créons aujourd'hui.
Mais est-il nécessaire d'attendre demain ?


## Notre code propage nos partis pris

Oui, nous avons des partis pris.
Oui, ils se retrouvent dans notre code.

J'ai entendu parler d'un développeur de Facebook qui croit le principe de la transparence totale[^fn-total-transparency].
Comment cela se traduit-il au sein d'un réseau social qui a plusieurs fois été critiqué pour son approche de la vie privée ?
Mais nous sommes dans le subjectif.[^fn-bias-subjectivity]
Choisissons un exemple plus concret.

[^fn-total-transparency]: L'idée de la transparence totale est que personne ne devrait rien cacher.
[^fn-bias-subjectivity]: Les partis pris ne sont que subjectivité.
Je pense subjectivement avoir raison et vous impose donc cette justesse, même si vous n'en voulez pas.
J'espère, en retournant à du concret, éviter au moins quelques-uns de mes partis pris dans ce billet.

Connaissez-vous la couronne de fleurs de Snapchat ?
C'est tout mignon et ça fait juste ça : ajouter une couronne de fleurs à votre photo.
Sauf que non ! Cela fait bien plus que ça.
Krizia Liquido de _Verily_ [s'est rendu compte que les filtres Snapchat modifient votre apparence][verily-snapchat-filters], à la façon des modifications Photoshop de célébrités dans les magasines de mode.

![Une comparaison côte à côte des photos de Krizia Liquido sans et avec le filtre de la couronne de fleurs.](kl_snapchat_v3.jpg)

Si vous observez attentivement les différences, vous en noterez plusieurs sur la photographie filtrée :
- sa peau est éclaircie et lissée, et son visage est aminci ;
- son nez est plus étroit ;
- ses yeux sont plus larges, plus clairs, plus ressemblants à des yeux de biche.

> Quand je vois ces images côte à côte, je suis déçue de ne pas avoir juste une couronne --- je ressemble soudain à une poupée avec une peau en porcelaine.
> {{< ref-cite "verily-snapchat-filters" >}}

Pour résumer, quelqu'un a conçu ce filtre --- avec ses propres goûts --- et l'a offert au monde --- où des gens peinent à accepter leur propre image.
Oui, il aurait pu aider ces personnes à se sentir plus beaux, mais l'effet a été inverse : [de jeunes gens, de moins de 30 ans, vont voir leur chirurgien esthétique avec leur téléphone et leur filtre Snapchat, pour demander de ressembler à leur alter ego altéré][snapchat-dysmorphia].
C'est irréaliste, souvent presque impossible.

Mais le fait est là : **en imposant leurs partis pris esthétiques au sein des filtres Snapchat de leur création, ils ont renforcé l'insatisfaction des gens vis-à-vis de l'image que leur renvoient les miroirs**.
Ce problème a déjà été soulevé par Photoshop dans les magasines --- raison pour laquelle des lois ont été promulguées ---, mais, parce que personne n'a prédit cette conséquence d'un gadget ludique et innocent, de jeunes gens se sentent encore un peu moins bien dans leur peau.

La technologie amplifie l'impact de nos opinions et l'intelligence artificielle ira bien plus loin que ce que nous pouvons déjà voir.


## L'IA s'en mêle

### L'IA ne remettra pas en cause ce qu'on lui dit

On entend souvent parler d'intelligence artificielle, ces temps-ci.
Parfois pour vendre un algorithme compliqué avec plein de `if`, parfois pour désigner un réseau neural pratiquant l'apprentissage machine (_machine learning_).
Concentrons-nous sur cette seconde catégorie pour notre discussion : notre IA apprend sur base de règles établies par des humains et de données sélectionnées par des humains.

Une fois de plus, ces humains ont des partis pris.
Un enfant, en grandissant, pourra s'interroger sur les préceptes qui ont servi de fondations à son éducation, mais nous sommes loin des algorithmes d'apprentissage capables d'en faire autant.
**Ils construisent leur « réflexion » sur les bases que nous leur donnons**.
Si ces bases sont bancales, eh bien...
Avez-vous entendu parler de Tay ?

![L'avatar du bot Tay de Microsoft](Tay_bot_logo.jpg "Tay")

[Tay][wiki-tay-ia] était un robot Twitter, conçu pour interagir avec les autres utilisateurs du service.
Peu après sa mise en ligne, elle a commencé à publier des messages racistes ou sexuellement significatifs en réponse aux gazouilleurs.
Pourquoi ? Simplement parce que des trolls ont trouvé drôle de l'alimenter délibérément avec du contenu à controverse.
Le robot n'a fait que construire sur ces bases.

Microsoft a plus tard publié un autre robot,  [Zo][wiki-bot-zo], pour tenter de corriger le tir.
Des limites avaient été ajoutées, mais ces décisions n'ont pas été bien reçues non plus.

> Zo est politiquement correcte au pire extrême possible. Mentionnez l'un de ses déclencheurs, et elle se transforme en une petite morveuse encline à critiquer.
> {{< ref-cite "zo-worse-than-tay" >}}


### L'IA décidera pour vous

Connaissez-vous le dilemme du tramway ?

> Imaginez qu'un juge se retrouve face à des émeutiers qui demandent qu'un coupable soit trouvé pour un certain crime et, si cela n'est pas fait, qui menacent de se venger de manière sanglante sur une partie spécifique de la communauté.
> Le coupable réel étant inconnu, le juge se retrouve avec pour seule solution de condamner à mort un innocent pour prévenir le bain de sang.
> Imaginons parallèlement un autre exemple où le pilote d'un avion qui est sur le point de s'écraser doit choisir de dévier ou non son avion depuis une zone plus habitée vers une zone moins habitée.
> Afin de rendre les deux situations le plus semblables possible, imaginons plutôt le conducteur d'un tramway hors de contrôle qui ne peut que choisir de dévier ou non sa course depuis une voie étroite vers une autre : cinq hommes travaillent sur l'une et un homme est situé sur l'autre.
> La voie prise par le tram entraînera automatiquement la mort des personnes qui s'y trouvent.
> Dans le cas des émeutiers, ces derniers ont en otage cinq personnes, ce qui fait en sorte que les deux situations amènent le sacrifice d'une vie pour en sauver cinq.
> <cite>traduit de <em>The Problem of Abortion and the Doctrine of the Double Effect</em>, Philippa Foot</cite>

Posé de cette façon, on pourrait presque croire à une question triviale.
Maintenant, imaginez un véhicule autonome qui transporte une famille et risque de renverser un nombre équivalent d'enfants qui jouent sur la route.
S'il ne peut sauver qu'un seul groupe, doit-il prioriser les passagers ou les enfants ?
Question bonus : si vous savez que votre voiture peut vous sacrifier pour sauver des étrangers sur la route, vous sentirez-vous à l'abri à son bord ?

[![L'illustration du choix moral proposé ci-dessus](mit-moral-machine.gif)][mmm]

Une fois de plus, tout dépend du parti pris par celles et ceux qui ont programmé la voiture.
Si vous pensez que c'est une tâche aisée, cliquez sur l'image pour vous tester face à [la Machine Morale du MIT][mmm].


### Le besoin de lignes de conduite

En 2015, des dizaines d'experts sur l'IA, ainsi que de grands noms comme Stephen Hawking et Elon Musk, ont signé [une lettre ouverte sur l'intelligence artificielle][wiki-ai-open-letter].
Elle a mis en évidence les bénéfices potentiels de l'IA, mais appelait également à la prudence, à propos de préoccupations à court et long termes, comme les soucis liés à la vie privée ou le risque d'une superintelligence échappant à tout contrôle.

En 2018, des professeurs et scientifiques se sont associés pour écrire la [Déclaration de Montréal pour une IA responsable][ai-montreal].
Elle propose des lignes de conduite éthiques pour le développement des IA.

**Ces deux publications traduisent un besoin de cadre pour le développement de l'IA**, mais je n'ai rien entendu dans ce domaine en sus de ces travails ouverts.
Par ailleurs, si vous êtes intéressés, vous pouvez les signer [ici](https://futureoflife.org/ai-open-letter-signatories/) et [ici](https://www.montrealdeclaration-responsibleai.com/i-am-signing-up).



## Conclusion

La technologie continuera à évoluer et faire évoluer la société, mais **nous devons considérer les conséquences économiques, sociales et éthiques _avant_ d'y être**, et arrêter de légiférer _après_ avoir constaté les problèmes.
Oui, [connecter nos cerveaux à des machines][cnn-neuralink] est une idée aussi vieille que la science-fiction, mais quel bien --- ou quel mal --- en ressortira-t-il ?
À présent, c'est à vous de réfléchir et d'orienter le futur dans la bonne direction.


{{% references %}}

[data]: {{< relref path="/blog/2020/01-intro-sustainable-it/03-thirst-for-data" >}}
[black-mirror]: https://www.imdb.com/title/tt2085059
[black-mirror-nosedive]: https://www.imdb.com/title/tt5497778
[black-mirror-brb]: https://www.imdb.com/title/tt2290780
