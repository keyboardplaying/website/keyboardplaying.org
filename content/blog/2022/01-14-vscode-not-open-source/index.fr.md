---
date: 2022-01-14T07:00:00+01:00
title: Visual Studio Code n'est peut-être pas si ouvert que vous le pensez
subtitle: Le diable est dans les détails
slug: visual-studio-code-pas-open-source
description: |-
  Quand Visual Studio Code a été annoncé, je l'ai cru open source.
  En réalité, il y a quelques nuances.
author: chop
categories: [ software, software-creation ]
tags: [ open-source, programming, web-dev ]
keywords: [ Visual Studio Code, VS Code, vscode, open source, Microsoft, FLOSS, VSCodium, Theia, IDE ]

references:
- id: vscodium-site
  name: Site de VSCodium
  title: false
  url: https://vscodium.com/
  lang: en
- id: theia-site
  name: Site de Theia IDE
  title: false
  url: https://theia-ide.org/
  lang: en
---

Microsoft publiant ses outils sous license open source a fait un certain effet sur la communauté des développeurs.
Il y a cependant une subtilité : la license MIT s'applique au code source, mais pas au binaire distribué.

Voici quelques explications et deux alternatives possibles.

<!--more-->

## Quelques détails sur ce qui (n')est (pas) open source

### La version courte

Dans mes souvenirs, le monde a tremblé lorsque VS Code a été annoncé.
Microsoft, publier un outil sous license open source ?
Quelle nouveauté !
Mais il semblerait finalement que nous ayions entendu ce qui nous intéressait.

Microsoft a bien créé un dépôt Github, `vscode`, sous license MIT.
Le produit qu'ils mettent à disposition au téléchargement, Visual Studio Code, est cependant disponible sous [cette autre license][vscode-license], pas du tout compatible avec le FLOSS.

Voici quelques extraits de la license (traduits par mes soins) :

> Vous pouvez désactiver plusieurs [collectes de données], mais pas toutes.

> Cet accord vous donne uniquement certains droits d'utilisation du logiciel. Microsoft se réserve tous les autres droits. Excepté le cas où la loi applicable vous donne plus de droits en dépit de cette limitation, vous ne pouvez utiliser ce logiciel que comme expressément permis dans cet accord. Ce faisant, vous devez respecter toute limitation technique du logiciel qui ne vous permet de l'utiliser que de certaines façons.

En toute objectivité, ce n'est pas le seul produit dans le monde du développement à faire montre de cette dualité.
Par exemple, [dans ce commentaire][vscode-maintainer-comment], un des mainteneurs de `vscode` donne quelques exemples : Chrome est construit à partir des sources de Chromium, le JDK Oracle est compilé à partir d'OpenJDK et les produits de Jetbrains sont basés sur la plateforme IntelliJ (en attendant Fleet).

Il n'y a qu'un point qui chagrine : dans la plupart de ces cas, le nom permet de faire la différence entre ce qui est open source et ce qui ne l'est pas.
Pour Visual Studio Code, l'abréviation VS Code et le dépôt `vscode`, la distinction n'est pas évidente.



### Explication

Dans [le même commentaire][vscode-maintainer-comment], le mainteneur nous explique ce qui arrive entre le moment où le code et ouvert et celui où le binaire final et fermé est publié.

> Le truc cool [...], c'est que vous avez le choix d'utiliser le produit Visual Studio Code sous notre license ou vous pouvez construire une version de l'outil directement depuis le dépôt `vscode`, sous license MIT.
>
> Voilà comment ça marche. Quand vous compilez depuis le dépôt `vscode`, vous pouvez configurer l'outil qui en résultera en personnalisant le fichier `product.json`. Ce fichier contrôle des choses comme les adresses de la Galerie, de « Send-a-Smile », de télémétrie, les logos, les noms, en encore plus.
>
> C'est exactement ce que nous faisons quand nous compilons Visual Studio Code. Nous clonons le dépôt `vscode`, nous y appliquons un `product.json` personnalisé qui inclut des fonctionnalités spécifiques de Microsoft (télémétrie, galerie, logo, etc.), et nous produisons ensuite un binaire que nous distribuons sous notre license.
>
> Quand vous clonez et compilez depuis le dépôt `vscode`, aucune de ces adresses n'est configurée dans le `product.json` par défaut. Par conséquent, vous générez un binaire « propre », sans les personnalisations de Microsoft, qui est par défaut sous license MIT.

En résumé, quand Microsoft compile Visual Studio Code, ils ajoutent leurs éléments de marque, leur license et un module de télémétrie.
« Télémétrie ? »
La chose qui réalise les collectes de données auxquelles vous n'avez pas le droit de vous extraire, et vous ne pouvez même pas regarder comment elle fonctionne ni ce qu'elle collecte de façon transparente.

Cela ne vous dérange peut-être pas.
Dans le cas contraire, la suite pourrait vous intéresser.


## Des alternatives open source à Visual Studio Code

### VSCodium

L'équipe de [VSCodium][vscodium-site] a suivi les conseils du mainteneur : ils ont pris les sources de `vscode`, ont créé leur `product.json` et ont partagé leur binaire — en conservant la license MIT, cette fois, ce qui en fait un logiciel FLOSS.
Bien entendu, ils ont complètement désactivé la télémétrie.

Vous pouvez parcourir [leur repo][vscodium-repo] ou simplement l'installer, en utilisant votre gestionnaire de paquets préféré : VSCodium est disponible à travers le Windows Package Manager, Chocolatey, Homebrew, Scoop, Snap, et la plupart des gestionnaires courants de Linux (à condition d'ajouter le dépôt associé).
Il y a certainement la solution que vous cherchez.


### Theia IDE

[Eclipse Theia][theia-site] est une autre solution.
Les créateurs trouvent que VS Code est un superbe outil, mais pas vraiment ce qu'ils en espéraient.
Plutôt que de l'envelopper dans un Electron, ils ont développé une plateforme pour IDE à exécuter sur poste ou dans le nuage.

Oui, une _plateforme pour IDE_ : Theia n'a pas la prétention d'être un IDE clés en main, ce sont les fondations pour construire un éditeur.
Plusieurs outils basés dessus existent déjà, certains sont même mis en avant sur la page du projet.

L'interface graphique a de grands airs de ressemblance avec VS Code, mais les concepteurs annoncent une architecture différente, plus modulaire.

Ils proposent de [télécharger une version bureau de Theia][theia-blueprint], qu'ils nomment Eclipse Theia Blueprint.
Ils insistent sur le fait que ce n'est pas une solution apte à remplacer un IDE et soulignent que Theia Blueprint est pour l'instant en beta.

Au sujet de la license, au vu du nom, vous aurez certainement deviné qu'il s'agit de l'Eclipse Public License 2.0.


### Une astuce sur les extensions

VSCodium et Theia supportent tous deux les extensions de VS Code, mais avec un petit piège : ils ne peuvent pas accéder au marketplace de Microsoft.
[open-vsx]: {{< relref path="/blog/2022/01-21-open-vsx" >}}
Ils utilisent Open VSX à la place (plus à ce sujet la [semaine prochaine][open-vsx]).
Cela signifie que, lorsque vous cherchez une extension, vous ne la trouverez que si sont développeur la publie également sur Open VSX.

Si ce n'est pas le cas, pas de panique !
Rendez-vous sur la page du marketplace officiel, cliquez sur _Download_ et installez manuellement le fichier .vsix dans votre éditeur.


[vscode-license]: https://code.visualstudio.com/license
[vscode-maintainer-comment]: https://github.com/Microsoft/vscode/issues/60#issuecomment-161792005

[vscodium-site]: https://vscodium.com/
[vscodium-repo]: https://github.com/VSCodium/vscodium

[theia-blueprint]: https://theia-ide.org/docs/blueprint_download

[open-vsx]: {{< relref path="/blog/2022/01-21-open-vsx" >}}
