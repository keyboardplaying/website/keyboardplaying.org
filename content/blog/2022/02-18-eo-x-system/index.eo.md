---
date: 2022-02-18T07:00:00+01:00
title: La X-sistemo en Esperanto
#subtitle: A witty line as a subtitle
slug: x-sistemo-esperanto
description: |-
  Esperanto uzas nekutimajn diakritajn literojn, kiujn ne ĉiuj operaciumoj permesas enigi.
  Iuj aliroj ofertas solvojn.
  Jen la plej ofta hodiaŭ.
author: chop
categories: [ writing ]
tags: [ esperanto ]
keywords: [ esperanto, diakritoj, H-sistemo, X-sistemo, digrafo, transliterumo, anstataŭo, Open VSX, VSCodium, Theia ]

references:
- id: wiki-eo-substitutions
  name: Substitutions of the Esperanto alphabet
  title: true
  url: https://en.wikipedia.org/wiki/Substitutions_of_the_Esperanto_alphabet
  lang: en
  author:
    name: Wikipedia
---

Kiam oni komencas Esperanton, oni komencas per la alfabeto.
Kelkaj latinaj literoj ne estas uzataj.
Aliaj estas diakritaj en kombinoj, kiuj ne ekzistas en aliaj lingvoj.
Enigi ĉi tiujn signojn en tipa komputila sistemo povas esti defio.
Jen solvo.

<!--more-->

## La nekutimaj literoj de Esperanto

Do jen la leteroj de Esperanto, kiujn vi ne trovos en aliaj lingvoj: _ĉ_, _ĝ_, _ĥ_, _ĵ_, _ŝ_ kaj _ŭ_ (kaj ilia majuskla ekvivalento).

Krom se vi iom agordis vian sistemon por skribi en Esperanto, tajpante <key>^</key> kaj tiam <key>g</key> plej verŝajne montros `^g` , ne `ĝ`.
Mi eĉ ne parolas pri tajpi _ŭ_.
Kun iom da aranĝo, via maŝino kapablas ĝin, sed mi ne volas skribi al vi instrukcion hodiaŭ.

Hodiaŭ, mi preferas diri al vi manieron skribi Esperanton kiam vi ne povas fari tiun agordon.


## Using Digraphs

### Flankaĵo

Dum la kelkaj semajnoj, kiujn mi pasigis en Germanio, mi devis plenumi kelkajn administrajn procedojn, kun komputilaj formularoj, kiuj estis poste presitaj.
Io kaptis mian atenton dum unu el ili.
La komizino laboris kun Cobol-simila komputila ekrano, kia mi vidis multajn fojojn en apotekoj kaj aliaj malgrandaj butikoj en Francio, sed tio ne estas tio, kion mi rimarkis.
Kion mi rimarkis estis, ke kiam ajn ŝi devintus tajpi signon kun _Umlaut_ (dierezo), ŝi tajpis la senakcentan literon sekvitan de _e_ anstataŭe.

Pripensante, estis nenio mistera: supersignitaj literoj ne estas ASCII[^fn-ascii] signoj.
Por uzi la _Umlaut_[^fn-umlaut] en sistemo kiu ne subtenas ĝin postulas solvon.
Digrafoj estis la kutima solvo en Germanio.
Ĝi ankaŭ estis uzata por anstataŭigi _Esszett_ (_ß_), kutime per _ss_ aŭ foje _sz_.

[^fn-ascii]: Nu, ili estas parto de la _plilongigitaj_ ASCII-signoj.

[^fn-umlaut]: La _Umlaut_ ne estas tie por estetiko, en la germana.
Ĝi ŝanĝas kaj la prononcon de la vorto kaj ĝian signifon.
Ekzemple, _schon_ signifas "jam" dum _schön_ signifas "bela".
Oni povus ankaŭ krei embarasajn situaciojn pro konfuzado de _schwül_ (malseka) kaj _schwul_ (geja).


### Unua provo en Esperanto

Zamenhof sugestis ion similan al Esperanto:

* Por _ŭ_, uzu _U_.
* Por cirkumfleksa akcento, uzu _h_ post la letero (ekz. _ŝ_ ↦ _sh_).

Ĉi tio estis nomita H-sistemo.

Ne ĉio estis perfekta.
Unue, estis necese atenti la skribadon de certaj kunmetitaj vortoj.
Ekzemple, _flughaveno_ (flughaveno) estas la kuntiriĝo de _flugo_ (la ago de flugi) kaj _haveno_ (la haveno), sed oni devas rimarki, ke tio ne estas la vorto _fluĝaveno_ skribita en la H-sistemo.
Zamenhof do aldonis streketojn aŭ apostrofojn en kazoj kie la _h_ estis ĝusta (en nia ekzemplo, _flug'haveno_).

La alia malfacilaĵo pri ordigo.
Tipe, ĉiuj vortoj komencantaj per _ĉ_ estu post ĉiuj vortoj komencantaj per _c_ (_ci_ estu antaŭ _ĉu_).
Kun sistemo H, naiva varo tamen ne respektus ĉi tiun regulon, metante _ci_ _post_ _ĉu/chu_.



## La X-sistemo

La sistemo, kiun mi malkovris, kiam mi komencis la lecionojn pri [Ikurso][ikurso], estas sufiĉe simila al la H-sistemo:

* Por _ŭ_, uzu _ux_.
* Por cirkumfleksa akcento, uzu _x_ post la letero (ekz. _ŝ_ ↦ _sx_).

Ne surprize, ĝi nomiĝas la X-sistemo.
Ĝi solvas (preskaŭ) ĉiujn limojn de la H-sistemo:

* _x_ ne estas parto de la Esperanta alfabeto, kio evitas ambiguaĵojn.
* Ordigo estas plejparte ĝusta (_ĉu/cxu_ estas post _ci_).
Estas ankoraŭ kelkaj eraroj en la kazo de mislokigita _z_, sed tio malofte okazas.

Ĝi ne estas perfekta sistemo, kaj ĝi havas siajn proprajn problemojn, sed ĝi estas oportuna maniero skribi Esperanton kiam Esperantaj diakritaj signoj ne estas disponeblaj.
Plejofte oni eĉ povas aŭtomatigi anstataŭigi ĉi tiujn digrafojn per supersignitaj literoj.



## En VSCodium

Ĉu vi volas uzi System X en via plej ŝatata VS Code-kongrua redaktilo?
Estas etendaĵo por tio!
Want to use the X-System in your favourite VS Code-compatible editor?
There's an extension for that!
[Klavaro][klavaro-gh] ebligas al vi anstataŭigi digrafojn.
Vi povas malŝalti ĝin per klako kiam vi ne tajpas Esperanton.

Kaj ĉar la programisto ankaŭ deplojis ĝin [sur Open VSX][klavaro-vsx], [vi povas facile aldoni ĝin al VSCodium aŭ Theia!][openvsx]


[ikurso]: https://ikurso.esperanto-france.org/
[klavaro-gh]: https://github.com/paulbarre/vscode-klavaro
[klavaro-vsx]: https://open-vsx.org/extension/paulbarre/klavaro
