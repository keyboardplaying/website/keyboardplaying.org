---
date: 2022-02-18T07:00:00+01:00
title: Le système X en espéranto
#subtitle: A witty line as a subtitle
slug: systeme-x-esperanto
description: |-
  L'espéranto utilise des lettres diacritées peu communes, que tous les systèmes d'exploitation ne permettent pas de saisir.
  Certaines approches proposent des solutions.
  Voici la plus courante aujourd'hui.
author: chop
categories: [ writing ]
tags: [ esperanto ]
keywords: [ esperanto, diacritiques, système H, système X, digraphe, translittération, substitution, Open VSX, VSCodium, Theia ]

references:
- id: wiki-eo-substitutions
  name: Substitutions of the Esperanto alphabet
  title: true
  url: https://en.wikipedia.org/wiki/Substitutions_of_the_Esperanto_alphabet
  lang: en
  author:
    name: Wikipedia
---

Quand on démarre l'espéranto, on commence par l'alphabet.
Certaines lettres latines ne sont pas utilisées.
D'autres sont diacritées dans des combinaisons qui n'existent pas dans d'autres langues.
La saisie de ces caractères sur un système informatique habituel peut s'avérer un défi.
Voici une solution de contournement.

<!--more-->

## Les lettres inhabituelles de l'espéranto

Voici donc les lettres de l'espéranto que vous ne trouverez pas dans d'autres langues : _ĉ_, _ĝ_, _ĥ_, _ĵ_, _ŝ_ et _ŭ_ (et leurs variantes majuscules).

À moins d'avoir un peu configuré son système pour écrire en espéranto, il est fort probable que taper <key>^</key> puis <key>g</key> affiche `^g` , pas `ĝ`, sans parler de trouver comment écrire _ŭ_.
Avec un peu de configuration, votre machine en est capable, mais je ne veux pas vous écrire un mode d'emploi aujourd'hui.

Aujourd'hui, je préfère vous parler d'un moyen d'écrire en espéranto lorsque vous ne pouvez pas faire cette configuration.


## L'utilisation de digraphes

### Une parenthèse

Au cours des quelques semaines que j'ai passées en Allemagne, il m'a fallu remplir quelques démarches administratives, avec des formulaires informatisés ensuite imprimés.
Quelque chose a attiré mon attention au cours de l'une d'elle.
L'employée travaillait sur un écran de type Cobol, le même genre que j'ai souvent vu dans les pharmacies et autres petits commerces en France, mais ce n'était qu'un détail.
Ce que j'ai remarqué, c'était que, chaque fois qu'elle aurait dû taper un caractère avec un _Umlaut_ (un tréma), elle tapait à la palce la lettre non accentuée suivie d'un _e_.

En y réfléchissant, il n'y avait rien de mystérieux : les lettres accentuées ne sont pas des caractères ASCII[^fn-ascii].
Pour utiliser l'_Umlaut_[^fn-umlaut] dans un système qui ne le supporte pas, il faut une solution de contournement.
Les digraphes étaient la solution habituelle en Allemagne.
Elle servait aussi à remplacer l'_Esszett_ (_ß_), habituellement par _ss_ ou occasionnellement _sz_.

[^fn-ascii]: Ils font partie des caractères ASCII _étendus_, mais je digresse déjà bien assez comme ça.

[^fn-umlaut]: L'_Umlaut_ n'est pas là pour l'esthétique, en allemand.
Il change à la fois la prononciation du mot et son sens.
Par exemple, _schon_ signifie « déjà » tandis que _schön_ signifie « beau ».
On pourrait aussi créer des situations embarrassantes en confondant _schwül_ (humide) et _schwul_ (homosexuel).


### Première tentative en espéranto

Zamenhof proposa une approche similaire en espéranto :

* Pour _ŭ_, utiliser _u_.
* Pour un accent circonflexe, utiliser un _h_ après la lettre (p.ex. _ŝ_ ↦ _sh_).

Ceci a été surnommé le système H.

Tout n'était pas parfait.
Premièrement, il fallait faire attention à l'écriture de certains mots composés.
Par exemple, _flughaveno_ (aéroport) est la contraction de _flugo_ (l'action de voler) et _haveno_ (le port), mais il faut savoir qu'il ne s'agit pas du mot _fluĝaveno_ écrit dans le système H.
Zamenhof ajouta donc des traits d'union ou des apostrophes dans les cas où le _h_ était correct (dans notre exemple, _flug'haveno_).

L'autre difficulté concernant le tri.
Typiquement, tous les mots commençant par _ĉ_ devraient être après tous les mots commençant par _c_ (_ci_ devrait être avant _ĉu_).
Avec le système H, un tri naïf ne respecterait cependant pas cette règle, plaçant _ci_ _après_ _ĉu/chu_.



## Le système X

Le système que j'ai découvert en débutant les cours sur [Ikurso][ikurso] est assez proche du système H :

* Pour _ŭ_, utiliser _ux_.
* Pour un accent circonflexe, utiliser un _x_ après la lettre (p.ex. _ŝ_ ↦ _sx_).

Sans surprise, on l'appelle le système X.
Il résout (presque) toutes les limitations du système H :

* _x_ ne fait pas partie de l'alphabet espéranto, ce qui évite les ambiguïtés.
* Le tri est dans l'ensemble correct (_ĉu/cxu_ est après _ci_).
Il reste quelques erreurs dans le cas d'un _z_ mal placé, mais ceci se produit rarement.

Ce n'est pas un système parfait, et il a ses propres problèmes, mais c'est un moyen pratique d'écrire en espéranto lorsque les diacritiques de l'espéranto ne sont pas accessibles.
Dans la plupart des cas, on peut même automatiser un remplacement de ces digraphes par les lettres accentuées.



## Dans VSCodium

Vous souhaitez utiliser le système X dans votre éditeur compatible VS Code préféré ?
Il y a une extension pour ça !
[Klavaro][klavaro-gh] (« clavier » en espéranto) vous permet de remplacer les digraphes à la volée.
Vous pouvez le désactiver d'un clic si vous n'êtes pas en train de taper de l'espéranto.

Et puisque le développeur l'a également déployé [sur Open VSX][klavaro-vsx], [vous pourrez facilement l'ajouter à VSCodium ou Theia !][openvsx]


[ikurso]: https://ikurso.esperanto-france.org/
[klavaro-gh]: https://github.com/paulbarre/vscode-klavaro
[klavaro-vsx]: https://open-vsx.org/extension/paulbarre/klavaro
[openvsx]: {{< relref path="/blog/2022/01-21-open-vsx" >}}
