---
date: 2022-02-04T07:00:00+01:00
title: What's Up? (February 2022)
#subtitle: A witty line as a subtitle
slug: whats-up
description: |-
  The blog's evolving, with a new publication calendar and new formats.
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ blog, stories, stylesheet, minimalism ]
---

A new year has begun.
So far, we've been holding up our good resolutions for the website.

<!--more-->

## What's Changed?

### A New Publication Calendar

You may have noticed: we publish more often.
Since the year has begun, we suceeded in not missing a week.

On the blog, first.
The publication day has changed.
Until now, new posts were shared on Monday.
It moved to Friday.
There's absolutely no audience-seeking-related purpose, just a search for regularity.

See, when publishing on Monday, you think you have the whole week to write a post.
But the work days go by, and you're too tired to add the blogging to the todos, but that doesn't matter, because the weekend is coming.
And then it's here, and you realize you need to rest, in a way or another, and your post is never ready on time.

Thus was born a new constraint: write _before_ being in weekend.
Here's to hoping it keeps working.

The stories have been active, too.
The [weekly challenges][stylohebdo] are back, and the participations are published as soon as they're ready and translated, usually on Saturday or Sunday.
The [bimonthly challenges][defidustylo] will soon be back, too.
For now, I'm getting late on the last one.


### New Post Formats

This is intimately related with the above subject: the posts are getting shorter.
Sure, it makes it easier to find the time to write, but it also goes for reading: you'll more easily find five minutes to read an article than a full quarter of an hour (with the required concentration) to follow an author's divagation.

The goal is therefore to remain on topic, to be brief about it and, if necessary, to split the post into a series if they grow too long.

Wild new formats also appeared, to try to share knowledge in a kind-of-game way.
Thus are [regex quizzes][regex-quizzes] born.
We'll have to wait a bit to see if readers like it, but from our first exchanges, we're quite confident there'll be an audience.


## What's Next?

A graphical rework's in the pipe.
We never were really satisfied with the website's appearance, but no UI or UX designer ever sent us feedback, so we just did our best.

No matter, in accordance with our attempts to push [sustainable IT][s-it], we'll try a more minimaslist interface (and finally write the print stylesheet; that's been [in the issue tracker][print-css-issue] forever).
We hope it'll be done by the end of the first quarter, but can't make any promise.

We'll see in two months, for next "What's Up?"


[stylohebdo]: {{< relref path="/inspirations/weekly" >}}
[defidustylo]: {{< relref path="/inspirations/bimonthly" >}}
[regex-quizzes]: {{< relref path="/blog/2022/01-28-regex-quizzes" >}}
[s-it]: {{< relref path="/tags/sustainable-it" >}}
[print-css_issue]: https://gitlab.com/keyboardplaying/website/keyboardplaying.org/-/issues
