---
date: 2022-02-04T07:00:00+01:00
title: Quoi d'neuf ? (février 2022)
#subtitle: A witty line as a subtitle
slug: quoi-d-neuf
description: |-
  Le blogue évolue, avec un nouveau planning de publication et de nouveaux formats.
author: chop
categories: [ kp.org ]
tags: [ news ]
keywords: [ blogue, histoires, feuille de style, minimalisme ]
---

Une nouvelle année a débuté.
Jusqu'ici, nous sommes parvenus à tenir les bonnes résolutions que nous avons prises pour le site.

<!--more-->

## Qu'est-ce qui a changé ?

### Un nouvel agenda des publications

Vous l'avez peut-être remarqué : nous publions plus souvent.
Depuis que l'année a commencé, il y a eu de l'activité toutes les semaines.

Sur le blogue, d'abord.
Le jour de publication a changé.
Jusqu'à présent, le but était de publier le lundi.
Désormais, c'est le vendredi.
Le but n'a rien à voir avec une vaine recherche d'audience, c'est uniquement d'être plus régulier.

Les publications le lundi devaient laisser le week-end comme dernier recours pour écrire si la semaine ne l'avait pas permis.
En pratique, la semaine était consacrée à d'autres sujets en prévoyant d'utiliser la réserve, puis le week-end servait à se ressourcer, après cinq épuisantes journées de labeur.
C'est ainsi que la nouvelle contrainte est d'écrire le billet avant de pouvoir partir en week-end.

Les histoires sont actives également.
Les [Stylos Hebdos][stylohebdo] ont bien repris et ils sont partagés ici dès qu'ils sont prêts, souvent le samedi ou dimanche.
Les [défis du stylo][defidustylo] (bimestriels) les rejoindront bientôt.
Je suis pour l'instant un peu en retard pour le dernier.


### De nouveaux formats

Ce point a un lien étroit avec le précédent : les billets sont devenus plus concis.
Bien entendu pour avoir le temps d'écrire, mais aussi pour vous faciliter la lecture : il est plus aisé de trouver cinq minutes pour lire un court article que de trouver le temps et la concentration pour suivre les divagations d'un auteur pendant un quart d'heure.

Le but est donc de rester plus centré sur un sujet, de l'aborder brièvement et, dans la mesure du possible, de découper en série les billets qui deviendraient trop conséquents, pour les publier sur plusieurs semaines.

Font également leur apparition de nouveaux formats pour essayer de partager des connaissances de manière plus ludique.
C'est ainsi que nous vous parlions la semaine dernière des [quiz regex][regex-quizzes] à venir.
Il nous faudra encore patienter un peu avant de savoir si vous aimez ce concept, mais les premiers échanges avec certains d'entre vous laissent bon espoir.


## Et ensuite ?

Nous avons une refonte graphique dans les tuyaux.
Nous n'avons jamais été tout à fait satisfait de l'aspect général du site, mais aucun UI ou UX designer n'est passé par ici pour nous donner des conseils.

Qu'à cela ne tienne, en accord avec nos tentatives de faire connaître le [numérique responsable][nr], nous allons viser une interface plus minimaliste que l'actuelle (et finalement nous atteler à créer une feuille de style pour les impressions, un sujet [qui attend depuis trop longtemps][print-css-issue]).
Nous avons l'espoir de pouvoir finir ceci avant la fin du trimestre, mais sans pouvoir le garantir.

La réponse dans deux mois, dans le prochain « Quoi d'neuf ? »


[stylohebdo]: {{< relref path="/inspirations/weekly" >}}
[defidustylo]: {{< relref path="/inspirations/bimonthly" >}}
[regex-quizzes]: {{< relref path="/blog/2022/01-28-regex-quizzes" >}}
[nr]: {{< relref path="/tags/sustainable-it" >}}
[print-css_issue]: https://gitlab.com/keyboardplaying/website/keyboardplaying.org/-/issues
