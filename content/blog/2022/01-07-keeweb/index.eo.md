---
date: 2022-01-07T07:00:00+01:00
title: Keeweb, Nextcloud etendaĵo por aliri Viajn KeePass pasvortojn
#subtitle: A witty line as a subtitle
slug: keeweb-nextcloud-keepass-integrigo
description: |-
  Keeweb estas etendaĵo por Nextcloud, por administri KeePass-dosierojn en la retinterfaco.
  Se vi havas tion bezonon, ĉi tio povus esti la solvo.
author: chop
categories: [ software ]
tags: [ security ]
keywords: [ sekureco, pasvorto, adminstrilo por pasvortoj, dufaza aŭtentigo, unufoja pasvorto, sinkronigo, nextcloud, keepass, keepassxc, keepass2android, libera programaro ]
---

Pasintjare, mi [skribis afiŝon][pwd-manager] por proponi Open Source alternativon al administriloj por pasvortojn kiel LastPass aŭ 1Password.
Io, kion vi mem gastigus, por ke vi restu en kontrolo di viaj sentemaj datumoj.

Tamen estis unu grava limigo: ĉi tiu solvo postulas, ke vi instalu aplikaĵon por legi viajn pasvortojn en komputilo.
Hodiaŭ, mi afiŝas ion, se vi _ne povas_ instali aplikaĵon sur la menciita komputilo.

<!--more-->

## La solvo

Mia solve ne ŝanĝas.
Se vi serĉas Open Source manieron konservi kaj administri viajn pasvortojn, [legu ĝin][pwd-manager] (kaj ne hezitu komenti se necese).

Jen rapida resumo:

1. Kreu vian KDBX dosieron kun KeePass aŭ KeePassXC.
2. Konservu ĝin en vian Nextcloud servilon.

Vi tiam povas elŝuti la dosieron en ajnan fidindan komputilon kaj uzi KeePass(XC).
Aŭ vi povas fari tion en via aptelefono, kun la bona aplikaĵo, kiel Keepass2Android.

![Diagramo ilustranta la solvon]({{< relref path="/blog/2021/03-08-open-source-pwd-sync" >}}/sync-pwd-keepassxc-nextcloud-otp-authy.eo.svg)

Nun la problemo: vi eble bezonos viajn pasvortojn en komputilo, sur kiu vi ne povas instali KeePass.
Se tia estas la kazo, legu plu.


## Aliri viajn pasvortojn en retinterfaco

Jen [Keeweb][keeweb]: tio estas etendaĵo por Nextcloud, por malfermi KDBX dosierojn en via retumilo.
Vi devas instali ĝin en vian Nextcloud servilon kiel aliajn aplikaĵojn (en la sekcio _Integriĝo_).

Tiam vi povos malfermi viajn dosierojn en la retinterfaco.
La plej facila manieron estas navigi al via KDBX dosiero, kaj alklaki ĝin.
La ekrano de Keeweb aperos kaj vi devos la ĉefan pasvorton de via dosiero.

De tie, vi povos serĉi viajn pasvortojn, kopii ilin kaj vidi la nunan valoron de tempobazita konfirmkodo, se vi elektis administri tiujn en KeePassXC.
La teksto velkos kiam la kodo maljuniĝos, tiam revenos al plena koloro kiam nova kodo estas generita.

Vi ankaŭ povos uzi la retinterfaco por krei aŭ ĝis datigi pasvortojn.

Ĝi ne estas la plej bona interfaco (amiko fakte raportis al mi ke ĝi ne estis pensita por porteblaj aparatoj), sed ĝi helpos se vi bezonos.


## Pensi antaŭ ol aldoni etendaĵon

Vi devus esti vere singarda kiam temas pri via KDBX dosieron.
Ĝi estas io sentema kaj altvalora.
Ĝi enhavas ĉiujn ŝlosilojn de via interreta identeco kaj personaj datumojn.
Neniu devus povi aliri ĝin.[^fn-gif-gandalf]

Kvankam ĝi estas protektika per pasvorto, nova maniero por vi de aliri vian dosieron povas fariĝi nova maniero por atakanto ŝteli ĝin se malkovro de vundebleco.

Mi ne diras, ke vi tute evitu Keeweb, mi nur konsilas eviti ĝin se vi ne vere bezonas ĝin.

[^fn-gif-gandalf]: Mi uzus [Gandalf][gif-gandalf] por emfazi ĉi tion, [se GIF-oj ne havus median spuron][it-footprint].
"Tenu ĝin sekreta. Tenu ĝin sekura."

[pwd-manager]: {{< relref path="/blog/2021/03-08-open-source-pwd-sync" >}}
[keeweb]: https://apps.nextcloud.com/apps/keeweb

[gif-gandalf]: https://media.giphy.com/media/GQSpO8dpDesuI/giphy.gif
[it-footprint]: {{< relref path="/series/intro-sustainable-it" lang="en" >}}
