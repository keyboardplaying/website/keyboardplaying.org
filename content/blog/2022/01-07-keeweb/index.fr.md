---
date: 2022-01-07T07:00:00+01:00
title: Keeweb, une extension Nextcloud pour gérer vos mots de passe
#subtitle: A witty line as a subtitle
slug: keeweb-nextcloud-keepass-integration
description: |-
  Keeweb est une interface web intégrée à Nextcloud, permettant de gérer les fichiers KeePass.
  Si c'est l'un de vos besoins, voici peut-être la solution.
author: chop
categories: [ software ]
tags: [ security ]
keywords: [ sécurité, mot de passe, gestionnaire de mots de passe, 2fa, otp, synchronisation, nextcloud, keepass, keepassxc, keepass2android, open source ]
---

L'an dernier, j'ai [écrit un billet][pwd-manager] pour proposer une alternative ouverte aux gestionnaires de mots de passe comme LastPass, 1Password et similaires.
Quelque chose à héberger vous-même afin de garder la main sur vos données sensibles.

Il demeurait cependant une limitation importante : la solution proposée reposait sur l'installation d'une application pour la lecture de vos mots de passe sur un ordinateur.
Aujourd'hui, j'y ajoute ce court billet pour le cas où vous ne _pourriez pas_ installer d'application sur ladite machine.

<!--more-->

## Remise en contexte

Les bases de ma solution précédente restent les mêmes.
Si vous cherchez une solution open source pour gérer vos mots de passe, [lisez en diagonale mon précédent billet][pwd-manager] (et n'hésitez pas à partager vos retours au besoin).

Si vous voulez seulement un résumé, le voici :

1. Créez un fichier KDBX avec KeePass ou KeePassXC.
2. Sauvegardez ce fichier dans votre Nextcloud.

Vous pourrez ensuite le télécharger/synchroniser sur tout ordinateur de confiance et l'ouvrir avec KeePass(XC).
Cela fonctionne également sur votre ordiphone avec l'application appropriée, comme Keepass2Android sous Android.

![Un diagramme illustrant la solution générale]({{< relref path="/blog/2021/03-08-open-source-pwd-sync" >}}/sync-pwd-keepassxc-nextcloud-otp-authy.fr.svg)

C'est ici qu'apparaît le problème : si vous avez besoin de vos mots de passe sur un ordinateur sur lequel vous ne pouvez pas installer KeePass, votre solution ne pourra a priori plus vous aider.
Continuez votre lecture si vous êtes dans cette situation.


## Accéder à vos mots de passe depuis une interface web

C'est là qu'apparaît en sauveur [Keeweb][keeweb] : il s'agit d'une extension Nextcloud qui vous permet de gérer vos fichiers KDBX depuis votre navigateur web.
Il vous suffit de l'installer comme n'importe quelle autre application Nextcloud sur votre serveur (elle est dans la section _Intégration_).

Une fois ceci fait, vous pourrez ouvrir vos fichiers depuis l'interface web.
La manière la plus simple est certainement de naviguer jusqu'à votre fichier KDBX et de cliquer dessus.
L'écran de Keeweb s'affichera automatiquement et vous demandera de saisir le mot de passe principal de votre fichier.

À partir de là, vous aurez la possibilité de chercher vos mots de passe, les copier et voir la valeur courante d'un TOTP, si vous avez choisi de les gérer dans KeePassXC.
Le texte s'estompera à mesure que vieillira le TOTP et retrouvera sa vivacité lorsque le code suivant sera généré.

Vous pourrez bien entendu utiliser l'interface web également pour créer de nouveaux mots de passe et mettre à jour vos données.

Ce n'est pas l'interface la plus agréable au monde (un ami m'a d'ailleurs rapporté qu'elle n'est pas conçue pour un usage sur appareil mobile), mais elle dépanne bien.


## Une seconde de réflexion avant action

Je vous incite à la plus grande prudence lorsqu'il est question de vos fichiers KDBX?
C'est un bien sensible et précieux.
Il contient toutes les clés à votre identité en ligne et vos données personnelles.
Personne ne devrait être capable d'y accéder.[^fn-gif-gandalf]

Bien qu'il soit protégé par un mot de passe, chaque nouvelle façon pour vous d'accéder à votre fichier peut amener une vulnérabilité qu'un attaquant pourrait exploiter pour vous le voler.

Je ne vous dis pas d'éviter complètement Keeweb, je vous conseille simplement de l'éviter si vous n'en avez pas réellement besoin.

[^fn-gif-gandalf]: J'utiliserais volontiers [Gandalf][gif-gandalf] pour insister sur ce poitn [si l'empreinte environnementale des GIFs était négligeable][it-footprint].
Pour aller droit au but : « Gardez-le caché. Mettez-le en sécurité. »

[pwd-manager]: {{< relref path="/blog/2021/03-08-open-source-pwd-sync" >}}
[keeweb]: https://apps.nextcloud.com/apps/keeweb

[gif-gandalf]: https://media.giphy.com/media/GQSpO8dpDesuI/giphy.gif
[it-footprint]: {{< relref path="/series/intro-sustainable-it" >}}
