---
date: 2022-04-01T07:00:00+02:00
title: Computer Keyboards Around the World
slug: computer-keyboards-around-world
description: |-
  There are around 7000 languages ​​and 50 writing systems in the world. So, if you are passionate about languages, travel, culture and IT, this theme on computer keyboards around the world should be of great interest to you, and perhaps motivate you to learn a foreign language!
author: Amazing Talker
guest:
  url: https://en.amazingtalker.com/
categories: [ keyboards ]
#tags: [ java ]
keywords: [ keyboard, keyboard layout, keyboard history, azerty, qwerty, qwertz, dvorak ]
---

There are around 7000 languages ​​and 50 writing systems in the world. So, if you are passionate about languages, travel, culture and IT, this theme on **computer keyboards around the world** should be of great interest to you, and perhaps motivate you to [learn a foreign language][at-learn]!

Using a keyboard abroad will now seem so easy.

<!--more-->

## Latin Alphabet Keyboards

There are several types of computer keyboards.
Each with their specificities and symbols.
Let's discover together the keyboards of the Latin alphabet.

### Azerty

The Azerty keyboard was created for a specific purpose: to allow typists of the time to use the typewriter without blocking the letters or tangling them when they typed on the machine.
Indeed, the machines being mechanical at the time, it was necessary to hit the keys hard at high speed for the selected letters to appear on the paper to be printed.
To improve the writing and typing system, the first keyboards were created, without Azerty.
Which was problematic because of the typing speed, but not only.
Thus, the first Azerty keyboards were created in the nineteeth century to solve this problem.

The primary objective was to distance the most frequently used letters from each other, but also to substantially slow the typing speed of typists and prevent the machine arms from becoming tangled or jammed.

Just look at the layout of the letters even today: the letter E somewhat distant from the letter A and both of which are located on the left side of the keyboard, to cite just one example.

You will discover several functions on the keys of the Azerty keyboard.
For example:

* The <kbd>Enter</kbd> key which allows you to validate datavalidate a data or to go to the line.
* The <kbd>Esc</kbd> key, which allows you to exit an unwanted situation or operation.
* The <kbd>Delete</kbd> key which allows data to be erased; and many others.

Unlike France, some keys on the Belgian keyboard are labeled in English.
This is the case of the <kbd>Enter</kbd> key entitled "Enter," of the <kbd>Shift</kbd> key corresponding to "Shift lock," etc.
This is because the language spoken in the norhtern half of Belgium is Dutch.


### Qwerty

![The Qwerty keyboard](qwerty.png)

The Qwerty keyboard is widespread all over the world.
In America, but also in Europe.
Designed in the nineteenth century (1868) by Christopher Latham Sholes, a former printer's apprentice, then a print publisher, in Wisconsin in the United States.
The keyboard is called Qwerty in reference to the first six letters on the keyboard.

We can cite some examples of countries using this type of keyboard: Denmark, Finland, Iceland, Italy, the Netherlands, Portugal, Russia, Spain, Sweden, the United Kingdom, Ireland, the United States, Mexico, Brazil, China, Taiwan, Hong Kong, India, Burma, South Korea, Thailand, Vietnam, Israel, Pakistan, Turkey, etc.; to cite only a few.
It is recommended to use computer keyboards according to your country, because the arrangement of letters, characters or symbols vary from one country to another.

For example: on an international US Qwerty keyboard, you will find the <kbd>Enter</kbd> or <kbd>Return</kbd> key horizontally; the <kbd>Shift</kbd> key, for capitals, is larger than on some Qwerty keyboards.
This is the case of the Dutch Qwerty keyboard, commonly used in the Netherlands.
In addition, the <kbd>2</kbd> key features the euro symbol.
To activate it, just press <kbd>Alt</kbd>+<kbd>2</kbd> at the same time.

Let's also take the case of Quebec where more than the majority of the population speaks French.
French accent keys have been added to the keyboard to meet the needs of the population using the keyboard.


### Qwertz

![The Qwertz keyboard](qwertz.png)

Most Germanic countries use the Qwertz keyboard, but not only, such as Germany, Austria, Switzerland, Croatia, Czech Republic, Hungary, etc. For example.

In German-speaking countries, the letter Z is particularly used.
Hence the arrangement of the other letters Q, W, E, R, T, Z.
To adapt each keyboard to its country, symbols, letters can sometimes be added to the keyboard.

In Hungary, for example, the quotation marks are located on the <kbd>2</kbd> key.
The exclamation mark (!) on the <kbd>4</kbd> key.
To access the pound symbol (#), just press the <kbd>Alt Gr</kbd> key.
Ditto for the ampersand (&) and the at sign (@).
While in Germany, the at sign (@) is located to the right of the <kbd>Tab</kbd> key for tabulation and the pound symbol (#) to the left of the <kbd>Enter<kbd> key.

In Croatia, for example, to access the at symbol (@), simply press the <kbd>Alt</kbd> key, while the question mark (?) is located to the left of the + and = symbols.


### Dvorak Keyboards

![The Dvorak keyboard](dvorak.png)

The Dvorak keyboard was born at the beginning of the twentieth century (1936), and is the work of Professor August Dvorak, professor at the University of Washington.
This keyboard is characterized by its efficiency with regard to the layout of the keys.
At least it was created that way.

You will see on the central line all the vowels, the most used consonants, to facilitate the use of the left and right hand when typing on the keyboard with both hands.
Even if in practice going from an Azerty, Qwerty, Qwertz to Dvorak keyboard is much more complex, hence the failure of the generalization of this type of keyboard in the world; although this type of keyboard has some advantages such as comfort in terms of use due to the general ergonomics of the keyboard.
An increase in typing speed due to the arrangement of different keys, etc.


## Cyrillic Keyboards

![The Cyrillic keyboard](cyrillic.png)

The Cyrillic keyboard is a typical Russian keyboard.
With the characters of the Russian alphabet.
It works the same way as other types of keyboards: Qwerty, Qwertz…

There are several known keys: <kbd>Shift</kbd>, <kbd>Ctrl</kbd>, <kbd>Alt Gr</kbd>, <kbd>Caps Lock</kbd>, etc.
As well as Arabic numerals: 1, 2, 3, 4, 5, 6, 7, 8, 9, 0.
But what makes this keyboard special is the absence of Latin characters, at least in whole or in part, which correspond to the 26 letters of the English alphabet.
The Russian alphabet has about 33 letters.

There are 9 Greek letters, 7 letters still from the Latin alphabet such as A, E, M, etc. But these letters sound Russian, not Latin. And 17 purely Russian letters.

You will discover this type of letters on a Russian keyboard: И, Й, Ц, Ч, Н, Ш, Щ, Х, Ы, Ж, В, Э, Ю, Я, Ё.

However, the layout of the keyboard elements is the same as on an American keyboard.


## Chinese Keyboards

Of the Qwerty type, the Chinese keyboard also has specificities.
In the past, Chinese keyboards had many characters.
Today, more modern, they resemble American-style keyboards.
The easiest way is to type in pinyin on the computer.
It is a phonetic system created for non-Chinese to convert the English alphabet into Chinese characters.
However, this technique is not without difficulty, because writing and speaking in Chinese are not the same thing.

Indeed, if in the Chinese language multiple characters have the same pronunciation, they do not have the same meaning, so it is up to artificial intelligence, that is to say your computer, to determine the context of your sentences.
However, by getting used to writing on your computer, the program takes your habits into account and by necessity is able to suggest the right characters to use.

To master a Chinese keyboard, you must therefore know the Chinese alphabet and Chinese characters.
You have two methods.
One that is interested in sound with reference to pinyin and which is used in China or Zhuyin rather used in Taiwan.

### The Zuyin or Bopomofo Keyboards

![The Zuyin or Bopomofo keyboard](zuyin.png)

However, there are other keyboards, less widespread and which take the shape into account (you must therefore know how to write the Chinese character before typing it on the keyboard).

### The Wubi (五筆)

![The Wubi keyboard](wubi.png)

### The Cangjie (倉頡)

![The Cangjie keyboard](cangjie.png)


## Japanese Keyboards

![The Japanese keyboard](japanese.png)

The Japanese keyboard is also Qwerty type.
You will even find the following letters written on the keyboard: Q, W, E, R, T, Y.
On a Macbook, for example, you have the <kbd>tab</kbd>, <kbd>control</kbd>, <kbd>shift</kbd>, <kbd>caps</kbd>, <kbd>options</kbd> and <kbd>command</kbd> keys located on the left of the keyboard, and the <kbd>delete</kbd>, <kbd>enter</kbd>—or <kbd>return</kbd>— keys, located on the right of the keyboard.
There are two modes on the Japanese keyboard: the Latin alphabet or romaji, hiragana or even Katakana.

On the machine, kanji transcriptions are also available. Unlike Katakana, hiragana is used to spell Japanese words. You will find the following symbols or letters on this type of keyboard: た　て　い　す　か　ん.

Going from kana to kanji is easy thanks to some specific keys of the keyboard.
There also is software allowing to converting the kanji to hiragana.


## Korean Keyboards

![The Korean keyboard](korean.png)

Just like the Japanese keyboard, the Korean keyboard is part of the Qwerty family.
This keyboard is composed of numbers: 1, 2, 3, 4, 5, 6, 7, 8, 9, 0.
On key <kbd>2</kbd> there is the symbol of the at symbol (@), on key <kbd>3</kbd> there is the pound sign (#); the dollar sign ($) is represented by the <kbd>4</kbd> key.
The <kbd>shift</kbd> and <kbd>lock</kbd> keys are used to trigger other functions as well as the symbols on the keyboard.

You will discover the following symbols typical of the Hangeul alphabet: 
ᄀ ᄁ ᄂ ᄃ ᄄ ᄅ ᄆ ᄇ ᄈ ᄉ ᄊ ᄋ ᄌ ᄍ ᄎ ᄏ ᄐ ᄑ ᄒ.


## Indian Keyboards

![The Indian keyboard](hindi.png)

The script used on the Indian keyboard is Devanagari.
Numerals on the keyboard differ somewhat from Arabic numerals and look like this: ० १ २ ३ ४ ५ ६ ७ ८ ९.
There are consonants, as well as vowels like on all computer keyboards around the world.
You will find for example these symbols: अ आ इ ई उ ऊ क ख ग घ ङ.
The <kbd>Tab</kbd>, <kbd>Caps Lock</kbd>, <kbd>Shift</kbd>, <kbd>Ctrl</kbd> and <kbd>Alt</kbd> keys are located on the left of the keyboard while the <kbd>Enter</kbd> and <kbd>Backspace</kbd> keys are located on the right.
The ampersand (&) is located on the <kbd>7</kbd> key, while the at sign (@) and the pound sign (#) are respectively located on the <kbd>2</kbd> and <kbd>3</kbd> keys.


## Thai Keyboards

![The Thai keyboard](thai.png)

The Thai keyboard is also Qwerty type and has the same layout as an American type keyboard.
The at sign (@), pound (#), ampersand (&) and exclamation point (!) symbols are located on the following keys: <kbd>2</kbd>, <kbd>3</kbd>, <kbd>6</kbd> and <kbd>1</kbd>.

Numbers from 0 to 9 are represented like this: ๐ ๑ ๒ ๓ ๔ ๕ ๖ ๗ ๘ ๙.

The <kbd>Caps Lock</kbd>, <kbd>Ctrl</kbd> and <kbd>Shift</kbd> keys are located on the left of the keyboard and the <kbd>Enter</kbd>, <kbd>Alt Gr</kbd>, <kbd>Menu</kbd> keys are on the right of the keyboard.


## Arabic Keyboards (and Its Variants)

![The Arabic keyboard](arabic.png)

Most keyboards in the Arab world are Qwerty type.
But, there are two other kinds of Arabic keyboards: phonetic and Holy Quran.
The symbols of the at sign (@), the ampersand (&), the exclamation mark (!) or the hash mark (#) appear respectively on the numbers 2, 7, 1 and 3.

The <kbd>Tab</kbd>, <kbd>Control</kbd>, <kbd>Shift</kbd> keys are located on the left like the majority of Qwerty type keyboards, while the <kbd>Return</kbd>, <kbd>Delete</kbd> and <kbd>BS</kbd> (for _Backspace_) keys are located on the right.

Here are some symbols that appear on the Arabic keyboard for the letters H, F, Q, W, N for example: ف ق و ن ح


## Hebrew Keyboards

![The Hebrew keyboard](hebrew.png)

The Hebrew keyboard has Arabic numerals: 1, 2, 3, 4, 5, 6, 7, 8, 9, 0.
The ampersand (&) is located on the number 1, the pound symbol (#) on the <kbd>3</kbd> key and the at sign (@) on the 0.
You will find many cantillation signs and other symbols there.
This is the example of: א ב ד ה ס נ מ.
These are the letters A, B, D, H, S, N and M.


## Ethiopian Keyboards

![The Ethiopian keyboard](ethiopian.png)

Figure on the Ethiopian keyboard the symbols of the Amharic alphabet.
Qwerty type, you will find on the left side the <kbd>Tab</kbd>, <kbd>Control</kbd>, <kbd>Shift</kbd>, <kbd>Caps</kbd>, <kbd>Option</kbd> and <kbd>Command</kbd> keys.
On the right side are the <kbd>Delete</kbd>, <kbd>Enter</kbd>, <kbd>Return</kbd> keys.
On each Latin letter, there are letters from the Ethiopian alphabet.


## Conclusion

There are almost as many **computer keyboards in the world** as there are languages.
As you will have understood, each country has its specificities due to the language used, the symbols, etc.

What was your favorite keyboard?
And why not [learn a new language][at-learn] related to this keyboard?


[at-learn]: https://en.amazingtalker.com/
