---
date: 2022-04-01T07:00:00+02:00
title: Les claviers à travers le monde
slug: claviers-travers-monde
description: |-
  Il existe dans le monde environ 7000 langues et 50 systèmes d'écriture.
  Alors, si vous êtes à la fois passionné de langues, de voyage, de culture et d'informatique, ce thème sur les claviers d'ordinateurs à travers le monde devrait grandement vous intéresser, et peut-être vous motiver à apprendre une langue étrangère !
author: Amazing Talker
guest:
  url: https://fr.amazingtalker.com/
categories: [ keyboards ]
#tags: [ java ]
keywords: [ clavier, disposition de clavier, histoire des claviers, azerty, qwerty, qwertz, dvorak ]
---

Il existe dans le monde environ 7000 langues et 50 systèmes d'écriture.
Alors, si vous êtes à la fois passionné de langues, de voyage, de culture et d'informatique, ce thème sur les **claviers d'ordinateurs à travers le monde** devrait grandement vous intéresser, et peut-être vous motiver à [apprendre une langue étrangère][at-apprendre] !

Utiliser un clavier à l'étranger vous paraîtra désormais si simple.

<!--more-->

## Les claviers de l'alphabet latin

Il existe plusieurs types de claviers d'ordinateurs.
Chacun avec leurs spécificités et symboles.
Découvrons ensemble les claviers de l'alphabet latin.

### Azerty

Le clavier Azerty a été créé dans un but précis : permettre aux dactylographes de l'époque d'utiliser la machine à écrire sans bloquer les lettres ou les emmêler lorsqu'ils frappaient sur la machine.
En effet, les machines étant mécaniques à l'époque, il fallait taper fort sur les touches à une grande vitesse pour que les lettres sélectionnées apparaissent sur le papier à imprimer.
Pour améliorer le système d'écriture et de frappe, les premiers claviers ont été créés, sans Azerty.
Ce qui était problématique en raison de la vitesse de frappe, mais pas que. Ainsi, les premiers claviers Azerty voient le jour au <span class="century">xix</span>ème siècle pour résoudre ce problème.

L'objectif premier était d'éloigner les lettres les plus fréquemment utilisées les unes des autres, mais aussi de ralentir substantiellement la vitesse de frappe des dactylographes et éviter que les bras de machine s'emmêlent ou se bloquent.

Il suffit d'observer la disposition des lettres encore aujourd'hui : la lettre E quelque peu éloignée de la lettre A et qui toutes les deux se situent du côté gauche du clavier, pour ne citer qu'un exemple.

Vous découvrirez plusieurs fonctions sur les touches du clavier Azerty.
Par exemple :

* La touche <kbd>Entrée</kbd> qui permet de valider une donnée ou d'aller à la ligne.
* La touche <kbd>Echap</kbd> qui permet de quitter une situation ou opération que l'on ne souhaite pas.
* La touche <kbd>supprimer</kbd> qui permet d'effacer des données ; et bien d'autres.
Le clavier Azerty est utilisé dans certains pays comme la France, la Belgique.

À la différence de la France, certaines touches du clavier belge sont libellées en anglais.
C'est le cas de la touche Entrée intitulée <kbd>Enter</kbd>, de la touche majuscule correspondant à <kbd>Shift lock</kbd>, etc.
Cela est dû à la langue parlée dans la moitié nord de la Belgique : le néerlandais.


### Qwerty

![Le clavier Qwerty](qwerty.png)

Le clavier Qwerty est répandu dans le monde entier.
En Amérique, mais aussi en Europe.
Conçu au <span class="century">xix</span>ème siècle (1868) par Christopher Latham Sholes, ancien apprenti imprimeur, puis éditeur de presse écrite, dans le Wisconsin aux États-Unis.
Le clavier s'appelle Qwerty en référence aux six premières lettres figurant sur le clavier.

Nous pouvons citer quelques exemples de pays utilisant ce type de clavier : le Danemark, la Finlande, l'Islande, l'Italie, les Pays-Bas, le Portugal, la Russie, l'Espagne, la Suède, le Royaume-Uni, l'Irlande, les États-Unis, le Mexique, le Brésil, la Chine, Taïwan, Hong Kong, l'Inde, la Birmanie, la Corée du Sud, la Thaïlande, le Viêt Nam, Israël, le Pakistan, la Turquie, etc. ; pour n'en citer que quelques-uns.
Il est recommandé d'utiliser les claviers d'ordinateurs selon votre pays, car la disposition des lettres, les caractères ou symboles varient d'un pays à l'autre.

Par exemple : sur un clavier Qwerty US international, vous trouverez la touche <kbd>Enter</kbd> ou <kbd>Return</kbd> à l'horizontale ; la touche <kbd>Shift</kbd>, pour la majuscule, est plus grande que sur certains claviers Qwerty.
C'est le cas du clavier Qwerty néerlandais, couramment utilisé aux Pays-Bas.
De plus, sur la touche <kbd>2</kbd> figure le symbole de l'euro.
Pour l'activer, il suffit d'appuyer à la fois <kbd>Alt</kbd>+<kbd>2</kbd>.

Prenons également le cas du Québec où plus de la majorité de la population parle français.
Des touches d'accentuation françaises ont été ajoutées au clavier pour répondre aux besoins dans l'usage du clavier.


### Qwertz

![Le clavier Qwertz](qwertz.png)

La plupart des pays germaniques utilisent le clavier Qwertz, mais pas que, comme l'Allemagne, l'Autriche, la Suisse, la Croatie, la République Tchèque, la Hongrie, etc.
À titre d'exemple.

Dans les pays germanophones, la lettre Z est particulièrement utilisée, d'où la disposition des autres lettres Q, W, E, R, T, Z.
Pour adapter chaque clavier à son pays, des symboles, lettres peuvent parfois être rajoutés sur le clavier.

En Hongrie par exemple, les guillemets se situent au niveau de la touche 2.
Le point d'exclamation (!) sur la touche <kbd>4</kbd>.
Pour accéder au symbole dièse (#), il suffit d'appuyer sur la touche <kbd>Alt Gr</kbd>.
Idem pour l'esperluette (&) et l'arobase (@).
Tandis qu'en Allemagne, l'arobase (@) se situe à droite de la touche <kbd>Tab</kbd> pour tabulation et le symbole dièse (#) à gauche de la touche <kbd>Enter</kbd>.

En Croatie par exemple, pour accéder au symbole arobase (@), il suffit d'appuyer sur la touche <kbd>Alt</kbd>, tandis que le point d'interrogation (?) se situe à gauche des symboles + et =.


### Dvorak

![Le clavier Dvorak](dvorak.png)

Le clavier Dvorak a vu le jour au début du <span class="century">xx</span>ème siècle (1936), et est l'œuvre du professeur August Dvorak, enseignant à l'université de Washington.
Ce clavier est caractérisé par son efficacité eu égard à la disposition des touches. Du moins, il a été créé en ce sens.

Vous verrez sur la ligne centrale l'ensemble des voyelles, les consonnes les plus usités, pour faciliter l'usage de la main gauche et droite lorsque l'on tape sur le clavier avec les deux mains.
Même si en pratique passer d'un clavier Azerty, Qwerty, Qwertz à Dvorak est bien plus complexe, d'où l'échec de la généralisation de ce type de clavier dans le monde ; bien que ce type de clavier présente des avantages comme un confort en termes d'utilisation dû à l'ergonomie générale du clavier.
Une augmentation de la vitesse de frappe en raison de la disposition des différentes touches, etc.


## Le clavier cyrillique

![Le clavier cyrillique](cyrillic.png)

Le clavier cyrillique est un clavier typiquement russe.
Avec les caractères de l'alphabet russe.
Il fonctionne de la même manière que les autres types de claviers : Qwerty, Qwertz…

Il existe plusieurs touches connues : <kbd>Shift</kbd>, <kbd>Ctrl</kbd>, <kbd>Alt Gr</kbd>, <kbd>Caps Lock</kbd>, etc.
Ainsi que des chiffres arabes : 1, 2, 3, 4, 5, 6, 7, 8, 9, 0.
Mais, ce qui fait la spécificité de ce clavier, c'est l'absence des caractères latins du moins en tout ou partie qui correspondent aux 26 lettres de l'alphabet en français.
L'alphabet russe compte environ 33 lettres.

Il existe 9 lettres grecques, 7 lettres quand même issues de l'alphabet latin comme le A, E, M, etc.
Mais ces lettres ont une consonance russe et non latine.
Et 17 lettres purement russes.

Vous découvrirez ce type de lettres sur un clavier russe : И, Й, Ц,Ч, Н, Ш, Щ, Х, Ы, Ж, В, Э, Ю, Я, Ё.

Toutefois, la disposition des éléments du clavier est la même que sur un clavier américain.


## Les claviers chinois

De type Qwerty, le [clavier chinois][clavier-chinois] présente aussi des spécificités.
Autrefois, les claviers chinois comportaient de nombreux caractères.
Aujourd'hui, plus modernes, ils ressemblent aux claviers de style américain.
Le plus simple est de taper en pinyin sur l'ordinateur.
Il s'agit d'un système phonétique créé pour les non-chinois afin de convertir l'alphabet anglais en caractères chinois.
Cependant, cette technique n'est pas sans difficulté, car écrire et parler en chinois, ce n'est pas la même chose.

En effet, si dans la langue chinoise de multiples caractères ont la même prononciation, ils n'ont pas le même sens, de sorte qu'il revient à l'intelligence artificielle, c'est-à-dire votre ordinateur de déterminer le contexte de vos phrases.
Toutefois, en prenant l'habitude d'écrire sur votre ordinateur, le programme prend en compte vos habitudes et, par la force des choses, est en mesure de proposer les bons caractères à utiliser.

Pour maîtriser un clavier chinois, vous devez donc connaître l'alphabet chinois et les caractères chinois.
Vous disposez de deux méthodes.
L'une qui s'intéresse au son en référence au pinyin et qui est utilisée en Chine ou au Zhuyin plutôt utilisée à Taïwan.

### Le clavier Zuyin ou Bopomofo

![Le clavier Zuyin ou Bopomofo](zuyin.png)

Il existe cependant d'autres claviers, moins répandus et qui tiennent compte de la forme (il faut donc savoir écrire le caractère chinois avant de le taper sur le clavier).

### Le Wubi (五筆)

![Le clavier Wubi](wubi.png)

### Le Cangjie (倉頡)

![Le clavier Cangjie](cangjie.png)


## Le clavier japonais

![Le clavier japonais](japanese.png)

Le clavier japonais est aussi de type Qwerty.
Vous trouverez même les lettres suivantes inscrites sur le clavier : Q, W, E, R, T, Y.
Sur un Macbook, par exemple, vous trouverez les touches <kbd>tab</kbd>, <kbd>control</kbd>, <kbd>shift</kbd>, <kbd>caps</kbd>, <kbd>options</kbd> et <kbd>command</kbd> à gauche du clavier, et les touches <kbd>delete</kbd>, <kbd>enter</kbd> — ou <kbd>return</kbd> —, à sa droite.
Il existe deux modes sur le clavier japonais : l'alphabet latin ou romaji, le hiragana ou encore Katakana.

Sur la machine, les transcriptions en kanji sont également proposées.
À la différence du Katakana, le hiragana est utilisé pour épeler les mots japonais.
Vous découvrirez les symboles ou lettres suivants sur ce type de clavier : た　て　い　す　か　ん.

Pour passer du kana au kanji, c'est simple grâce à certaines touches particulières du clavier.
Il existe aussi des logiciels permettant de convertir le Kanji en Hiragana.


## Le clavier coréen

![Le clavier coréen](korean.png)

Tout comme le clavier japonais, le clavier coréen fait partie de la famille des claviers de type Qwerty.
Ce clavier est composé de chiffres : 1, 2, 3, 4, 5, 6, 7, 8, 9, 0.
Sur la touche <kbd>2</kbd> figure le symbole de l'arobase (@) ; sur la touche <kbd>3</kbd>, il y a le dièse (#) ; le dollar ($) est représenté sur la touche <kbd>4</kbd>.
Les touches majuscule et verrouillage permettent de déclencher d'autres fonctions ainsi que les symboles sur le clavier.

Vous y découvrirez les symboles suivants typiques de l'alphabet Hangeul :
ᄀ ᄁ ᄂ ᄃ ᄄ ᄅ ᄆ ᄇ ᄈ ᄉ ᄊ ᄋ ᄌ ᄍ ᄎ ᄏ ᄐ ᄑ ᄒ.


## Le clavier hindi

![Le clavier hindi](hindi.png)

C'est le clavier indien.
L'écriture utilisée est le devanagari.
Les chiffres du clavier diffèrent quelque peu des chiffres arabes et se présentent sous cette forme : ० १ २ ३ ४ ५ ६ ७ ८ ९.
Il existe des consonnes, ainsi que des voyelles comme sur l'ensemble des claviers d'ordinateurs à travers le monde.
Vous trouverez par exemple ces symboles : अ आ इ ई उ ऊ क ख ग घ ङ.
Les touches <kbd>Tab</kbd>, <kbd>Caps Lock</kbd>, <kbd>Shift</kbd>, <kbd>Ctrl</kbd> et <kbd>Alt</kbd> se situent à gauche du clavier tandis que les touches <kbd>Enter</kbd> et <kbd>Backspace</kbd> sont situées à droite.
L'esperluette (&) se situe au niveau de la touche <kbd>7</kbd> tandis que l'arobase (@) et le dièse (#) sont respectivement situés sur les touches <kbd>2</kbd> et <kbd>3</kbd>.


## Le clavier Thaï (Thaïlande)

![Le clavier Thaï](thai.png)

Le clavier Thaï est également de type Qwerty et a la même disposition qu'un clavier de type américain.
Les symboles de l'arobase (@), du dièse (#), l'esperluette (&), ainsi que le point d'exclamation (!) sont situées sur les touches <kbd>2</kbd>, <kbd>3</kbd>, <kbd>6</kbd> et <kbd>1</kbd>.

Les chiffres de 0 à 9 sont représentés de cette manière : ๐ ๑ ๒ ๓ ๔ ๕ ๖ ๗ ๘ ๙.

Les touches <kbd>Caps Lock</kbd>, <kbd>Ctrl</kbd> et <kbd>Shift</kbd> sont situées à gauche du clavier, et les touches <kbd>Enter</kbd>, <kbd>Alt Gr</kbd>, <kbd>Menu</kbd>, à sa droite.


## Le clavier arabe (et ses variantes)

![le clavier arabe](arabic.png)

La plupart des claviers du monde arabe sont de type Qwerty.
Mais il existe deux autres sortes de claviers arabes : phonétique et Saint Coran.
Les symboles de l'arobase (@), l'esperluette (&), le point d'exclamation (!) ou encore le dièse (#) figurent respectivement sur les chiffres 2, 7, 1 et 3.

Les touches <kbd>Tab</kbd>, <kbd>Control</kbd>, <kbd>Shift</kbd> sont situées à gauche comme sur la majorité des claviers de type Qwerty, tandis que les touches <kbd>Return</kbd>, <kbd>Delete</kbd> ou <kbd>BS</kbd> — pour _Backspace_ — sont situées à droite.

Voici quelques symboles qui apparaissent sur le clavier arabe pour les lettres H, F, Q, W, N par exemple : ف ق و ن ح


## Le clavier hébreu

![Le clavier hébreu](hebrew.png)

Le clavier hébreu dispose de chiffres arabes : 1, 2, 3, 4, 5, 6, 7, 8, 9, 0.
L'esperluette (&) est située sur le chiffre 1, le symbole dièse (#) sur la touche <kbd>3</kbd> et l'arobase (@) sur le 0.
Vous y découvrirez bon nombre de signes de cantillation et d'autres symboles.
C'est l'exemple de : א ב ד ה ס נ מ.
Ce sont les lettres A, B, D, H, S, N et M.


## Le clavier éthiopien

![Le clavier éthiopien](ethiopian.png)

Figurent sur le clavier éthiopien les symboles de l'alphabet amharique.
De type Qwerty, vous découvrirez sur la partie gauche les touches <kbd>Tab</kbd>, <kbd>Control</kbd>, <kbd>Shift</kbd>, <kbd>Caps</kbd>, <kbd>Option</kbd> et <kbd>Command</kbd>.
Sur la partie droite figurent les touches <kbd>Delete</kbd>, <kbd>Enter</kbd>, <kbd>Return</kbd>.
Sur chaque lettre latine, il y a des lettres issues de l'alphabet éthiopien.


## Conclusion

Il y a presque autant de **claviers d'ordinateurs dans le monde** qu'il y a de langues.
Vous l'aurez compris, chaque pays a ses spécificités dues à la langue utilisée, les symboles, etc.

Quel a été votre clavier préféré ?
Et pourquoi ne pas [apprendre la langue][at-apprendre] liée à ce clavier ?


[at-apprendre]: https://fr.amazingtalker.com/
[clavier-chinois]: https://blog.amazingtalker.com/fr/chinois/46081/
