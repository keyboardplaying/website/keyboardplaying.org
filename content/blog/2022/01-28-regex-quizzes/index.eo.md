---
date: 2022-01-28T07:00:00+01:00
title: Baldaŭ, regex-kvizoj
#subtitle: A witty line as a subtitle
slug: baldau-regex-kvizoj
description: |-
  Nova ludeto okazos ĉe Keyboard Playing, kiel maniero lerni aferojn pri regulaj esprimoj, kiujn vi eble ankoraŭ ne konas.
author: chop
categories: [ software-creation ]
tags: [ regex, news ]
keywords: [ regex, regulaj esprimoj, kvizo ]

references:
- id: wiki-regex
  name: Regula esprimo
  title: true
  url: https://eo.wikipedia.org/wiki/Regula_esprimo
  lang: eo
  author:
    name: Vikipedio
---

Mi tre ŝatas regulajn esprimojn.
Iom tro, eble, sed mi konscias pri tio kaj mi ne tiom uzas ilin.

Tamen mi uzas kelkajn amuzajn ŝablonojn, kaj mi dividos kelkajn el ili ĉi tie por lernado.

<!--more-->


## Pri regulaj esprimoj

Se vi laboras en programarkreado, vi verŝajne jam aŭdis[^fn-sentence]: "Ni nur uzu regex por fari tion!"
Jes, tiel estas: regulaj esprimoj estas mirinda ilo.
Por programistoj, kiuj ĵus malkovras ilin, ofte esas io magia pri ili.
Aŭ repuŝa.

[^fn-sentence]: Se vi ĵus komencis vian karieron, atendu kelkajn monatojn kaj diru al mi, ĉu mi malpravas. :)

Mi estis unu el la fascinitaj, kaj ankoraŭ estas.
Ili havas mirindaj uzojn.
Tamen, kiel vi povas diveni, kiel ĉiufoje kiam programisto entuziasmiĝas pri io nova, ili provas [uzi ĝin ĉie][xkcd-regex], eĉ kiam ĝi ne estas la plej bona solvo por la kunteksto.

Sed mi antaŭiĝas.


### Kio estas regulaj esprimoj?

Tio afiŝo ne estos leciono aŭ lernilo.
Vikipedio difinas ilin tiel:

I won't go make a lesson or tutorial here.
Wikipedia defines them thus:

> **Regula esprimo** (angle _regular expression_ aŭ mallonge _regexp_) en komputiko estas ĉeno kiu kongruas kun serio da ĉenoj laŭ iuj sintaksaj reguloj.
> Regulaj esprimoj estas uzataj en multaj tekstoredaktiloj kaj utilas por serĉi kaj redakti pecon da teksto laŭ ia ŝablona rekono.
> {{< ref-cite "wiki-regex" >}}


### Kion vi diris?

Bone, jen mallonga ekzemplo.
Se vi vere ne scias pri ili, pluraj retejoj havas tre bonajn enkondukojn aŭ lecionojn.

Ni prenu la ŝablono `^(Hello|Bonjour|Saluton)`.
Ĝi rekonas ĉu ĉeno komenciĝas (`^`) per "Hello", "Bonjour" aŭ "Saluton".
Se estas vere, la regex ankaŭ kaptos tiun vorton en _grupo_.
Do, tiu regex rekonus "Hello, World!" (la grupo enhavus "Hello"), sed ne "Hey World! Hello!"

Vi kompreneble povus adapti la ŝablono por testi nur se la ĉeno _enhavas_ tiujn vortojn: `(Hello|Bonjour|Saluton)` rekonus ambaŭ ĉenojn.

Mi haltu ĉi tie.
Mi eble enkondukos aliajn konceptojn estonte, por klarigi la regex, kiujn mi dividos.


### Kion mi riproĉas regulaj esprimoj

Nu, fakte nenio.
Estas programistoj, kiujn uzas ilin, eĉ kiam ili ne estas la plej bona vojo.

Vidu, la problemo kun regex estas, ke ili estas kiel alia programlingvo.
Unu kiu ne estas evidenta por ĉiuj.
Kompreneble, tiuj, kiuj skribas kompleksajn ŝablonojn, malofte prenas la tempon por dokumenti ilin.
Do, uzi regulaj esprimojn reduktas la legeblecon de la kodo.

Estas ankaŭ rendimentaj kaj sekurecaj problemoj, depende de la programlingvo kaj de la ŝablono.

Esence, limigu viajn uzojn de la regulaj esprimoj al simplaj kazoj de eniga validigo, serĉo, aŭ serĉo kaj anstataŭigo.
Tiuj estas la kazoj en kiuj regulaj esprimoj elstaras.

Kaj se via ŝablono fariĝas tro komplika/rekursiva/nelegebla, eble tio estas signo, ke vi devus serĉi alian solvon.


## Pri tiuj kvizoj

Mi renkontis iujn amuzajn regex.
Eĉ kelkajn mi mem skribis!

Mi ŝatas ilin, eĉ se mi evitas uzi ilin.
En multaj okazoj, mi pensis "ĉu mi povus skribi ŝablonon por fari tion?"
Ofte, jes, sed la solvoj estis malfacile legeblaj.
Aŭ ili estis la okazo malkovri iun koncepton pri regex, kiu mi ankoraŭ ne konis.

La kvizoj estos bazitaj sur tiaj esprimoj.
Mi donos la ŝablono unu tagon antaŭe en [Twitter][tw-kp].

Poste, vendrede, mi klarigos ĝin en afiŝo.
La regex estos ĉe la supro, antaŭ ĉio alia.
Vi tiel havos ŝancon kompreni ĝin antaŭ legi la klarigon se vi ne povis vidi ĝin en Twitter.

Tiuj ne tro ofte okazos, ĉar mi ne faras ilin por enuigi vin.
Unufoje ĉiu du monatoj ŝajnas bona ritmo.
Mi esperas, ke vi ĝuos tiun formaton.

Ĝis post du monatoj por la unua kvizo!

[xkcd-regex]: https://xkcd.com/208/
[tw-kp]: https://twitter.com/KeyboardPlaying
