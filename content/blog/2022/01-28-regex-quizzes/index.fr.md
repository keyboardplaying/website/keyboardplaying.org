---
date: 2022-01-28T07:00:00+01:00
title: Prochainement, les quiz Regex
#subtitle: A witty line as a subtitle
slug: prochainement-quiz-regex
description: |-
  Un nouveau petit jeu va arriver sur Keyboard Playing, comme moyen d'apprendre des choses que vous ne connaissez peut-être pas encore sur les regex.
author: chop
categories: [ software-creation ]
tags: [ regex, news ]
keywords: [ regex, expressions régulières, expressions rationnelles, quiz ]

references:
- id: wiki-regex
  name: Expression régulière
  title: true
  url: https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re
  lang: fr
  author:
    name: Wikipédia
---

J'aime beaucoup les expressions régulières.
Sans doute un peu trop,  mais j'en suis conscient et je les utilise somme toute assez peu.

Cela ne m'empêche pas d'essayer quelques motifs amusants, et j'en partagerai quelques-uns ici pour le plaisir d'apprendre.

<!--more-->


## Au sujet des expressions régulières[^fn-traduction]

[^fn-traduction]: En écrivant ces lignes, j'imagine Innamincka soupirer et essayer de me dire télépathiquement qu'« expressions rationnelles » est la traduction correcte, mais je vais retenir le phrasé que la plupart des développeur·euse·s comprendront du premier coup.

Si vous travaillez dans la création logicielle, vous avez certainement déjà entendu la phrase[^fn-sentence] : « Une regex sera parfaite pour ça ! »
Oui, ça se passe comme ça : les expressions régulières sont un outil formidable.
Pour les développeur·euse·s qui viennent de le découvrir, elles ont souvent quelque chose de magique.
Ou de repoussant.

[^fn-sentence]: Si vous venez de démarrer votre carrière, attendez quelques mois avant de me dire que vous ne l'avez jamais entendue. :)

Je faisais partie des fascinés, et le suis toujours.
Elles ont des applications merveilleuses.
Toutefois, comme vous pouvez vous en douter, comme à chaque fois qu'un·e codeur·euse s'enthousiasme pour quelque chose de nouveau, il ou elle essaie de [l'utiliser à toutes les sauces][xkcd-regex], même quand ce n'est pas la meilleure solution pour le contexte.

Mais je m'emballe.


### Que sont les expressions régulières ?

Ce billet ne sera pas le lieu d'un cours magistral.
Wikipédia les définit ainsi :

> En informatique, une **expression régulière** ou **expression rationnelle** ou **expression normale** ou **motif**, est une chaîne de caractères, qui décrit, selon une syntaxe précise, un ensemble de chaînes de caractères possibles. Les expressions régulières sont également appelées **_regex_** (un mot-valise formé depuis l'anglais _regular expression_).
> [...]
> Les expressions régulières sont aujourd’hui utilisées pour programmer des logiciels avec des fonctionnalités de lecture, de contrôle, de modification, et d'analyse de textes ainsi que dans la manipulation des langues formelles que sont les langages informatiques. 
> {{< ref-cite "wiki-regex" >}}


### Kékidi ?

Certes, ce n'est pas clair.
Allons-y pour un bref exemple.
Pour une véritable leçon ou un tutoriel, de nombreux sites seront plus complets et vous présenteront mieux le concept.

Prenons le motif `^(Hello|Bonjour|Saluton)`.
Il peut être utilisé pour détecter si une chaîne de caractère commence (`^`) « Hello », « Bonjour » ou « Saluton ».
Le cas échéant, la regex capturera ce premier mot dans un _groupe_.
Ainsi, « Hello, World! » serait validé par ce motif (le groupe contiendrait `Hello`), mais « Hey World! Hello! » ne le serait pas.

Il suffirait d'une adaptation mineure afin de seulement tester si la chaîne _contient_ ces mots : `(Hello|Bonjour|Saluton)` validerait les deux chaînes de caractères.

Comme écrit plus haut, je n'irai pas trop loin dans les détails.
Il se pourrait que j'introduise certains concepts à l'avenir, pour expliquer les expressions que je partage.


### Ce que je reproche aux expressions régulières

Je ne leur reproche rien.
C'est aux développeur·euse·s que je reproche leur usage inapproprié lorsque de meilleures solutions existent.

Le problème avec les regex est qu'elles sont comme un autre langage.
Un qui n'est pas évident pour tout le monde.
Bien entendu, ceux qui écrivent des motifs un peu recherchés prennent rarement le temps de les documenter ou expliquer.
C'est ainsi qu'un outil devient une pénalité pour la lisibilité et donc pour la maintenabilité.

Elles ont également des problèmes de performance[^fn-perf-java-regex] et de sécurité, qui dépendent du langage et de la complexité du motif utilisé.

[^fn-perf-java-regex]: En école d'ingénieur, un professeur voulait améliorer les performances de Java en matières de regex et proposer sa solution à Sun.
Je n'ai jamais su si son projet avait abouti.

Pour éviter ces questions, je recommande de limiter l'utilisation des expressions régulières aux cas simples de validation des données utilisateur, de recherche, ou de recherche et remplacement.
Ce sont les cas pour lesquels les regex excellent.

Et si votre motif devient trop compliqué/récursif/illisible, c'est certainement un signe que vous devriez réfléchir à une autre solution.


## Et donc, ces quiz ?

Donc oui, j'ai croisé des motifs assez drôles au fil de ma carrière.
J'en ai même écrit quelques-uns moi-même.

Même si je fais de mon mieux pour les éviter, j'aime la syntaxe des expressions régulières.
Il m'est plusieurs fois arrivé de me demander « est-ce que je peux faire ça avec une regex ? »
La réponse était généralement « oui », mais à chaque fois que je me suis posé la question, le motif obtenu était assez… moche.
Toutefois, c'était généralement l'occasion de découvrir une nouvelle notation ou un nouveau concept.

Les quiz seront basés sur ces expériences.
Je communiquerai la regex [sur Twitter][tw-kp] la veille.

Puis, le vendredi, je la partagerai dans un billet.
On aura au sommet la regex pour celles et ceux qui n'auraient pas pu la voir sur Twitter.
Les explications seront sous le motif.

J'espacerai les quiz, pour ne pas vous lasser trop vite (et parce que ma réserve n'est pas monstrueuse non plus).
Un quiz tous les deux mois semble raisonnable.
J'espère que le format vous plaira.

À dans deux mois pour le premier quiz.

[xkcd-regex]: https://xkcd.com/208/
[tw-kp]: https://twitter.com/KeyboardPlaying
