---
date: 2022-04-08T07:00:00+02:00
title: Au sujet de Lombok
subtitle: Il y a une annotation pour ça !
slug: lombok
description: |-
  Java a la réputation d'être un des langages de programmation les plus verbeux.
  En particulier, il nécessite souvent du code dit « _boilerplate_ » (constructeurs, accesseurs…).
  Lombok est une solution pour vous aider à réduire cette verbosité.
author: chop
categories: [ software-creation ]
tags: [ java, programming ]
keywords: [ Java, Lombok, annotations, code boilerplate, accesseurs, constructeurs, Maven, IDE, logging, immuable ]

references:
- id: lombok
  name: Project Lombok
  title: false
  url: https://projectlombok.org/
  lang: en
---

Il y a quelques mois, notre équipe a accueilli un nouveau venu.
En plus de notre mission, il découvrait aussi Java, après quelques années en tant que développeur .NET.
Parmi ses premières réactions, il y a eu plusieurs « Quoi, tu dois faire ça toi-même, en Java ? En .NET, c'est le compilateur qui le fait pour toi ! »

Il est vrai qu'en Java, le langage et les conventions amènent une quantité non négligeable de code à faible valeur ajoutée.
Mais ne craignez pas !
Le chevalier Lombok vient à votre rescousse.

<!--more-->


**Ce billet n'est pas un tutoriel**, il s'agit simplement d'une présentation rapide de ce que Lombok peut faire pour vous.


## L'objectif

### Qu'est-ce que le code _boilerplate_ ?

Imaginez-vous écrire une classe Java, en respectant toutes les bonnes pratiques.
Chaque champ doit être privé et être exposé à travers des accesseurs.

Cela signifie qu'à chaque fois que vous ajoutez un champ, vous devez déclarer de nouveaux _getters_ et _setters_.
Si votre objet est immuable, ils doivent tous être initialisés à l'instanciation.
Ceci implique que votre constructeur doit leur fournir une valeur et, en cas de nouveaux champs, vous devrez le mettre à jour également.

C'est _ça_, le code _boilerplate_.
Il embarque très peu de logique, mais vous devez l'écrire pour lier votre tout et respecter les recommandations.
C'est un code frustrant, parce qu'on a souvent l'impression qu'une machine pourrait le faire à notre place (et vous pouvez certainement le générer avec quelques clics ou raccourcis dans votre IDE).

Quand vous venez d'autres langages où tout est implicite ou simplement déclaratif (comme TypeScript ou C#), prendre le temps d'explicitement taper ce code ressemble à une relique d'un autre temps.
Cela paraît obsolète, ou tout au mieux pénible.

C'est pour lutter contre ceci que Lombok a été créé.


### Quelle solution propose Lombok ?

<aside><p>Lombok remplace les annotations par du code, au moment de la compilation.</p></aside>

Lombok gravite autour des annotations : au lieu d'écrire le _boilerplate_ vous-même, vous déclarez ce que vous voulez avoir dans votre classe.
Par exemple, si vous avez besoin d'un constructeur qui prend un argument par champ, vous annotez votre classe avec `@AllArgsConstructor`.
Je vous donne quelques autres exemples [plus bas](#exemples).

Mais qu'arrive-t-il à ces annotations ?
On a pris l'habitude qu'elles soient utilisées à l'exécution pour personnaliser le comportement, injecter un aspect ou autre fonctionnalité de ce style (Spring, Jackson, merci pour votre aide).
Lombok est cependant différent.

En sus de fournir une collection d'annotations, Lombok est également un processeur d'annotations : s'il est sur le _path_ de votre compilateur Java, il analysera votre code annoté au moment de la compilation et générera les classes en respectant vos indications.
Grâce à cela, il vous apporte du confort au moment de l'écriture du code, mais n'a aucun impact sur les performances de votre application puisque la réflexion ne sera pas utilisée.[^fn-reflexion]

[^fn-reflexion]: Dans le pire des cas, l'impact sera sur la durée de compilation, mais à moins d'avoir un projet vraiment imposant et une utilisation massive de Lombok, je ne suis pas certain que vous puissiez faire la différence.


## Par où commencer ?

Je préfère insister : **ce billet n'est pas un tutoriel**.
Je ne fais que gratter la surface afin de vous indiquer ce que vous pourrez découvrir si vous creusez plus profondément.

Pour travailler avec Lombok, vous aurez besoin de :

* une dépendance Maven ;
* un plugin pour votre IDE.

C'est parti, entrons dans la matrice.


### Configuration Maven

Rien de compliqué, vous allez voir.
Dans votre POM, ajoutez la dépendance :

```xml
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.18.22</version>
  <scope>provided</scope>
</dependency>
```

C'est fait !
Cela suffit dans la plupart des cas.


#### Un possible piège

Il y a une configuration supplémentaire dans deux cas :
* si vous utilisez JDK9+ avec un module-info.java ;
* ou si vous avez déjà configuré un processeur d'annotations[^fn-annot-proc].

[^fn-annot-proc]: Normalement, le Maven Compiler Plugin détecte les processeurs présents parmi les dépendances.
Cependant, si des processeurs ont été manuellement configurés, cette détection est désactivée.

Si vous êtes dans cette situation, vous devez [configurer la balise `annotationProcessorPaths` du Maven Compiler Plugin](https://maven.apache.org/plugins/maven-compiler-plugin/compile-mojo.html#annotationProcessorPaths).
Cela ressemble à ça :

```xml
<annotationProcessorPaths>
	<path>
		<groupId>org.projectlombok</groupId>
		<artifactId>lombok</artifactId>
		<version>1.18.22</version>
	</path>
</annotationProcessorPaths>
```

C'est tout bon !


#### Une remarque sur le _scope_ de la dépendance

Vous avez peut-être remarqué que j'ai mis la dépendance dans le scope `provided`.
En résumé, c'est une façon de dire à Maven : « Je n'ai pas besoin que cette dépendance soit incluse à l'exécution. »
Vous avez besoin des annotations lorsque vous compilez, mais elles sont éliminées de la classe.
Il est donc inutile qu'elles soient chargées sur le classpath puisqu'elles ne seront jamais appelées.

**Si vous utilisez le fat jar de Spring Boot** (et probablement d'autres mécanismes similaires), **le scope `provided` est ignoré**.
Dans ce cas, vous pouvez vous appuyer sur la balise `<optional>true</optional>` pour indiquer de ne pas embarquer la dépendance.

```xml
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.18.22</version>
  <scope>provided</scope>
  <optional>true</optional>
</dependency>
```


### Le plugin de l'IDE

Votre IDE compile généralement vos classes à chaque fois que vous les modifiez, mais en s'appuyant généralement sur ses propres réglages et en se moquant bien des processeurs d'annotations, à moins d'installer les extensions qui vont bien.
Avec Lombok, cela implique un éditeur qui va passer son temps à se plaindre qu'il ne connaît pas le _getter_ que vous invoquez.

Lombok fournit les plugins, que vous utilisiez [un IDE basé sur Eclipse](https://www.projectlombok.org/setup/eclipse), [un produit IntelliJ](https://www.projectlombok.org/setup/intellij), [Netbeans](https://www.projectlombok.org/setup/netbeans) ou un [éditeur compatible avec Visual Studio Code](https://www.projectlombok.org/setup/vscode).
Ouvrez simplement la page _ad hoc_ sur [le site de Lombok][lombok] (sous le menu « _Install_ ») et découvrez la méthode d'installation pour votre outil.


## Quelques exemples d'annotations Lombok {#exemples}

Tout est en place.
Par quoi commencer ?

Je propose de voir les basiques : les accesseurs.


### Accesseurs

Imaginons que nous écrivons un DTO pour laquelle nous n'avons pas envie d'écrire explicitement _getters_ et _setters_.
Il nous suffit d'annoter la classe (ou les champs concernés) avec [`@Getter` and `@Setter`][lombok-getter-setter], et le travail de développement est terminé !

Par défaut, les accesseurs seront `public`, mais il est possible de surcharger ce réglage.

```java
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book {
    private String title;
    private String author;

    @Getter(access = AccessLevel.NONE)
    private String somePrivateFieldThatMustNotHaveAGetter;
}
```

Bien moins verbeux que nos POJO habituels, non ?


### Constructeurs

Il vous faut un constructeur pour instancier votre classe.
Lombok met à votre disposition [trois annotations][lombok-constructor] pour ce but :

* `@NoArgsConstructor` est explicite.
* `@AllArgsConstructor` l'est aussi.
* `@RequiredArgsConstructor` crée un constructeur avec un paramètre pour chaque champ qui doit être initialisé à l'instanciation (p.ex. les champs `final`).

Comme c'est le cas pour les accesseurs, vous pouvez modifier la visibilité via le paramètre `access`.

```java
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Book {
    private String title;
    private String author;
}
```

Et là, le petit bonheur : vous n'avez plus besoin de mettre à jour votre constructeur à chaque fois que vous modifiez les champs.
Tout ça se fera automatiquement ![^fn-refactor-calls]

[^fn-refactor-calls]: Bien entendu, il faudra adapter les appels à ce constructeur, mais le compilateur sera capable de vous indiquer rapidement toutes les invocations maintenant invalides.


### _Builders_

Avec le temps, j'ai de plus en plus tendance à faire des objets immuables.
Toutefois, l'initialisation d'un POJO immuable comportant de nombreux champs est pénible et le constructeur prenant tous les arguments perd souvent rapidement en clarté.
Pour ces raisons, j'adore le pattern _builder_.

Et [Lombok a une annotation pour ça aussi][lombok-builder] !

Je peux donc modifier ma classe `Book` ainsi :

```java
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Book {
    private String title;
    private String author;
}
```

Et au lieu d'un `new`, j'utiliserai le code suivant pour initialiser une instance :

```java
Book.builder()
    .title("Roverandom")
    .author("John Ronald Reuel Tolkien")
    .build();
```

Si vous avez déjà écrit un _builder_ vous-même, vous devez apprécier le temps économisé grâce à une petite annotation.


### Bien, bien plus encore

Nous arrêterons ici les exemples car je crains de vous ennuyer.
Sachez simplement que Lombok offre également des annotations pour générer vos méthodes [`equals()` et `hashcode()`][lombok-equals], vos [`toString()`][lombok-tostring], ou même [ajouter un champ `log`](https://projectlombok.org/features/log) de votre [framework de logging][about-logging] préféré.

Sachez aussi que vous pouvez modifier certains comportements par défaut via un fichier `lombok.config` à placer à la racine de votre projet, juste à côté de votre `pom.xml`.


## Pour aller plus loin

Bien entendu, vous pourrez trouver de nombreux tutoriels sur Lombok, mais je vous invite chaudement à regarder [la documentation officielle][lombok-doc].
Elle est bien faite, chaque annotation est détaillée avec un exemple, le code Java « vanilla » équivalent, et les configurations possibles.

Et comme pour tout nouvel outil, un peu d'expérimentation vous aidera à le prendre plus rapidement en main.



{{% references %}}

[lombok-getter-setter]: https://projectlombok.org/features/GetterSetter
[lombok-constructor]: https://projectlombok.org/features/constructor
[lombok-builder]: https://projectlombok.org/features/Builder
[lombok-equals]: https://projectlombok.org/features/EqualsAndHashCode
[lombok-tostring]: https://projectlombok.org/features/ToString
[lombok-doc]: https://projectlombok.org/features/all

[about-logging]: {{< relref path="/blog/2021/04-12-about-logging" >}}
