---
date: 2022-04-08T07:00:00+02:00
title: Pri Lombok
subtitle: Estas prinoto por tio!
slug: lombok
description: |-
  Java laŭdire estas unu el la plej multvortaj programlingvoj.
  Aparte, ĝi ofte postulas "_boilerplate_" kodon (konstruilo, aliriloj…).
  Lombok estas solvo por helpi vin redukti tiun vortecon.
author: chop
categories: [ software-creation ]
tags: [ java, programming ]
keywords: [ Java, Lombok, prinotoj, boilerplate kodon, aliriloj, konstruilo, Maven, IDE, protokolado, neŝanĝebla ]

references:
- id: lombok
  name: Project Lombok
  title: false
  url: https://projectlombok.org/
  lang: en
---

Kelkajn monatojn antaŭe, nia teamo bonvenigis novulon.
Aldone al nia misio, li ankaŭ malkovris Java, post kelkaj jaroj kiel .NET-programisto.
Inter iliaj unuaj reagoj, estis pluraj "Kio, vi devas fari ĝin vi mem, en Java? En .NET, estas la tradukilo, kiu faras ĝin por vi!"

Vere, en Java, la lingvo kaj konvencioj kondukas al signifa kvanto de malaltvalora kodo.
Sed ne timu!
Lombok celas simpligi tiun taskon por vi.

<!--more-->

**Ĉi tiu afiŝo ne estas lernilo**, sed simple rapida enkonduko pri tio, kion Lombok povas fari por vi.


## La celo

### Kio estas _boilerplate_ kodo?

Imagu skribi Java klason, respektante ĉiujn bonajn praktikojn.
Ĉiu kampo devas esti `private` kaj alirebla per… nu, aliriloj.

Ĝi signifas, ke ĉiufoje vi aldonas ckampon, vi devas deklari novajn _getters_ kaj _setters_.
Se via objekto estas neŝanĝebla, ĉiuj kampoj devas esti inicialigitaj al la generado.
Tio implicas, ke via konstruilo devas provizi ilin per valoro kaj novaj kampoj devas esti aldonitaj al la argumentoj.

_Tio_ estas _boilerplate_ kodo.
Estas kodo en ĉiuj projektoj, kun malmulte da logiko, frustrante da skribi ĉar oni ofte sentas, ke maŝino povus fari ĝin anstataŭe al homo (fakte, la plimulto de la programadaj medioj povas generi ĝin per kelkaj klakoj aŭ klavarokomandoj).

Kiam oni konas aliajn lingvojn, kie ĉio estas implicita aŭ simple deklaracia (kiel TyepScript aŭ C#), eksplicite tajpi ĉi tiun kodon aspektas kiel relikvo el alia tempo.
Ĝi ŝajnas malaktuala, aŭ almenaŭ dolora.


### Kiel Lombok solvas tion?

<aside><p>Lombok anstataŭigas prinotojn per kodo, je programtradukado.</p></aside>

Lombok rondiras cirkaŭ prinotoj: anstataŭ tajpi mem la _boilerplate_ kodo, vi deklaras tion, kion vi volas have en via klaso.
Ekzemple, se vi bezonas konstruilon kiu prenas unu argumenton per kampo, vi prinotas vian klason per `@AllArgsConstructor`.
Mi donos kelkajn aliajn ekzemplojn [malsupre](#ekzemploj).

Sed kio okazas al ĉi tiuj prinotoj?
Ni alkutimiĝis, ke ili estas uzataj ĉe rultempo por personecigi la konduton, injekti aspekton aŭ alian helpon por malkovri la klason (Spring, Jackson, dankon pro via helpo).
Lomnok tamen estas malsama.

Krom provizi kolekton de prinotoj, Lombok ankaŭ estas prinotprocesoro: se ĝi estas sur la klasvojo de via Java kompililo, ĝi analizos la prinotan kodon je kompiltempo kaj generos klasojn laŭ viaj indikoj.
Dank'al tio, tajpi la kodon estas pli komforta, sed estas neniun efikon al la rendimento de via aplikaĵo ĉar reflektado ne estos uzata tiam.[^fn-reflektado]

[^fn-reflektado]: En la plej malbona kazo, la efiko estos sur la kompiltempo, sed krom se vi havas vere grandan projekton kaj multe uzas Lombok, mi ne certas, ke vi povas fari la diferencon.


## Kie komenci?

Mi preferas insisti: **tio afiŝo ne estas lernilo**.
Mi nur montras al vi tion, kion vi povus malkovri, se vi pli studos Lombok.

Por labori kun Lombok vi bezonos:

* Maven dependeco;
* etendaĵo por via programa medio.


### Agordo de Maven

Ne estas komplika, vi vidos.
En via POM, aldonu la dependecon:

```xml
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.18.22</version>
  <scope>provided</scope>
</dependency>
```

Ĉi tio estas ĉio!
En le plimulto de la kazoj, ĝi sufiĉas.


#### Esceptoj

Vi bezonos plian agordon en du kazoj:
* se vi uzas JDK9+ kun module-info.java;
* aŭ se vi jam agordis prinotprocesoron[^fn-prinotprocesoro].

[^fn-prinotprocesoro]: Normale, Maven Compiler Plugin aŭtomate detektas la prinotprocesorojn el la dependecojn.
Tamen, se procesorojn estis mane agorditaj, tiu detekto estas malŝaltita.

Se vi estas en ĉi tiu situacio, vi devas [agordi la etikedoj `annotationProcessorPaths` de Maven Compiler Plugin](https://maven.apache.org/plugins/maven-compiler-plugin/compile-mojo.html#annotationProcessorPaths).
Ĝi aspektas jene:

```xml
<annotationProcessorPaths>
	<path>
		<groupId>org.projectlombok</groupId>
		<artifactId>lombok</artifactId>
		<version>1.18.22</version>
	</path>
</annotationProcessorPaths>
```

Ĉio estas bona!


#### Noto pri la _scope_ de la dependeco

Vi eble rimarkis, ke mi metis la dependecon en la scope `provided`.
Ĝi estas maniero diri al Maven, "Mi ne bezonas ĉi tiun dependecon inkluzivita ĉe rultempo."
Vi bezonas la prinotojn por la programtradukado, sed ili estas forigitaj de la klaso tiam.

**Se vi uzas la _fat jar_ de Spring Boot** (kaj verŝajne aliajn similajn mekanismojn), **la _scope_ `provided` estas ignorita**.
En ĉi tiu kazo, vi povas uzi la etikedon `<optional>true</optional>` por indiki ne enigi la dependecon.

```xml
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.18.22</version>
  <scope>provided</scope>
  <optional>true</optional>
</dependency>
```


### La etendaĵo de la programa medio

La programaj medoij kutime kompilas la klasojn ĉiufoje kiam oni modifas ilin, sed ili uzas siajn proprajn agordojn por tio kaj ne zorgas pri prinotprocesoroj, krom se vi instalis la ĝustajn etendaĵojn.
Por Lombok, la medio avertos, ke ĝi ne konas la la _getter_ kiun vi alvokas.

Lombok provizas la etendaĵojn por [Eclipse-bazita medioj](https://www.projectlombok.org/setup/eclipse), [IntelliJ produktoj](https://www.projectlombok.org/setup/intellij), [Netbeans](https://www.projectlombok.org/setup/netbeans) aŭ [redaktilo kongrua kun Visual Studio Code](https://www.projectlombok.org/setup/vscode).
Nur retumu al la adekvata paĝon en [la retejo de Lombok][lombok] (en la menuo « _Install_ ») kaj eltrovu la instalan metodon por via ilo.


## Kelkajn ekzemploj de Lombok prinotoj {#ekzemploj}

Ĉio estas en loko.
Per kio komenci?

Mi proponas vidi la bazaĵojn: aliriloj.


### Aliriloj

Ni imagu, ke ni tajpas DTO, kaj ni ne volas ekspliciti _getters_ kaj _setters_.
Ni nur bezonas prinoti la klason (aŭ kampojn) per [`@Getter` kaj `@Setter`][lombok-getter-setter], kaj la programado estas farita!

Defaŭlte, aliriloj estos `public`, sed eblas anstataŭi ĉi tiun agordon.

```java
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book {
    private String title;
    private String author;

    @Getter(access = AccessLevel.NONE)
    private String somePrivateFieldThatMustNotHaveAGetter;
}
```

Malpli multvorta ol niaj regulaj POJO-oj, ĉu ne?


### Konstruiloj

Vi bezonas konstruilon por krei vian klason.
Lombok disponigas [tri prinotojn][lombok-constructor] por tiu celo:

* `@NoArgsConstructor` estas memklarigebla.
* `@AllArgsConstructor` ankaŭ estas.
* `@RequiredArgsConstructor` kreas konstruilon kun argumento por ĉiu kampo, kiu devas esti pravigita ĉe instiigo (ekz. `final` kampoj).

Kiel ĉe aliriloj, vi povas ŝanĝi la videblecon per la parametro `access`.

```java
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Book {
    private String title;
    private String author;
}
```

Kaj tie, la bonŝanca ĉarmo: vi ne plu bezonas ĝisdatigi vian konstruilon ĉiufoje kiam vi modifas la kampojn.
Ĉi tio ĉio okazos aŭtomate![^fn-refactor-calls]

[^fn-refactor-calls]: Kompreneble, alvokoj al tio ĉi konstruilo devos esti adaptitaj, sed la kompililo povos rapide informi vin pri iuj alvokoj, kiuj nun estas nevalidaj.


### _Builders_

Kun la tempo, mi emas fari neŝanĝeblajn objektojn.
Tamen, krei neŝanĝeblan POJO kun multaj kampoj estas doloro, kaj la kosntruilo kun ĉiuj argumentoj ofte rapide perdas klarecon.
Pro ĉi tiuj kialoj, mi amas la ŝablono _builder_.

Kaj [Lombok havas prinoton por tio ankaŭ][lombok-builder]!

Mi do povas ŝanĝi mia `Book` klason tiel:

```java
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Book {
    private String title;
    private String author;
}
```

Kaj anstataŭ uzi `new`, mi kreos instanco kun la sekva kodo:

```java
Book.builder()
    .title("Roverandom")
    .author("John Ronald Reuel Tolkien")
    .build();
```

Se vi iam tajpis _builder_ mem, vi devas aprezi la ŝparitan tempon per eta prinoto.


### Pli, multe pli

Ni ĉesos la ekzemplojn ĉi ti.
Mi ne volas enuigi vin.
Nur konsciu, ke Lombok ankaŭ provigas prinotojn por generi viajn [`equals()` kaj `hashcode()`][lombok-equals], viaj [`toString()`][lombok-tostring], aŭ eĉ [aldoni `log` kampon](https://projectlombok.org/features/log) de via plej ŝatata [registrada framo][about-logging].

Rimarku ankaŭ, ke vi povas modifi iujn defaŭltajn kondutojn per `lombok.config` dosiero ĉe la radiko de via projekto, apud via `pom.xml`.


## Por plu

Komprenenble, vi povas trovi multajn lernilojn pri Lombok, sed mi kore invitas vin rigardi [la oficialan dokumentaron][lombok-doc].
Ĝi estas bone, ĉiu pronoto estas detala kun ekzemplo, la ekvivalenta "vanila" Java kodo kaj eblaj agordoj.

Kaj kiel kun ajna nova ilo, iom da eksperimentado helpos vin lerni ĝin pli rapide.



{{% references %}}

[lombok-getter-setter]: https://projectlombok.org/features/GetterSetter
[lombok-constructor]: https://projectlombok.org/features/constructor
[lombok-builder]: https://projectlombok.org/features/Builder
[lombok-equals]: https://projectlombok.org/features/EqualsAndHashCode
[lombok-tostring]: https://projectlombok.org/features/ToString
[lombok-doc]: https://projectlombok.org/features/all

[about-logging]: {{< relref path="/blog/2021/04-12-about-logging" lang="en" >}}
