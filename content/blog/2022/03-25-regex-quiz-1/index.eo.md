---
date: 2022-03-25T07:00:00+01:00
title: Regex-kvizo \#1
subtitle: Kaj estis koloro.
slug: regex-kvizo-1
description: |-
  Jen la solvo al la regex-kvizo #1.Here is the solution to the regex quiz #1.
  This was inspired from a need to have color tags in a basic editor.
author: chop
categories: [ software-creation ]
tags: [ regex ]
keywords: [ regex, kvizo, koloro, etikedojn ]
---

Ni nur respondas la [Regex-kvizo #1][quiz-tweet].
La regex estis:

```
\[(#[a-fA-F0-9]{3,6}|[a-zA-Z]+)\](.+)\[\/\1\]
```

Por kio ĝi povas esti uzata?
Jen kion ni klarigos en ĉi tiu afiŝo.

<!--more-->

## Ni disekcu la regex

La kvizo enhavis indicon: oni bezonas `Matcher` por ekspluati ĝin, do verŝajne ĝi ne nur testos ĉu la enigo kongruas kun ĝi.[^fn-quiz-java]

[^fn-quiz-java]: La kvizo ankaŭ enhavis eraron.
Kiam oni uzas Java, oni devas eskapi la eskapoj, kio malfaciligas legi la regex.
Por koncentriĝi pri la regex en si mem, mi evitos Java en estontaj kvizoj.

![La (korektita) kvizo](regex-color-pipe.svg)

Ni koncentriĝu pri la regex.


### Eskapi Krampojn

Unue, ni rimarkas, ke la esprimo estas malagrabla legebla pro multaj eskapoj, precipe por krampoj: `[` kaj `]` estas antaŭtitaj per `\`, kio signifas, ke ili ne havas sian kutiman rolon en regex.
Anstataŭe, ili estas parto de la teksto, kiun ni serĉos, kiam ni provas la regex sur enigo.[^fn-escape-closing]

[^fn-escape-closing]: Vi povus ne eskapi la fermante `]` por fari ĝin iom pli facile legi.
`\[(#[a-fA-F0-9]{3,6}|[a-zA-Z]+)](.+)\[\/\1]` estas do ekvivalenta.


### Kaptante grupojn

Vi povas vidi du kaptante grupojn:

* Grupo #1 estas `(#[a-fA-F0-9]{3,6}|[a-zA-Z]+)`.
Ĝi kaptos:
  * aŭ `#` sekvata de deksesuma ĉeno de tri ĝis ses signoj;
  * aŭ ĉeno de literoj.
* Grupo #2 estas `(.+)`.
Ĝi kaptos ĉion inter `]` kaj `[`.


### Reuzo de kaptitaj grupoj

Estas unu apartaĵo, kiu eble ne estas evidenta, sed fundamenta por kompreni ĉi tiun regex: `\1` certigos, ke la regex kongruas nur se la teksto en ĉi tiu loko estas la sama kiel tio estis kaptita de grupo #1.

Ekzemple, se la grupo 1 kaptis `cyan`, tiam la regex kongruus nur se la teksto en la loko de la `\1` ankaŭ estas `cyan`.
Tio signifus, ke la enigo komenciĝus per `[cyan]` kaj finiĝas per `[/cyan]`.
Jes, tio estas etikedsistemo.


## Bilanco

Do, por resumi, ni havas:

* etikedsistemo;
* etikedoj kiuj enhavas HTML-koloron, aŭ en deksesuma formo aŭ kolornomo;
* enhavo.

Jes, tio estas sistemo por kolori la teksto, por anstataŭigi `[red]This is important.[/red]` per `<span style="color: red">This is important.</red>`.
Rapida kodo por fari ĝin estus:


```java
final String regex = "\\[(#[a-fA-F0-9]{3,6}|[a-zA-Z]+)\\](.+)\\[\\/\\1\\]";
final String input = "[red]This is important.[/red]";

Pattern pattern = Pattern.compile(regex);
Matcher matcher = pattern.matcher(input);

if (matcher.find()) {
    String output = String.format(
        "<span style=\"color: %s\">%s</span>",
        matcher.group(1),
        matcher.group(2)
    );
    System.out.println(output);
} else {
    System.out.println(input);
}
```


## La rakonto

Kiel vi povas supozi, niaj uzantoj devis povi aldonu koloron al sia teksto, uzante redaktilon kiu manipulis nur neprilaboritan tekston.
phpBB-similaj etikedoj ŝajnis sufiĉe facila solvo, kaj regex estis rapida maniero analizi ĝin.

Ĝi estas baza uzado, sed ĝi estas bela ekzemplo de `\1` por kapti kongruajn etikedojn.
En nia kazo, ĉi tio ebligas nestumon de etikedoj se necese (`[red]This is [#ff9900]very[/#ff9900] important.[/red]`), sed estas multaj aliak kazoj, kie ĉi tio povus esti same grava.
Ni vidos alian uzon de tiu lertaĵo en nia sekva regex-kvizo, post du monatoj.


[quiz-tweet]: https://twitter.com/KeyboardPlaying/status/1506882385496915970
