---
date: 2022-04-29T07:00:00+02:00
title: Spring's Constructor-Based Dependency Injection with Lombok
subtitle: When the picture of the puzzle appears
slug: spring-constructor-dependency-injection-lombok
description: |-
  Lombok makes Spring's constructor-based injection easy.
  You just need to know a simple trick to use the `@Value` annotation.
author: chop
categories: [ software-creation ]
tags: [ spring-boot, java, programming ]
keywords: [ dependency injection, java, spring, spring boot, lombok, autowired, constructor, immutable ]
---

In the past, we advised [using the constructor to inject dependencies][kp-spring] with Spring, and we [introduced Lombok][kp-lombok].
Guess what?
Those go quite well along together, except maybe for a little trick to known when you wish to use Spring's `@Value`.
We'll cover all that in this post.

<!--more-->

## Generate the Injecting Constructor with Lombok

Let's make an immutable service that injects its dependencies through a constructor.

```java
import org.springframework.stereotype.Service;

@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    public Library(BooksDatabase booksDb, BorrowersDatabase borrowersDb) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
    }
}
```

If we apply [what we showed with Lombok last time][kp-lombok], a more concise way to do this would be:


```java
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;
}
```

And we're done!
Plus, no maintenance needed of the constructor if we add or remove fields.
Ain't that a match made in heaven?


## The Annoying Case of `@Value`

### Don't Mix Up!

Lombok has `lombok.Value`, Spring has `org.springframework.beans.factory.annotation.Value`.
They don't have the same purpose at all.
In this post, we'll focus on Spring's `@Value`.


### The Problem

To use Spring's constructor-based injection, everything needs to go through the constructor.
If you need to inject configuration, you must be able to set the `@Value` on the parameters.


```java
@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;
    private final int maxLendingDays;

    public Library(
      BooksDatabase booksDb,
      BorrowersDatabase borrowersDb,
      @Value("${library.lending.days.max}") maxLendingDays
    ) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
        this.maxLendingDays = maxLendingDays;
    }
}
```

However, since Lombok writes your constructor in your stead, how can you do that?


### The Naive Solution

When first confronted to this issue, the instinct of my team was to do that:

```java
@Service
@RequiredArgsConstructor
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private int maxLendingDays;
}
```

But what happens in this case?

First, Lombok will generate a constructor taking an argument for both `final` fields, but not for the integer.
The generated code would look like this:

```java
@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private int maxLendingDays;

    public Library(BooksDatabase booksDb, BorrowersDatabase borrowersDb) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
    }
}
```

When Spring encounters this code, it creates the bean in two steps:

1. Since a constructor is present, it uses it to inject the two concerned fields.
2. It then uses reflection to set the non-`final`, `@Value`-annotaetd field.
All of the gain of constructor-based injection is then lost.


### The Real Solution

[We said][kp-lombok] Lombok's behaviour can be customized with a `lombok.config` file.
This is one of the times it gets very interesting.

You can for instance [tell Lombok to copy certain annotations on the generated constructors' parameters][lombok-constructors].
Add the following line to your configuration file:

```properties
lombok.copyableAnnotations += org.springframework.beans.factory.annotation.Value
```

Then update your code thus:

```java
@Service
@RequiredArgsConstructor
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private final int maxLendingDays;
}
```

The generated code will look like the following:

```java
@Service
public class Library {
    private final BooksDatabase booksDb;
    private final BorrowersDatabase borrowersDb;

    @Value("${library.lending.days.max}")
    private final int maxLendingDays;

    public Library(
      BooksDatabase booksDb,
      BorrowersDatabase borrowersDb,
      @Value("${library.lending.days.max}") maxLendingDays
    ) {
        this.booksDb = booksDb;
        this.borrowersDb = borrowersDb;
        this.maxLendingDays = maxLendingDays;
    }
}
```


## My Other Tips

### `@RequiredArgsConstructor` vs `@AllArgsConstructor`

I like `@RequiredArgsConstructor` because it lets me decide which fields should be passed to the constructor.
Yet, in the case of a Spring bean, I think all fields should be initialized this way in 99% of cases, so using `@AllArgsConstructor` is a valid solution, and it saves some thinking time ("will this field be included in the constructor's arguments?" Yes, it will!).


### Adding `@Autowired` to a Generated Constructor

You may wish your constructor to carry the `@Autowired` annotation.
Through optional[^fn-autowired-constructor], it may help devs unfamiliar with Spring to understand how the class is initialized during runtime, for instance.
This can be set through your Lombok annotation:

```java
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Library {
    // ...
}
```

[^fn-autowired-constructor]: `@Autowired` on constructor is only required if the class has several constructors.
In such cases, exactly one constructor should be thus annotated.


## Conclusion

I hope this short post will help you get rid of some of the boilerplate code that remained for the instanciation of your Spring beans.

Don't hesitate to share tips that could help in such contexts.


[kp-spring]: {{< relref path="/blog/2021/01-04-spring-constructor-injection" >}}
[kp-lombok]: {{< relref path="/blog/2022/04-08-lombok" >}}
[lombok-constructors]: https://projectlombok.org/features/constructor
