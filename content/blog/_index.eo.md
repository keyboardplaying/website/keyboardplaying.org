---
title: Blogo
url: /blogo
aliases: [ /blog ]
classes: [ large-page ]

cascade:
  scripts: [ main ]
---
