---
date: 2021-12-20T07:00:00+01:00
title: What's (Been the Hold-)Up? (December 2021)
#subtitle: A witty line as a subtitle
slug: whats-up
description: |-
  Esperanto and mail notifications are now on the menu.
  There's also a FAQ for guest articles.
author: chop
categories: [ kp.org ]
tags: [ news, esperanto ]
keywords: [ esperanto, staticman, mailgun ]
---

Seven months since the last update, four since the last post.
What's changed since then?

<!--more-->

## What's Changed?

### Esperanto (since June)

I've said for a few months that I was thinking of it, it's now a reality: this website now exists in Esperanto.
As for French, when the page exists in Esperanto, an "EO" bigram appears near the {{< icon icon="flag" title="Flago" >}} icon.

Not many pages have been translated for now, and I can't guarantee I'll systematically write new ones in Esperanto too, but don't hesitate to notify me if you think one page would be a great fit for Esperanto.


### Mail notifications (since August)

I've been bothered for a while by the fact that readers that left a comment weren't notified in case someone answered them.
This has changed: you can now subscribe for mail notifications on a page you comment.
Some notes about it:

* If you do subscribe, you will receive an email each time someone comments on the same page.
* You can unsubscribe at any time, using the link included in every notification you will receive.
* For GDPR compliance, you will need to opt-in twice: first, you will need to check the box saying you want to get notified about new comments.
Then, you will receive an email with a link to confirm your subscription.
If you fail to click this link, you _won't_ receive notifications.

The solution relies on [a fork][staticwoman] of [Staticman][staticman] and [Mailgun][mailgun].
I may go into the detail of explaining how all this works, in a separate post.


### Guest articles (since December)

I've received some emails from people who were interested in publishing guest articles, but everytime I answered, the correspondence stopped.
I understand that the conditions we can offer are not the most attractive, but common courtesy would be to politely decline.

To spare myself and other potential guests the loss of time, I wrote a quick [FAQ about guest articles on Keyboard Playing][guest-articles].
If you're interested, you now know what to do!


## And _That_ Took All Your Time?

Obviously not, I've been distracted with other activities.
I especially wrote two texts for calls, hoping they'll get published.
[One of them][voyage-demain] was not, I'm still waiting on an answer for the other.

I also spend a lot of time following what becomes [the book we published this year][carrefour].
We knew we weren't in it for the money, but so far, we didn't even sell enough to cover for the expenses.
I might explain a bit, later.
It may be too late to get it for Christmas, but if you're looking for some early-year present to give to a French-speaking person, don't hesitate!


## What's Next?

I honestly don't know.
Ideas are popping, for the website and for other projects.
I only lack the time to make all of them a reality.

I couldn't make one post each month this year, maybe one every two months would be more realistic for 2022.
We'll try and see.

I wish you all a merry Christmas and a happy new year, and I hope you'll come back in 2022.


[staticwoman]: https://github.com/hispanic/staticwoman
[staticman]: https://staticman.net
[mailgun]: https://www.mailgun.com/
[guest-articles]: {{< relref path="/about/guest-articles" >}}
[voyage-demain]: {{< ref path="/stories/2021/voyage-de-demain" lang="fr" >}}
[carrefour]: {{< ref path="/blog/2021/05-31-carrefour-disponible" lang="fr" >}}
