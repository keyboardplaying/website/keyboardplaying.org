---
date: 2021-12-20T07:00:00+01:00
title: Quoi d'neuf ? (décembre 2021)
#subtitle: A witty line as a subtitle
slug: quoi-d-neuf
description: |-
  Espéranto et notifications par courriel sont désormais au menu.
  Nous avons également une FAQ concernant les articles invités.
author: chop
categories: [ kp.org ]
tags: [ news, esperanto ]
keywords: [ esperanto, staticman, mailgun ]
---

Sept mois depuis le dernier bilan, quatre depuis le dernier billet.
Qu'est-ce qui a changé depuis ?

<!--more-->

## Qu'est-ce qui a changé ?

### Espéranto (depuis juin)

J'en parle depuis plusieurs mois, c'est maintenant chose faite : ce site offre désormais du contenu en Espéranto.
Comme pour l'anglais, lorsqu'une page existe en espéranto, vous verrez les lettres "EO" à côté de l'icône {{< icon icon="flag" title="Flago" >}}.

Peu de contenus ont été traduits pour l'instant, et je ne peux pas promettre que j'écrirai systématiquement les nouveaux en Espéranto, mais n'hésitez pas à signaler les pages que vous voudriez voir traduites.


### Notifications par courriel (depuis août)

Cela faisait longtemps que j'étais dérangé par l'idée que les lecteurs qui laissaient des commentaires n'étaient pas informés en cas de réponse.
C'est désormais corrigé : lorsque vous partagez une note sur une page, vous pouvez choisir de recevoir des courriels de notification pour les futurs commentaires.
Quelques remarques :

* Si vous vous abonnez, vous recevrez un courriel à chaque fois qu'un nouveau commentaire est ajouté à la page.
* Vous pouvez vous désinscrire à tout moment, en utilisant le lien inclus à la fin de chaque notification que vous recevrez.
* Afin de répondre aux exigences du RGPD, il vous faudra confirmer votre volonté de vous inscrire.
Si vous cochez la case appropriée au moment où vous saisissez votre commentaire, vous recevrez un courriel avec un lien sur lequel il faudra cliquer pour valider votre abonnement.
Si vous n'ouvez pas ce lien, vous ne recevrez _pas_ les notifications.

La solution s'appuie sur [une fourche][staticwoman] de [Staticman][staticman], ainsi que sur [Mailgun][mailgun].
J'aborderai peut-être les détails du fonctionnement dans un autre billet.


### Articles invités (depuis décembre)

J'ai reçu quelques messages de personnes intéressées par l'idée de publier un article sur ce site.
Cependant, à chaque fois que je réponds, la conversation s'arrête brutalement.
Je peux comprendre que les conditions ne soient pas les plus attractives, mais un message pour décliner me semblerait poli.

Pour m'épargner ainsi qu'à ces personnes une perte de temps, j'ai donc écrit une rapide [FAQ à propos des articles invités sur Keyboard Playing][guest-articles].
Si vous êtes intéressé·e, vous savez maintenant quoi faire !


## Et c'est _ça_ qui a pris tout ton temps ?

Évidemment non, j'ai été distrait par d'autres activités.
J'ai notamment écrit deux textes pour des appels (concours), en espérant les voir publier.
[L'un d'eux][voyage-demain] ne l'a pas été, j'attends encore les résultats pour le second.

J'ai également consacré beaucoup de temps au suivi [du livre que nous avons publié cette année][carrefour].
Nous savions que ce livre ne nous rendrait pas riches, mais les ventes ne suffisent pas à couvrir nos dépenses.
J'entrerai peut-être un peu plus dans les détails, plus tard.
C'est trop tard pour l'avoir à Noël, mais si vous cherchez des idées de cadeau pour le début de l'année, n'hésitez pas !


## Et ensuite ?

En toute franchise, aucune idée.
Les idées se multiplient, aussi bien pour le site que pour d'autres projets.
Je manque juste de temps pour toutes les concrétiser.

Je ne suis pas parvenu à maintenir le rythme d'un billet par mois cette année, peut-être qu'un tous les deux mois sera plus réaliste pour 2022.
Nous essaierons et verrons.

Je vous souhaite à toutes et tous d'excellentes fêtes de fin d'année, et espère vous revoir en 2022.


[staticwoman]: https://github.com/hispanic/staticwoman
[staticman]: https://staticman.net
[mailgun]: https://www.mailgun.com/
[guest-articles]: {{< relref path="/about/guest-articles" >}}
[voyage-demain]: {{< relref path="/stories/2021/voyage-de-demain" >}}
[carrefour]: {{< relref path="/blog/2021/05-31-carrefour-disponible" >}}
