---
date: 2021-03-15T07:00:00+01:00
title: What's Up? (March 2021)
#subtitle: A witty line as a subtitle
slug: whats-up
description: |-
    The home page is now a bit more useful, finding related content has become easier, and an Esperanto version of the site might appear before the end of the year.
author: chop
categories: [ kp.org ]
tags: [ news, esperanto ]
keywords: [ blog, stories, related content, espéranto, download proxy ]
---

It's been two months since the last "What's Up?" post, time to see the main changes since then.

<!--more-->

## What's Changed?

### A Better Home Page

I wanted to make the home page better for an age.
Having only the main links was not enough to get an idea from what would be found in these.
So I finally made the change and the latest blog entry, the latest story and the most recently updated tool[^fn-tool] are displayed under the menu entries.

[^fn-tool]: Right now, there's only the Download Proxy, but maybe that'll change in the future.


### Related Content

I like to know when there are other articles related to the post I'm reading, that might help me find more info on the same topic.
There are tags and categories, but these may be too vast as they do not target the specific topic of the current page.

So, I leveraged [Hugo's mechanism for relating content][hugo-related] and configured it to use both tags and keywords.
That implied going through all posts to _add_ those keywords, as I didn't set them initially.
You won't see them, but they're here. :)

The results are sometimes surprising, like a post you expect that won't show, or one that's barely related but is in the top 2 results.
Still, on the whole, it provides links to potentially interesting pages on the website.


## What's Next?

### An Esperanto version of the website?

I've begun learning Esperanto last month.
I like the philosophy behind this language and I'm considering publishing an Esperanto version of the website.
Translating existing work would take time, but it'd also be a great way of progressing and learning the vocabulary I need most for my work.
A friend [has already been supplying me with valuable resources][tw-eo-rsc].
Just a bit more learning, and then I think I'll be advanced enough to start practicing.

Would you be keen on seeing Keyboard Playing in Esperanto[^fn-kp-eo]?
Which blog entries or stories do you think I should translate first?

[^fn-kp-eo]: I suppose the site's name would best translate to _Ludi kun klavaroj_, but it would actually remain _Keyboard Playing_, as for the French version.

[hugo-related]: https://gohugo.io/content-management/related/
[tw-eo-rsc]: https://twitter.com/XanderLeaDaren/status/1365177830334738435
