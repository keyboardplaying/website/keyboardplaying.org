---
date: 2021-05-10T07:00:00+02:00
title: La concordance des temps dans un récit
subtitle: Une introduction
slug: concordance-temps-recit
description: |-
  Il est parfois difficile de savoir quel temps employer dans un récit.
  Voici un très rapide résumé.
author: chop
categories: [ writing ]
#tags: [ java ]
keywords: [ grammaire, conjugaison, temps, verbe ]
---

En relisant des textes, je m'amuse parfois d'erreurs dans la concordance des temps.
Ce sont souvent des fautes d'inattention, le passé devenant soudain un présent.

De temps en temps, on est tout de même un peu perdu et on oublie les temps à utiliser.
Voici un petit rafraîchissement.

<!--more-->

## Ce qu'est ou n'est pas ce billet

Je vous propose ici un point _rapide_ sur le sujet.
Pas de long discours, pas de références nombreuses…
Juste ce que je sais/crois savoir de mes cours de français déjà lointains et mes lectures en français de moins en moins nombreuses.

On va parler de ce qui se passe, ce qui s'est passé ou se passera.
Je laisserai en revanche de côté ce qui pourrait se passer, aurait pu se passer…
Pour l'instant, on reste simple, quitte à faire une deuxième session plus tard.

Si j'ai manqué des points essentiels, signalez-le en commentaire et je ferai des corrections.


## Pourquoi la concordance des temps ?

Comme la plupart des conseils de grammaire ou de stylistique que l'on peut vous donner, l'enjeu principal est double :

* **vous faire comprendre** de vos lecteurs : vous pouvez alourdir vos phrases avec de multiples références temporelles, mais le texte indigeste ne facilite pas la lecture.
En employant les bons temps, vous vous assurez normalement une bonne compréhension avec moins de travail.
* **assurer l'implication** de vos lecteurs.
Il n'y a rien de pire, quand je suis plongé dans l'univers de ma lecture, que de tomber sur une grosse faute d'orthographe ou de grammaire.
Un verbe dans un mauvais temps ou mauvais mode[^fn-implication], qui tue le sens de la phrase ou du récit, c'est comme un caillou qui ferait dérailler le train de l'histoire : ça semble insignifiant, mais ça casse complètement l'inertie d'un truc gigantesque.

[^fn-implication]: Certaines erreurs de concordance m'ont cependant échappé lors de premières relectures car j'étais tellement pris par le récit que je n'y faisais plus attention.


## Les temps du récit

Lorsque l'on raconte une histoire, il y a trois moments principaux :
* le **présent** de l'histoire, ce qui se passe au moment où l'on raconte ;
* le **passé**, ce dont on se souvient, les flashbacks, ce qui s'est passé pendant une ellipse et qu'on raconte seulement maintenant pour créer un peu de suspense ;
* le **futur**, ce qui se passera presque certainement dans le futur de l'histoire.


## Les temps à employer

### Dans un récit au présent

Commençons par le présent parce que c'est plus simple.
Dans tous les cas, on utilise l'indicatif pour la narration :
* le **présent** se raconte au présent ;
* le **passé** peut être plus subtil : le passé composé pour les actions et l'imparfait pour les descriptions ou l'état ;
* le **futur** est très compliqué : on utilise le futur simple.

Imaginons un petit exemple, guère intéressant d'un point de vue littéraire, mais qui a l'avantage d'utilise les trois temps.

> En arrivant sur le pont du navire, il **se retourne** pour observer sa bien-aimée.
> Elle **est** magnifique, 
> Il **se souvient** de leur rencontre, il y a à peine six mois.
> Il l'**a bousculée** en sautant sur le quai et le sourire qu'elle **affichait** l'**a séduit** sur le champ.
>
> La laisser aujourd'hui pour six mois en mer lui **crève** le cœur, mais il n'**a** aucune inquiétude : elle l'**attendra**.

Au **présent**, notre marin « se retourne » (c'est une action) et sa bien aimée « est » magnifique (c'est une description).

Au **passé**, il « l'a bousculée » (action), mais elle « affichait » un sourir à tomber (état, description).

Au **futur**, elle « l'attendra ».


### Dans un récit au passé

Il est plus courant d'écrire les histoires au passé, mais les règles vues pour le présent se transcrivent presque directement.

* Le **présent** se raconte au passé simple pour les actions et à l'imparfait pour les descriptions, l'état, l'habituel (passé simple pour ce qui est bref, imparfait pour ce qui dure dans le temps).
* Le **passé** se raconte au plus-que-parfait. Certaines ressources indiquent que le passé du passé simple est le passé antérieur, mais je ne trouve pas de tête un exemple de lecture où je l'aurais vu.
* Le **futur** se raconte au présent du conditionnel. Oui, on a quitté l'indicatif.

En reprenant notre petit exemple :

> En arrivant sur le pont du navire, il **se retourna** pour observer sa bien-aimée.
> Elle **était** magnifique, 
> Il **se souvenait** de leur rencontre, à peine six mois plus tôt.
> Il l'**avait bousculée** en sautant sur le quai et le sourire qu'elle **avait affiché** l'**avait séduit** sur le champ.
>
> La laisser aujourd'hui pour six mois en mer lui **crevait** le cœur, mais il n'**avait** aucune inquiétude : elle l'**attendrait**.

Au **présent**, notre marin « se retourna » (c'est une action) et sa bien aimée « était » magnifique (c'est une description).

Au **passé**, il « l'avait bousculée » (action), mais elle « avait affiché » un sourir à tomber (état, description).

Au **futur**, elle « l'attendrait ».


## Pour conclure

Comme prévu, le sujet ici n'est abordé que brièvement.
Si le français est votre langue native, tout ceci vous paraît peut-être naturel sans que vous ayiez besoin d'y réfléchir[^fn-lacets].
Si c'est le cas, tant mieux.

Pour les autres, on peut aller plus loin si ça vous intéresse.
On n'a pas abordé la possibilité du présent de narration dans un récit au passé, le cas du présent de vérité générale…

Le français compte dix-sept temps et quatre modes, encore faut-il savoir quand les utiliser à bon escient.

[^fn-lacets]: En fait, souvent, c'est comme les lacets : c'est quand on essaie d'y réfléchir qu'on se perd.
