---
date: 2021-05-31 07:00:00+02:00
title: Au _Carrefour des imaginaires_
#subtitle: A witty line as a subtitle
slug: carrefour-imaginaires-disponible-ligne-libraires
description: |-
  Nous avons travaillé à autoéditer un receuil de nouvelles.
  Après un an de travail, il est finalement disponible en ligne et chez vos libraires.
  Voici quelques informations sur ce livre.
cover:
  banner: false
  src: /img/carrefour/cover-v2.jpg
  alt: Couverture du livre Carrefour des imaginaires
  by: commudustylo
author: commudustylo
categories: [ writing ]
keywords: [ nouvelles, recueil, livre, vente, autoédition, communauté, écriture, dessin ]

tipping: false

references:
- id: tw-commu
  name: Twitter de la Communauté
  url: https://twitter.com/commudustylo
  lang: fr
- id: discord-commu
  name: Discord de la Communauté
  url: https://discord.gg/bCGPmqEPTh
  lang: fr
- id: bookelis-physique
  name: _Carrefour des imaginaires_, broché
  url: https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html
  lang: fr
  author:
    name: Bookelis
- id: fnac-physique
  name: _Carrefour des imaginaires_, broché
  url: https://livre.fnac.com/a15996237/Du-Stylo-La-Communaute-du-Stylo-Carrefour-des-imaginaires
  lang: fr
  author:
    name: FNAC
- id: chapitre-physique
  name: _Carrefour des imaginaires_, broché
  url: https://www.chapitre.com/BOOK/collectif/carrefour-des-imaginaires,82541496.aspx
  lang: fr
  author:
    name: Chapitre.com
- id: decitre-physique
  name: _Carrefour des imaginaires_, broché
  url: https://www.decitre.fr/livres/carrefour-des-imaginaires-9791035942304.html
  lang: fr
  author:
    name: Decitre
- id: amazon-physique
  name: _Carrefour des imaginaires_, broché
  url: https://www.amazon.fr/Carrefour-imaginaires-Du-Stylo-Communauté/dp/B094QKXKD4/?tag=kp0b3-21
  lang: fr
  author:
    name: Amazon
- id: bookelis-epub
  name: _Carrefour des imaginaires_, epub
  url: https://www.bookelis.com/romans/44365-Carrefour-des-imaginaires.html#/65-type_livre-epub
  lang: fr
  author:
    name: Bookelis
- id: fnac-epub
  name: _Carrefour des imaginaires_, epub
  url: https://livre.fnac.com/a15996237/Du-Stylo-La-Communaute-du-Stylo-Carrefour-des-imaginaires?NUMERICAL=Y
  lang: fr
  author:
    name: FNAC
- id: kobo-epub
  name: _Carrefour des imaginaires_, epub
  url: https://www.kobo.com/fr/fr/ebook/carrefour-des-imaginaires
  lang: fr
  author:
    name: Kobo.com
- id: google-books
  name: _Carrefour des imaginaires_, epub
  url: https://www.google.fr/books/edition/Carrefour_des_imaginaires/cz0sEAAAQBAJ
  lang: fr
  author:
    name: Google Books
- id: amazon-kindle
  name: _Carrefour des imaginaires_, Kindle
  url: https://www.amazon.fr/Carrefour-imaginaires-Communauté-du-Stylo-ebook/dp/B093K4KXXZ/?tag=kp0b3-21
  lang: fr
  author:
    name: Amazon
---

Il y a bientôt quatre mois, chop a annoncé [sur ce site][whatsup-carrefour-available] la disponibilité de notre livre.
Après quelques mois de travail supplémentaire et quelques péripéties, nous pouvons enfin compléter : **notre livre est disponible en version physique ou électronique sur vos plateformes habituelles.**

Mais qui sommes-nous ?
Quel est ce livre ?
Combien coûte-t-il et comment sera utilisé cet argent ?
Où pouvez-vous le trouver ?
Qu'en pensent ceux qui l'ont déjà lu ?
Est-ce que vous pouvez en lire un passage ?
Vous rendra-t-il riche ?

Autant de questions auxquelles nous allons essayer de répondre en restant aussi brefs que possibles.


<!--more-->

## Qui sommes-nous ?

### Le groupe

« Nous », c'est la Communauté du Stylo.
Initialement, c'est un gars, {{< author "tony" >}}, qui a lancé [un serveur Discord][discord-commu] avec quelques motivés pour se retrouver et s'entraider.
Avec une petite particularité : si les collectifs d'écrivains ou de dessinateurs sont courantes, il est moins habituel d'en trouver un qui regroupe les deux, mais l'alchimie entre nos _écriteurs_ et nos _dessineux_ est plutôt bonne.


### Les défis

Nous nous imposons des exercices récurrents, hebdomadaires (les [#StyloHebdo][tw-stylohebdo]) pour travailler un peu chaque semaine, même un tout petit peu, ou bimestriels (les [#DefiDuStylo][tw-defidustylo]) pour pouvoir offrir des œuvres plus conséquentes.
L'objectif principal est de nous stimuler, de nous motiver à créer de nouvelles choses.

Tout commence généralement avec un thème ou une contrainte légère.
Pour les défis du stylo, on propose souvent une règle optionnelle afin que ceux qui veulent un exercice plus complet puissent s'y essayer.
Si vous êtes curieux, {{< author "chop" >}} partage certaines de ses participations [sur ce site][chop-hebdo].



## Quel est ce livre ?

### La génèse

Pourquoi avoir pris le temps de vous parler de nos défis ?
Parce qu'ils sont la fondation de ce livre.
Nous avions envie de publier un premier ouvrage, fruit de notre collaboration, initialement pour l'offrir à nos proches.
Il nous a fallu un moment pour réaliser que nos challenges pouvaient être notre matière première.


### Le résultat

Un an de défis bimestriels, six thèmes : sommeil, opposition, peur, Noël, souffle et immortalité.
Il nous a fallu faire des choix afin de ne garder que ce qui nous paraissait être les meilleures histoires à vous partager.

Nous avons ainsi récolté **quinze nouvelles, une pièce de théâtre en cinq actes et neuf planches illustrées, occupant près de trois-cent-soixante pages**.
Les styles et les univers sont variés, couvrant ainsi un vaste spectre.
De la  science fiction au fantastique, flirtant parfois avec l’horreur ou embrassant pleinement l’absurde, nous avons écrits nos récits avec passion et enthousiasme.

Une fois tout cela assemblé et mis en forme, il a fallu mettre au point une couverture, illustrée et colorée par nos dessineux.
Au sein de ce dessin aux couleurs chaudes se cachent plusieurs références à nos nouvelles[^fn-cover-refs].

[^fn-cover-refs]: Pourrez-vous toutes les trouver ?

{{< figure src="/img/carrefour/cover-v2-small.jpg" link="/img/carrefour/cover-v2.jpg" title="La couverture de notre livre" alt="Une couverture portant le titre Carrefour des imaginaires. L'illustration comprend une table sur laquelle sont posés une bière, une paire de dés et un verre de whisky. En second plan, une maison au bord d'un étang, où l'on aperçoit un arc et un sapin de Noël. En arrière-plan, une ville et deux vaisseaux spatiaux." >}}



## Combien coûte-t-il et comment sera utilisé cet argent ?

**Le livre papier coûte 15 € et son équivalent numérique est vendu à 5,99 €**.
Si vous optez pour la version matérielle, les frais de port sont à la discrétion de la boutique que vous choisirez.

L'argent que vous versez sert en premier lieu à couvrir les frais du livre.
La fabrication de chaque exemplaire physique s'élève à 9,34 €.
S'ajoutent la TVA de 5,5 % versée à l'État (0,78 € pour le physique, 0,31 € pour l'électronique), divers frais liés à la distribution via les canaux…

Les auteurs ont fait le souhait de ne pas tirer de profit de ce recueil.
Si nous obtenons des bénéfices, ils seront injectés dans une cagnotte que la Communauté pourra utiliser pour de futurs projets, pour bénéficier d'un accompagnement plus aguerri par exemple.



## Où pouvez-vous le trouver ?

Nous avons voulu faire en sorte que notre livre soit accessible de la façon qui soit la plus habituelle pour vous.


### La version papier

Vous avez un ou une libraire ?
Soyons honnêtes : nous serions surpris (mais flattés) d'apparaître en rayon chez elle ou lui.
En revanche, **vous pouvez commander le livre chez 7000 libraires de France, Belgique, Suisse ou Québec**[^fn-francophone].

[^fn-francophone]: Du fait que plusieurs membres de la communauté travaillent ou vivent à l'étranger, nous sommes conscients des difficultés à l'étranger et avons fait de notre mieux pour faciliter l'accès à l'œuvre.

Si **vous préférez commander en ligne**, vous avez le choix de la boutique : [Bookelis][bookelis-physique], [FNAC][fnac-physique], [Chapitre.com][chapitre-physique], [Decitre][decitre-physique] ou même [Amazon][amazon-physique].


### La version numérique

Là encore, nous avons fait de notre mieux pour faire en sorte que vous trouviez aisément une solution pour vous :
* l'epub peut être acheté sur [Bookelis][bookelis-epub], [FNAC][fnac-epub] ou [Kobo][kobo-epub] ;
* pour une lecture depuis votre tablette Android, vous pouvez vous diriger vers la solution maison [Google Books][google-books] ;
* pour Kindle enfin, l'option la plus naturelle est de regarder chez [Amazon][amazon-kindle].


## Qu'en pensent ceux qui l'ont déjà lu ?

Nos premiers lecteurs ont été nos proches, ceux qui ignoraient parfois nos activités créatrices et ont trouvé ce volume au pied de leur sapin.
Au-delà de la surprise, nous avons eu beaucoup de retours positifs.
Certains parents de nos auteurs ont pu détailler quelles nouvelles ils avaient préférées, l'un d'entre eux ayant convaincu une quinzaine de ses amis d'acheter le livre pour soutenir la Communauté.

Vous pourriez penser que ces critiques sont naturellement biaisées, mais un de nos lecteurs, lui-même auteur, a partagé son avis sur Twitter.
Nous le reproduisons ici, tant sa lecture de notre premier tome nous a touchée.

> J'ai terminé le _Carrefour des imaginaires_ par [@commudustylo][tw-commu].
> Un recueil de nouvelles [...] qui vous fera sourire, qui vous touchera, mais surtout, qui vous fera réfléchir !
> Car c'est avant tout un recueil qui questionne l'humain dans un large registre.
>
> Qu'est-ce qui fait de nous des humains ?
> Pouvons-nous tout nous permettre ?
> Quand devenons-nous les monstres qui nous terrifient ?
> Quand dépassons-nous la limite de l'éthique ?
> Quand nous prenons-nous pour des démiurges ?
>
> J'ai trouvé ce recueil fascinant à plus d'un titre.
>
> D'abord, chaque auteur a son style, mais tous sont complémentaires.
> Je n'ai jamais eu l'impression qu'un récit n'avait pas sa place.
> C'est un gage de qualité, il est souvent difficile de conserver une qualité égale tout au long d'un recueil, mais la Communauté du Stylo prouve qu'elle refuse de tomber dans l'amateurisme.
> L'objet livre est beau, la mise en page aérée, et les illustrations agrémentent le tout de la plus belle des manières.
> Les concepts des nouvelles, leur exécution, la plume des auteurs, le trait des illustrateurs, tout est fait avec le plus grand soin.
>
> Je ne peux que vous inviter à soutenir la Communauté du Stylo en découvrant leur première œuvre dès maintenant !
>
> <cite><a href="https://twitter.com/GGuegan1Auteur/status/1354012799442235393" rel="nofollow">Guillaume Guegan</a>, auteur de <a href="https://www.editionslivresque.com/product-page/noxiance-tome-1-le-pouvoir-ancestral" class="ref-title">La Noxiance</a>


## Est-ce que vous pouvez en lire un passage ?

Mais bien entendu !
Plus d'un, même, car il serait difficile de vous donner une idée de la variété de ce livre, de nos styles et de nos thèmes.
Voici donc sept extraits pour vous.

> La révolution digitale n’avait pas atteint le sommet de l’Olympe.
> La faute principalement à la flemme du technicien qui était venu faire les raccordements de la fibre, mais qui, devant la montagne d’escaliers à monter, avait plutôt préféré laisser un petit mot tout en bas de la montagne disant sobrement « client absent, repassera demain ».
> C’était il y a trois mois.
>
> <cite><span class="ref-title">Jeux de dieux</span>, {{< author "tony" >}}</cite>


> Le temps des réminiscences est révolu.
> Le barbu est dans le panier, il me faut à présent partir.
> Il ne reste qu’un petit détail à régler : je parcours des yeux les chaussettes accrochées à la cheminée pour retrouver celle du petit Eddy, ce petit tortionnaire des cours de récréation.
> Je prélève dans ma poche un beau morceau de charbon que j’avais gardé au chaud spécialement pour lui.
> S’il préfère un cadeau du vieux Nicolas pour l’an prochain, il arrêtera d’embêter ses petits camarades.
>
> <cite><span class="ref-title">Les pères Noël</span>, {{< author "chop" >}}</cite>


> Il fit taire l’assistance en tapotant sa flûte de champagne avec un couteau et toute l’attention se concentra sur lui.
>
> — Mesdames et Messieurs, c’est avec une très grande fierté que nous allons ce soir vous faire la démonstration de la première application concrète du projet Éternité.
> Le temps qu’il m’a fallu pour en arriver là me semble tout aussi long que ce que le futur nous réserve et pourtant, c’est l’immortalité qui nous tend les bras.
> Je vous le dis, mes amis, ce jour est le plus important de nos vies, mais aussi le plus important de l’histoire passée et future de l’humanité.
>
> <cite><span class="ref-title">L'ambition de l'immortalité</span>, {{< author "ngorzo" >}}</cite>


> Le Hope se dirige vers Gliese 581, un système qui regroupe plusieurs planètes pouvant accueillir la vie humaine et donc permettre l’établissement de la plus grande colonie externe au système solaire.
> Mais pour établir cet essaim, il faut tout d’abord que nos voyageurs arrivent en vie.
> C’est là qu’entrent en jeu les robots SATAR.
>
> <cite><span class="ref-title">Hope</span>, {{< author "ngorzo" >}}</cite>


> S’installant au sommet de la plus haute montagne, elle commença à construire une maison dans laquelle elle allait vivre, avec assez de pièces pour accueillir ses futurs élèves.
> L’École de la Pensée Miranda Jacks, voilà qui sonnait plutôt bien à ses oreilles.
> Dans la petite ville de la vallée, elle commença à faire passer le mot, le concept de son école et laissait entendre son secret, mais chaque fois les passants la regardaient d’un air perturbé.
> Il faut dire que sa méthode n’était pas la plus efficace : passer dans les rangs pour dire aux gens qu’elle était une femme morte dans le corps d’un jeune adolescent et qu’elle savait vaincre la mort avait tendance à provoquer des moqueries, voire de la peur.
> Mais l’important était de faire passer le message.
>
> <cite><span class="ref-title">Les enfants de Miranda Jacks</span>, {{< author "tony" >}}</cite>


> D’un geste lent, [il] sort un briquet et une cigarette d’un vieux paquet.
> Il m’en propose une en me tendant le paquet, par ce geste simple que font les gens qui se connaissent pour inciter une personne à l’accompagner.
>
> — Ça fait bien longtemps que je ne fume plus, merci.
>
> Étrangement, il n’insiste pas et pose le paquet sur un buffet à proximité.
> Il s’y reprend à quatre fois avant que le briquet daigne produire une flamme.
> Il allume sa cigarette, le tabac diffusant rapidement son odeur âcre tout autour de lui, puis il pose le briquet à côté du paquet.
>
> L’ambiance est particulière.
> Je n’avais pas anticipé de discuter avec l’un des meurtriers de ma famille.
>
> <cite><span class="ref-title">Sombre aurore</span>, {{< author "ngorzo" >}}</cite>


> À l’époque, dans les années 2000, nous étions jeunes et drôles.
> Toujours la vanne, la vanne, la vanne.
> Une bande de potes qui se fend la gueule.
> Fallait voir les soirées de fou furieux.
> Une fois on a pissé dans la bière de Polo.
> La crise de rire.
> Même que Bruno en est mort.
> Mort de rire, t’y croirais pas si tu le voyais.
> Nous, on pensait pas qu’il était mort, alors on l’a foutu à poil et on l’a mis dehors, pour rigoler quoi.
> C’est une ou deux heures plus tard, quand on s’est aperçu qu’il ne bougeait pas par -10, qu’on a commencé à réaliser qu’il jouait trop bien le mort pour ne pas l’être vraiment.
> Mais l’histoire que je veux te raconter n’a rien à voir avec celle-là.
> De toute façon, une fois qu’on a enterré le cadavre dans la forêt, ça devient moins drôle.
>
> <cite><span class="ref-title">J'irai niquer ta mère un jour de printemps</span>, {{< author "sancho" >}}</cite>


## Vous rendra-t-il riche ?

Matériellement, nous pouvons penser que personne ne deviendra riche grâce à ce livre, et ce n'était pas son but.
Cependant, nous nous permettrons de conclure sur une citation issue du monde littéraire :

> Tout homme s'enrichit quand abonde l'esprit.
>
> <cite>Luna Lovegood dans <span class="ref-title">Harry Potter et l'Ordre du phénix</span>, par J. K. Rowling</cite>

D'ailleurs, si vous souhaitez participer aux échanges de la Communauté, voire vous lancer vous-même dans le dessin ou l'écriture, n'hésitez pas à nous contacter via [notre Twitter][tw-commu] ou même rejoindre [notre serveur Discord][discord-commu].

À très bientôt ?



{{% references %}}

[whatsup-carrefour-available]: {{< relref path="/blog/2021/01-18-whats-up" >}}
[chop-hebdo]: {{< relref path="/inspirations/weekly" >}}
[tw-stylohebdo]: https://twitter.com/search?q=%23StyloHebdo
[tw-defidustylo]: https://twitter.com/search?q=%23DefiDuStylo
