---
date: 2021-02-15T07:00:00+01:00
title: Why I Don't Like the Word "Developer"
#subtitle: A witty line as a subtitle
slug: why-dont-like-word-developer
description: |-
    "Developer" is often used as a generic word to describe a variety of jobs.
    Yet, despite the wide set of skills it covers, I often find it reductive.
    That's why I've been trying something else on this website.
author: chop
categories: [ software-creation ]
#tags: [ management ]
keywords: [ semantic, communication, career ]
---

You may or may not have noticed it, but I try to avoid the terms "developer," "coder" and "programming" on Keyboard Playing, except maybe when I'm focusing on the activity of writing code.
There's a reason to it: in our field, it's common to use these words to describe a variety of jobs.
Actually, about anyone who writes or edits code may be called a developer.

<!--more-->

Yet, I often have the impression that the  word "developer" diminishes the role of the people working on making a software project a reality.
So, what's wrong with the word "developer"?


## Just a Bit of Context

What I'll be writing here may seem exaggerated.
I know things are clearer and nuanced in the head of many people.
Yet, I feel that confusions and incomprehension would be understandable for people who have never worked in the heart of a development team.
That's what I'll be talking about here.


## Let's Get an Imaged View

Going back to [my favorite image for software creation projects][metaphor], imagine you are building a house.
There's a whole team behind it.

There's the client, the one who wants the house.
There's a project manager, the one who will handle the contract and make sure everything's all right.
There's a tech lead, who has several roles; he'll be:
- the expert engineer who decides of all technical constraints, materials and so on;
- the architect, who draws the plans of how things will connect to each other;
- the senior builder, who can teach his teammates about things they've never encountered before.

And there are the developers.
They are all the other builders: bricklayers, plumbers, electricians, roofers…


## "Developer" Seems Diminutive

Calling all these different specialties with the same word seems reductive to me.
It reinforces the feeling that developers are interchangeable, that you can replace one with another.
Just like any mason will be able to lay bricks, any developer will be able to write lines of code.

Of course it's not that simple!
Writing lines of code require an understanding, a bird-eye view.
Obviously, not all developers are equal.
We all have our specialties, things we prefer, fields we're more performant in.


## Some Developers Use the Word to Avoid Tasks

What's worse is that some developers have taken a taste to this vision.
A developer writes code, that's all.
They don't have to _think_ about it, the thinking should be done by other people.

I've heard more than once developers complain about things not being their role.
Designing an interface, designing an algorithm, fixing an unanticipated issue…
That's something they commonly do in France, but I've heard that, in other cultures[^fn-cultures], everything must be clearly analyzed and detailed: developers _just_ write code.

[^fn-cultures]: I've heard this from friends and colleagues who worked on projects outsourced to other countries, or who moved to other countries themselves.
They said so from at least three different cultures not related to each other.

That's a vision I don't really understand.
To me, it goes against personal and career growth.
When I'm asked to describe my role as a tech lead, I usually like to say that I'm "_just_ an experienced developer."
But how does an experienced developer become a tech lead if they _only_ write code without ever thinking about it?


## "Developer" Is Exclusive

When working on a software project, there are many ways to contribute.
Writing code is only one thing.
We could also speak of the physical data model (the structure of your database).
It's not uncommon for business analysts to design this.

Yet, defining a physical data model is a technical task.
It actually requires two skill sets, as you need to understand the data you'll be manipulating so that you can create an efficient and relevant representation, while mastering the DBMS[^fn-dbms] you'll use so that you can avoid the little traps it includes.

[^fn-dbms]: (R)DBMS = (Relational) DataBase Management System.
Yes, school is far for me, too.
It's the engine (Oracle, PgSQL, MySQL/MariaDB…) you've selected, each one having its own specificities and tips to be aware of.

We can have another example.
If your project has a user interface, it'll certainly include graphical elements (images, icons…).
Without those, your application wouldn't be the same.
Still, creating them is one task developers usually avoid because it's not something they feel competent/comfortable in.

With these examples in mind, we can see that developers are not the only ones who contribute to building a software solution.
Letting people think so seem to diminish the work of other actors.


## An Alternative?

What can we say if we don't want to use "developers"?
Something that might be a tad more generic without being _too_ generic.

Over the years, I tried several denominations for my bio on social networks: _coding addict_, _programming artist_[^fn-borrowed]…
Often, such terms seem bombastic or pedantic.

I've seen UX designers qualify themselves as "experience creators" and that inspired me: no matter our specialty, we do more than just writing code, **we create a piece of software**.
That's why I try to replace "developer" and "programming" with "[software creator/creation][soft-creation]."

This term is wider, includes more skills than just "writing code" ("experience creators" can be software creators, too).
It's an attempt to give people in our field more room to personally grow.

What do you think?


[^fn-borrowed]: This one, I borrowed from [Arkanosis](https://github.com/arkanosis).

[metaphor]: {{< relref path="/blog/2021/02-08-metaphor-development" >}}
[metaphor-story]: {{< relref path="/stories/2019/11/29-wc-metaphore" >}}
[soft-creation]: {{< relref path="/categories/software-creation" >}}
