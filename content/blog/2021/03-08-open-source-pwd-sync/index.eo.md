---
date: 2021-03-08T07:00:00+01:00
title: Open Source, mem-gastigita kaj sinkronigita administrilo por pasvortojn
#subtitle: A witty line as a subtitle
slug: sinkronigita-malfermitkoda-administrilo-pasvortojn
description: |-
  Multaj pasvortoj, dufazaj aŭtentigoj por pluraj kontoj…
  Mi bezonas administrilo por pasvortojn, disponebla ĉie, kaj mi ĵus adoptis malfermitkodan stakon.
  Jen kelkaj spuroj el miaj esploroj kaj eksperimentoj.
author: chop
categories: [ software ]
tags: [ security ]
keywords: [ sekureco, pasvorto, adminstrilo por pasvortoj, dufaza aŭtentigo, unufoja pasvorto, memgastigado, sinkronigo, lastpass, nextcloud, keepass, keepassxc, keepass2android, klavararanĝo, Linux, Windows, libera programaro ]
---

Ĝis antaŭ nelonge, mi uzis LastPass por konservi miajn pasvortojn.
La [ŝanĝoj al la senpaga nivelo][lastpass-free-changes] tamen instigis min pripensi alternativan solvon.
Sed prefere ol iri al konkuranto, mi preferis starigi libera solvon, kiun mi povis plene administri.


<!--more-->

## Miaj uzoj de administrilo por pasvortojn

Mi ne uzas la centra funkcio de administrilo por pasvortojn: la genero de forta kaj hazardan pasvorto.
**Mi ne povas uzi ĝin.**
Kiel IT-serviprovizanto, mi laboras en multaj komputiloj, en diversaj lokoj, kaj mi ne ĉiam povas instali programaron por havi miajn pasvortojn.
Mi tamen devas povi ensaluti en miajn kontojn, do mi devas koni miajn pasvortojn.

Tamen, mane tajpi longajn, fortajn pasvortojn sur klavaroj kun ŝanĝeblaj aranĝoj[^fn-kbd-layout] estas peniga.
Tial **solvo por enigi pasvortojn por mi en fidindaj maŝinoj estus bonvena**.
Malgranda gratifiko: ĉi tiuj maŝinoj povas esti kun Windows aŭ Linux.

Mi havas eĉ pli bazan bezonon: unika pasvorto por ĉiu konto necesigas memoron.
Kiam kelkaj servoj havas specifajn regulojn por validigi pasvortojn[^fn-pwd-rules], la afero estas eĉ pli komplika.
La primara funkcio ke mi bezonas de mia administrilo por pasvortojn estas ke ĝi **memoras miajn kompleksajn pasvortojn por mi**.

Mi ankaŭ uzas dufazan aŭtentigon[^fn-2fa] por ĉiuj miaj kontoj, kiuj proponas ĝin.
La plej ofta, mi elektas tempobazitajn unufojajn pasvortojn kiel duan paŝon, kaj **mi bezonas ricevi tiujn pasvortojn laŭpeto**, kie ajn mi estas.
Mi prefereas generi ilin en mia poŝtelefono se estas eble.

Ĉu mi bezonas skribi ĝin?
Tiu ŝlosilingo povas malŝlosi _ĉiujn_ miajn pordojn.
Se iu iam akirus ĝin, li devus ne povi uzi ĝin.
**Ĉiuj ĉi tiuj datumoj devas esti protektaj kaj akceseblaj nur de mi**.

Por resumi kaj prioritatigi, jen miaj atendoj:

* Ĉifritaj kaj protektigaj datumoj.
* Plurplatforma subteno (Windows + Linux).
* Eblo generi tempobazitajn konfirmkodojn laŭpeto.
* Pasvortoj disponeblaj en iu ajn fidinda aparato.
* Aŭtomata plenigo de aŭtentigajn formojn.
* (Generado de fortaj kaj hazardaj pasvortoj.)

[^fn-kbd-layout]: Mi jam devis uzi tri klavaranĝojn en la sama semajno (AZERTY, QWERTY, QWERTZ).
Miajn fingroj kutime eraras dum kelkaj horoj ĝis kelkaj tagoj post ĉiu ŝanĝo.
[^fn-pwd-rules]: En 2021, oni ankoraŭ vidas regulojn kiel "precize 8 signoj" aŭ "nur literaj aŭ ciferaj signoj."
Estas timiga pri la haketaj algoritmoj uzitaj por protekti tiujn pasvortojn, sed ĝi estas subjekto por alia afiŝo.
[^fn-2fa]: La dupâŝa aŭtentigo, aŭ dufaktora aŭtentigo (2FA), estas plifortigita aŭtentigomekanismo. Dum tio, vi devas provizi pliaj identecopruvojn.
La pruvoj povas esti io, kion vi scias (pasvorto, sekreta demando), kion vi posedas (aparato, kiel la telefono, kiu ricevas la SMS kun la konfirmkodo) aŭ kio vi estas (biometriga identigo).



## Mia antaŭa solvo

Lastatempe, mi anstataŭgis mian generatoro de dupaŝaj pasvortoj per LastPass, por havi ĉio en la sama loko.
Ĝi ne estas rekomendita: se iu povus akiri miajn LastPass datumojn, li havus ambaŭ miajn pasvortojn kaj dupaŝajn konfirmkodojn, sed pli pri tiu subjekto poste.
Sekureco ĉiam estas kompromiso inter kosto kaj uzebleco.

Mia solva estas simple: cîu estis sur la serviloj de LastPass.
Kiam mi bezonis ensaluti, la retumilkromprogramo petis mian ĉefan pasvorton kaj plenigis la formon.
Mi tiam prenis mian poŝtelefonon kaj generis konfirmkodon por pruvi mian identeco.

![Diagramo ilustranta la prezentitan solvon](sync-pwd-otp-lastpass.eo.svg)

Estas du problemoj kun tio:
* Ĉiu estas ĉe LastPass.
Okaze de rompo, kiel [iam okazis][lastpass-2015-breach], malgraŭ ilia laboro por [sekurigi la datumojn][lastpass-security], mi povus perdi ĉion.
* Ĉiu estas ĉe LastPass.
Se ili decidas ŝanĝi la kondiĉojn laŭ kiuj mi aliras _miajn_ datumojn, [kiel ili anoncis][lastpass-free-changes], mi devas submeti aŭ iri.
Mi elektis ĉi-lastan opcion, tial la malfermitkodan stakon, kiun mi proponas al vin.



## Ekvivalenta solvo, sed mem-gastigita kaj Open Source

### Administrilo por pasvortojn…

Mi konas [KeePass] delonge.
Por administratoj de pluraj maŝinoj aŭ datumbazoj, tio estas bonega solvo.
Ĝi stokas ĉion en ĉifrita KDB dosiero, protektika per majstra pasvorto.
Ĝi ankaŭ inkluzivas sian fortan pasvortan ĝenerilon.

* Ĉifritaj kaj protektigaj datumoj: okej.
* Generado de fortaj kaj hazardaj pasvortoj: okej.

Ĝi tamen havas gravan limigon: ĝi estas kodita en C#, tial desegnita por Windows.
Oni povas ruli ĝin en Linux danke al Mono, sed mi preferis operaciuman aplikaĵon.
Feliĉe, alternativoj aperis tiucele.

[KeePassXC] estas unu, sufiĉe konata kaj aktiva.
Ĝi uzas la saman dosierformaton (KDBX), sed estis dizajnita por funkcii en Windows, Linux kaj Makintoŝo OS.

* Ĉifritaj kaj protektigaj datumoj: okej.
* Plurplatforma subteno (Windows + Linux): okej.
* Generado de fortaj kaj hazardaj pasvortoj: okej.

KeePassXC ankaŭ komprenas tempobazitajn unufojajn pasvortojn, do vi povas konservi ilin en la sama dosiero kaj generi ilin en la aplikaĵo.

* Eblo generi tempobazitajn konfirmkodojn laŭpeto: okej.

### Sinkronigita…

Ĉar ĉiuj datumoj estas en ununura dosieron, vi povas disponigi ĝin en ĉiuj fifindaj maŝinoj, per memgastiga Dropbox-simila solvo.
Mi rekomendas [Nextcloud], ke vi povas instali sur via propra servilo por sinkronigi kun ajnaj aparatoj, kiujn vi volas, senpage.

### Ankaŭ ĉe aptelefono

Vi eble bezonas manieron aliri viajn datumojn krom komputilo, se vi ne povas instali KeePassXC ĉe via kliento, ekzemple.
Feliĉe, eblas sinkronigi kaj malfermi tiujn KDBX dosierojn ankaŭ en aptelefono.

Sur Androido, [Keepass2Android] kapablas.
Mi ankaŭ rigardis [KeePassDX], sed mi ne trovis solvon por malfermi dosieron gastigitan sur Nextcloud.
Sur iOS (sed ni forlasas la malfremitkodan sferon), [Strongbox] kaj [KeePassium] ŝajnas esti bonaj kandidatoj.

* Pasvortoj disponeblaj en iu ajn fidinda aparato: okej.

### Aŭtoplenigo

Fine, KeePassXS permesas vin profiti de aŭtomata plenigo en retpaĝoj.
Vi nur devas instali la etendon [KeePassXC-Browser] de via retumilo kaj fari iom da agordo.
Ĝi tiam plenigos ambaŭ pasvortojn kaj tempobazitajn konfirmkodojn, por vi kondiĉe ke KeePassXC rulas sur via maŝino kaj ke la datumbazo ne estas ŝlosita.

* Aŭtomata plenigo de aŭtentigajn formojn: okej.


### Ĝisdatigita diagramo

Jen en unu bildo kiel ĉi tiu nova solvo artikulas:

![Diagramo ilustranta la proponitan solvon bazitan sur KeePassXC + Nextcloud](sync-pwd-otp-keepassxc-nextcloud.eo.svg)




## Konservi tempobazitajn unufojajn pasvortojn aliloke

Mi skribis ĝin pli frue: konservi la tempobazitajn konfirmkodojn en la sama loko kiel la pasvortojn ne estas bona praktiko.
La principo mem de la dufaza aŭtentigo estas konfirmi la identeco de iu, kiu jam konas la pasvorton.

Mi tamen havas grandan problemon kun la kutimaj konfirmkodogeneratoroj: la plej multaj el ili ne havas enkonstruitan mekanismon de konservado/eksporto/importado, kio signifas, ke vi perdos ĉion se via telefono estas rompita aŭ perdita.

[Authy] estas uzebla generatoro, kun la kapablo konservi aŭ sinkronigi viajn tempobazitajn konfirmkodojn interrete.
Denove aperas la demande: ĉu vi volas konfidi viajn datumojn al alia servi, pri kiu vi ne havas kontrolo?
Vi estas libera, kompreneble.

![Diagramo ilustranta la proponitan solvon bazitan sur KeePassXC + Nextcloud + Authy](sync-pwd-keepassxc-nextcloud-otp-authy.eo.svg)

[^fn-recovery]: Mi troigas.
Vi devas havi metodon por ensaluti tiujn kontojn okaze de perdo de generatoro, sed reagordi kelkajn dekojn da kontoj kiam la dufaza aŭtentigo ne funkcias, estas tre dolora.


## Sen KeePassXC

(Ĉi tiu alineo estas aldono de 2022-01-07.)

Eble vi ne povas instali aŭ ruli KeePassXC en via komputilo.
Tiam, vi eble interesiĝos pri [mia lasta afiŝo][post-keeweb] por kompletigi ĉi tiun. 


[lastpass-free-changes]: https://blog.lastpass.com/2021/02/changes-to-lastpass-free/
[lastpass-2015-breach]: https://www.mcafee.com/blogs/enterprise/cloud-security/lastpass-breach-by-the-numbers-91-enterprises-exposed/
[lastpass-security]: https://www.lastpass.com/security/what-if-lastpass-gets-hacked
[keepass]: https://keepass.info/
[keepassxc]: https://keepassxc.org/
[nextcloud]: https://nextcloud.com/
[keepass2android]: https://play.google.com/store/apps/details?id=keepass2android.keepass2android
[keepassdx]: https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free
[strongbox]: https://apps.apple.com/us/app/strongbox-password-safe/id897283731
[keepassium]: https://apps.apple.com/us/app/keepassium-keepass-passwords/id1435127111
[keepassxc-browser]: https://keepassxc.org/docs/KeePassXC_GettingStarted.html#_setup_browser_integration
[authy]: https://authy.com/
[post-keeweb]: {{< relref path="/blog/2022/01-07-keeweb" >}}
