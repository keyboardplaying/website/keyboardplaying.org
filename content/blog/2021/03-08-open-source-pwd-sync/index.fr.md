---
date: 2021-03-08T07:00:00+01:00
title: Un gestionnaire de mots de passe auto-hébergé, synchronisé et open source
#subtitle: A witty line as a subtitle
slug: gestionnaire-mot-passe-synchronise-open-source
description: |-
  De nombreux mots de passe, une authentification à deux facteurs sur plusieurs comptes…
  J'ai besoin d'une solution de gestion des mots de passe disponible partout et je viens de migrer vers une pile open source.
  Voici quelques pistes issues de mes recherches et essais.
author: chop
categories: [ software ]
tags: [ security ]
keywords: [ sécurité, mot de passe, gestionnaire de mots de passe, 2fa, otp, autohébergement, synchronisation, lastpass, nextcloud, keepass, keepassxc, keepass2android, disposition de clavier, linux, windows, open source ]
---

Jusqu'à récemment, j'utilisais LastPass pour stocker mes mots de passe.
Les [changements du niveau gratuit][lastpass-free-changes] m'ont cependant poussé à reconsidérer et, comme beaucoup d'utilisateurs, à changer de crèmerie.
Toutefois, plutôt que de partir chez un concurrent, ma foi dans les fournisseurs de service ayant (une fois de plus) été mise à mal, j'ai préféré monter une solution libre que je pourrais intégralement gérer.


<!--more-->

## Mes usages d'un gestionnaire de mots de passe

Commençons par la fonctionnalité qui ne me sert pas : une fonctionnalité centrale de ces gestionnaires est la possibilité de générer un mot de passe fort et aléatoire.
**Je ne peux pas m'en servir**.
En tant que prestataire de services informatiques, je travaille sur de nombreux ordinateurs, dans des locaux variés, et je ne peux pas toujours installer un logiciel pour avoir mes mots de passe.
Cela n'empêche pas que je dois pouvoir me connecter à mes comptes, dont je dois par conséquent connaître les mots de passe.

Et pourtant, taper à la main des mots de passe longs et forts sur des claviers à la disposition changeante[^fn-kbd-layout] est pénible.
C'est pourquoi **une solution pour saisir les mots de passe à ma place sur les machines de confiance serait bienvenue**.
Petit bonus : ces machines peuvent tourner sous Windows ou Linux.

Il y a un besoin plus basique encore : un mot de passe unique pour chaque compte, ça commence à demander de la mémoire.
Quand en plus certains fournisseurs ont leurs propres règles pour contrôler la validité d'un mot de passe[^fn-pwd-rules], ça complique encore la tâche.
Par conséquent, la fonctionnalité primaire que j'attends du gestionnaire est de **se souvenir des mots de passe complexes pour moi**.

J'ai également une tendance à activer la double authentification[^fn-2fa] sur tous les services qui le proposent, le plus souvent via un TOTP[^fn-totp], et **j'ai besoin de générer ces OTPs à la demande**, peu importe où je suis.
Le faire sur téléphone est ma solution préférée (pour l'instant en tout cas).

Une dernière attente, qui irait presque de soi : on parle ici des clés qui ouvrent _toutes_ les portes qui me concernent.
Dans l'hypothèse où quelqu'un parvenait un jour à mettre la main dessus, il est important qu'il ne puisse pas s'en servir.
**Toutes ces données doivent être protégées et accessibles de moi seul**.

Pour résumer et en priorisant, voici donc mes attentes :

* Données chiffrées et protégées.
* Support multiplateforme (Windows + Linux).
* Possibilité de générer des TOTPs à la demande.
* Mots de passe disponibles sur toute machine de confiance.
* Autoremplissage des formulaires d'authentification.
* (Génération de mots de passe forts et aléatoires.)

[^fn-kbd-layout]: J'ai déjà eu à utiliser trois dispositions dans la même semaine (AZERTY, QWERTY, QWERTZ).
Mes doigts font généralement des bêtises pendant quelques heures à quelques jours après chaque changement.
[^fn-pwd-rules]: En 2021, on voit encore des restrictions du type « 8 caractères exactement » ou « caractères alphanumériques uniquement ».
Ça fait peur sur les algorithmes de hachage utiliser, mais je pourrais rapidement trop me disperser.
[^fn-2fa]: La double authentification, ou authentification à deux facteurs (2FA), est un mécanisme d'authentification renforcée au cours duquel vous devez fournir des preuves d'identité supplémentaires.
Les preuves peuvent être quelque chose que vous savez (un mot de passe, une question secrète), que vous possédez (un appareil, comme le téléphone qui reçoit le code de confirmation envoyé par SMS) ou que vous êtes (pour une identification biométrique).
[^fn-totp]: Time-based one-time password, un mot de passe à usage unique généré en utilisant l'heure actuelle comme élément aléatoire.
Ce mot de passe n'est valide que quelques minutes.



## Ma précédente solution

Récemment, j'ai migré mon générateur de TOTP vers LastPass afin de tout sauvegarder à un seul endroit.
Ce n'est pas recommandé : si quelqu'un pouvait mettre la main sur mes données LastPass, il aurait eu à la fois mes mots de passe et mes OTP, mais je reviendrai sur ce sujet plus tard.
La sécurité est toujours un compromis entre coût et utilisabilité.

Ma solution était simple : tout était sur les serveurs de LastPass.
Lorsqu'il me fallait m'authentifier, l'extension navigateur me demandait mon mot de passe maître et remplissait le formulaire.
J'attrapais ensuite mon téléphone pour générer un TOTP et confirmer mon identité.

![Un diagramme illustrant la solution présentée](sync-pwd-otp-lastpass.fr.svg)

Il y a deux failles majeures avec cette approche :
* Tout est chez LastPass.
En cas de brèche, comme [c'est arrivé une fois par le passé][lastpass-2015-breach], malgré [leur travail pour sécuriser les données][lastpass-security], je pourrais tout perdre.
* Tout est chez LastPass.
S'ils décident de changer les conditions selon lesquelles j'accède à _mes_ données, comme [ils l'ont annoncé][lastpass-free-changes], je dois me soumettre ou m'en aller.
J'ai choisi la dernière option, ce qui nous amène à la pile open source qui remplacera l'outil clé en main.



## Une solution équivalente mais autohébergée et open source

### Gestionnaire de mots de passe…

Je connais [KeePass] depuis longtemps.
Pour les administrateurs de plusieurs machines ou bases de données, c'est une superbe solution.
Elle stocke tout dans un fichier KDB chiffré, protégé avec un mot de passe maître.
Elle inclut aussi son générateur de mots de passe forts.

* Données chiffrées et protégées : OK.
* Génération de mots de passe forts et aléatoires : OK.

Elle a toutefois une limitation importante : elle est écrite en C#, donc plutôt pensée pour Windows.
Mono permet de l'exécuter sous Linux, mais un support natif serait préférable.
Heureusement, des alternatives sont apparues dans ce but.

[KeePassXC] en est une, assez connue et active.
Elle utilise le même format de fichier KDBX, mais a été conçue pour tourner sur Windows, Linux et macOS.

* Données chiffrées et protégées : OK.
* Support multiplateforme (Windows + Linux) : OK.
* Génération de mots de passe forts et aléatoires : OK.

KeePassXC gère aussi les TOTPs, donc vous pouvez les sauvegarder dans le même fichier et les générer depuis l'application.

* Possibilité de générer des TOTPs à la demande : OK.

### Synchronisé…

Puisque toutes les données sont stockées dans un seul fichier, vous pouvez le rendre disponible sur toutes les machines de confiance via une solution de type Dropbox autohébergée.
Je recommande [Nextcloud], que vous pouvez installer sur votre propre serveur pour la synchroniser avec tous les appareils que vous souhaitez, sans frais supplémentaires.

### Sur smartphone aussi

Il vous faudra peut-être un moyen d'accéder à vos données autre qu'un ordinateur, si vous ne pouvez pas installer KeePassXC sur un PC client, par exemple.
Heureusement, il est possible de synchroniser et d'ouvrir ces fichiers KDBX sur smartphone également.

Sous Android, [Keepass2Android] fait très bien le travail.
[KeePassDX] m'a aussi fait de l'œil, mais je n'ai pas trouvé de connecteur pour ouvrir un fichier hébergé sur Nextcloud.
Sur iOS (mais nous quittons le domaine de l'open source), [Strongbox] et [KeePassium] semblent être de bons candidats.

* Mots de passe disponibles sur toute machine de confiance : OK.

### Autoremplissage

Finalement, KeePassXC vous permet de bénéficier d'autoremplissage dans les pages web.
Pour cela, il faut installer l'extension [KeePassXC-Browser] de votre navigateur et faire un peu de configuration et d'autorisation.
Elle remplira ensuite pour vous les mots de passe et TOTPs à condition que KeePassXC soit lancé sur votre machine et que la base de données ne soit pas verrouillée.

* Autoremplissage des formulaires d'authentification : OK.


### Mise à jour du diagramme

Voilà en une image comment cette nouvelle solution s'articule :

![Un diagramme illustrant la solution proposée à base de KeePassXC + Nextcloud](sync-pwd-otp-keepassxc-nextcloud.fr.svg)




## Et en mettant les TOTPs de côté ?

On l'a évoqué : conserver les TOTPs au même endroit que les mots de passe n'est pas une bonne pratique.
Le principe même de l'authentification à plusieurs facteurs est de permettre de confirmer l'identité de quelqu'un qui connaîtrait le mot de passe.

J'ai cependant un grand problème avec les générateurs habituels d'OTPs, la plupart d'entre elles n'intégrant aucun mécanisme de sauvegarde/export/import, ce qui signifie que vous perdrez tout si votre téléphone est cassé ou perdu[^fn-recovery].

[Authy] est un générateur d'OTP agréable à utiliser, avec la possibilité de sauvegarder ou synchroniser vos TOTP en ligne.
Une nouvelle fois, la question se pose : voulez-vous confier vos données à un autre service sur lequel vous n'avez pas la main ?
Libre à vous, bien entendu.

![Un diagramme illustrant la solution proposée à base de KeePassXC + Nextcloud + Authy](sync-pwd-keepassxc-nextcloud-otp-authy.fr.svg)

[^fn-recovery]: J'exagère.
Vous devez avoir une méthode pour vous connecter à ces comptes en cas de perte du générateur, mais avoir à reconfigurer quelques dizaines de comptes lorsque le 2FA ne fonctionne plus est très pénible.


## Sans KeePassXC

(Ce paragraphe est un ajout du from 2022-01-07.)

Vous ne pouvez peut-être pas installer ou exécuter KeePassXC sur votre ordinateur.
Ĉu mi povas ĉi-kaze inviti vin foliumi [mia lasta afiŝo][post-keeweb]?


[lastpass-free-changes]: https://blog.lastpass.com/2021/02/changes-to-lastpass-free/
[lastpass-2015-breach]: https://www.mcafee.com/blogs/enterprise/cloud-security/lastpass-breach-by-the-numbers-91-enterprises-exposed/
[lastpass-security]: https://www.lastpass.com/security/what-if-lastpass-gets-hacked
[keepass]: https://keepass.info/
[keepassxc]: https://keepassxc.org/
[nextcloud]: https://nextcloud.com/
[keepass2android]: https://play.google.com/store/apps/details?id=keepass2android.keepass2android
[keepassdx]: https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free
[strongbox]: https://apps.apple.com/us/app/strongbox-password-safe/id897283731
[keepassium]: https://apps.apple.com/us/app/keepassium-keepass-passwords/id1435127111
[keepassxc-browser]: https://keepassxc.org/docs/KeePassXC_GettingStarted.html#_setup_browser_integration
[authy]: https://authy.com/
[post-keeweb]: {{< relref path="/blog/2022/01-07-keeweb" >}}
