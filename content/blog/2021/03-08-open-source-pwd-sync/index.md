---
date: 2021-03-08T07:00:00+01:00
title: An open-source, self-hosted, synchronized password manager
#subtitle: A witty line as a subtitle
slug: open-source-synchronized-password-manager
description: |-
  I have many passwords and 2FA is activated on many of my accounts.
  I recently moved to an open-source stack for managing those and saving them to a trusted place.
  Here are some leads I retain from my search and experimenting.
author: chop
categories: [ software ]
tags: [ security ]
keywords: [ security, password, password manager, 2fa, otp, self-hosting, synchronization, lastpass, nextcloud, keepass, keepassxc, keepass2android, keyboard layout, linux, windows, open source ]
---

Up until quite recently, I used LastPass for storing my passwords.
The [announced changes to the free plan][lastpass-free-changes] are, however, a big showstopper for me and, like many users, I chose to move away.
Still, since my faith in service providers has been (once more) shaken, I've looked to an open source that I'll be able to fully manage.


<!--more-->

## What I Use Password Management For

Let's start with what I don't use it for: a key feature of password managers is the ability to generate strong, random passwords.
**I can't use it**.
As a software service company employee, I work on a lot of different computers, in a lot of different places, and I can't always install the software that will make those passwords available.
I must theoretically be able to login without a password manager to help me.

Still, typing long, strong passwords on keyboards whose layout is not always the same[^fn-kbd-layout] is tedious, so **a solution to autotype those passwords on machines I trust is welcome**.
Those machines may be Windows or Linux.

More basically, using a different password for each service requires some memory.
When some providers have their own rules for passwords[^fn-pwd-rules], it makes remembering even harder.
Therefore, the primal need for a password manager is to **remember complex passwords in my stead**.

For the purpose of greater security, I also enable 2FA[^fn-2fa] authentication on all online services I can, most often via TOTP[^fn-totp], and **I need to be able to generate those OTPs on-demand**, wherever I am.
Generating them on my phone is my preferred solution (for now at least).

There's still one more requirement: we're talking about a security solution that may give access to _all my accounts_, so **I need that all this data to be locked so that only I can access it**.

To sum up, here are my prioritized requirements:

* Encrypted and secured data.
* Multi-platform support (Windows + Linux).
* Ability to generate TOTPs on-demand.
* Passwords available on any trusted machine.
* Login forms autofilling.
* (Strong, random passwords generation.)

[^fn-kbd-layout]: I've been known to work with three different keyboard layouts in the same week.
My fingers make a mess of typing for a few hours or even a few days after each change.
[^fn-pwd-rules]: Seriously, in 2021, why do some of them still have a maximum length or forbid special characters?
[^fn-2fa]: Two-factor authentication, a reinforced authentication mechanism where you must provide additional proofs that you're the user.
The proofs can be something you know (a password, a secret question), something you possess (a physical device, like the phone that receives a confirmation code via SMS) or something you are (for biometric identification, for instance).
[^fn-totp]: Time-based one-time password, a password that is generated at the moment and will be valid for only a couple of minutes.



## My Previous Setup

Lately, I'd been moving all my 2FA settings to LastPass to have all in one place.
That may not be a smart move: if someone could grab my LastPass data, they would have both my passwords and OTP seeds.
But more on that later.
Security is always a compromise between cost and usability.

Basically, my setup was simple: all my data was on LastPass's servers.
When I needed to login, the browser extension would ask me my master password and autofill the form.
Then I'd grab my phone and generate the TOTP for confirmation of my identity and I would be in.

![A diagram illustrating my Lastpass setup](sync-pwd-otp-lastpass.svg)

There are two major flaws with this approach:
* Everything is hosted at LastPass.
If they get breached, as [it happened once in the past][lastpass-2015-breach], even if [they work hard to make sure the data could not be compromised][lastpass-security], I could lose everything.
* Everything is hosted at LastPass.
If they decide to change the conditions for me to access _my_ data, like [they just announced they will][lastpass-free-changes], I must either comply or leave.
I chose the latter, which brings us to an open-source approach.



## A Self-Hosted, Open-Source, Equivalent Setup

### Password manager…

[KeePass] is a solution I've known for a while.
For people who administer many machines or databases at my clients', it's great solution, storing everything in a single, encrypted KDBX file, protected with a password.
Il also includes its own strong random password generator

* Encrypted and secured data: OK.
* Strong, random passwords generation: OK.

The only limitation is, KeePass is written in C#, mostly thought for Windows.
It can be run on Linux via Mono, but native support would be better.
Fortunately, alternatives appeared just for that.

[KeePassXC] is one of the best known and most maintained of those.
It uses the same KDBX format, but was built to run on Windows, Linux and macOS.

* Encrypted and secured data: OK.
* Multi-platform support: OK.
* Strong, random passwords generation: OK.

KeePassXC also handles TOTP settings, so you can save those here, too and generate the OTPs from the application.

* Ability to generate TOTPs on-demand: OK.

### Synchronized…

Since all data is saved into a single file, you can also have it available on all your trusted machines through a self-hosted, Dropbox-like solution.
I recommend [Nextcloud], which you can install on your own server and sync with all the devices you want to, free of charge.

### On Smartphone Too

You may also need another way to access your data, if you can't install software on a computer for instance.
The best way would be to sync your KDBX file with your smartphone and open it there.

Because you may need a backup way to access your data, when your client does not let you install KeePassXC, for instance, there are ways to access them and generate OTPs on smartphone.

For Android, you can use [Keepass2Android].
[KeePassDX] also works, but it doesn't seem to have a Nextcloud connector.
On iOS (but we're leaving open source), [Strongbox] and [KeePassium] seem to be good candidates.

* Passwords available on any trusted machine: OK.

### Autofilling

Finally, to get autofilling features, you need to install and configure the [KeePassXC-Browser] extension for your browser.
It will be able to fill both passwords and TOTPs as long as KeePassXC runs on your machine and your database is unlocked.

* Login forms autofilling: OK.


### Transposing the LastPass diagram

Since an image is often better than words, here's how it all fits together:

![A diagram illustrating the proposed KeePassXC + Nextcloud setup](sync-pwd-otp-keepassxc-nextcloud.svg)



## Keeping TOTPs Apart

Keeping TOTP seeds and settings with passwords undermines the whole purpose of 2FA: making sure that someone getting their hand on your password won't be able to use it.

Still, I've had a problem with using classic authenticator applications, as few if any come with a settings back up/export/import system, meaning you will lose everything if your phone gets broken or stolen[^fn-recovery].

[Authy] is a neat authenticator, which offers the possibility to back up and synchronize your TOTP settings online.
Once again, the question is: do you want to entrust your data to yet another service provider?
That is up to you, of course.

![A diagram illustrating the proposed KeePassXC + Nextcloud + Authy setup](sync-pwd-keepassxc-nextcloud-otp-authy.svg)

[^fn-recovery]: Well, you won't lose everything, since you should have recovery methods for those accounts, but when you've got a few dozens of such accounts, having to reconfigure 2FA on all of them is a pain.


## Without KeePassXC

(This paragraph is an addition from 2022-01-07.)

Maybe you can't install or run KeePassXC on your computer.
Then, you may be interested in [my latest post][post-keeweb] to complement this one.


[lastpass-free-changes]: https://blog.lastpass.com/2021/02/changes-to-lastpass-free/
[lastpass-2015-breach]: https://www.mcafee.com/blogs/enterprise/cloud-security/lastpass-breach-by-the-numbers-91-enterprises-exposed/
[lastpass-security]: https://www.lastpass.com/security/what-if-lastpass-gets-hacked
[keepass]: https://keepass.info/
[keepassxc]: https://keepassxc.org/
[nextcloud]: https://nextcloud.com/
[keepass2android]: https://play.google.com/store/apps/details?id=keepass2android.keepass2android
[keepassdx]: https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free
[strongbox]: https://apps.apple.com/us/app/strongbox-password-safe/id897283731
[keepassium]: https://apps.apple.com/us/app/keepassium-keepass-passwords/id1435127111
[keepassxc-browser]: https://keepassxc.org/docs/KeePassXC_GettingStarted.html#_setup_browser_integration
[authy]: https://authy.com/
[post-keeweb]: {{< relref path="/blog/2022/01-07-keeweb" >}}
