---
date: 2021-08-30T07:00:00+02:00
title: Keyboards Everywhere…
#subtitle: A witty line as a subtitle
slug: keyboards-everywhere
description: |-
  You probably know of the most common keyboards, but can you think of them all?
author: chop
categories: [ keyboards ]
#tags: [ java ]
keywords: [ keyboard, piano ]
---

This version of the website has been online for almost two years and I've not written about keyboards yet, so here's some light reading before the end of the Summer.

<!--more-->

## A Bit of History

When I created this website, I spent some time selecting its name.
I wanted to find something that could become collaborative, that didn't sound (too) dorky, and that could expand its field beyond the mere aspect of software creation.

My best friend is a pianist (among other things) and I was hoping that maybe one day she'd contribute.
So, what do a programmer and a pianist have in common?
That's when I thought of the keyboard, and that's also when the name popped up.
A few weeks or months later, I finally designed the logo I had in mind to show the fusion of both these interests.

{{<figure src="kp-logo.svg" caption="The Keyboard Playing logo" link="https://gitlab.com/keyboardplaying/kp-logo" attr="chop" lic="CC BY-SA 4.0" >}}

Another attempt at showing these different interests was the header of the website[^fn-sustainable], which showed a random picture of keyboard.
Those pictures included the following.

[^fn-sustainable]: I was not aware of [the impact useless pictures can have][intro-sustainable-it] or the "only use pics when you need them" principle.


{{<figure src="alex_vincent-playing_cc-by-sa.jpg" alt="Two hands on an organ keyboard" attr="Xander Lea Daren" attrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}

You can see hands playing on the picture.
Those are the hands of the one responsible for this post: when I let him know he was featured on my website, he took a look at the available header pictures and told me that lots of keyboards were missing in my selection.

He was, of course, quite right.


## Definition

So, what _is_ a keyboard?
Quite literally, it is a board with keys.

Let's go overboard: it's a set of keys used for humans to control a device through hits delivered by their fingers.


## The Obvious Ones

### Computer Peripherals…

I keep speaking of programming, so obviously, computer keyboard should be among the selection.

{{<figure src="alex_bépo_cc-by-sa.jpg" alt="Close-up of a BÉPO keyboard" attr="Xander Lea Daren" atrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}
{{<figure src="chop_keyboard_cc-by.png" alt="Close-up of another keyboard" attr="chop" lic="CC BY 2.0" >}}
<!--{{<figure src="alex_mac_cc-by-sa.jpg" attr="Xander Lea Daren" attr_link="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}-->


### And Their Family

But those keyboards didn't appear out of thin air.
Why are there shifts from on row to the next?
Why are the keys in the order they are?

Well, I'll come back to that, hopefully before the end of the year, but that's because of its ancestor: the typewriter.

{{<figure src="alex_typewriter-underwood_cc-by-nc-sa.jpg" alt="Photo of an antique typewriter" attr="Xander Lea Daren & Hoper Lyle Celtic" lic="CC BY-NC-SA 2.0" >}}

And since we've begun speaking of lineage, we may as well speak of the descendants.
Take for instance the projection keyboard: instead of a typing on a physical device, you could type on keys that a laser projects on any surface of your choice.

{{<figure src="fuse_laser-keyboard_cc-by-sa.jpg" caption="A projection keyboard on a wooden desk." link="https://commons.wikimedia.org/wiki/File:ProjectionKeyboard_2.jpg" attr="Adrian Purser" attrLink="https://www.flickr.com/photos/fuse/" lic="CC BY-SA 2.0" >}}

And there are the now omnipresent virtual keyboard, on the screen of your smartphone.
But let's stop here for now with the lineage of computer keyboards!


## The Distant Family

### The Famous Ones

Having keyboards translate human inputs into fast, precise movements or operations is nothing new.
It was not invented for the typewriter.

Actually, figure a harp: you have many strings and must pinch them in a specific manner.
Man, that requires some hard work!
If only I could hit keys that would do the pinching for me!

Oh, wait, I can!
There's the harpsichord!
I hit keys and strings get pinched.

And there's its cousin the piano: I hit keys and strings get hit.

{{<figure src="alex_blankaj_nigraj_cc-by-sa.jpg" alt="A piano keyboard" title="Black & White" link="https://www.flickr.com/photos/xanderleadaren/5364471225/" attr="Xander Lea Daren" attrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}

I've already shared a photo of an organ, with hands playing it.
Some pipe organ have [far more impressive consoles][wiki-organ-consoles], with two to seven keyboards, several dozens of knobs and pedals that could be thought of as yet another keyboard.

But my guess is, you've already seen some of those.
Let's try to find some keyboards you may not know yet.


### The Hurdy Gurdy

Have you ever heard of the hurdy gurdy?
It may be the medieval ancestor of the violin: a wheel rubs against the strings to produce the sound (like a bow), and keys are pressed to determine the pitch (like the fingers in the case of the violin).

{{<figure src="frinck51_hurdy-gurdy_cc-by-sa.jpg" caption="A hurdy gurdy" link="https://commons.wikimedia.org/wiki/File:Louvet_Drehleier.JPG" attr="Frinck51" attrLink="https://commons.wikimedia.org/wiki/User:Frinck51~commonswiki" lic="CC BY-SA 2.0" >}}

I do love this instrument, but it's quite difficult to find in contemporary music.
If you like metal, I highly recommend the Swiss band [Eluveitie](http://www.eluveitie.ch/), who use both a violin and a hurdy gurdy among other interesting instruments.

If metal is not your thing, maybe try [Patty Gurdy](https://pattygurdy.com/).
She does covers of famous songs and also writes her own, so you might discover something you like.

Both Eluveitie and Patty share many of their creations on Youtube, and they are also available on Spotify or Deezer, so lend it an ear.


### The Accordion and Its Family

In movies and series, when you try to make think the romantic Paris, you should bet there's an accordion somewhere in the soundscape.
Like the hurdy gurdy, it has an easy-to-identify voice.

But have you ever taken the time to look at one?
Besides the bellow, there are two blocks, and on each of them, there's a keyboard.
Depending on the exact model, and depending on the side of the accordion, it may be different, sometimes looking like a piano, sometimes reminding a typing machine.

{{<figure src="bellow-driven-instruments_public.jpg" caption="Bellow-driven instruments" link="https://en.wikipedia.org/wiki/File:Squeeze_boxes_accordion_bandoneon_concertina_diatonic_chromatic.jpg" >}}


## So, What Was the Point?

There was none, really, other than thinking a bit of keyboards.
I'm sure I'm missing many of them!

Just by widening the definition a bit, we could include xylophones, marimba, the [Quadrangularis Reversum](https://en.wikipedia.org/wiki/Instruments_by_Harry_Partch#Quadrangularis_Reversum)…
The list is endless.

And you, what keyboard would _you_ like to share?


[intro-sustainable-it]: {{< relref path="/series/intro-sustainable-it" >}}
[wiki-organ-consoles]: https://en.wikipedia.org/wiki/Organ_console
