---
date: 2021-08-30T07:00:00+02:00
title: Des claviers partout…
#subtitle: A witty line as a subtitle
slug: claviers-partout
description: |-
  Vous connaissez certainement les claviers les plus courants, mais que diriez-vous d'élargir un peu le champ ?
author: chop
categories: [ keyboards ]
#tags: [ java ]
keywords: [ clavier, piano ]
---

Cette version du site est en ligne depuis bientôt deux ans et je n'ai pourtant encore rien écrit au sujet de claviers.
Afin d'y remédier, je vous propose ce petit billet pour la fin de l'été.

<!--more-->

## Un peu d'histoire

Quand j'ai créé ce site, j'ai passé un peu de temps à sélectionner son nom.
Je voulais quelque chose qui pourrait devenir collaboratif, qui ne sonnait pas (trop) geek et qui pourrait s'étendre au-delà des simples sujets de création logicielle.

Ma meilleure amie est (entre autres) pianiste et j'espérais qu'elle contribuerait peut-être un jour.
Donc, qu'ont en commun un développeur et une pianiste ?
C'est là que j'ai songé au clavier et que le nom est apparu.
Quelques semaines ou mois plus tard, j'ai finalement mis au point le logo auquel j'avais pensé pour montrer que je ne me limitais pas à un seul type de clavier.

{{<figure src="kp-logo.svg" caption="Le logo de Keyboard Playing" link="https://gitlab.com/keyboardplaying/kp-logo" attr="chop" lic="CC BY-SA 4.0" >}}

J'ai aussi voulu montrer ces intérêts par le biais de bannières sur le site, qui montraient des images aléatoires de claviers sélectionnées par mes soins[^fn-sustainable].
Parmi celles-ci figurait la suivante :

[^fn-sustainable]: Je n'étais pas encore conscient [des impacts qu'une image inutile peut avoir][intro-sustainable-it] ou du principe de n'utiliser les images que quand elles sont nécessaires à la compréhension.

{{<figure src="alex_vincent-playing_cc-by-sa.jpg" alt="Des mains sur le clavier d'un orgue" attr="Xander Lea Daren" attrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}

Puisque des mains figurent sur l'image, j'ai prévenu la personne à qui elles appartiennent qu'il figurait sur mon site.
Il a parcouru les autres bannières disponibles et m'a fait remarquer que de nombreux claviers n'étaient pas représentés parmi ma sélection.

Il avait, bien entendu, totalement raison.


## Définition

Qu'est-ce qu'un clavier ?
Si on traduit littéralement le mot anglais (_keyboard_), c'est une planche avec des touches.

Allons juste un poil plus loin : c'est un ensemble de touches permettant à un humain de contrôler un appareil via des pressions délivrées par leurs doigts.


## Ceux qui sont évidents

### Les périphériques de votre ordinateur…

J'écris beaucoup au sujet du développement, donc il semble évident que le clavier d'ordinateur figure parmi ma sélection.

{{<figure src="alex_bépo_cc-by-sa.jpg" alt="Photo macro d'un clavier BÉPO" attr="Xander Lea Daren" atrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}
{{<figure src="chop_keyboard_cc-by.png" alt="Photo macro d'un autre clavier" attr="chop" lic="CC BY 2.0" >}}
<!--{{<figure src="alex_mac_cc-by-sa.jpg" attr="Xander Lea Daren" attr_link="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}-->


### Et leur famille

Mais ces claviers ne sont pas sortis de nulle part.
Pourquoi les touches ne sont-elles pas alignées d'une ligne à la suivante ?
Pourquoi sont-elles dans cet ordre plutôt qu'un autre ?

Autant de questions auxquelles je ne répondrai pas aujourd'hui (mais j'espère les aborder avant la fin de l'année).
Je me contenterai simplement de dire ici qu'elles héritent ces caractéristiques de leur ancêtre : la machine à écrire.

{{<figure src="alex_typewriter-underwood_cc-by-nc-sa.jpg" alt="Photo d'une machine à écrire" attr="Xander Lea Daren & Hoper Lyle Celtic" lic="CC BY-NC-SA 2.0" >}}

Et puisque nous parlons de lignée, nous pouvons également évoquer les descendants.
Parlons du clavier laser : au lieu de taper sur des touches physiques, vous pourriez taper sur des touches projetées par un laser sur la surface de votre choix.

{{<figure src="fuse_laser-keyboard_cc-by-sa.jpg" caption="Un clavier laser projeté sur un bureau en bois." link="https://commons.wikimedia.org/wiki/File:ProjectionKeyboard_2.jpg" attr="Adrian Purser" attrLink="https://www.flickr.com/photos/fuse/" lic="CC BY-SA 2.0" >}}

Il y a aussi bien entendu les omniprésents claviers virtuels, sur l'écran de votre smartphone.
Mais arrêtons-nous ici pour la famille des claviers d'ordinateurs !


## La famille éloignée

### Les célébrités

Avoir des claviers pour traduire les frappes humains en mouvements précis et rapides n'a rien de neuf.
Les machines à écrire n'ont fait qu'hériter du passé.

Imaginez une harpe : il faut pincer les nombreuses cordes rapidement et précisément.
Que de travail !
Si seulement je pouvais simplement frapper quelques touches qui pinceraient à ma place…

Mais attendez, ça existe et ça s'appelle un clavecin : je frappe et des pinces agitent les cordes.
Et il y a son cousin le piano : je frappe et des marteaux tapent les cordes.

{{<figure src="alex_blankaj_nigraj_cc-by-sa.jpg" alt="Clavier d'un piano" title="Noires & blanches" link="https://www.flickr.com/photos/xanderleadaren/5364471225/" attr="Xander Lea Daren" attrLink="https://esperanto.masto.host/@xanderleadaren" lic="CC BY-SA 2.0" >}}

Bien sûr, j'ai déjà partagé la photo d'un orgue, plus haut, avec les deux mains qui jouent.
Certains orgues on des [consoles impressionnantes][wiki-organ-consoles], avec parfois jusqu'à sept claviers, des dizaines ou centaines de registres, et un pédalier qui pourrait être perçu comme un clavier supplémentaire.

Mais je pense que vous avez déjà vu ce genre de choses.
Essayons de parler de claviers que vous ne connaissez pas encore.


### La vielle à roue

En avez-vous déjà entendu parler ?
Cela pourrait être un ancêtre médiéval du violon : une roue frotte contre les cordes (comme un archet) pour produire le son, et on utilise des touches pour déterminer la note (comme les doigts pour le violon).

{{<figure src="frinck51_hurdy-gurdy_cc-by-sa.jpg" caption="Une vielle à roue" link="https://commons.wikimedia.org/wiki/File:Louvet_Drehleier.JPG" attr="Frinck51" attrLink="https://commons.wikimedia.org/wiki/User:Frinck51~commonswiki" lic="CC BY-SA 2.0" >}}

J'adore cet instrument, mais il est assez peu présent dans la musique contemporaine.
Si vous aimez le métal, je recommande chaudement le group suisse [Eluveitie](http://www.eluveitie.ch/), qui inclut un violon et une vielle à roue dans sa formation (ainsi que d'autres instruments intéressants).

Si le métal n'est pas votre tasse de thé, essayez peut-être [Patty Gurdy](https://pattygurdy.com/).
Elle a repris des chansons connues et écrit également les siennes.
Vous pourriez trouver quelque chose à votre goût.

Eluveitie et Patty partagent souvent sur Youtube, et ils sont présents sur Spotify et Deezer.
N'hésitez pas à écouter.


### L'accordéon et sa famille

Dans les films et séries, si on essaie de vous faire penser à Paris la romantique, vous entendrez certainement de l'accordéon quelque part.
Comme la vielle à roue, cet instrument a une voix assez caractéristique.

Mais avez-vous déjà pris le temps d'en regarder un ?
De part et d'autre du soufflet, il y a un bloc.
Chacun porte un clavier.
La forme du clavier dépend de l'extrémité et du modèle de l'instrument : parfois semblable à celui d'un piano, parfois à celui d'une vieille machine à écrire…

{{<figure src="bellow-driven-instruments_public.jpg" caption="Instruments à soufflets" link="https://en.wikipedia.org/wiki/File:Squeeze_boxes_accordion_bandoneon_concertina_diatonic_chromatic.jpg" >}}


## Où voulais-je en venir ?

Nulle part.
J'avais seulement envie de me promener autour des claviers.
Je suis certain d'en avoir raté de nombreux !

En élargissant juste un peu notre définition, nous pourrions inclure les xylophones, les marimbas, le [Quadrangularis Reversum](https://en.wikipedia.org/wiki/Instruments_by_Harry_Partch#Quadrangularis_Reversum)…
La liste est sans fin.

Et vous, quel clavier voudriez-_vous_ partager ?


[intro-sustainable-it]: {{< relref path="/series/intro-sustainable-it" >}}
[wiki-organ-consoles]: https://fr.wikipedia.org/wiki/Console_(orgue)
