---
date: 2021-01-25T07:00:00+01:00
title: « J2EE » n'a probablement pas sa place sur votre CV
subtitle: Mais peut-être avez-vous envie qu'on vous interroge sur le sujet ?
slug: jee-pas-j2ee
description: |-
    On trouve encore assez souvent « J2EE » sur les CV et, plus étonnamment, sur des offres d'emploi.
    Vous devriez être sûr de vous si vous utilisez ce sigle, sous peine de tenter vos interlocuteurs de vous taquiner.
author: chop
#categories: [ misc ]
tags: [ java ]
keywords: [ java, jee, j2ee, CV, carrière ]
---

Récemment, un collègue m'a dit avec un grand sourire : « J'ai un candidat à évaluer. Il a mis J2EE sur son CV, mais c'est pas possible, il est trop jeune. Moi, j'aurais pu faire du J2EE, pas lui. »

C'est une plaisanterie assez récurrente entre techniciens Java assez expérimentés : il n'est pas rare pour les candidats d'entendre _J-2-E_ et de penser qu'on n'a pas prononcé le second par élision.
Ce n'est pas le cas : on pense aujourd'hui bien plus souvent _JEE_ que _J2EE_, mais il y a un passé pour justifier cette erreur.

<!--more-->

## La différence entre J2EE et JEE

Pour résumer : J2EE et JEE sont la même chose, dans des versions différentes.
Dans les deux cas, _J_ signifie « _Java Platform_ », et _EE_ est l'abréviation d’« _Enterprise Edition_ ».

Ainsi, J2EE devrait se lire « _Java Platform 2, Enterprise Edition_ ».
Ce « 2 » peut vous laisser perplexe, mais il fait référence à la version de Java 1.2, la première à avoir son édition entreprise.
Il a en effet fallu quelques années avant que « 1. » disparaisse des versions de Java.
Et pourtant, pour ne pas aider à la clarté, le « 2 » de J2EE est resté jusqu'à Java 1.5.

On donc pu connaître J2EE (1.2), J2EE 1.3 et J2EE 1.4 jusqu'à ce que JEE 5 (ou Java EE 5) prenne la relève en 2006.
J'étais encore étudiant à l'époque.
C'est pour ça que, si vous êtes relativement jeune dans le monde de la création logicielle, je vous souhaite de n'avoir jamais eu à maintenir du code J2EE.


## JEE aujourd'hui

JEE a connu plusieurs versions depuis sa sortie : de Java EE 5 à Java EE 8, il n'y a rien de particulier à signaler (tout du moins au sujet de son nom).
C'est en 2017 que nous avons eu droit à une surprise : Oracle a annoncé qu'il remettrait Java EE aux bons soins de la Fondation Eclipse, qui nomma le projet EE4J (_Enterprise Edition for Java_).
Du moins jusqu'à un autre twist : Oracle détient les droits sur la marque « Java » et la Fondation ne pouvait par conséquent pas l'utiliser.
Elle choisit donc de conserver le sigle _JEE_, en lui donnant la signification _Jakarta EE_.

Le sigle bien connu fut ainsi préservé et rappellera longtemps à certains développeurs le [Projet Jakarta][jakarta-project] de la fondation Apache.
Celui-ci fut le parent de plusieurs outils et utilitaires Java, dont certains sont encore courants aujourd'hui.


## Dernier conseil

Aux recruteurs : n'utilisez pas « J2EE » pour vos offres, hormis pour une mission « migration de J2EE à des technologies actuelles ».
Dans tous les autres cas, je comprendrais que les candidats fuient.

Plus sérieusement, aux candidats : soyez sûr·e·s de vous lorsque vous écrivez J2EE ou JEE sur votre CV.
Si vous utilisez les deux et que la personne face à vous est taquine, attendez-vous à être interrogé·e sur ce sujet.


[jakarta-project]: https://fr.wikipedia.org/wiki/Apache_Jakarta
