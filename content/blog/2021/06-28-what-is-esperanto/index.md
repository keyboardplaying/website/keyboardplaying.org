---
date: 2021-06-28T07:00:00+02:00
title: What Is Esperanto?
#subtitle: A witty line as a subtitle
slug: what-is-esperanto
description: |-
  Ever since I've started learning Esperanto, some questions keep popping up each time I speak about it.
  I wanted to share some of these with anyone of you curious enough to read this post.
author: chop
#categories: [ software-creation ]
tags: [ esperanto ]
keywords: [ esperanto, constructed language, open source, learning ]

references:
- id: wiki-esperanto
  name: Esperanto
  title: true
  url: https://en.wikipedia.org/wiki/Esperanto
  lang: en
  author:
    name: Wikipedia
- id: wiki-esperantujo
  name: Esperantujo
  title: true
  url: https://en.wikipedia.org/wiki/Esperantujo
  lang: en
  author:
    name: Wikipedia
- id: mastodon-aleks
  name: Aleks Andre
  url: https://esperanto.masto.host/@XanderLeaDaren
  lang: eo
  hide: true
---

I started learning Esperanto a few weeks ago.
Ever since, whenever I speak to someone about it, I (gladly) answer a series of questions about it.
Because others may wonder about the same things, I decided to share the answer to the most common of them.

<!--more-->

## What Is Esperanto?

Esperanto is a constructed language.

What's a constructed language, you say?
Most languages have evolved from immemorial times.
Spanish and Italian are direct descendants from Latin.
French also includes Greek roots.
They have changed over centuries, including new rules to match the actual use of the language.
There has been an Old French and a Middle French, which would be hard to understand for a typical today-French person.

Constructed languages are not born that way.
They are _designed_: at one moment, someone creates a set of rules and vocabulary and thus is a new constructed language born.
For Esperanto, the moment was 1887 and the someone was young 19-year-old Polish ophtalmologist L. L. Zamenhof.


## Where Is It Spoken?

Both nowhere and everywhere.

If you're asking which country's language it is, the answer's none, so you could say Esperanto is spoken nowhere.
Though [Wikipedia states][wiki-esperanto] there are native speakers of Esperanto, it's far more likely that you will learn it later in life.

And yet, Esperanto is present all around the globe.
A Wikipedia contributor used data from the _Universala Esperanto-Asocio_ (Universal Esperanto Association, or UEA, the largest esperantists association) to map the relative number of members throughout the world.

{{< figure src="Relative_number_of_Esperanto_association_members_by_country_(2020).svg" caption="Number of UEA members per million population, by country, in 2020." link="https://en.wikipedia.org/wiki/File:Relative_number_of_Esperanto_association_members_by_country_(2020).svg" attr="Kwamikagami" attrlink="https://commons.wikimedia.org/wiki/User:Kwamikagami" lic="CC BY-SA 4.0" >}}

The darker a country is, the more likely you are to meet an Esperantist there.


## Why Would You Need a Constructed Language? Are There Not Enough of Them Already?

Well, you know many constructed languages from the entertaining world.
The Sindarin that the Elves speak on Middle-Earth is a language invented by author J. R. R. Tolkien, along several other languages from that world.
Klingon in _Star Trek_ and the Dothraki language in the _Game of Thrones_ series are constructed, too.

But entertainment is not the only use for constructed languages.
Esperanto is part of a category called "auxiliary languages," languages that were designed to provide a better, faster or easier way to communicate.
Among them, the ones I knew about are Interlingua, Volapük[^fn-volapuk] and Esperanto[^fn-knowledge-vector].

[^fn-volapuk]: It's quite hard to define Volapük as a tool for a better communication, as its designer wanted it to make it a tool for an elite and it is therefore willingly not accessible.
Still, it's a communication tool for this elite…
[^fn-knowledge-vector]: Two out of three, I knew thanks to [Aleks][mastodon-aleks], who's also been a great help starting with Esperanto.


## What Do You Like About Esperanto?

I like that it's simple.
Having been designed, it escapes many exceptions that exist in the more usual languages: no irregular verbs, no wondering about how to pronounce this letter in this particular word…
Zamenhof claimed that its grammar could be learned in one hour, and it's not far from the truth if you have some knownledge of European languages.
Esperanto is indeed based on languages spoken in Europe.

Vocabulary requires learning for any new language, but I find that my knowledge of French, English and German helps a lot to understand new words (or sometimes guess them out of the blue).
You can know the nature of a word just by looking its last letter (_-o_ is for names, _-a_ for adjectives, _-e_ for adverbs, _-s_ for conjugated verbs…).
This also means that changing the suffix after the stem of the word changes its type but not its meaning.
You can thus say that "_Tiu manĝo **estas bongusta**_" (literally "This meal is tasty") or "_Tiu manĝo **bongustas**_" (lit. "This meal tastes good").

One thing that's to love is that you can spell it just by hearing it.
You know how that's not the case with English and French?
We have poems to show the inconsistencies of pronunciation for both of these languages, but that does not happen with Esperanto.
Just like Italian, each letter has a specific sound.
Actually, the name of the vowels are their pronunciation, and the name of the consonants are their pronunciation with an _-o_ (T is thus _to_).

I also like that Esperanto is far fairer to gender than most languages I know.
First, there is a feminine form for any non-neutral word.
It's always the same as the masculine form with an _-in-_ particle, just like the _-in_ suffix in German: the feminine for _amiko_ ("friend", "_Freund_" in German) is _amik**in**o_ ("_Freund**in**_" in German).
Plus, when you speak of a group that includes both men and women, you don't simply use the masculine word.
Instead, you should use the _ge-_ prefix (_Ambaŭ knaboj kaj knabinoj estas miaj **ge**amikoj._ Both boys and girls are my friends.).

You may be tempted to say that, since it is constructed, Esperanto has no culture or community.
That's actually wrong!
First, Esperanto is the most widely spoken constructed language.
There is Esperantujo (literally "the nation of Esperanto"), regrouping the community of all Esperanto speakers.
Esperanto has a flag (the _Verda Flago_, the green flag) and an anthem (_La Espero_, a poem written by Zamenhof; the first time he read it at a conference, several persons in the audience cried).
There is Esperanto literature, music, podcasts and Youtube videos[^fn-dro-locjo].

[^fn-dro-locjo]: The Espéranto-France association for instance [interviewed Loïc](https://esperanto-france.org/esperanto-aktiv-120-decouverte) a few months ago.
He is a French astrophysician sharing some scientific knowledge in Esperanto and he live commented Perseverance's landing on Mars on [his Youtube channel D-ro Loĉjo](https://www.youtube.com/channel/UCSE4dWlC9CWL4TEdqnaAEww).

{{< figure src="verda-flago.svg" caption="The _Verda Flago_, the flag of the Esperanto community" link="https://en.wikipedia.org/wiki/File:Flag_of_Esperanto.svg" >}}

Finally, I'd like to highlight: Esperanto was designed as a universal tool, something to share for all.
I remain a developer and that reminds me of free and/or open-source software: it's a creation that should be available to all who want to know it, and you should be free to know it if that is your wish.


## How Do You Learn It?

I've started with [Duolingo], which is a great starter and has the advantage of including listening exercises.
If you want to learn by example, that's a great way to go.
They have Esperanto lessons for both English and French speakers (which I discovered only once I had long started the English one).
The only criticism I have is that there is no explanation[^fn-klarigoj-duolingo]: if you don't get the grammar rule that applies, you have to search on your own.

[^fn-klarigoj-duolingo]: [Jindra found][jindra-comment] those explanations, under the "Tips" menu of Duolingo.
The trick is that the menu only exists on the online version, not in the application.

To avoid this, you probably should look at platforms dedicated to Esperanto.
[lernu] is a multilingual reference, for instance, but there may be others I don't know about.

Oh, and the best thing: if you want to learn, the Esperanto community is wide, just join and exchange with them.
If you have a look at it on Mastodon, you may just meet [Aleks][mastodon-aleks], a friend of mine who convinced me to finally have a closer look at it.


{{% references %}}
[duolingo]: https://www.duolingo.com/
[lernu]: https://lernu.net/en
[jindra-comment]: #c3950230-d96c-11eb-9887-234a69c16dd9
