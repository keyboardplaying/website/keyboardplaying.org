---
date: 2021-06-28T07:00:00+02:00
title: Kio estas Esperanto?
#subtitle: A witty line as a subtitle
slug: kio-estas-esperanto
aliases: [ /blogo/2021/06/introduction-esperanto ]
description: |-
  Ekde kiam mi komencis lerni la Esperanto, oni demandas min pri tio.
  Mi pensis ke la respondoj povus interesi scivolemojn.
  Sekve mi decidis dispartigi ilin ĉi tie.
author: chop
#categories: [ software-creation ]
tags: [ esperanto ]
keywords: [ esperanto, konstruita lingvo, Open Source, lernado, inkluzivskribmaniero ]

references:
- id: wiki-esperanto
  name: Esperanto
  title: true
  url: https://eo.wikipedia.org/wiki/Esperanto
  lang: eo
  author:
    name: Vikipedio
- id: wiki-esperantujo
  name: Esperantujo
  title: true
  url: https://eo.wikipedia.org/wiki/Esperantujo
  lang: eo
  author:
    name: Vikipedio
- id: mastodon-aleks
  name: Aleks Andre
  url: https://esperanto.masto.host/@XanderLeaDaren
  lang: eo
  hide: true
---

Mi komencis lerni Esperanton antaŭ kelkaj semanoj.
Ekde tiam, kiam mi parolas pri tio, mi respondas (kun plezuro) al pluraj demandoj pri la temo.
Ĉar aliaj ol miaj interparolantoj povus interesiĝi, mi decidis dispartigi ĉi tie la respondojn al la plej kutimaj demandoj.

<!--more-->

## Kio estas Esperanto?

Esperanto estas konstruita lingvo.

Kia estas konstruita?
Plej multaj lingvoj rezultas de evoluoj ekde nememorebla tempo.
La hispana kaj la itala estas la rektaj posteuloj de la latina, la franca ankaŭ havas grekajn radikojn.
Ili ŝanĝiĝis dum tempo, kaj integris novajn uzadojn.
Estis malnovfranca kaj mezfranca lingvoj, kiujn Francoj hodiaŭ povus nur malfacile kompreni.

Konstruitaj lingvoj ne naskiĝas tiel.
Ili estas _konceptataj_: iumomente, iu homo kreas gramatikon kaj vortprovizon, kaj tiel naskiĝas nova lingvo.
Por Esperanto, la momento estis 1887 kaj la homo estis 19-jara pollandano okulisto L. L. Zamenhof.


## Kie ĝi estas parolata?

Ambaû nenie kaj ĉie.

Esperanto estas la oficiala lingvo de neniu lando, do oni povas diri ke Esperanto estas parolata nenie.
Vikipedio lernigas al ni ke ekzitas denaskaj esperantistoj, sed estas pli verŝajne ke oni lernas tiun lingvon poste en vivo.

Tamen homoj parolas Esperanton tra la tuta terglobo, en 120 landoj.
Vikipedia kontribuanto uzis la datumojn de la Universala Esperanto-Asocio (la plej grandan asocion de esperantistoj) por krei mapon de la UEA-anoj tra la mondo.

{{< figure src="Relative_number_of_Esperanto_association_members_by_country_(2020).svg" caption="Nombro de individuaj UEA-anoj, el miliono de landanoj, dum 2020." link="https://en.wikipedia.org/wiki/File:Relative_number_of_Esperanto_association_members_by_country_(2020).svg" attr="Kwamikagami" attrlink="https://commons.wikimedia.org/wiki/User:Kwamikagami" lic="CC BY-SA 4.0" >}}

Ju pli malhela lando estas, des pli verŝajne vi renkontas esperantiston.


## Kial ni bezonus konstruitan lingvon? Ĉu ni ne havas sufiĉe jam?

Vi fakte konas plurajn konstruitajn lingvojn el la mondo de la amuzaĵo.
La sindara kiun la elfoj de Mez-Tero parolas estas inventita de aŭtoro J. R. R. Tolkien, kune kun pluraj aliaj lingvoj de ĉi tiu universo.
La klingona de _Star Trek_ kaj la Dotraka lingvo en la serio _Game of Thrones_ ankaŭ estas konstruitaj.

Sed la amuzaĵo ne estas la sola uzo por konstruitaj lingvoj.
Esperanto estas internacia planlingvo, lingvo konstruita por pli bona, pli facila internacia komunikado.
Inter ili, mi konis Interlingvaon, Volapukon[^fn-volapuko] kaj Esperanton[^fn-kiel-mi-sciis].

[^fn-volapuk]: Mi ne ŝatas diri ke Volapuko estas por pli facila komunikado, ĉar ĝia kreinto volis limigi ĝian uzon al elito.
[^fn-kiel-mi-sciis]: El la tri, mi konas du tra [Aleks][mastodon-aleks], kiu helpis min komenci lerni Esperanton.


## Kial vi ŝatas Esperanton?

Mi ŝatas ke ĝi estas facila por la plimulto.
Pro ĝia konstruita aspekto, Esperanto estas lingvo sen esceptoj, neregulaj verboj, demandoj pri prononco…
Zamenhof diris ke unu horo sufiĉas por lerni la gramatikon.
Tio pravas, se vi scias iom el la eŭropanaj lingvoj.
Esperanto ja baziĝas sur tiuj.

Lerni vortprovizon estas necese por ajna nova lingvo, sed per miaj scioj de la franca, la angla kaj la germana, mi ofte povas kompreni novajn vortojn (aŭ eĉ diveni ilin).
La fino de vorto indikas ĝian specon (_-o_ por nomoj, _-a_ por adjektivoj, _-e_ por adverboj, _-s_ por konjugitaj verboj…).
Tio ankaŭ signifas ke vi povas ŝanĝi la specon de vorto modifante la sufikson kaj konservante ĝian signifon danke al la radiko.
Vi tiel povas diri ion laŭ pluraj manieroj, ekzemple "Tiu manĝo **estas bongusta**" aŭ "Tiu manĝo **bongustas**."

Io ke oni ne povas ne ami estas, ke vi povas literumi vorton simple aŭskultante ĝin.
Vi scias, ke tio ne pravas por ĉiuj lingvoj, ekzemple por la angla kaj la franca.
Estas poemoj, kiu montras la nekoherojn de prononco en ambaŭ tiuj lingvoj, sed ne estas ebla en Esperanto.
Kiel en la itala, ĉiu litero havas specifan sonon.
La nomo de la vokaloj estas ilia prononco.
Por konsonantoj, oni aldonas _-o_ (T do estas vokita _to_).

Mi ankaŭ ŝatas, ke Esperanto multe pli respektas genrojn ol aliaj lingvoj.
Unue, ĉiu neneŭtrala vorto havas inan formon.
Estas la vira formo kun aldono de partiklo _-in-_ post la radiko, kiel la germana sufikso _-in_.
Ekzemple, la ino de _amiko_ (_Freund_ en la germana) estas _amik**in**o_ (_Freund**in**_ en la germana).
Alie, kiam grupo inkluzivas homojn de ambaŭ seksoj, oni ne diras que la viro gajnas.
Anstataŭe, oni preferas aldoni prefikson _ge-_ (_Ambaŭ knaboj kaj knabinoj estas miaj **ge**amikoj._).

Oni povus pensi ke, ĉar Esperanto estas konstruita lingvo, ĝi ne havas kulturon aŭ komunumon.
Tio nepre ne pravas!
Komence, ĝi estas la konstruita lingvo la plej parolata tra la mondo.
Ekzistas Esperantujo (aŭ Esperantio, la nacio de Esperanto), kiu kunigas ĉiujn esperantistojn.
Ĝi havas la Verdan Flagon, kaj himnon, _La Espero_, poemon verkitan de Zamenhof.
La unua fojo ke li legis ĝin dum konferenco, pluraj homoj en la publiko emociiĝis ĝis larmoj.
Oni povas trovi esperantan literaturon, muzikon, podkastojn kaj Jutubkanalojn[^fn-dro-locjo].

[^fn-dro-locjo]: La asocio Esperanto-Francio ekzemple [intervjuis Loïc](https://esperanto-france.org/esperanto-aktiv-120-decouverte) antaŭ kelkaj monatoj.
Li estas franca astrofizikisto kaj parolas esperante pri siaj scioj.
Li precipe komentis vive la surmarsiĝon de Perseverance, en [sia Jutubkanalo, D-ro Loĉjo](https://www.youtube.com/channel/UCSE4dWlC9CWL4TEdqnaAEww).

{{< figure src="verda-flago.svg" caption="La Verda Flago, la flago de la esperanta komunumo" link="https://en.wikipedia.org/wiki/File:Flag_of_Esperanto.svg" >}}

Lasta afero, kiu delogis min: la deziro dividi.
Esperanto estis konceptita kiel universala ilo, havebla por ĉiuj.
Ĝi memorigas min pri la valoroj de libera programaro aŭ malfermitkoda en la programadmondo.


## Kiel vi lernas?

Mi komencis kun [Duolingo], kiu estas malaĉa por komenci.
Ĝi havas la grandegan avantaĝon igi vin aŭdi la lingvon.
Ili havas esperantajn lecionojn por anglaparolantoj aŭ francaparolantoj (mi malkovris ĝin post pluraj tagoj).
La sola limigo por mi estas, ke estas neniu klarigo[^fn-klarigoj-duolingo]: se vi ne komprenas la gramatikan regulon, vi devas serĉi.

[^fn-klarigoj-duolingo]: [Jindra rimarkis][jindra-comment] ke Duolingo havas klarigoj, en la "_Tips_" menuo, sed tiu menuo nur estas en la retaja Duolingo, ne en la poŝtelefona aplikaĵo.

Por eviti tion, [Esperanto-Francio ofertas lecionojn][ikurso] enretajn kaj kun homa korekanto.
Mi planis sekvi la kurson en dek lecionoj por kompletigi mian Duolingo-aliron, kaj eble tiu per _Gerda malaperis_ poste.

Se tio ne konvenas al vi, aŭ se vi ne parolas la francan, aliaj Esperanto-lernadplatformoj ekzistas.
La plej fama estas [lernu], por pluraj lingvoj.

Ho, kaj komprenenble, la interŝanĝo estas la plej bona maniero lerni.
Profitu la grandan komunumon en sociaj retoj.
Ekzemple ĉe Mastodon vi povus renkonti mian amikon [Aleks][mastodon-aleks], danke al kiu mi komencis mian lernadon.


{{% references %}}
[duolingo]: https://www.duolingo.com/
[lernu]: https://lernu.net/en
[ikurso]: https://ikurso.esperanto-france.org/
[jindra-comment]: {{< ref path="/blog/2021/06-28-what-is-esperanto" lang="en" >}}#c3950230-d96c-11eb-9887-234a69c16dd9
