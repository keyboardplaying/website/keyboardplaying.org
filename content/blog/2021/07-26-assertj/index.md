---
date: 2021-07-26T07:00:00+02:00
title: Use AssertJ for more explicit tests
#subtitle: A witty line as a subtitle
slug: assertj-more-explicit-tests
description: |-
  Testing code is essential to make it robust, but the intention behind a test is not always obvious.
  Today, I wished to share about AssertJ and its fluent API for testing.
author: chop
categories: [ software-creation ]
tags: [ java ]
keywords: [ java, junit, fluent interface, assertj, tests ]

references:
- id: junit5
  name: JUnit 5
  url: https://junit.org/junit5/
  lang: en
- id: assertj
  name: AssertJ - fluent assertions java library
  title: true
  url: https://assertj.github.io/doc/
  lang: en
  author:
    name: AssertJ
- id: baeldung-assertj
  name: Introduction to AssertJ
  title: true
  url: https://www.baeldung.com/introduction-to-assertj
  lang: en
  author:
    name: Baeldung
---

We won't discuss the merits of testing today, I'll just suggest a library that helps them write differently and, according to me, makes them more comprehensible.

<!--more-->

## My Issue with Tests

In my whole career, JUnit is the only framework I used for testing Java code (well, two frameworks, since I've used JUnit 4 and have more recently grown accustomed to [Jupiter][junit5]).
Call me ignorant if you will, but I love this framework; it provides most of the bases I need for testing.

One detail that quite often comes back to bother me, though, is the writing of the assertions.
Reading them is not always obvious, for several reasons:

* There are common mistakes, like **inverting the "expected" and the "actual" parameters**, which can make debugging a nightmare if you trust the code and don't take a step back about what it's supposed to do.
* It's probably among **the most procedural pieces of code** we have to write in Java.
Just a sequence of instructions.
* I've never known a developer (me included) who bothered to write messages for unverified assertions, and even comments are few (but I guess I could write an entire post on that).

I could go on, but you get the gist.

There are many ways to write tests that are more understandable.
We may talk about Cucumber and Gherkin, but not today.
Today, I'll focus on fluent interfaces and more especially on AssertJ.


## Fluent Interface to the Rescue

Do you know what fluent interface is?
It belongs to the object-oriented world and relies heavily on chaining method calls.
For instance, I suppose the Builder pattern could be considered fluent interface:

```java
final Book gameOfThrones = Book.builder()
    .title("A Game of Thrones")
    .author("George R. R. Martin")
    .series("A Song of Ice and Fire")
    .build();
```

I like this approach better than what I knew before that.
Calling setters one by one always seemed a bit heavy to me…

```java
final Book gameOfThrones = new Book();
gameOfThrones.setTitle("A Game of Thrones");
gameOfThrones.setAuthor("George R. R. Martin");
gameOfThrones.setSeries("A Song of Ice and Fire");
```

… and the all-args constructor also allow for immutable objects but can quickly become verbous and non-yieldy:
* You need to know the position of each argument: code reviews become harder without documentation or smart IDE.
* It declares either many variants or you might leave many fields `null`.

```java
// I don't know the publication year, leave empty
final Book gameOfThrones = new Book("A Game of Thrones", "George R. R. Martin", null, "A Song of Ice and Fire");
```

This was a crude example of what can be done with a fluent interface.
The Stream API is another.

However, there's an interesting fact with several implementations I've seen: you get the feeling that, instead of writing a list of statement, you write a sentence, something that even a non-coder could read.
AssertJ's among these implementations.


## Getting Started with AssertJ

What would you get if you wrote your tests with AssertJ?
Well, let's see a few examples:

```java
assertThat(tyrion)
    .isNotEqualTo(robert)
    .isIn(lannisterFamily);

assertThat(melisandre.getName())
    .startsWith("Mel")
    .endsWith("dre")
    .isEqualToIgnoringCase("melisandre");

assertThat(nedsChildren)
    .hasSize(5)
    .contains(robb, arya)
    .doesNotContain(joffrey);
```

Now, imagine writing this with JUnit's assertions and tell me which you prefer.

To get started with AssertJ, you need to import your dependency, so in Maven:

```xml
<dependency>
  <groupId>org.assertj</groupId>
  <artifactId>assertj-core</artifactId>
  <version>3.20.2</version>
  <scope>test</scope>
</dependency>
```

And then, in your test classes, replace JUnit's assertions with one import:

```java
import static org.assertj.core.api.Assertions.assertThat;
```

From there, you can use your editor autocompletion, [the official documentation][assertj] or [some renowned tutorial][baeldung-assertj] to know what assertions are made available to you.

`assertThat` is the main method, but several others are available.
From [the documentation][assertj]:

```java
import static org.assertj.core.api.Assertions.assertThat;  // main one
import static org.assertj.core.api.Assertions.atIndex; // for List assertions
import static org.assertj.core.api.Assertions.entry;  // for Map assertions
import static org.assertj.core.api.Assertions.tuple; // when extracting several  properties at once
import static org.assertj.core.api.Assertions.fail; // use when writing exception tests
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown; // idem
import static org.assertj.core.api.Assertions.filter; // for Iterable/Array assertions
import static org.assertj.core.api.Assertions.offset; // for floating number assertions
import static org.assertj.core.api.Assertions.anyOf; // use with Condition
import static org.assertj.core.api.Assertions.contentOf; // use with File assertions
```

Now, give AssertJ a spin and tell us what you think.
As for me, I know that, since I discovered it, I have a tendency to replace all my JUnit assertions.
You can see examples of JUnit 5 used with AssertJ in my [Download proxy project][dlproxy-tests].



{{% references %}}
[dlproxy-tests]: https://gitlab.com/keyboardplaying/download-proxy/-/blob/main/src/test/java/org/keyboardplaying/web/dl/service/DownloadProxifierTest.java
