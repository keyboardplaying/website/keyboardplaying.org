---
title: Inspiroj kaj defioj
description: |-
  Mi estas parto de komunumo de lernantaj verkistoj.
  Por resti motivata kaj pli skribi, ni kreas malgrandajn defiojn, ĉiusemajne kaj dumonate.
  Mi dividas ĉi tie kelkajn, kiujn mi konsideras akcepteblaj.
  Ne hezitu komenti por helpi min plibonigi ilin.   
url: /rakontoj/inspiroj

cascade:
  classes: [ large-page ]
---
