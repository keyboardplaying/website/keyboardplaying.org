---
title: Kantofikcioj
description: |-
  Kelkaj el niaj defioj estas noveloj inspiritaj el kantoj.
  La aŭtoro kutime elektas la kanton, pri kiu li volas skribi. 
weight: 4
slug: kantofikcioj

termCover:
  banner: false
  src: debby-hudson-yNdzXDpW8n4-unsplash.jpg
  alt: Plumo proksime de notlibro kun aŭdiloj sur ĝi
  by: Debby Hudson
  authorLink: https://unsplash.com/@hudsoncrafted
  link: https://unsplash.com/photos/yNdzXDpW8n4
  license: Unsplash
---
