---
title: Songfics
description: |-
  Some of our challenges are short stories inspired from songs.
  The author usually chooses the song they want to write about.
weight: 4
slug: songfic

termCover:
  banner: false
  src: debby-hudson-yNdzXDpW8n4-unsplash.jpg
  alt: A pen near a notebook with headphones on it
  by: Debby Hudson
  authorLink: https://unsplash.com/@hudsoncrafted
  link: https://unsplash.com/photos/yNdzXDpW8n4
  license: Unsplash
---
