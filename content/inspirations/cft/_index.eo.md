---
title: Alvokoj por tekstoj
description: |-
  Eldonejoj foje faras alvokojn por tekstoj (ĝenerale konkursoj por kiuj la prezo estas publikigota).
  La tekstoj ĉi tie verkitaj estis verkitaj por ĉi tiu speco de konkurso kaj ne estis konservitaj nek profitas de la interkonsento de la eldonejo por esti publikigitaj en la retejo.
weight: 3
slug: alvokoj
---
