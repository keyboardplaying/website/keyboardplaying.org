---
title: Défis du Stylo
description: |-
  Les défis du Stylo sont proposés tous les deux mois et visent une production un plus conséquente et plus structurée que le Stylo hebdo.
weight: 2
slug: bimestriels

termCover:
  banner: false
  src: milkovi-FTNGfpYCpGM-unsplash.jpg
  alt: Deux mains tapent sur une machine à écrire
  by: MILKOVÍ
  authorLink: https://unsplash.com/@milkovi
  link: https://unsplash.com/photos/FTNGfpYCpGM
  license: Unsplash
---
