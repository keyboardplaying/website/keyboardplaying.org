---
title: Programming
description: |-
  When writing code, we need tools and best practices.
  This is where we can share about those.
weight: 5
slug: programming

termCover:
  banner: false
  src: fabian-grohs-XMFZqrGyV-Q-unsplash.jpg
  alt: MacBook with a code editor open, next to an open notebooks with a check list and drafts
  by: Fabian Grohs
  authorLink: https://unsplash.com/@grohsfabian
  link: https://unsplash.com/photos/XMFZqrGyV-Q
  license: Unsplash
---
