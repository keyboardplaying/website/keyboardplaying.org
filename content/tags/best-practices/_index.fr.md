---
title: Bonnes pratiques
description: |-
    Les patrons de conception (_design patterns_) sont connus car ils proposent des solutions optimales à des problèmes courants.
    De même, de nombreuses pratiques sont habituelles en création logicielle sans pour autant être optimales.
    C'est la raison d'être des bonnes pratiques : partager avec tous une façon de faire qui semble meilleure que les autres dans la majorité des cas.
    Et puisqu'il s'agit de partager, le blogue semble être un bon endroit.
weight: 17
slug: bonnes-pratiques
---
