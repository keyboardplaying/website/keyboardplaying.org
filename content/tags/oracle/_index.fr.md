---
title: Oracle
description: |-
  Oracle vend des technologies de base de données et des logiciels d'entreprise.
  Elle est notamment connues pour ses bases de données et son rachat du JDK de Sun il y a quelques années.
weight: 12
slug: oracle

termCover:
  banner: false
  src: oracle-headquarters--davidlohr-bueso.jpg
  alt: Le siège d'Oracle à Redwood City
  by: Davidlohr Bueso
  authorLink: https://www.flickr.com/photos/daverugby83/
  link: https://www.flickr.com/photos/daverugby83/8259922368
  license: CC BY 2.0
---
