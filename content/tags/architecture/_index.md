---
title: Architecture
description: |-
  To make a great software solution, coding skillfully is not enough.
  The functional and technical design plays an important part.
  In other words, laying bricks is not enough, you need to know why and how.
  Here, we'll rather speak of the how.
weight: 17
slug: architecture

termCover:
  banner: false
  src: sven-mieke-fteR0e2BzKo-unsplash.jpg
  alt: A pencil and architect ruler on blueprints
  by: Sven Mieke
  authorLink: https://unsplash.com/@sxoxm
  link: https://unsplash.com/photos/fteR0e2BzKo
  license: Unsplash
---
