---
title: Architecture
description: |-
  Créer une belle solution logicielle ne passe pas seulement par du beau code.
  Les conceptions fonctionnelle et technique jouent un rôle important.
  Autrement dit, ce n'est pas tout de poser des briques, il faut savoir pourquoi et comment.
  Nous adresserons ici davantage le comment.
weight: 17
slug: architecture

termCover:
  banner: false
  src: sven-mieke-fteR0e2BzKo-unsplash.jpg
  alt: Un crayon posé à côté d'une règle d'architecte sur un plan
  by: Sven Mieke
  authorLink: https://unsplash.com/@sxoxm
  link: https://unsplash.com/photos/fteR0e2BzKo
  license: Unsplash
---
