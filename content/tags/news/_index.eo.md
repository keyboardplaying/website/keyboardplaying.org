---
title: Novaĵoj
description: |-
  Espereble blogaj afiŝoj iom reagos al la novaĵo.
  Tiuj asociitaj kun ĉi tiu ŝlosilvorto faras aŭ reklamas ĝin mem.
slug: novajoj

termCover:
  banner: false
  src: elijah-o-donnell-t8T_yUgCKSM-unsplash.jpg
  alt: Viro legas brulantan gazeton
  by: Elijah O'Donnell
  authorLink: https://unsplash.com/@elijahsad
  link: https://unsplash.com/photos/t8T_yUgCKSM
  license: Unsplash
---
