---
title: Java
description: |-
  Java est un langage de programmation encore répandu, bien adapté à la construction d'applications portables.
  Peut-être avons-nous encore quelques chose à en apprendre.
weight: 10
slug: java

termCover:
  banner: false
  src: mike-kenneally-tNALoIZhqVM-unsplash.jpg
  alt: Une tasse au milieu de grains de café.
  by: Mike Kenneally
  authorLink: https://unsplash.com/@asthetik
  link: https://unsplash.com/photos/tNALoIZhqVM
  license: Unsplash
---
