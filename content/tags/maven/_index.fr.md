---
title: Maven
description: |-
  Apache Maven est un outil d'automatisation de production des projets logiciels, principalement utilisé pour des projets Java, conçu pour standardiser la façon dont les projets sont compilés.
weight: 11

termCover:
  banner: false
  src: maven-logo.jpg
  alt: Logo de Maven par Apache
  by: chop
  license: ASF 2.0
  basedOn:
    - link: https://svn.apache.org/repos/asf/comdev/project-logos/originals/maven.svg
      title: Apache Maven logo
      by: Apache
      license: ASF 2.0
    - link: https://unsplash.com/photos/znBJkXzo8ik
      title: Close-up photo of pink feathers
      by: Lakeisha Bennett
      license: Unsplash
---
