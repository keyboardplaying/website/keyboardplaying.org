---
title: Maven
description: |-
  Apache Maven is a build automation tool, primarily used in Java projects, designed to standardize the way projects are compiled.
weight: 11

termCover:
  banner: false
  src: maven-logo.jpg
  alt: Apache Maven logo
  by: chop
  license: ASF 2.0
  basedOn:
    - link: https://svn.apache.org/repos/asf/comdev/project-logos/originals/maven.svg
      title: Apache Maven logo
      by: Apache
      license: ASF 2.0
    - link: https://unsplash.com/photos/znBJkXzo8ik
      title: Close-up photo of pink feathers
      by: Lakeisha Bennett
      license: Unsplash
---
