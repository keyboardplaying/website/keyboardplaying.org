---
title: Bases de données
description: |-
  La plupart des projets informatiques ont besoin de stocker des données.
  La plupart utilise une base de données, qui peut être structurée (SQL) ou non (NoSQL).
weight: 5
slug: bases-de-donnees

termCover:
  banner: false
  src: campaign-creators-IKHvOlZFCOg-unsplash.jpg
  alt: Une personne dessine un diagramme sur une vitre. La photo montre le mot « database ».
  by: Campaign Creators
  authorLink: https://unsplash.com/@campaign_creators
  link: https://unsplash.com/photos/IKHvOlZFCOg
  license: Unsplash
---
