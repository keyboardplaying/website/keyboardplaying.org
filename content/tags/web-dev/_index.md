---
title: Web development
description: |-
  Web development covers many technologies: HTML, CSS, JavaScript, WebAssembly...
  And the tools are numerous and interesting.
  Let's have a look at them!
weight: 10
slug: web-development

termCover:
  banner: false
  src: aral-tasher-vWsubsaym3o-unsplash.jpg
  alt: A split screen, with a code editor on the left and several logos, among which web frameworks', on the right
  by: Aral Tasher
  authorLink: https://unsplash.com/@araltasher
  link: https://unsplash.com/photos/vWsubsaym3o
  license: Unsplash
---
