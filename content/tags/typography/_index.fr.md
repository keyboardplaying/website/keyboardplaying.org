---
title: Typographie
description: |-
  La typographie recouvre plusieurs choses : les polices, les caractères spéciaux, les régles sur leur usage...
weight: 15
slug: typographie

termCover:
  banner: false
  src: metal-movable-type.jpg
  alt: "Caractères mobiles d’imprimerie. On peut lire en anglais : « Le renard rapide saute par dessus le chien paresseux (équivalent anglophone de « Portez ce vieux whisky au juge blond qui fume »), comme s’il était dans le septième ciel de la typographie avec Herman Zapf, l’artiste le plus célèbre du »"
  by: Willi Heidelbach
  link: https://commons.wikimedia.org/wiki/File:Metal_movable_type.jpg
  license: CC BY-SA
---
