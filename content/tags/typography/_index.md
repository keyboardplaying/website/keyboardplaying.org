---
title: Typography
description: |-
  Typography can define a range of things: fonts, special characters, the rules about how to use them...
weight: 15
slug: typography

termCover:
  banner: false
  src: metal-movable-type.jpg
  alt: "Note: the plate says - \"The quick brown fox jumps over the lazy dog and feels as if he were in the seventh heaven of typography together with Hermann Zapf, the most famous artist of the\""
  by: Willi Heidelbach
  link: https://commons.wikimedia.org/wiki/File:Metal_movable_type.jpg
  license: CC BY-SA
---
