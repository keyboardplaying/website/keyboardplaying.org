---
title: Open Source
description: |-
  Malfermi vian programaron---aŭ aparataron---signifas, ke homoj scios kiel ĝi funkcias, povos modifi ĝin kaj daŭre uzi ĝin kiam vi ne plu povas subteni ĝin.
weight: 17
slug: open-source
---
