---
classes: [ large-page ]
---

Un endroit où partager [des histoires]({{< relref path="/stories" >}}) et [des réflexions]({{< relref path="/blog" >}}).
