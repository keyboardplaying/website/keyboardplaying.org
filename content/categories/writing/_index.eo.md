---
title: Skribo
description: |-
  Vi ne volas esti verkisto, sed vi povas provi fariĝi unu kun iom da praktiko.
  Ni dividu ĉi tie kelkajn konvenciojn kaj rekomendojn pri skribo.
weight: 4
slug: skribo

termCover:
  banner: false
  src: coding-extends-writing.jpg
  alt: Kajera paĝo kun fontoplumo. Sur la paĝo, ĝi legas "public class Coding extends Writing".
  by: chop
  license: CC BY 4.0
---
