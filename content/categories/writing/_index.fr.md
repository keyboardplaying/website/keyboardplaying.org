---
title: Écriture
description: |-
  N'est pas écrivain qui veut, mais on peut essayer de le devenir avec un peu d'entraînement.
  Partageons ici quelques conventions et recommandations au sujet de l'écriture.
weight: 4
slug: ecriture

termCover:
  banner: false
  src: coding-extends-writing.jpg
  alt: Une page de carnet avec un stylo plume. Sur la page, on peut lire « public class Coding extends Writing ».
  by: chop
  license: CC BY 4.0
---
