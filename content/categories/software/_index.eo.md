---
title: Programaro kaj ciferecaj teknologioj
description: |-
  Programaro kaj cifereca teknologio ĉirkaŭas nin, ili faciligas nian vivon.
  Tamen ili ne estas panaceo kaj ni ne ĉiam scias ĉion pri ili.
  Jen kelkaj pensoj pri programaro kaj cifereca, kaj eble kelkaj ideoj pri kiel pli bone uzi ilin.
weight: 3
slug: programaro-kaj-ciferecaj-teknologioj

termCover:
  banner: false
  src: thisisengineering-raeng-3Z3gnA99PL8-unsplash.jpg
  alt: Tekkomputilo montranta komputilan helpdezajnan programon.
  by: ThisisEngineering RAEng
  authorLink: https://unsplash.com/@thisisengineering
  link: https://unsplash.com/photos/3Z3gnA99PL8
  license: Unsplash
---
