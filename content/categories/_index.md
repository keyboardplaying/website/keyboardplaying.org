---
title: Categories
description: |-
  The blog posts are sorted in categories.
  Those may seem a bit broad, but [tags](/blog/tags/) are also used to help you look for what may interest you.
url: /blog/categories

cascade:
  classes: [ large-page ]
---
