---
title: Claviers
description: |-
  C'est le nom du site, après tout.
  Tous les types de claviers pourront être abordés.
weight: 2
slug: claviers

termCover:
  banner: false
  src: xanderleadaren-typematrix-bepo.jpg
  alt: Un clavier TypeMatrix avec une disposition BÉPO.
  by: Alexandre André
  authorLink: https://esperanto.masto.host/@xanderleadaren
  link: https://www.flickr.com/photos/xanderleadaren/9484344043/
  license: CC BY-SA 2.0
---
