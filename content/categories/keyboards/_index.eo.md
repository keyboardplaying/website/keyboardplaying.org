---
title: Klavaroj
description: |-
  Ĝi ja estas en la nomo de la retejo.
  Ĉiuj specoj de klavaroj alireblas.
weight: 2
slug: klavaroj

termCover:
  banner: false
  src: xanderleadaren-typematrix-bepo.jpg
  alt: TypeMatrix klavaro kun aranĝo BÉPO.
  by: Aleks André
  authorLink: https://esperanto.masto.host/@xanderleadaren
  link: https://www.flickr.com/photos/xanderleadaren/9484344043/
  license: CC BY-SA 2.0
---
