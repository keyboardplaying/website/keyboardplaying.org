---
title: Projects
description: |-
  Most passionate developers have side projects.
  I know I do.
  Here are some news about the best of them.
weight: 2
slug: projects

termCover:
  banner: false
  src: glenn-carstens-peters-RLw-UC03Gwc-unsplash.jpg
  alt: A hand is writing a bucket list on a notebook page
  by: Glenn Carstens-Peters
  authorLink: https://unsplash.com/@glenncarstenspeters
  link: https://unsplash.com/photos/RLw-UC03Gwc
  license: Unsplash
---
