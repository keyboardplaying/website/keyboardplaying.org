---
title: Projets
description: |-
  Un développeur passionné a souvent de nombreux projets personnels.
  Je ne fais pas exception ; voici quelques nouvelles sur certains de mes projets.
weight: 2
slug: projets

termCover:
  banner: false
  src: glenn-carstens-peters-RLw-UC03Gwc-unsplash.jpg
  alt: Une main écrit une liste de choses à faire sur une page de cahier
  by: Glenn Carstens-Peters
  authorLink: https://unsplash.com/@glenncarstenspeters
  link: https://unsplash.com/photos/RLw-UC03Gwc
  license: Unsplash
---
