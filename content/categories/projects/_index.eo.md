---
title: Projektoj
description: |-
  Pasia programisto ofte havas multajn personajn projektojn.
  Mi ne estas escepto; jen kelkaj novaĵoj pri iuj el miaj projektoj.
weight: 2
slug: projektoj

termCover:
  banner: false
  src: glenn-carstens-peters-RLw-UC03Gwc-unsplash.jpg
  alt: Mano skribas faroliston sur notlibropaĝo
  by: Glenn Carstens-Peters
  authorLink: https://unsplash.com/@glenncarstenspeters
  link: https://unsplash.com/photos/RLw-UC03Gwc
  license: Unsplash
---
