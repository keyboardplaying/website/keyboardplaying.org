---
date: 2020-04-01T18:50:41+02:00
title: Jugement
slug: aelg-jugement
authors: [ chop, tony ]
license: CC BY-NC-ND 4.0

description: |-
    Deux condamnés ont le temps d'un trajet vers la prisonef pour apprendre à se connaître.

universes: [ caravan ]
keywords: [ chronique, micronnouvelle, science fiction, aliens, vaisseaux spatiaux ]

preface: |-
    Ma communauté d'écriture m'a fait découvrir le podcast [Aliens et les Garçons](http://aliensetlesgarcons.space/).
    En les contactant, on a fini par se lancer un petit défi : réaliser une chronique pour eux, sur le thème de leur prochain épisode : « jugement ».
    Petit défi supplémentaire : nous l'avons écrite à quatre mains avec [Tony](https://tonymalghem.com).
    Pour la version audio, c'est [ici](http://aliensetlesgarcons.space/2020/04/01/e33-hors-serie-nos-auditeurs-ont-du-talent-vol-1/).

cover:
  src: /img/covers/aelg.png
  alt: Aliens & les Garçons, un podcast de science-fiction plutôt sympathique
  by: Aliens & les Garçons
  authorLink: http://aliensetlesgarcons.space/
  link: http://aliensetlesgarcons.space/
---

La Citadelle était déjà loin.
La navette s’était amarrée au vaisseau-cité, siège du Conseil où se réunissaient toutes les races du convoi pour prendre ensemble les décisions affectant la Caravane.
En moins d’une minute, Räkô avait été poussé à bord et le transport s’était élancé, les pilotes ne prenant pas le temps de s’assurer que leur nouveau passager s’était bien harnaché.

Il observait ce qu’il pouvait des vaisseaux passer à travers le hublot, tous différents.
Par leur taille, leurs formes, leurs couleurs… Aussi variés que les races à qui ils appartenaient.
Ça, c’était un vaisseau Fartagien.
Et ça, un Bith.
Celui-ci, aucune idée.
Celui-là… celui-là, c’était celui des Atituls.
Il ne reverrait pas le sien tout de suite, la navette l’emmenant à l’autre extrémité du convoi.

Une voix le tira de sa rêverie, juste sous le hublot.

--- T’es un bufo, non ?

Räkô baissa la tête et constata qu’un Tonien occupait le harnais qui lui faisait face.

--- Un bufo, c’est un amphibien indigène de Tollana, que nous élevons au sein de la Caravane à des fins culinaires.
Mais oui, je suis un Omeimonti et l’on surnomme les miens ainsi.

--- Désolé, j’en avais jamais vu des comme toi.
J’ai jamais vu de bufo non plus, en fait.
Pourquoi on vous appelle comme ça ?

--- Parce que nous sommes amphibiens nous aussi, j’imagine.
Ou à cause de l’apparence de notre peau.
Les gens aiment bien oublier qu’on est là.

--- J’ai rien dit pour pas être méchant, mais ouais, t’as pas forcément tes chances pour Miss Univers.
Et donc, qu’est-ce que tu fais là ?

--- Je suppose que nous faisons la même chose, toi et moi : je vais prendre une chambre à la prisonef.

--- Tu fais exprès d’être un connard ou c’est par nature ?
Bien sûr qu’on va tous les deux en taule !
C’que j’te d’mande, c’est pourquoi t’y vas, toi.

Räkô réalisa qu’effectivement, il se comportait avec une suffisance qui ne lui vaudrait rien de positif là où il se rendait.
Le vaisseau-prison n’avait de vaisseau que le nom.
C’était un bloc de métal flottant vers la tête de la Caravane, qualifié d’habitable à défaut de meilleur mot.
Il n’y avait qu’une unique prison pour tous les délinquants et criminels du convoi, peu importe leur race et la raison de leur incarcération.
Räkô ferait mieux de se faire des amis plutôt que de se mettre ses futurs voisins à dos avant même d’avoir emménagé.
Il soupira avant de reprendre.

--- Excuse-moi, j’étais concentré sur ma contrariété.
Reprenons à zéro : je m’appelle Räkô Førús.
Et toi, quel est ton nom ?

--- Togmal Neghym.
Et donc, c’est quoi, ton histoire ?

--- Pour faire court, disons que je suis condamné pour avoir volé à une race qui ne reconnait pourtant pas la notion de propriété et donc n’avait aucune loi pour me juger.

--- Ah, toi aussi, tu t’es fait baiser par le système ?
Vas-y, raconte !
J’me sentirai moins seul.

--- Où commencer ?
Je suppose que tu sais que les Omeimontis sont responsables de tout ce qui a rapport à l’eau potable dans la Caravane, tout du moins pour les races qui ne sont pas autosuffisantes dans ce domaine.

--- J’ai surtout entendu dire que les bufi… Pardon, que vous en consommiez beaucoup.

--- Voilà pourquoi on ne nous aime pas !
Oui, en tant qu’amphibiens, nous avons besoin de beaucoup d’eau et des rumeurs disent que nous la gardons pour nous.
C’est vrai et faux : nous stockons de l’eau parce qu’il nous en faut, mais nous la partageons volontiers.
Nous sommes devenus experts dans le domaine par nécessité.
Pas de voyage spatial sans ça.
C’est pour cette raison que nous intervenons sur les systèmes de recyclage d’eau de nombreuses races de la Caravane.
Et malgré ça, on peine à obtenir… En fait, on peine à obtenir à peu près tout.
Comme les races du convoi nous prennent pour des voleurs d’eau, ils négocient n’importe quelle ressource à l’extrême.
Les races mécaniques sont les plus ouvertes.
Parce qu’elles sont plus rationnelles et que l’eau les intéresse très peu.

--- C’est complètement con, si c’est vous qui fournissez l’eau à tout le convoi !

--- Oui, je sais.
Je l’ai plaidé devant le Conseil plusieurs fois.
Ils sont tous d’accord sur le principe, mais rien ne change dans les faits.
Bref, il y a de ça plusieurs systèmes, je suis intervenu à bord d’un vaisseau Atitul.
Tu connais un peu ?

--- Les aristos ?
Ouais, on dit qu’ils sont nés avec un tournevis en platine dans la main et un poil qui les empêche de s’en servir !

--- C’est ça.
Je connaissais les rumeurs, mais effectivement, ils vivent dans une abondance indécente.
Ils avaient une panne sur un système de recyclage.
À un moment, j’ai eu besoin d’une pièce que leur ingénieur n’avait pas.
Il est rentré dans la chambre de son collègue, sans gêne et en est ressorti avec la vanne qu’il me fallait.
Quand j’ai voulu savoir comment ça se passerait pour la facturation, il m’a expliqué que, chez les Atituls, la notion de propriété n’existe pas.
Tout est à tout le monde et chacun est libre de se servir.
C’est beau, comme philosophie.
Et surtout, quand on baigne dans l’opulence, ça ne dérange pas vraiment !

--- J’avoue.
C’est pas donné à tout le monde !

--- Malheureusement.
Du coup, j’ai craqué.
En plus de ne pas avoir de souci matériel, ils sont effectivement très paresseux.
Ils ne nous surveillent pas et ne cherchent pas à apprendre.
En fait, ils ne font aucune maintenance sur leurs systèmes.
Donc j’ai monté une opération pour prendre aux trop riches et récupérer pour nous qui sommes dans le besoin.
J’ai monté un réseau avec les gars qui interviennent chez eux, de sorte à piquer des pièces à chaque intervention.
Le mot d’ordre était de toujours rester raisonnable, d’attraper un ou deux trucs dans la réserve qui pourraient nous être utiles, mais qui ne manqueraient pas.

--- Et tu t’es fait choper, et comme « la souveraineté de la justice » est laissée à chaque espèce, ils t’ont jugé et condamné à la taule.

--- Pas exactement.
Ils ont fini par remarquer notre manège et ont effectivement demandé à être mis face à un responsable, mais ils n’ont pas pu me juger.
Puisque personne n’est propriétaire des biens dans leur loi, la notion même de vol n’a pas de sens.
En conséquence de quoi, ils ont sollicité l’Arbitre de la Citadelle.

--- Quoi, l’asexué qui aide quand deux personnes sont pas d’accord ?

--- Celui-là même.
Les deux en désaccord cette fois-ci, c’étaient moi et le responsable de l’inventaire Atitul.
Dans ce cas, chaque parti a droit à deux avoués pour le représenter, et trois avoués neutres sont présents pour s’assurer que les votes n’aboutiront pas à la nullité.

Räkô ne sentit pas le ralentissement à cause des amortisseurs inertiels de la navette, mais il vit que les vaisseaux défilaient de moins en moins vite à l’extérieur.
Ils approchaient de la prisonef et il devait abréger.

--- Pour te la faire courte, j’ai pris deux gars du réseau comme avoués, face à deux Atituls.
Les trois neutres ont été sélectionnés aléatoirement parmi les Siégeants du Conseil.
L’argument principal du type de l’inventaire, c’est que suivre son stock lui donnait du travail.
C’EST DANS LE TITRE DE SON RÔLE, FRAK !
Pardon.
Heureusement, ses avoués étaient un peu plus intelligents.
Ils ont expliqué que, si la propriété n’existe pas au sein de la communauté Atitul, c’est bien cette communauté qui en a l’usufruit et qui a donc été spoliée…

--- Attends, je pige plus rien là !
Tu peux rester sur la langue commune, s’il te plaît ?

--- Oui, pardon.
Ce qu’on a volé n’est pas à un Atitul en particulier, mais ça appartient aux Atituls en général.
On a expliqué qu’on ne prenait que des choses qui ne manqueraient pas et dont nous avions besoin.
Un des avoués neutres a quand même voté pour nous, à la fin.
Là où je l’ai mauvaise, en revanche, c’est qu’un de mes gars a voté contre moi, soi-disant que je renforçais la mauvaise image des Omeimontis.
Gorram d’hypocrite !
C’était pas le dernier à chiper des trucs au passage.

--- Et donc, l’Arbitre t’a condamné à de la taule ?

--- Il était ennuyé, mais oui.
Les Atituls avaient insisté sur une sanction exemplaire, pour être sûrs que tout le monde ne se mette pas à leur dérober « leurs » biens.
L’Arbitre a demandé à chacun des avoués de prononcer une peine raisonnable selon eux et a fait la moyenne : je vais passer dix systèmes à bord de la prisonef.

--- Tu vas voir passer dix systèmes enfermé pour avoir pris des trucs qui n’appartenaient à personne ? !
Je te l’accorde, c’est raide !
Ça me consolerait presque.

Le hublot était passé dans l’ombre pendant qu’ils échangeaient.
Le bruit des répulseurs magnétiques, qui avait remplacé celui de l’impulsion, se tut à son tour, un léger choc métallique précédant le silence à l’extérieur.
Concentrés sur leur conversation, ils ne s’étaient rendu compte de rien.

--- Et toi du coup, qu’est-ce qui t’amène ic…

La porte arrière s’ouvrit et un garde cria :

--- Vous fermez bien gentiment vos mouilles et vous me suivez !

Les deux compagnons de galère sortirent de la navette, volontaires forcés par les pistolasers que les gardiens pointaient sur eux.
Deux gardiens ouvraient la marche, suivis par Räkô et Togmal.
Deux autres suivaient, prêts à dégainer, veillant à ce que les nouveaux pensionnaires ne tentent pas de s’enfuir.
Le petit groupe avançait lentement, trop au goût des gardes qui mettaient de petits coups de tonfa électrique derrière les genoux des prisonniers pour leur faire presser le pas, avec un résultat relativement peu productif.

Une fois les contrôles de sécurité habituels réalisés, à grands coups de gants en plastique et de toux, les deux criminels se retrouvèrent assignés dans la même cellule, par le plus grand des hasards de la narration.

--- Je n’aurais pas cru que nous nous reverrions si vite, mais cela va te permettre de me raconter ton histoire.

--- Et moi qui avais pourtant appelé en avance pour réserver une chambre solo… 

S’installant sur le lit du bas qu’il venait de désigner comme le sien, le Tonien commença son histoire.

--- Bon, ben moi aussi le système me l’a mis à l’envers.
J’ai quatre gosses à la maison, c’est peut-être peu pour vous les batraciens, mais pour mon espèce, c’est rare.
Extrêmement rare, même.
Les femelles n’ont généralement qu’un seul ovule fécondable, et c’est terminé.
Mais une fois de temps en temps, la chance tombe sur une de nos femmes et bam, elle peut faire plusieurs enfants.
J’te cache pas qu’en termes de survie de l’espèce, c’est pas folichon, folichon.
Ces femmes sont tellement rares qu’en temps normal, quand on se rend compte de leur capacité reproductive, on te les enlève pour les filer aux chefs, aux pensants… Aux gens importants quoi, pas aux grouillots du peuple comme moi.

--- Beau respect de la femme…

--- Ouais, et d’la famille aussi.
Déjà que la femme est reléguée au rang de poule pondeuse, mais en plus on abandonne les gamins derrière.
’Faudrait pas avoir des p’tits bâtards dans les tours d’ivoire.

--- Vous avez des tours dans votre vaisseau ?

--- Putain, mais fais un effort !
C’est une image.

Un garde passa à ce moment, faisant passer sa matraque sur les barreaux de la prison qui résonnaient dans un insupportable écho, rebondissant sur les parois de la terrible boîte de métal.

--- Bref.
Donc ils sont venus un matin en voulant me laisser moi tout seul avec ma gamine et mes triplets, et m’arracher ma femme.
Deux espèces de connards comme ceux qui nous ont conduits jusqu’ici, des ordures embrigadées par le gouvernement.
Et comme moi, je l’aime ma femme, ben j’me suis pas laissé faire.
J’ai pris un tabouret, et bam quoi.
En plein dans sa mouille.


--- Bam, en plein dans sa mouille ?

--- Ouais.
Un coup de tabouret en plus sur le buffet.
Le crâne fendu en deux par un des barreaux quoi.
C’était pas joli à voir, mais il l’avait bien cherché.

--- Du coup… T’as tué un membre du gouvernement ?
De sang froid ?
J’ai du mal à voir comment le système ne t’a pas respecté…

--- Attends, j’y arrive.
J’suis pas le premier à avoir réagi comme ça, comme tu peux l’imaginer.
Y a une jurisprudence, où si on attaque ces sal… gens pendant une descente, on a quelques petits soucis, des TIG, mais c’est à peu près tout.

--- C’est… intéressant, comme système.
Mais donc ?

--- Eh ben le problème, c’est que le garde que j’ai attaqué, c’était un Pneuma.
Et la particularité des Pneumas, c’est que quand ils meurent, ils reviennent sous forme d’esprit.
Et dans la loi des Pneumas, quand un esprit se présente devant la justice, il devient le témoin principal du procès.

--- Indéniablement pratique, mais ça ne sent pas très bon pour toi.

--- Comme tu dis… Donc le jugement, le gars raconte ce qui s’est passé, mais le pire, c’est qu’il a réussi à émouvoir le jury, parce que sa femme était enceinte de leur premier enfant, qu’ils pensaient qu’ils y arriveraient pas, mais en fait ça venait de marcher.
J’te raconte pas comment ça a chialé dans les rangs de l’assemblée.
Même moi, j’ai chialé.
J’ai foutu une famille en l’air pour sauver la mienne, pour qu’au final…

Sa voix se brisa dans un demi-sanglot.
Il détourna le regard vers le mur pour ne pas laisser son compagnon de cellule voir la larme coulant sur sa joue.
Il se racla la gorge avant de reprendre.

--- Pour qu’au final, ma femme et mes enfants soient exécutés.
J’ai privé une famille de leur père, et en rétribution, ils ont privé un père de sa famille.

Le silence retomba dans la cellule, brisé uniquement par les cris provenant du reste de la prison.
Räkô ouvrait et fermait la bouche, cherchant quelque chose à dire.
Rien ne lui vint et il jugea plus sage de se taire.
