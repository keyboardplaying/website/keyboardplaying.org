---
date: 2019-09-05T08:58:57+02:00
title: Maman, je peux avoir un chien ?
slug: maman-je-peux-avoir-un-chien
author: chop
license: CC BY-NC-ND 4.0

description: |-
  C'est une journée qui commence comme les autres, jusqu'au moment où Klem pose une question inattendue à sa mère.

inspirations: [ bimonthly ]
keywords: [ nouvelle, science fiction, ville, cyberpunk, animal domestique, chien, parentalité, enfance, souvenirs, dialogue ]

challenge:
  period: 2019-08
  rules:
  - type: general
    constraint: Le texte doit comporter un duel ou une opposition, et se terminer par un _happy ending_ (au moins un point bénéfique doit ressortir de la confrontation)
---

Après avoir versé le lait dans le bol de céréales de Klem, Tilda se retourna pour attraper le pichet.
La centrifugeuse avait terminé d'extraire le jus des oranges et du pamplemousse qu'elle y avait mis quelques minutes auparavant.
Alors qu'elle remplissait le verre de son garçon, celui-ci porta, un peu maladroitement, la cuillère à sa bouche.
Elle posa le pichet sur la table et se dirigea vers le plan de travail et la cuisinière.
La cuillère tinta contre le bol tandis que Klem la remplissait à nouveau, puis le bruit d'une céréale tombant mollement sur la table se fit entendre.
Rien de grave.

Elle attrapa les quatre tranches de poitrine fumée reconstituée que Fris avait achetées pour elle et lui, et les sortit de leur emballage réutilisable.
Elle les posait dans la poêle lorsque la voix de leur fils s'éleva dans son dos :

--- Maman, je peux avoir un chien ?

Elle resta abasourdie pendant quelques secondes tant la question lui semblait sortie de nulle part.
Elle avait certainement mal compris.
À cause du bruit de la viande artificielle qu'elle venait de mettre sur le feu, c'était ça !

Comme toujours lorsqu'elle discutait avec son fils, elle lui prêta toute son attention, s'asseyant face à lui, légèrement penchée en avant :

--- Je ne t'ai pas bien entendu à cause de la casserole.
Tu veux bien répéter ?

--- Est-ce que je peux avoir un chien ? répéta Klem.

Son dos se relâcha et s'affaissa contre le dossier de la chaise.
Elle avait donc bien compris.
Mais d'où pouvait lui venir cette idée ?
Comme tous ceux ayant grandi dans une arcologie, ces gigantesques immeubles-cités pratiquement autosuffisants, Fris ne connaissait que très vaguement le concept d'animaux domestiques, et elle savait que, comme leur enfant, il n'avait jamais vu de chien.
Quant à elle, si elle lui avait un peu parlé de sa vie d'enfant au sein d'une exploitation agricole, elle était certaine de n'avoir jamais parlé de compagnon canin à son fils.

--- Où as-tu entendu parler de chiens ? demanda-t-elle.

--- À l'école, répondit-il avec un grand sourire.
La maîtresse nous a montré des films sur comment les gens vivaient au vingtième siècle.
Dans un, ils avaient des animaux dans les maisons.
Des chats et des chiens.
Le film expliquait que les chiens étaient des loups qui avaient appris à vivre avec les humains.

Bien entendu, à l'école !
Tout devenait immédiatement plus clair.
Comment répondre à une telle requête ?
Objectivement, obtenir un chien serait compliqué.
S'en occuper encore plus.
Subjectivement, elle n'avait absolument pas envie de se confronter à nouveau à un animal dans sa vie.

Malgré tout, Fris et elle voulaient que Klem développe son esprit critique.
À sept ans, il arrivait à l'âge délicat où l'on peut commencer à lui lâcher la bride et le laisser devenir responsable.
Jusqu'ici, ils pensaient pouvoir être fiers de leur éducation ; Klem faisait souvent montre d'une capacité de raison et de réflexion qui paraissait bien en avance sur celle de ses camarades de classe.
Il n'était pas rare que, en enchaînant ses questions selon un raisonnement parfaitement logique, il laisse ses parents sans réponses.

Mais pas aujourd'hui.
Tilda ne voulait pas de chien chez elle, mais il était hors de question qu'elle déroge à leur habitude en refusant catégoriquement et sans justification.
L'époque où Klem était un enfant qui acceptait sans discuter la parole des adultes était terminée.
Elle voulait encourager cet esprit indépendant, pas l'étouffer.

--- Mais tu sais, dit-elle en hésitant légèrement, c'était il y a longtemps, le vingtième siècle.
La vie a beaucoup changé, depuis.
Déjà, les chiens étaient beaucoup plus nombreux ; on pouvait en acheter assez facilement.

--- Lexx a dit que sa voisine est vétérinaire.
Elle a pu aller voir le cabinet et elle a vu plein de chiens !
Et aussi des chats, et quelques autres animaux.

--- Ah. Et est-ce que Lexx a parlé des formalités exigées pour pouvoir adopter un chien ?

--- Oui, la maîtresse lui a demandé si elle savait.
Elle a dit qu'il fallait remplir des papiers pour prouver qu'on était capable de s'occuper du chien.

--- Et tu es sûr que nous pourrions prouver ça ?

Fris entra dans la cuisine en marmonnant un vague « bonjour », proche du grognement.
Tilda savait qu'il ne communiquerait pas de façon plus construite avant d'avoir pu commencer à manger.
Klem le savait aussi, visiblement, car il répondit à sa mère sans s'occuper de son père, lequel se dirigeait vers la cuisinière.

--- Oui, déclara le garçon, j'en suis certain !
Lexx a dit que pour la plupart des familles, il suffit de prouver que l'un des parents a un travail au moins de catégorie C.
Avec le travail de papa en catégorie B et le tien en catégorie A, il n'y aurait aucun problème !

Fris, qui avait retourné la poitrine dans la poêle, grogna à ce moment.
Tilda ne lui avait jamais connu la moindre susceptibilité quant au fait qu'elle travaillait dans une catégorie supérieure.
Elle en déduisit que le lard était resté trop longtemps à son goût sur la même face.
Heureusement qu'il n'avait pas brûlé !
L'humeur matinale de Fris se serait alors prolongée sur une bonne partie de la journée.

Klem observait sa mère, qui réfléchissait à la conversation tout en écoutant ce que faisait son époux dans son dos.
Tandis qu'il vidait la poêle dans les assiettes et se saisissait des œufs, elle résolut de poursuivre la discussion.
Peut-être qu'en exposant progressivement toutes les contraintes que représente un chien, l'une d'elles deviendrait un argument convaincant pour Klem.

--- Admettons, dit-elle, que nous prenions un chien.
Tu sais qu'il n'utilise pas les toilettes des humains et qu'il faut le promener régulièrement.
Qui s'en occupera et où ?

--- Je peux m'en occuper !
En me levant tôt le matin, je peux y aller avant l'école.
À midi, tu pourrais venir me chercher avec lui, et le soir aussi.
Lexx a dit que les chiens peuvent se promener dans les parcs.
Il faut juste ramasser leur caca et prendre un des motolaveurs citoyens s'ils font des saletés ailleurs que dans les parcs.

Tilda ne put réprimer un petit rire, mais il fut couvert par le bruit de Fris brouillant les œufs dans la poêle qui avait servi à cuire le lard.

--- Mais si je viens te chercher avec le chien et qu'il ne peut pas se retenir, ce sera à moi de m'en occuper !
D'ailleurs, comme tu es trop jeune pour emprunter un motolaveur, ce sera toujours à ton père ou moi de m'en occuper.

Cette phrase fut suivie d'un blanc.
Au vu du silence soudain et du regard de Klem concentré sur un point derrière Tilda, Fris s'était retourné à cette dernière déclaration.
Klem se concentra à nouveau sur sa mère tandis que la cuillère reprenait la préparation des œufs.

--- Je veux bien le faire, répondit alors leur fils.
Ce sont les règles de l'arcologie qui m'empêchent de le faire seul, mais je le ferai avec vous.

--- Encore heureux !

Elle entendit la machine à café se mettre en route et moudre le grain ; Fris allait servir le petit déjeuner.
Klem savait qu'il était inutile de continuer à discuter pour le moment et il se concentra méticuleusement sur ses céréales, maintenant devenues tout à fait molles.
La plaque à induction bipa lorsque Fris l'arrêta avant de verser les œufs brouillés dans les assiettes.
Tilda attrapa le pichet et remplit leurs deux verres de jus d'orange.
Le café commença à couler et les assiettes, portées par Fris, se posèrent à leurs places.
Il eut à peine le temps de retourner à la machine que celle-ci s'arrêta et il put revenir, une tasse pleine dans chaque main.
L'une d'elles fut déposée à côté de l'assiette de Tilda, puis Fris se laissa tomber dans sa chaise, la sienne en main, avant de la porter à sa bouche et d'en boire une grande gorgée.
Il déposa ensuite son café et attrapa prestement ses couverts, se focalisant sur son assiette.
Les deux hommes de sa vie concentrés sur leur petit déjeuner, Tilda, perdue dans ses pensées, mâchonna le sien sans vraiment y faire attention.

Son bol terminé, Klem y déposa sa cuillère, se leva, le prit dans ses mains et alla le déposer dans le lave-vaisselle, avant de partir se préparer pour l'école.
Ses parents l'observèrent quitter la pièce en souriant mais sans dire un mot.
Après avoir entendu la porte de sa chambre se fermer, Tilda se tourna vers son mari.

--- Qu'en penses-tu ? lui demanda-t-elle.

--- Que je n'ai pas envie de m'en mêler, répondit-il sans émotion.
Tu es la seule dans ce foyer à savoir ce qu'implique d'avoir un chien.

--- Justement.
Klem n'a pas tort sur le fait que nous vivons bien.
Nous n'aurions aucune difficulté à trouver le temps et le budget pour nous en occuper.

--- Je vois bien que tu n'es pas conquise par l'idée.
Pèse bien le pour et le contre.
Parle-lui des difficultés auxquelles tu songes.
Raconte-lui, si tu le juges pertinent.
C'est parce que tu n'as pas envie que nous en ayons à nous en occuper nous-mêmes ?
Parce que j'avoue que la perspective des sorties pluriquotidiennes et du ramassage de crottes ne me fait que moyennement envie moi-même…

--- Non, ce n'est pas ça. C'est…
Elle laissa sa phrase en suspens quelques secondes et se tourna vers la porte en entendant les pas de Klem se diriger vers la cuisine pour les rejoindre.

--- Maman, je suis prêt pour l'école ! déclara-t-il en arrivant au seuil de la pièce.

--- Je vois ça ! répondit-elle avec un sourire.
Mettons-nous en route, dans ce cas !
Tu fais un bisou à papa ?

Leur fils s'approcha de son père pour déposer un bisou sur sa joue tandis que Tilda attrapait sa sacoche dans l'entrée.

--- Bonne journée, chéri !
lui cria-t-elle avant de quitter leur domicile, suivie de Klem qui prit sa main.

Ils franchirent la porte et marchèrent jusqu'au bout de leur rue, et s'arrêtèrent devant une porte coulissante double.
Tilda annonça leur destination (« cours élémentaire Stephen Hawking ») à l'écran qui ornait l'une d'elles, puis se tourna vers son garçon.

--- Tu as vraiment envie d'un chien ?
Tu ne préférerais pas une Manta, comme celle de Niels ?

Elle se souvenait de l'anniversaire du meilleur ami de son fils.
Lorsqu'elle était allée chercher Klem, elle avait découvert le drone compagnon qu'il avait reçu comme cadeau.
Sa forme était inspirée des raies marines, mais l'appareil se déplaçait en lévitant et suivait partout l'enfant auquel il avait été lié.
Celle de Niels dégageait en temps normal une agréable lueur blanche, très légèrement bleutée.

Klem avait levé les yeux, la tête légèrement penchée sur la droite, dans une intense réflexion.
Il regarda sa mère à nouveau.

--- Une Manta, c'est une machine.
C'est pas pareil !

--- Mais elle te comprend.
Tu as bien vu comment elle réagissait à ce qu'on disait autour d'elle.
Tu as vu les tours que Niels lui a fait faire sans même avoir besoin de lui apprendre !

Les Mantas embarquaient une intelligence virtuelle qui leur permettait d'exprimer des pseudo-émotions.
Un compliment la faisait frissonner et virer au bleu vif, d'autant plus intense s'il venait de Niels.
En revanche, lorsque Tilda avait demandé au garçon s'il était content de son robot, la Manta avait manifesté son outrage en devenant rouge et en oscillant de gauche à droite ; la réaction était sans ambiguïté : elle n'était pas qu'un simple robot !

--- Mais une Manta, ce n'est pas une vraie amie.
Un chien, ça peut l'être.

La double porte s'ouvrit et ils montèrent à bord de la large cabine, déjà à moitié occupée, tout en continuant à discuter.

--- Un chien ne pourra pas venir avec toi à l'école, alors que la Manta pourrait.

--- Quand on rentre en classe, la maîtresse ordonne à toutes les Mantas de s'endormir et elles obéissent.
Niels a déjà essayé de réveiller Vlobby, mais elle écoute la maîtresse plus que lui.

--- Au moins, elle écoute.
Tu devras tout apprendre à un chien.
À être sage, à faire ce que tu lui dis, à ne pas faire ses besoins ailleurs que là où il peut.
C'est difficile, il lui faudra du temps pour apprendre.

--- Mais c'est normal, maman !
C'est grâce à ça qu'on sait que ce n'est pas une machine !
Et…

À ce point, il plongea directement son regard dans celui de sa mère.
Il ne regardait pas seulement le visage de la personne à qui il parlait, il voyait au-delà.
Ou en donnait la sensation, en tout cas.
La cabine de translation changea de direction, passant du mouvement vertical au mouvement horizontal.
Ceci combiné à l'intensité du lien entre elle et son fils, Tilda éprouva un vertige.

--- … moi aussi, vous devez m'apprendre et parfois, je me trompe ou je fais des bêtises.
Vous m'apprenez que ce n'est pas bien.
Je ferai pareil avec le chien.
Et même quand vous me grondez, je vous aime.
Et même quand on se dispute avec Niels, on reste amis.
Ce sera la même chose avec un chien.

Tilda parvint à se libérer du regard de son fils mais ne savait plus que lui dire.
Pouvait-il vraiment n'avoir que sept ans ?
Klem n'insista pas et attendit la fin du trajet.
La cabine fit plusieurs arrêts au cours desquels des passagers montèrent ou descendirent.

Tilda ne pouvait pas parler de son propre traumatisme.
Sur la ferme, ses parents avaient plusieurs chiens, mais son préféré était Fripouille, un gigantesque Beauceron.
Il était dans la famille avant qu'elle ne vienne au monde et l'avait adoptée dès sa naissance.
Elle l'avait chevauché comme une monture.
Une fois où son père l'avait fait crier en la chatouillant, il s'était interposé et avait montré les crocs pour la défendre.
Elle pouvait tout lui faire.

Mais un jour, au retour d'une classe verte, un peu avant les dix ans de Tilda, Fripouille avait changé.
Il ne répondait plus à son nom, leurs jeux habituels ne semblaient plus l'intéresser.
C'était Fripouille, mais il se comportait comme un autre chien.

Il avait fallu attendre deux semaines pour que ses parents avouent la vérité : à la poursuite d'un lapin, Fripouille s'était précipité sous les roues du tracteur.
Les parents de Tilda, ne sachant pas comment le lui annoncer, avaient préféré faire cloner Fripouille et payer une croissance accélérée, pour avoir un chien identique.
Ils n'étaient pas scientifiques et ne savaient pas que la personnalité de Fripouille et sa mémoire ne seraient pas transférées au clone.
Qu'ils auraient juste un autre chien, physiquement identique au premier.

Tilda ne s'était plus jamais approchée du clone.
La perte de Fripouille et la trahison de ses parents l'avaient dévastée, et elle s'était promis de ne plus jamais se lier d'affection avec un animal.
Dès que ses études le lui permirent, elle quitta la ferme familiale pour commencer une nouvelle vie au sein d'une arcologie.

Elle sortit de ces tristes souvenirs en sentant le bras de Klem tirer le sien hors de la cabine, juste en face de l'école.

--- Au revoir, maman ! lui dit alors son fils.
Passe une bonne journée !

--- Bonne journée, répondit-elle comme assommée.

Elle regarda son fils s'éloigner, l'esprit divisé.
Elle pouvait se permettre d'arriver plus tard à son travail aujourd'hui et décida d'appeler les parents de Lexx pour avoir les coordonnées du cabinet vétérinaire de leur voisine ; certainement recevrait-elle de bons conseils pour quelqu'un dans sa situation.
Se tournant à nouveau face aux portes coulissantes, elle énonça l'adresse qu'elle venait de recevoir et attendit.

Le trajet ne fut pas long --- mais aucun trajet n'était réellement long, au sein d'une arcologie, à moins de bouder la cabine pour se déplacer à pied.
Tilda descendit et constata que le cabinet était situé juste à côté de l'arc de lumière, la gigantesque baie vitrée qui permettait au soleil d'éclairer l'intérieur de l'arcologie tout au long de sa course de la journée.
Cela faisait des années qu'elle n'avait pas été aussi proche de la lumière extérieure.
Elle pensa à la ferme.
Ses parents venaient bien lui rendre visite, mais l'inverse n'était pas vrai.
Elle n'avait pas quitté la cité de béton et d'acier depuis des lustres.

Elle se retourna et vit une petite fille qui se dirigeait vers le cabinet, accompagnée de ses parents.
Elle avait les deux bras vers l'avant et, posé sur eux, en travers de sa poitrine, un adorable chiot à peine sevré.
Le poil ébouriffé, couleur de sable, avec ses pattes énormes pour son petit corps et ses yeux curieux, il ressemblait à une peluche.
Tilda savait qu'il était inutile de voir la vétérinaire, à présent.
Sa décision était prise : Klem aurait un chien…
