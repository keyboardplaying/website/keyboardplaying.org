---
date: 2021-10-30T13:37:42+02:00
title: La caravane
slug: voyage-de-demain--la-caravane
author: chop
license: CC BY-NC-ND 4.0

description: |-
  La caravane traverse le désert et a trouvé une ferme abandonnée pour la nuit.
  Peut-être recèle-t-elle un trésor oublié.

inspirations: [ cft ]
keywords: [ micronouvelle, science fiction, voyage, épuisement des ressources ]

postface: |-
  #### Ce texte a été écrit dans le cadre d'un appel à textes
  
  L'[appel à textes](https://www.utopiales.org/appel-a-ecriture-du-futur/) par les Utopiales 2021.

  Le thème ? « Voyager, c’est partir, découvrir, recommencer, revenir… ou rester ! Qu’il soit terrestre, spatial, initiatique, intérieur, en terres connues ou inconnues, imaginez votre voyage de demain ! »

  La longueur était limitée à 3000 caractères.
---


Bob s’appuie sur l’imposant pommeau de sa selle pour faire son rapport, que Gina complète, un peu en retrait.
Le bâtiment est en ruines ; rien ne laisse penser qu’il ait été occupé récemment.

Josh soupire.
À son âge, il n’aspire qu’à la tranquillité.
Mener les caravanes le long de routes inconnues, éviter les bandits…
C’est pour les jeunes, tout ça !
L’aventure n’a plus le même attrait, aujourd’hui.

Le soleil approche de l’horizon ; il doit décider.
En temps normal, il préfère éviter tout ce qui peut servir de repaire, mais ils ont tout intérêt à tirer profit de ces murs pour la nuit.

— On ne trouvera pas mieux pour s’arrêter ce soir, tranche-t-il d’une voix rauque et fatiguée.
Je fais l’annonce ici, galopez pour passer le mot en queue de convoi.
Pendant que ses éclaireurs s’exécutent, il passe les rênes à son voisin de banc, se lève et se hisse péniblement sur la toile de son chariot.
Le sable l’empêche d’ôter le foulard qui protège sa bouche et son nez, mais il sait faire porter sa voix.

— Encore quelques kilomètres, les amis.
Ce soir, on dort dans une ferme.
Ne mettez pas les bêtes dans les enclos, le bois est vermoulu.
On fait comme d’habitude.

Une heure plus tard, la caravane s’est arrêtée, les wagons en cercle autour du vieux corps de ferme.
Les caravaniers, le visage et l’allure fatigués, se sont répartis les tâches : certains nourrissent bétail et animaux de trait, tandis que d’autres cuisinent pour leurs compagnons.
Un groupe actionne la pompe du vieux puits dans l’espoir d’ajouter quelques litres d’eau potable aux réserves.

Josh et Bob sont sur le toit et organisent les tours de garde ; la hauteur sera un atout pour les sentinelles. Gina gère quant à elle le couchage dans la maison pour ceux qui souhaitent dormir à l’abri du vent et du sable, mais hors des chariots.

Les enfants ne semblent pas ressentir l’épuisement des adultes.
Pleins d’énergie qu’ils n’ont pas pu dépenser pendant la journée, ils courent et explorent les lieux.
Le petit Phinéas sort soudain de la grange à toutes jambes et appelle Josh.

— Papy ! Papy ! Viens voir ! Il y a un chariot bizarre, dans la grange !

Tapant sur l’épaule de Bob, Josh descend avec précaution l’échelle posée sur le côté de la maison.
Phinéas attrape sa main et le traîne pratiquement dans la grange, où l’aîné ne peut retenir un juron lorsqu’il se retrouve nez à nez avec un emblème de bélier chromé.
Le chariot de Phinéas est tout de métal et de verre.

— Mon Dieu ! Je n’en ai pas vu depuis que j’avais ton âge. Mon grand-père conduisait ça.

— C’est quoi, papy ?

— C’est un pick-up, gamin. Comme nos wagons, sauf qu’il n’y a pas besoin de chevaux pour les tirer, et ça va beaucoup plus vite.

— C’est vrai ? Pourquoi on n’utilise pas ça, alors ?

Josh se tourne vers son petit-fils avec un sourire amer.

— Parce qu’il faut de l’essence pour le faire avancer. Et on n’en a plus, de l’essence… 
Allez, viens !
On va dire à Toby de préparer ce qu’il faut pour l’emporter demain.
On devrait pouvoir en tirer un bon prix.
