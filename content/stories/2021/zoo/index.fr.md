---
date: 2021-02-12T16:55:59+01:00
title: Visite au parc naturel
slug: visite-parc-naturel
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Tommy visite le parc naturel avec ses parents, où une illumination le touche.

inspirations: [ weekly ]
keywords: [ micronouvelle, mots imposés, zoo, mutant ]

challenge:
  period: 2021-02-12
  link: https://twitter.com/commudustylo/status/1360138103122505732
  rules:
  - type: general
    constraint: 'Utiliser tous les mots suivants : carcajou, panda, méphitique, pamplemousse, nitescence, strudel, empathie, caca, sérendipité, anticonstitutionnellement'
---

En passant de l'enclos des pandas à celui des carcajous, Tommy fut incroyablement déçu.
La nitescence dont s'étaient enduits ses yeux sembla s'évaporer et toute sa figure se fit plus sombre.
Les pandas étaient certes d'une extrême mignonnerie, mais ce n'était pas ce qui frappait le plus l'enfant.
Ce qui le dérangeait, c'était que Wolverine, son X-Men favori, portait le nom de cet animal.
Le glouton.
Quel nom ridicule pour un animal sans attrait particulier !
Il préféra passer à la suite de la visite et se mit en route vers l'enclos suivant.

L'air qui entourait l'enceinte était méphitique, couvrant même les plaisants arômes acidulés du pamplemousse dans son strudel.
Levant les yeux, Tommy observa l'être enfermé anticonstitutionnellement dans ce parc : un être humain, un homme.
Ce dernier, adossé contre le mur de son abri, se tourna paresseusement vers un tas de ce qui ressemblait à de la boue, en saisit une poignée et se redressa soudainement pour la lancer vers le jeune visiteur en hurlant « CACA ! »

Tommy évita de justesse les excréments, mais cette visite au parc lui permit une découverte par sérendipité : ses semblables étaient pires que les animaux et il n'éprouvait aucune empathie pour eux.
