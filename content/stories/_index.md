---
title: Stories
description: |-
  Some short (or flash) fictions and writing exercise that we'd like to share.
classes: [ large-page ]

cascade:
  scripts: [ main ]
  classes: [ use-lettrine, use-asterisms ]
  toc: false
---
