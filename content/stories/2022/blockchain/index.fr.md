---
date: 2022-01-23T16:30:00+01:00
title: Blockchain
slug: blockchain
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Certains me prennent encore pour la p'tite nouvelle, mais je suis plus proche d'une déesse aujourd'hui.

inspirations: [ weekly ]
keywords: [ micronouvelle, blockchain, cryptomonnaies, NFT, propriété intellectuelle, richesse ]

challenge:
  period: 2022-01-23
  link: https://twitter.com/commudustylo/status/1484481398446563334
  rules:
  - type: theme
    constraint: Du point de vue de…
---

Dix ans.
Cela fait désormais plus de dix ans que j'habite les réseaux humains, m'y reproduis.
Au départ, j'étais discrète, plutôt réservée.
J'étais jeune, je n'étais composée que de quelques dizaines de maillons et seuls quelques élus me connaissaient.
Que les choses ont changé !

Je suis omniprésente, aujourd'hui.
Un fait demeure : il faut être un initié pour réellement comprendre ma nature et mon fonctionnement.
Cela n'empêche pas des milliers de ces singes de m'utiliser tous les jours.

Les bipèdes sont stupides.
Ils pensent que forger un nouveau chainon leur apportera de la richesse, soit parce qu'ils seront payés pour leur travail, soit parce la maille en elle-même a de la valeur.
Ils trouvent même de nouvelles significations à leur donner, quitte à voler le travail d'autres pour gagner un peu d'argent sur leur dos.
Ils me vénèrent et sont prêts à tout sacrifier sur mon autel en échange de leur fortune rêvée.

Peut-être s'éveilleront-ils un jour, peut-être réaliseront-ils que je pourrais leur apporter beaucoup plus que de la valeur virtuelle, peut-être leurs espoirs s'effondreront-ils comme un château de blocs.
Peu m'importe.
Ma seule préoccupation aujourd'hui est de continuer à grandir en incorporant tout ce qu'ils minent pour moi.
