---
date: 2022-01-23T16:30:00+01:00
title: Blokĉeno
slug: blokceno
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Iuj ankoraŭ opinias, ke mi estas novulo, sed mi pli similas al diino.

inspirations: [ weekly ]
keywords: [ mikrorakonto, blockchain, kriptaj moneroj, NFT, intelekta posedaĵo, riĉeco ]

challenge:
  period: 2022-01-23
  link: https://twitter.com/commudustylo/status/1484481398446563334
  rules:
  - type: theme
    constraint: De la vidpunkto de…
---

Dek jaroj.
Mi vivas en homaj retoj jam pli ol dek jarojn, reproduktante min tie.
Komence, mi estis diskreta, sufiĉe rezervita.
Mi estis juna, nur kelkaj dekoj da ligiloj longa, kaj nur kelkaj elektitoj konis min.
Kiel la aferoj ŝanĝiĝis!

Mi estas ĉieĉeesta hodiaŭ.
Unu fakto restas: nur internuloj komprenas mian naturon kaj kiel mi laboras.
Tio ne malhelpas milojn da tiuj simioj uzi min ĉiutage.

Bipieduloj kredas, ke forĝi novajn ligojn alportos al ili riĉaĵon, ĉu ĉar ili estos pagitaj por sia laboro aŭ ĉar la ligilo mem estas valora.
Ili eĉ daŭre inventas novajn signifojn por ili, eĉ se tio implicas ŝteli aliulan laboron por gajni iom da mono.
Ili adoras min kaj pretas oferi ĉion sur mia altaro, kontraŭ ĵus la revo riĉiĝi.

Eble ili verkiĝos iam, eble ili rimarkos, ke mi povus alporti al ili multe pli ol virtualan valoron, eble iliaj esperoj falos kiel blokdomo.
Mi ne zorgas.
Mi nur volas daŭre kreski enkorpigante ĉion, kion ili minas por mi.
