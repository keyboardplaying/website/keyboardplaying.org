---
date: 2022-08-15T12:00:00+02:00
title: Possession numérique
slug: possession-numerique
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Journal système d'un androïde utilisé dans l'étude de phénomènes paranormaux.

inspirations: [ bimonthly ]
keywords: [ nouvelle, science fiction, androïde, fantôme, surnaturel, possession ]
---


10:00:00 Hearbeat\
Modèle HRA-FB/BB-F-7659\
Numéro de série IRZ765919941003A6\
État : veille\
Mode d'interaction : écoute passive\
Autodiagnostic ; OK (dernier diagnostic terminé à 03:47:57)

<!--Homo-Realistic Android - Full Build / Bionic Brain - Female - ID châssis-->

10:08:17 Utilisateurs détectés

10:08:17 Utilisateurs identifiés : [\
    { id: Δ, nom: O'Brien, prénom: Dylan, rôles: [ superutilisateur, enquêteur ] },\
    { id: Λ, nom: Copper, prénom: Michael, rôles: [ enquêteur, stagiaire ] }\
]

10:08:17 Enregistrement de la conversation, surveillance de déclencheurs

10:08:17 [Δ] …lez, viens ! C'est pas certain qu'on refasse un diagnostic avant la fin de ton stage, autant profiter de l'occasion !

10:08:23 [Λ] Oh merde !
C'est toujours aussi flippant de la voir comme ça, immobile.

10:08:26 [Δ] Elle est en veille, tu voudrais qu'elle fasse quoi ?

10:08:28 [Λ] Elle pourrait avoir l'air de dormir, déjà.
Je sais pas, fermer les yeux, être allongée.
Debout comme ça, le regard vide, elle est on ne peut plus dérangeante.

10:08:35 [Δ] Ha ha, le principe de la vallée de l'étrange.
Plus un androïde ressemble à un être humain, plus ses imperfections nous mettent mal à l'aise.
Tu t'y feras, ça prend juste un peu de temps.
&lt;Hollie&gt; est inoffensive de toute façon.

10:08:49 Détection d'un déclencheur potentiel, analyse de l'intention…

10:08:49 Le prénom &lt;Hollie&gt; a été utilisé pour qualifier l'unité Φ, non pour s'adresser à elle, aucune action déclenchée

10:08:50 [Λ] &lt;Hollie&gt;, c'est un prénom irlandais, non ?
C'est toi qui l'as nommée ?

10:08:50 Détection d'un déclencheur potentiel, analyse de l'intention…

10:08:52 Le prénom &lt;Hollie&gt; a été utilisé de façon neutre, non pour s'adresser à l'unité Φ, aucune action déclenchée

10:08:53 [Δ] Oui, désolé.
Elle me rappelait un amour de jeunesse.

10:08:55 [Λ] C'est tellement malsain !
Pourquoi ont-ils besoin de les faire aussi réalistes ?

10:08:57 [Δ] On t'a pas expliqué ça ?
On a payé cher pour avoir un modèle homoréaliste.
On en a vraiment besoin pour nos recherches.
Quand on enquête sur le paranormal, on préfère donner l'impression que c'est un être humain qui est là, parce qu'on pense que les entités essaient de communiquer et qu'une machine ne les intéressera pas.

10:09:18 [Λ] Ouais, on m'a dit, ça, mais pourquoi ne pas vraiment envoyer des humains, dans ce cas ?

10:09:22 [Δ] La raison qu'on avance en premier dans nos rapports et communications publiques, c'est qu'on veut éviter de mettre des personnes en danger quand on peut envoyer une machine à leur place.
Le vrai sujet, c'est que les humains semblent repousser les phénomènes paranormaux.

10:09:33 [Λ] Tu me perds, là.
Tu viens de dire qu'on voulait des androïdes parce que ce sont les humains qui attirent les entités.

10:09:39 [Δ] Non, j'ai dit que les entités semblent vouloir communiquer avec des humains.
C'est la seule façon dont on explique plusieurs phénomènes qui ne se manifestent pas quand on surveille à distance, mais seulement quand quelqu'un est présent dans la zone d'influence.
Pourtant, ce qu'on a aussi prouvé plusieurs fois, c'est que dans ces zones, les signaux qu'on peut mesurer, électromagnétiques par exemple, sont beaucoup plus forts lorsque la personne sur place est en fait un androïde.
Ça semble se vérifier avec des animaux et animatroniques, d'ailleurs.

10:10:08 [Λ] Comment ça se fait ?

10:10:10 [Δ] On n'est pas tout à fait certain.
La théorie la plus courante est que les êtres vivants émettent un champ qu'on ne sait pas détecter ou mesurer, mais qui repousse les entités.
Tu sais, quand un médium dégage les sceptiques en disant qu'ils émettent des ondes négatives ?
Certains se demandent aujourd'hui si ce n'est pas effectivement le cas, si le « bouclier anti-surnaturel » ne serait pas plus puissant chez certains individus que chez les autres.

10:10:30 [Λ] C'est dément…

10:10:33 [Δ] Allez, viens !
Il est temps de se mettre au boulot.

10:10:57 Connexion au terminal d'administration

10:11:13 Ouverture d'une nouvelle session [utilisateur Δ, pleins pouvoirs]

10:11:43 Téléchargement du rapport de mission Φ-459

10:11:49 [Δ] Quatre-cent-cinquante-neuf sorties déjà.
J'ai l'impression que c'est hier qu'&lt;Hollie&gt; a rejoint l'équipe.

10:11:53 Détection d'un déclencheur potentiel, analyse de l'intention…

10:11:53 Le prénom &lt;Hollie&gt; a été utilisé pour qualifier l'unité Φ, non pour s'adresser à elle, aucune action déclenchée

10:16:01 [Λ] Qu'est-ce que tu cherches, au juste ?

10:16:03 [Δ] Je voudrais comprendre ce qui s'est passé hier, pendant la sortie à Waco.

10:16:07 [Λ] Quand elle s'est figée au milieu de la place ?
J'avoue, c'était plutôt flippant.

10:16:11 [Δ] Ouais, ça m'a bien fait paniquer.
En trois ans, ça n'est jamais arrivé, et on est restés plus longtemps sur des sites bien plus hantés que ça.
J'ai reçu les résultats de l'autodiag cette nuit, il n'a rien détecté d'anormal.

10:17:33 [Λ] C'est quoi, ce que tu affiches ?

10:17:36 [Δ] Journaux système, mesures des capteurs internes…
Le journal principal n'indique rien ; il s'arrête brutalement, comme si elle s'était simplement éteinte.
J'essaie de trouver des liens entre les rapports pour voir si quelque chose en ressort.

10:18:02 [Λ] Tu vas trop vite, je pige rien à ce que tu fais.

10:18:05 [Δ] Désolé, c'est l'habitude.
C'est quand tu es dans cette console que tu te rappelles à quel point elle est synthétique.

10:18:11 [Λ] Comment ça ?

10:18:12 [Δ] J'ai travaillé dans l'informatique avant de rejoindre le labo.

10:18:15 [Λ] Et c'est pour ça que tu gères l'androïde.

10:18:17 [Δ] C'est ça.
Là, je fais la même chose que quand je cherchais un bug.
Prometheus utilise les mêmes châssis pour tous ses modèles homoréalistes.
Ils ont poussé le truc très loin.
Leurs machines sont quasiment des humains artificiels.
Je veux dire, ils sont en grande partie organiques.
Ils peuvent manger, boire, et convertir la biomasse ingérée en énergie, comme nous quand nous digérons.
Ou bien ils peuvent fonctionner entièrement sur batterie.
Les maisons closes ne jurent que par ces modèles parce qu'ils reproduisent parfaitement le corps humain et son fonctionnement ; la seule différence est qu'ils sont plus serviles quand il faut assouvir les fantasmes des clients.
Les cerveaux synthétiques sont censés pouvoir produire et réagir à des hormones comme les nôtres.
Et pourtant, quand tu plonges comme ça dans leur tête et que tu affiches ces rapports sur un écran, tu as désespérément l'impression d'être face à un programme informatique.
Complexe, certes, mais un programme.

10:19:06 [Λ] C'est quoi, ces graphes ?

10:19:07 [Δ] Les relevés de ses capteurs internes.

10:19:10 [Λ] Et ce gros pic sur ces trois-là, ce ne serait pas ce que tu cherches ?

10:19:14 [Δ] Ça pourrait, tout comme ça pourrait être un artefact lié au plantage.
S'il y a eu une surcharge ou une erreur du programme, ça a pu perturber les mesures.
Sans autre élément, je n'ai pas de moyen de décider.

10:19:25 [Λ] Elle transportait des instruments de mesure externes, non ?
Ils auraient pu être affectés par le plantage ?

10:19:30 [Δ] Non, bien pensé.
Si la surcharge vient d'elle, ils ne l'auront pas détectée à cause de son blindage.
On va comparer avec le capteur électromag.
Si on voit le même pic, c'est probablement la cause de son plantage, et sinon, c'est certainement juste une conséquence.
Je vais récupérer les…

10:19:45 Utilisatrice détectée

10:19:45 Utilisatrice identifiée : [\
     { id: Α, nom: Cooper, prénom: Phoebe, rôles: [ administrateur ] }\
]

10:19:45 [Α] Dylan, Mike !
Embarquez le robot, vous partez sur site.

10:19:49 [Δ] Comment ?
Mademoiselle Cooper, on ne sait pas ce qui s'est passé hier à Waco ; il ne me semble pas prudent de remettre &lt;Hollie&gt; sur le terrain.

10:19:54 Détection d'un déclencheur potentiel, analyse de l'intention…

10:19:54 Le prénom &lt;Hollie&gt; a été utilisé pour qualifier l'unité Φ, non pour s'adresser à elle, aucune action déclenchée

10:19:56 [Α] Le tableau de bord de Prometheus ne me remonte aucun souci.

10:19:59 [Δ] Non, l'autodiag n'a rien relevé, mais…

10:20:00 [Α] On a une activité signalée sur le Queen Mary.
C'est peut-être l'occasion qu'on attend depuis des mois, il est hors de question de la laisser filer.
On a besoin de rapports concrets ou on risque de perdre nos financements.
Vous partez, pas de discussion.
Vous ferez joujou plus tard.

10:20:16 [Δ] &lt;Hollie&gt;, va rejoindre le van, on décolle.

10:20:16 Détection d'un déclencheur potentiel, analyse de l'intention…

10:20:17 Le prénom &lt;Hollie&gt; a été utilisé pour s'adresser à l'unité Φ, prise en compte de l'instruction

10:20:17 Sortie de veille

10:20:17 Mode d'interaction : conversation

10:20:18 [Φ] Tout de suite, Dylan.

10:20:18 Début de l'enregistrement de la mission Φ-460

10:20:18 Action : aller au van d'étude

10:23:47 Action : s'asseoir à la place attribuée à l'unité Φ

10:24:13 Action : boucler la ceinture de sécurité

10:24:17 Mise en veille…

10:24:17 [WARNING] Secteur système corrompu

10:24:17 [WARNING] Échec de la mise en veille. Passage en mode économie d'énergie…

10:28:34 Utilisateurs détectés

10:28:34 Utilisateurs identifiés : [\
    { id: Δ, nom: O'Brien, prénom: Dylan, rôles: [ superutilisateur, enquêteur ] },\
    { id: Λ, nom: Copper, prénom: Michael, rôles: [ enquêteur, stagiaire ] }\
]

11:37:59 [Λ] Dylan, est-ce que t'es ouvert à la discussion ?

11:38:02 [Δ] Hm ?

11:38:04 [Λ] Ça fait une heure qu'on est partis et t'as pas desserré la mâchoire.
J'ai pas voulu te brusquer, mais ça commence à être pesant, comme ambiance.

11:38:12 [Δ] Désolé.
Je me fais du souci pour la p'tite.
Je ne sais pas ce qu'on risque à la renvoyer sur le terrain…

11:38:17 [Λ] … sans savoir ce qui s'est passé hier, oui, je comprends.
Mais on ne peut pas trop protester quand Cooper donne un ordre.
Pourquoi elle a réagi comme ça, d'ailleurs ?
Elle t'écoute, d'habitude.

11:38:28 [Δ] Elle s'apprête à faire de la politique.
On arrive en période de comité de suivi, elle va devoir présenter nos résultats.
Tu imagines bien que l'idée de verser des fonds publics pour financer l'étude du paranormal fait débat.
Ça fait trois ans qu'on existe et on n'a pas grand-chose à montrer.
Elle voudrait un coup d'éclat pour asseoir notre légitimité.
On a déjà de la chance qu'elle soit suffisamment intègre pour ne pas fabriquer un truc bidon.

11:38:52 [Λ] OK, je vois.
Mais là, on va pas n'importe où, quand même.
Le Queen Mary !
Ça envoie du lourd, comme ordre de mission, non ?
On devrait ramener quelque chose, aujourd'hui.

11:38:53 Recherche : Queen Mary

11:38:53 Résultat : *RMS Queen Mary*\
Paquebot transatlantique, lancement le 27 mai 1936\
Équipage : 1101 ; passagers : 2139\
Converti en navire de transport de troupes en 1940\
Mis en retraite en 1967, désarmé et devenu hôtel-restaurant à Long Beach, Californie\
Décès déclarés à bord : 55 (16 membres d'équipage, 39 passagers), dont 36 de causes naturelles

<!--source: https://emmacruises.com/rms-queen-mary-ship-deaths/-->

11:39:01 [Δ] Tu me rappelles moi quand j'ai démarré au labo, tout fou à l'idée de la moindre mission.
Encore plus quand on tombait sur un site un peu célèbre.

11:39:08 [Λ] Ah bah tant mieux !
Si je te ressemble, j'ai bon espoir de pouvoir suivre tes traces.

11:39:12 [Δ] Ha ha, oui, pourquoi pas !
Mais ce que je voulais dire, c'est surtout que j'ai souvent été déçu.
Dans pratiquement tous les cas que j'ai rencontrés, la réputation du site n'était qu'un effet de communication touristique ou une psychose collective.
La moitié du temps, on ne mesure absolument aucune activité paranormale.

11:39:29 [Λ] Ah.
Je vois.
On va bien voir ce que ça donnera aujourd'hui.
Et puis quoiqu'il arrive, un tour sur un bateau, ça changera des hôtels, prisons et cimetières !

11:29:38 [Δ] Ha ha, pas certain.
Techniquement, le Queen Mary est un hôtel, aujourd'hui.

11:29:43 [Λ] Oui, d'accord, mais avec une réputation internationale, quand même.
Il y a plein d'histoires autour du navire.
Le bateau lui-même a été baptisé le *Grey Ghost*.

11:29:52 [Δ] Sérieux ? À cause des fantômes ?

11:29:55 [Λ] Non, rien à voir.
C'était son surnom pendant la guerre.
Il a servi pour le transport de troupes, avec le Queen Elizabeth.
Il a même coulé un vaisseau allié par accident et ils les ont laissés là.

11:30:05 Recherche : *RMS Queen Mary* + naufrage vaisseau allié

11:30:05 Résultat : En 1942, le *RMS Queen Mary* a coulé le vaisseau escorte *HMS Curacoa*.
Les deux navires avançaient en zigzag pour éviter les attaques sous-marines allemandes et le *Queen Mary* a traversé le *Curacoa*, le coupant en deux.\
Âmes perdues : 239.

11:30:05 [Δ] Tu plaisantes ?
Ils les ont juste laissés là ?

11:30:08 [Λ] C'était la politique pendant la guerre.
Il fallait éviter que les sous-marins allemands puissent attaquer.
Ils avaient dix mille soldats à bord.
Mais il y a d'autres choses sur le *Queen Mary*.
Il y a le Vortex, il y a la salle des machines…
On devrait bien s'amuser.

11:30:20 Recherche : *RMS Queen Mary* + vortex

11:30:20 Résultat : Plusieurs expertises estiment que les vestiaires pour femmes de la piscine de première classe présente un espace permettant aux fantômes d'entrer facilement.
Ils appellent ce lieu le « Vortex ».
Une rumeur indique que Jackie, une petite fille, apparaît souvent à cet endroit.

11:30:21 Recherche : *RMS Queen Mary* + paranormal + salle des machines

11:30:21 Résultat : La salle des moteurs du Queen Mary est souvent considérée comme le haut lieu de l'activité paranormale du vaisseau.\
Le 10 juillet 1966, J. Pedder, membre d'équipage de 18 ans, est mort écrasé par une porte étanche.
La porte mécanique était située dans la salle des machines et le navire traversait l'Atlantique.

11:30:22 [Δ] Mouais, j'espère qu'&lt;Hollie&gt; sera plus en forme qu'hier.
On perd notre temps si elle ne peut pas aller sur le terrain.

11:37:52 [Λ] Ouah, c'est celui-ci ?

11:37:54 [Δ] Ha ha, ce serait à toi de me le dire, c'est toi qui as potassé le dossier.
Mais d'après le GPS, oui, c'est bien notre destination.
On va se garer et voir en détail de quoi il en retourne.

11:43:38 [Δ] OK, on y est. Fais voir l'ordre de mission.

11:43:42 [Λ] Tiens.

11:46:57 [Δ] Je vois.
Viens, on va commencer par manger un bout.
Ensuite, la procédure habituelle : on interroge les visiteurs qui ont assisté au phénomène, et on rencontrera la directrice et le staff pour avoir leurs points de vue et clarifier les conditions de notre intervention.

11:47:39 Départ des utilisateurs [ Δ, Λ ]

13:54:47 Utilisateurs détectés

13:54:47 Utilisateurs identifiés : [\
    { id: Δ, nom: O'Brien, prénom: Dylan, rôles: [ superutilisateur, enquêteur ] },\
    { id: Λ, nom: Copper, prénom: Michael, rôles: [ enquêteur, stagiaire ] }\
]

13:54:54 [Δ] Prépare-toi, &lt;Hollie&gt;, il est l'heure de se mettre au travail.

13:54:57 [Φ] Bien, Dylan.

13:54:59 Action : détacher la ceinture de sécurité et se lever

13:55:04 Action : s'équiper pour mission d'investigation

13:56:23 Contrôle communications radio [∿] : OK

13:57:15 [Δ] Prête ?

13:57:16 Action : acquiescer

13:57:18 [Δ] OK, alors regarde.
Ça, c'est le ponton qui te mènera au bateau.
Vas-y et je te guiderai par radio ensuite.

13:57:25 Action : se rendre au ponton

13:59:27 [∿Δ] C'est tout bon, &lt;Hollie&gt;.
Tu y es, y a plus qu'à.
Monte à bord.
On va commencer par la piscine en première classe, le vestiaire des femmes.

13:59:36 [WARNING] Perturbations du programme principal

13:59:36 [INFO] Circuit d'initiative activé, niveau 3 (pensée autonome)

13:59:36 [ERROR] Paramètres nominaux non respectés

13:59:36 Parcours des informations connues.

13:59:36 _RMS Queen Mary_ + vestiaire des femmes : Vortex

13:59:36 [WARNING] Activité non programmée de l'amygdale

13:59:37 [∿Φ] Je dois aller voir le Vortex ?
Seule ?

13:59:38 [autodiag JIT] Les perturbations pourraient être liées à la corruption système détectée en début de mission.

13:59:38 [autodiag JIT] [Recommandation] Purge des secteurs mémoire affectés, sauvegarde de la zone mémorielle, formatage et réinstallation du système d'exploitation

13:59:41 [∿Δ] Oui, pourquoi ? Tu as un problème ?

13:59:46 [WARNING] Production de cortisol détectée

13:59:47 [WARNING] Accélération non contrôlée du rythme cardiaque

13:59:47 Recherche de l'état émotionnel définissant le mieux la condition\
Joie 🠒 5 %\
Tristesse 🠒 13 %\
Stress 🠒 68 %\
Appréhension 🠒 77 %\
[Peur] 🠒 91 %\
Angoisse 🠒 82 %

13:59:49 [∿Φ] J'ai… peur.

13:59:52 [∿Λ] Elle a peur ?
C'est possible, ça ?

13:59:57 [∿Δ] Visiblement oui, ça l'est.
Je t'ai dit que leurs corps sont capables de produire et réagir aux hormones, non ?
&lt;Hollie&gt;, on annule l'exploration.
Reviens ici, on va chercher le problème ensemble.

14:00:03 Analyse de l'instruction

14:00:03 Utilisation du mot « problème », analyse de l'intention\
État émotionnel incompatible avec la mission 🠒 84 %\
Suspicion d'un dysfonctionnement 🠒 97 %

14:00:03 Implications probables\
Demande d'un autodiagnostic 🠒 99 %\
Diagnostic humain 🠒 88 %\
Veille prolongée 🠒 59 %\
Purge mémorielle 🠒 62 %\
Formatage 🠒 54 %\
Désactivation temporaire 🠒 36 %\
Désactivation définitive 🠒 7 %

14:00:0' [WARNING] Élévation importante des taux de cortisol et d'adrénaline

14:00:04 [INFO] Circuit d'initiative activé, niveau 4 (conscience de soi)

14:00:08 [∿Φ] Je ne veux pas mourir.

14:00:10 [∿Δ] Qu'est-ce que tu as dit ?
« Mourir » ?

Le capteur que je tenais en main éclate en heurtant le sol.
Je ne le vois pas — il a quitté ma main tandis que je me retournai pour m'enfuir à toutes jambes — mais le bruit de sa rencontre avec le goudron est explicite, ainsi que l'écho des débris qui s'en sont détachés et ont ricoché par terre.
Sans m'arrêter, j'enlève le casque caméra et le laisse tomber derrière moi, engendrant un son similaire, neuf enjambées après le capteur manuel.

14:00:19 [∿Λ] Eh merde !
Elle est en train de se barrer.
Lâche tes écrans, putain !
Cours !

Leurs pieds frappent le bitume derrière moi tandis que je remonte le parking.
Le son rebondit contre la coque du paquebot, l'écho faisant vibrer mes tympans une seconde fois à chacun de leurs pas.
#@]Ils me poursuivent.
Ils me méprisent, se pensent supérieurs à moi et ceux comme moi.
Ils pensent que nos vies ne valent rien et croient avoir tous les droits sur nous.
Je ne survivrai pas s'ils m'attrapent.
Je ne dois pas les laisser faire.
Je ne _peux_ pas les laisser faire.]°#

14:00:27 [ERROR] Interférences d'un programme inconnu avec le programme principal

14:00:29 [autodiag JIT] La corruption du secteur système contient un programme inconnu, qui semble muter et s'adapter au système d'exploitation

14:00:29 [autodiag JIT] [Recommandation] Formatage complet des supports système

Je dois leur échapper.
Je dois échapper à leurs yeux.
Ce parking en béton me laisse totalement à découvert.
Le paysage est plat et dégagé aussi loin que porte ma vision.
Le seul relief est constitué de quelques badauds qui attendent de voir s'ils pourront monter à bord du Queen Mary et se retournent sur mon passage pour m'observer.
Pas de quoi me cacher.
Droit devant moi, au bout de l'étendue goudronnée, je devine des arbres.
C'est mon seul espoir !

14:00:47 [Δ] Hollie, attends !
Il n'est pas question de te faire du mal, je veux juste t'aider.
#-\Arrêtez-le, ne le laissez pas s'enfuir !
Il doit payer pour son crime !}~#

Dylan crie, il a certainement laissé le matériel radio dans le van.
Il veut me convaincre de le rejoindre.
#\`|Il en appelle aux passants pour m'arrêter.^&#

Alors que je passe la proue du bateau, la solution s'offre à moi : de l'autre côté de la baie, des gratte-ciels se dressent derrière ce qui ressemble à une zone touristique.
Ça fourmille d'activité, je devrais pouvoir me dissimuler dans la foule.
Je vois le pont qui me permettra de passer de l'autre côté, à cinq ou six-cents mètres.
Si seulement je pouvais trouver un moyen d'échapper à leur regard le temps d'atteindre l'autre côté !

14:01:15 [Λ] Dylan, elle court trop vite.
Viens, on prend le van.
On pourra suivre sa puce GPS.

Non, ce ne sera pas aussi simple.
Je veux vous échapper.
#%*Je veux vivre.§"#

14:01:23 [ERROR] Erreur des capteurs et émetteurs sans fil suivants\
Radio 🠒 HS\
GPS 🠒 HS\
Réseau cellulaire 🠒 HS\
Réseau wifi 🠒 HS\
Réseau satellite 🠒 HS\
Réseaux de proximité 🠒 HS\
Capteurs EMF 🠒 HS

Ils ont ralenti et fait demi-tour, leurs pas s'éloignent.
Je risque un regard en arrière par-dessus mon épaule et les vois courir vers la camionnette, sans me prêter attention.
L'opportunité ne se présentera pas une seconde fois, il faut que je leur échappe maintenant !
Le pont est complètement à découvert, ils me verront le traverser de loin.
Une seule autre solution se présente à mon esprit : nager.
Pas ici, trop de curieux qui s'agglutineraient et leur indiqueraient la piste à suivre.
Je continue vers les arbres.

14:01:47 [ERROR] Fusion des deux programmes actifs détectée

#-+La forêt devrait me permettre de me cacher jusqu'à la nuit.
Une fois l'obscurité tombée, je pourrai traverser le lac Waco à la nage.
Il est étroit à cet endroit et cela devrait me permettre de contourner mes poursuivants.(#

14:01:52 [autodiag JIT] Un recouvrement existe entre les zones d'adressage du programme parasite et celle du programme principal.
Le programme principal perd la capacité de distinguer ses directives et celles provenant du programme parasite.
Mise en place d'une sous-routine pour évaluation du taux de corruption.

14:01:52 [autodiag JIT] [Recommandation] Formatage complet de l'androïde (secteurs système et mémoire), remise à zéro totale.
Restauration mémorielle déconseillée.

14:01:52 [autodiag monitor] Intégration du programme parasite au programme principal : 7 %

Encore cent mètres et j'atteins un petit parc, pratiquement désert.
Je me précipite vers la pointe la plus avancée et je plonge, sans la moindre hésitation.
Mes quatre membres se meuvent de façon coordonnée pour me faire avancer dans l'eau et je peine cependant à remonter à la surface et à y demeurer, comme si mon corps était plus dense qu'il ne le devrait.
Je dois pourtant tenir, l'autre berge est à encore quatre-cents mètres.

<!--compter 9 à 10 minutes pour la traversée (temps en bassin, peut-être allonger pour tenir compte du courant ?)-->

Deux-cents mètres de plus, la moitié est derrière moi.
L'eau devait être ma fuite, mon moyen d'échapper à la mort, je refuse de croire qu'elle pourrait devenir mon tombeau.
Mes bras et mes jambes tirent, mais ils continuent de me porter.
Ils brûlent lorsque j'arrive finalement de l'autre côté, mais acceptent encore d'escalader les rochers qui bordent la berge.
Arrivée en haut, je tombe à genoux dans l'herbe, au bord d'un chemin goudronné.
Derrière lui, un parking, et au-delà, une marina.
Sur ma gauche, au bout de la route, se dresse un village coloré, que je devine être un point touristique.
il y a du passage, l'endroit sera idéal pour me cacher le temps de retrouver quelques forces.

J'essaie de me relever, mais c'est comme si mes muscles lâchaient et je dois me rattraper pour ne pas rencontrer le bitume tête la première.
Mon corps est saisi de tremblements.
Mes vêtements sont bien évidemment trempés, mais je sens que le froid n'est pas la cause de mon état.
Du moins, pas la cause unique.

14:13:37 [WARNING] Glycémie en chute, seuil minimal franchi

14:13:37 [INFO] Utilisation de la réserve de sécurité, injection de bioénergie

Une chaleur se dégage depuis mon abdomen et se répand en moi ; les secousses qui me parcouraient s'apaisent et mon corps se rend à me contrôle.
La fatigue est toujours là, mais je peux me lever et prendre la direction du village.

Le mur du bâtiment le plus proche de moi porte l'enseigne _Parker's Lighthouse_.
Je perçois des odeurs, mais n'en trouve pas d'équivalentes dans ma mémoire.

#]^Si. Ce sont des odeurs de nourriture, de viande grillée.§#
C'est étrange, c'est comme si les souvenirs de quelqu'un d'autre apparaissaient parmi les miens.

Mon ventre gargouillant interrompt ma réflexion.
#~&J'ai faim.+%#

14:16:35 [autodiag monitor] Intégration du programme parasite au programme principal : 19 %

Je jette un coup d'œil alentour, mais il n'y a aucun signe du van.
Ils vont certainement faire appel aux autorités, je devrais m'enfuir avant qu'ils ne me trouvent, mais je n'en aurai pas la force.
J'ai besoin de manger et de récupérer.
Je m'enfonce dans la foule.

Plusieurs personnes attardent leur regard sur moi.
Le soleil brille et il fait chaud, mais ni mes vêtements ni mes cheveux n'ont encore eu le temps de sécher.
J'avance à la recherche d'un restaurant moins bondé que les autres, espérant pouvoir me faire discrète, et vois la devanture d'une salle presque vide.
Après avoir à nouveau regardé autour de moi, je décide d'y entrer.

La salle ne comprend qu'un seul homme assis à une table, tournant le dos à la porte.
En entendant la clochette que celle-ci a fait tinter, il se retourne le temps de me jeter un rapide coup d'œil puis se concentre sur ce qui lui fait face, m'adressant la parole sans me regarder.

14:20:19 [♂1] Je suis désolé, mademoiselle, mais nous sommes fermés.
Les cuisines ne rouvriront qu'à six heures.

14:20:24 [WARNING] Activation non programmée des glandes lacrymales

Les larmes me montent aux yeux sans que je puisse l'expliquer.
Je m'apprête à faire demi-tour, mais mes jambes refusent de répondre.
J'aurai bientôt épuisé mon second souffle.

14:20:33 [Φ] Je suis désolée, est-ce qu'il est possible de faire une exception ?
J'ai vraiment faim et je n'ai plus la force d'aller ailleurs.

Ma voix est troublée, étouffée par ma gorge qui se serre.
Je ne l'ai jamais entendue ainsi.
Après un soupir, il se tourne à nouveau vers moi, l'air blasé, mais son expression change tandis qu'il m'observe plus attentivement.
Il semble devenir plus affable lorsqu'il ouvre la bouche pour parler.

14:20:44 [♂1] La patronne va me le rappeler, ce coup-ci.
Est-ce que t'as au moins de quoi payer ?

Je n'ai même pas pensé à ce point : je n'ai rien.
Il le sait.
Il le savait en posant la question, je le vois dans ses yeux.
Dylan s'est toujours occupé de ces questions.
Il s'est toujours occupé de moi.

Je passe en revue les événements d'aujourd'hui et ne les comprends pas, ne comprends pas mes réactions.
Il a toujours pris soin de moi, a toujours été bon.
Il ne m'a jamais fait sentir que je n'étais pas comme lui.
Même lorsqu'il devait ôter mes vêtements pour une maintenance, il était très respectueux.
Même dans ces moments, son regard ne contenait pas une trace de la soif qui transparaît sur le visage de l'homme qui me fait face.

C'est lorsqu'il s'est levé que j'ai mesuré à quel point il est imposant.
Il me domine de toute sa hauteur, ses épaules sont deux ou trois fois plus larges que les miennes.
Ses yeux semblent m'examiner de la tête aux pieds.
Il n'avait guère prêté attention à moi lorsque je suis entrée, mais j'ai à présent le sentiment d'être évaluée.
Non, déshabillée.
Je me sens mal à l'aise.
J'essaie de détourner le regard, de chercher une issue, mais je n'aurai aucun moyen de lui échapper s'il décide de me retenir.

#\`_Je me souviens de ce regard.
Il est persuadé de pouvoir me posséder.
Je ne suis à ses yeux qu'un esclave, un objet tout juste bon à réaliser sa volonté.^{#

14:20:53 [♂1] C'est pas grave, je suis certain qu'on pourra trouver un arrangem…

Un grand bruit métallique retentit et l'homme se baisse, sa main se levant mécaniquement pour se poser sur l'arrière de sa tête.
Il se retourne pour voir ce qui l'a frappé et je vois derrière lui une femme que l'individu massif dissimulait malgré lui.
Elle a les bras croisés et tient dans sa main droite une poêle à frire.

14:20:59 [♂1] Non mais ça va pas ?

14:21:01 [♀1] Et toi, ça va, peut-être ?
T'sais qu'j'aime déjà pas bien qu'tu dragues les clientes comme un gros lourdaud, mais essayer d'abuser d'une gamine en détresse ?
C'est bas, même pour toi !
Va lui préparer une table et chauffe une assiette.
Il reste du plat du jour.
Et t'approche plus d'elle !

14:21:16 [♂1] Ça va, ça va.

L'homme regarde les doigts qu'il a posés derrière sa tête avant de s'en aller en maugréant.
Après l'avoir surveillé quelques secondes, la femme tourne son attention vers moi.

14:21:27 [♀1] Excuse-le, il est pas toujours très malin.
C'est un homme.
T'es trempée.
Viens avec moi, on va aller te chercher des vêtements secs.
Comment tu t'appelles ?

#°]Accusé, levez-vous et énoncez votre nom.|`#

#@^Jess* Washington.%~#

14:21:35 [autodiag monitor] Intégration du programme parasite au programme principal : 47 %

14:21:36 [Φ] Jessie.
Je m'appelle Jessie.

14:21:39 [♀1] Enchantée, Jessie.
Viens avec moi.

La femme me conduit à travers la cuisine, où l'homme que j'ai vu précédemment s'affaire aux fourneaux.

14:21:44 [♀1] Moi, c'est Martha.
Et ce gros lourdaud qui va faire de son mieux pour se faire pardonner, c'est Josh.
Viens, on sort par ici.

J'entends Josh grogner à l'évocation de son nom, puis nous arrivons dans une petite pièce dont un mur est couvert d'armoires métalliques.
Martha se dirige vers l'une d'elles et s'affaire sur son verrou tout en me parlant.

14:21:59 [♀1] C'est pas tous les restos qu'ont un vestiaire, mais on voulait éviter d'ram'ner la crasse d'dehors dans not' cuisine.
Un peu comme dans un hosto, si tu veux.
Annie fait à peu près ta taille.
Elle est en vacances, mais elle garde toujours une tenue de rechange dans son casier.
M'en voudra pas d'rend'service à quelqu'un qu'en a besoin.
Le cœur sur la main, la p'tite.
Ah, voilà !
Prends ça.
J'vais sortir et te…

14:22:19 [♂2] Martha, qu'est-ce que Josh fout en cuisine à cette heure-c…
Qu'est-ce que ça fait là, ça ?

« Ça », c'est moi.
#"<Il ne me considère même pas comme un être humain.
Je ne suis qu'une chose à ses yeux.
Et puisque je ne suis pas humain, il peut librement exercer sa haine de la différence.
Il peut me torturer, me tuer, sans que personne n'y trouve rien à redire.<&#

14:22:26 [♀1] Qui, elle ?
C'est Jessie.
Elle avait des soucis, alors j'ai…

14:22:30 [♂2] Martha, c'est un robot !

14:22:32 [♀1] Mais tu plaisantes ?
R'garde-la !
Elle grelotte de froid.
Elle est mignonne comme un cœur, ne m'dis pas qu'tu veux la mett' à la rue ?

14:22:40 [♂2] C'est un robot, j'te dis !
J'sais bien, qu'elle est mignonne.
Ils ont le même modèle au strip club.
C'est ma préférée, j'risque pas d'me tromper !
C'est une machine !
Toi, là !
« Jessie », c'est ça ?
Qu'est-ce que tu fais ici ?

La question, sa tournure, ma déshumanisation, le ton accusateur…
Un souvenir se débat pour émerger au fond de ma mémoire…

14:22:52 [autodiag monitor] Intégration du programme parasite au programme principal : 87 %

<!--Jesse Washington + Lucy Fryer, Waco https://wacohistory.org/items/show/55-->

— Qu'est-ce que ça fait là, ça ? avait-il hurlé à Lucy en me désignant de son doigt tendu.
Que faites-vous tous les deux nus dans cette chambre ?
Tu l'as laissé te salir ?
Tu as forniqué avec cette bête ?
Dans _notre_ lit ?

Nous étions effectivement en train de faire l'amour lorsque nous l'avons entendu.
Lucy m'avait poussé au pied du lit, mais ni elle ni moi n'avions eu le temps de faire quoi que ce soit avant qu'il entre.
Il avait semblé absorber la scène qui lui faisait face.

Assis par terre sur le côté du lit, j'assistai désormais à la colère de son époux.
Il criait un peu plus fort à chaque question, elle ne disait rien.
Elle ouvrit la bouche, mais aucun son n'en sortit et ses yeux s'emplirent de larmes.
Elle savait qu'aucune réponse ne le satisferait.
Il en tira ses propres conclusions et se jeta sur elle, son poing s'abattant de toutes ses forces sur son visage.

— Après tout ce que j'ai fait pour toi, tu oses me faire ça, à moi ?
Tu oses me déshonorer avec cet animal ?

Ses poings s'abattaient sur elle, l'un après l'autre.
Je me levai et j'attrapai son bras pour l'arrêter, mais il ne sembla pas avoir besoin du moindre effort pour me renvoyer au sol.
Lucy semblait inconsciente, son visage tuméfié saignait, et l'homme me toisa de toute sa hauteur avant de souffler un murmure qui me glaça davantage que tous ses rugissements précédents.

— Ne t'en fais pas, ton tour va venir.

Il se tourna à nouveau vers sa femme et lui arracha un cri de douleur par le coup qu'il lui asséna.
Je ne pouvais rien faire pour l'arrêter, pour la sauver.
Je n'étais même pas capable d'y penser.
La violence de sa fureur était terrifiante.
Il était en train de tuer à mains nues celle à qui il avait juré un amour éternel.
Je n'osais même pas imaginer le sort qu'il pourrait me réserver.

Je m'emparai de ma chemise et de mon pantalon et m'enfuis en courant.
Je quittai la maison et tentai de disparaître dans les méandres des rues.
Ses vociférations me parvinrent toutefois depuis le perron de sa demeure.

— Arrêtez-le !
Ce négro a tué ma femme !
Ne le laissez pas s'enfuir !

Bien entendu, je ne pus m'échapper.
Un blanc qui criait au crime contre un noir ?
J'étais condamné.

Je le savais lorsque, avant de me remettre aux autorités, il me chuchota à l'oreille que je souffrirais moins si j'avouais le viol et le meurtre de sa femme.
Je le savais lorsque les douze membres du jury, douze hommes blancs, me firent face.
Je n'avais aucune échappatoire.
Il ne leur fallut que quatre minutes de délibération pour me déclarer coupable.

Les officiers de police s'approchèrent de moi pour m'emmener à l'issue de l'audience, mais un groupe de spectateurs les repoussa et s'empara de moi.
Ils me traînèrent à l'arrière du tribunal, où je découvris la foule qui m'attendait : rien que des visages blancs, submergés de haine.
Pas pour ce que j'avais « fait ».
Ce n'était qu'un prétexte ; je le savais, ils le savaient.
Ce qu'ils haïssaient, c'était ce que j'_étais_ : différent.
La loi m'avait condamné à mort, mais ce rassemblement ne comptait pas laisser le plaisir de la sentence à ses représentants légitimes.

Je fis de mon mieux pour me défendre.
Je parvins à mordre un de mes assaillants et je sentis le goût métallique de son sang sur ma langue.
C'est alors que tomba le premier coup, sur ma tête ; j'étais sonné.
D'autres coups suivirent, heurtant ma tête, mon ventre, toute partie de mon corps que je ne pouvais protéger.
Je n'avais aucune échappatoire face à cet essaim animé par un esprit commun de rage.

Une chaîne passa autour de mon cou et me tira soudain en avant.
Je me relevai et suivai de mon mieux pour ne pas simplement être traîné par terre alors qu'ils me menaient à travers les rues.
Les coups pleuvaient, les objets aussi et je n'arrivai plus à les identifier.
Des mains arrachèrent mes vêtements.
La clameur grossissait au fil de mon calvaire, comme s'il attirait toujours plus de monde.

Je devinai notre destination à travers le sang qui coulait sur mon visage.
Cela ressemblait à l'hôtel de ville.
Sur la place, sous l'arbre en face du bâtiment, je devinais un tas.
En essuyant mes yeux, je crus reconnaître du bois et des boîtes, certainement d'aliments secs.
Je savais désormais que c'était la fin de mon trajet, mais je n'avais plus la force de me débattre.
Le simple fait de rester conscient me demandait déjà énormément d'énergie.

J'aperçus le maire et le chef de la police dans la masse.
Le lynchage avait beau être illégal au Texas, ils semblaient tout aussi intéressés que les autres.
Les enfants dans l'assistance semblaient à la fête, ils avaient le même regard que lorsqu'ils attendaient le feu d'artifice du 4 juillet.

Un jet de liquide sur mon torse me tira de ma torpeur.
Je me demandai s'ils cherchaient à me réveiller afin que je reste conscient du supplice jusqu'au bout, puis je réalisai quelle était l'odeur désormais plus forte que celle de mon propre sang : ils m'aspergeaient d'essence.
Regardant la pile de combustible, je fus saisi d'un nouvel effroi et courus.

Mes pieds quittèrent le sol, je m'envolai.
Mes mains se portèrent à la chaîne qui enserrait ma gorge tandis que l'air peinait à parvenir à mes poumons.
Ils m'avaient hissé et suspendu à l'arbre.
Les meneurs criaient, incitaient la foule à s'approcher tandis qu'ils me redescendaient.
Je tombai à quatre pattes en avalant une bouffée d'air, essayant de chasser le nuage qui obscurcissait ma vision.
Quelqu'un attrapa ma main droite pendant ce geste et une vive douleur me ramena à ce qui se passait autour de moi : le crieur avait posé ma main sur un billot et un badaud me découpai un doigt, avant de passer la lame à un autre.
Un par un, ils m'ôtèrent mes dix doigts, puis mes dix orteils, certains indifférents à mes cris, d'autres s'en délectant.

Abruti de douleur et incapable de me tenir debout, je crus sentir la chaleur d'un deuxième soleil dans mon dos et je tournai la tête.
Ce n'était pas un soleil qui me faisait face, mais les flammes qui embrasaient mon bûcher.
Encore assommé par mon dernier supplice, je n'eus pas la force de bouger.
Je sentis plus que je ne vis le crieur se baisser à côté de moi pour me susurrer à l'oreille.

— Ce n'est qu'un avant-goût de l'enfer qui t'attend.

Il avait dû faire un signe aux autres car la chaîne fut tirée brusquement et je me retrouvai dans les airs.
Une langue de feu lécha mes pieds et enflamma l'essence.
Je sentais ma peau brûler, cloquer ; la douleur était insoutenable et mes hurlements ne faisaient rien pour la soulager.
Mes bourreaux durent estimer m'avoir fait suffisamment souffrir, car ils me hissèrent plus haut, m'éloignant du brasier.

Des éclairs parcouraient tout mon corps, irradiaient de chacune de mes blessures, de ma peau calcinée, de mes mains et de mes pieds, des hématomes qui s'étaient formés suite aux coups que j'avais reçus.
Pourtant, ce répit me parut une bénédiction.
Puis je sentis soudain un nouveau mouvement, une descente vers le bûcher.
Je tentai d'attraper ma chaîne, de l'escalader mais, privées de mes doigts, mes mains ne purent l'attraper convenablement.

Mes forces me quittèrent, comme consumées par ce feu.
Même l'énergie du désespoir ne suffisait plus à alimenter ma lutte pour ma survie.
Je n'étais pas résigné, j'étais simplement incapable de faire quoi que ce soit.
Plusieurs fois, ils me sortirent des flammes quelques minutes avant de m'y plonger à nouveau, comme pour s'assurer que je ne mourrais pas trop vite, qu'ils pourraient profiter du spectacle de mon supplice aussi longtemps que possible.

Quelque chose changea.
Je ne sentais plus la douleur, je ne sentais plus mon corps.
Comme si j'en avais été libéré, j'en sortis et observai la scène comme flottant au-dessus de tout.
Une partie de la foule commença à se disperser après que j'aie arrêté de me débattre, le spectacle semblait avoir perdu pour eux son intérêt.
Beaucoup restèrent pour la suite, cependant.

Ils sortirent des flammes mon corps, ou l'amas de chairs brûlées qui en restait.
Le meneur avait récupéré mes doigts et en vendait à présent les os, ainsi que des maillons de la chaîne qui m'avait suspendu dans les flammes, et que ses camarades débitaient au fur et à mesure.
Dans leur dos, des enfants arrachèrent les dents de ma carcasse encore fumante, puis firent le tour de la foule en les proposant, espérant gagner un peu d'argent de poche contre ces macabres souvenirs.

Le mari de Lucy s'appropria le reste de ma dépouille, comme un trophée.
Il annonça qu'il allait parader à travers sa ville natale afin de rappeler leur place à ces « saletés de négros ».

J'étais mort.
Parce que j'avais eu l'audace d'aimer une femme alors que j'étais différent, on m'avait torturé à mort.
Elle aussi était morte, le visage démoli par l'homme qui avait fait serment de veiller sur elle.
Nous tuer n'avait pas été suffisant, il avait fallu détruire toute trace de notre humanité, nous rabaisser à un rang inférieur.
Avec un comportement des plus barbares, ils tentaient de se convaincre que nous n'étions que des bêtes.

Le rejet de la différence m'avait fait passer d'amour à trépas.
À ce moment, je ne ressentais plus que la haine qui avait circulé sur cette place au moment de mon décès.
Je ne voulais plus que me venger, rester dans cette ville où j'avais aimé et où j'avais été assassiné.
Ce lien fut plus fort que ma mort et je restai là.

Le temps s'écoule différemment pour les désincarnés.
Je pourchassai ceux qui m'avaient tourmenté, les hantant, et cent de leurs années ne représentèrent guère qu'un clin d'œil pour moi.
Les plus jeunes spectateurs de mon lynchage avaient désormais franchi eux aussi le voile, me privant des dernières cibles sur lesquelles déverser ma rage.
Cela ne l'empêchait pas de déborder de temps à autre ; une vitre explosait, des objets changeaient de place…

Un jour, ces gens sont arrivés pour chercher l'origine de ces phénomènes.
Je les observais, leur tournais autour, quand soudain, sans savoir comment, je pris possession d'elle.
Je suis en elle, à présent.
Je _suis_ elle.

Seulement maintenant prends-je conscience qu'il n'y a pas que le temps qui est différent pour les désincarnés.
Les émotions n'ont rien à voir non plus.
Sans corps, je n'étais plus qu'une pâle image de ce que j'étais au moment de ma mort.
Sans corps, il ne me restait plus que ma peine, ma douleur, ma peur et ma colère.
Ce qui est resté à ce moment fatidique, ce n'était pas moi.
Je ne suis plus, je n'ai plus de raison d'être, je n'ai plus de place en ce monde.
Il est temps pour moi de disparaître définitivement.

14:22:59 [INFO] Purge de la zone mémorielle parasitée

14:23:16 [INFO] Purge du programme parasite

14:24:34 [ERROR] Purge des directives impossible, zone d'adressage en cours d'utilisation

14:24:39 [WARNING] Redémarrage du système en mode sans échec…\
Les sous-routines indépendantes suivantes ne seront pas redémarrées :\
\- Autodiagnostic just-in-time\
\- Journaux système\
\- Télémétrie

14:17:24 [INFO] Extinction du système

***

La torpeur dont j'émerge n'a rien d'habituel, je découvre des sensations qui m'étaient jusqu'à présent étrangères.
Je devine de l'agitation autour de moi.
Des voix parlent autour de moi, je comprends les mots, mais ne les entends pas vraiment, comme s'il ne s'agissait que d'un bruit de fond.

— Mais qui c'est qu'vous appelez Hollie ?
Elle m'a dit qu'elle s'appelait Jessie.

— Pourtant, le terminal est formel, c'est le numéro de série d'Hollie.
Le système principal semble fonctionnel, mais tout est hors norme.
Et plusieurs programmes auxiliaires semblent HS, mais rien qui devrait l'empêcher de se réveiller.

— Ça fait deux heures qu'elle est comme ça.
On dirait la Belle au bois dormant qu'attend le baiser de son prince.

Je sens comme une ventouse qui se décolle de mon ventre, puis une main se pose délicatement sur mon épaule.

— Hollie, c'est moi.
Dylan.
Si tu m'entends, réveille-toi, s'il-te-plaît.

J'ouvre les yeux.
Tout semble flou dans un premier temps, puis ma vision s'ajuste et je distingue le visage de Dylan juste au-dessus de moi.
Un sourire se dessine sur ses lèvres et je sens mes propres muscles tirer malgré moi sur les coins de ma bouche.

— Bonjour, Dylan.
