---
date: 2022-01-08T15:30:00+01:00
title: Radioactive
slug: radioactive
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Je me réveille aux cendres et à la poussière, j'essuie mon front et transpire ma rouille.

inspirations: [ weekly, songfic ]
keywords: [ micronouvelle, post-apocalyptique ]
songfic:
  title: Radioactive
  link: https://youtu.be/ktvTqknDobU
  artist: Imagine Dragons
  artistLink: https://www.imaginedragonsmusic.com/

challenge:
  period: 2022-01-09
  link: https://twitter.com/commudustylo/status/1479407469822504963
  rules:
  - type: songfic
---

J'émerge lentement de mon sommeil.
Mon masque a bougé pendant la nuit.
L'acidité de l'atmosphère me fait comprendre qu'il a plu : le ciel a déversé les produits chimiques de nos nuages et l'humidité a fait remonter ceux des sols.
Pourtant, tout est déjà sec et je fais attention à ne pas respirer trop fort pour ne pas m'étouffer avec la poussière et les cendres charriées par le moindre courant d'air.

Mon corps est couvert de sueur et je tente vainement d'éponger mon front à l'aide de ma manche.
La trace laissée sur le tissu est couleur de rouille.
C'est d'ailleurs ce dont il s'agit, de l'oxyde de fer pulvérisé, flottant comme un pollen, couvrant l'ensemble du paysage, le teintant de bordeau.

Je me lève et fais quelques étirements, et je le sens au fond de moi.
Comme une pile dans chacun de mes os, comme une centrale nucléaire vivante, l'énergie irradie de moi.

Un mouvement attire mon attention : un nuage de poussière s'élève au loin, visible à travers la carcasse de bus de prison qui cache mon abri.
Je lève mon drapeau pour prévenir les autres et nous partons nous mettre en embuscade.
Nos vêtements sont teints, nos visages peints, nous ne sommes qu'une goutte de rouge dans un de rouille.
Notre cible laisse un sillon beige là où son passage a dérangé la couche superficielle.
Tout sera à nouveau écarlate avant la fin du jour.

Nos muscles se tendent alors que le moment de l'assaut approche.
Mes os irradient comme jamais, j'ai l'impression que l'énergie va brûler tout ce qui m'entoure ou même me faire exploser.
Personne ne m'arrêtera, je suis radioactive.
