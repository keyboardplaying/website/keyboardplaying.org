---
date: 2022-01-08T15:30:00+01:00
title: Radioactive
slug: radioactive
author: chop
license: CC BY-NC-ND 4.0

description: |-
  I'm waking up to ash and dust, \
  I wipe my brow and I sweat my rust.

inspirations: [ weekly, songfic ]
keywords: [ flash fiction, post-apocalyptic ]
songfic:
  title: Radioactive
  link: https://youtu.be/ktvTqknDobU
  artist: Imagine Dragons
  artistLink: https://www.imaginedragonsmusic.com/

challenge:
  period: 2022-01-09
  link: https://twitter.com/commudustylo/status/1479407469822504963
  rules:
  - type: songfic
---

I'm slowly waking up.
My mask moved in the night.
The acidity of the atmosphere is enough for me to know it rained: the sky poured the chemicals from our clouds and the ambiant humidity caused those in the soil to rise.
Yet, everything is dry already and I take care not to breathe too hard.
I wouldn't want to choke on the ash and dust carried by the slightest draft.

My body's drenched in sweat and I vainly try to wipe my brow with my sleeve.
The mark on the fabric has the color of rust.
That's what it is, actually: iron oxide, pulverized and floating like pollen, covering the whole landscape, coloring it maroon.

I stand up and do some stretching, and I feel it, deep inside me.
Like a battery in each of my bones, like a living nuclear plant, energy radiates from me.

A movement catches my eye: a dust cloud is rising in the distance, visible through the prison bus shell that hides our shelter.
I raise my flag to warn the others and we gain our positions for an ambush.
We dyed our clothes, painted our faces, we're only a drop of red in an ocean of rust.
Our target leaves a beige furrow where its passage disturbed the surface layer.
Everything will be scarlet again before the day is over.

Our muscles tighten as the moment of the assault approaches.
My bones heat up like never before.
I feel like the energy will burn everything around me, like I'll explode.
Nobody'll stop me, I'm radioactive.
