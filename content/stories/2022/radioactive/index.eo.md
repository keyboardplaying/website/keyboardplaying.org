---
date: 2022-01-08T15:30:00+01:00
title: Radioaktiva
slug: radioaktiva
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Mi vekiĝas al cindro kaj polvo, \
  Mi viŝas mian frunton kaj mi ŝvitas mian ruston.

inspirations: [ weekly, songfic ]
keywords: [ mikrorakonto, post-apokalipsa ]
songfic:
  title: Radioactive
  link: https://youtu.be/ktvTqknDobU
  artist: Imagine Dragons
  artistLink: https://www.imaginedragonsmusic.com/

challenge:
  period: 2022-01-09
  link: https://twitter.com/commudustylo/status/1479407469822504963
  rules:
  - type: songfic
---

Mi malrapide vekiĝas.
Mia masko moviĝis en la nokto.
La acideco de la atmosfero sufiĉas por ke mi sciu, ke pluvis: la ĉielo verŝis la kemiaĵojn de niaj nuboj, kaj la humideco altigis tiujn en la grundo.
Tamen, ĉio jam estas seka kaj mi zorgas ne tro forte spiri.
Mi ne volus sufoki pro la cindro kaj polvo, kiun portas la plej eta trablovo.

Mia korpo estas trempita de ŝvito kaj mi vane provas viŝi mian frunton per mia maniko.
La marko sur la ŝtofo havas la koloron de la rusto.
Je kio ĝi estas, fakte: feroksido, pulverigita kaj flosanta kiel poleno, kovrante la tutan pejzaĝon, kolorigante ĝin grenata.

Mi ekstaras kaj streĉadas, kaj mi sentis ĝin, profunde en mi.
Kiel baterio en ĉiu el miaj ostoj, kiel vivanta nuklea centralo, energio radias el mi.

Movo kaptas mian atenton: polva nubo leviĝas en la malproksimo, videbla tra la prizona busostaro, kiu kaŝas nian ŝirmejon.
Mi levas mian flagon por averti la aliajn kaj ni iras al niajn poziciojn por embusko.
Ni tinkturfarbis niajn vestaĵojn, ni pentris niajn vizaĝojn, ni estas nur guto de ruĝo en oceano de rusto.
Nia celo lasas flavgrizan sulkon kie sia trairo movis la surfactavolon.
Ĉio estos denove skarlata antaŭ la tago finiĝos.

Niaj muskoloj streĉiĝas kiam alproksimiĝas la momento de la sturmo.
Miaj ostoj varmiĝas kiel neniam antaŭe.
Mi sentas kiel la energio bruligos ĉion ĉirkaŭ mi, kiel mi eksplodos.
Neniu haltigos min, mi estas radioaktiva.
