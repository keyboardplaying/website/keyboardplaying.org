---
date: 2022-02-06T20:30:00+01:00
title: La petveturantino
slug: la-petveturantino
author: chop
license: CC BY-NC-ND 4.0
draft: true

description: |-
  La vojaĝo estas longa kaj soleca.
  Eble la petveturantino, kiun li venigas surŝipe, faros ĝin pli ĝuebla.

inspirations: [ weekly ]
keywords: [ mikrorakonto, blanka sinjorino, malaperata petveturantino, supernatura, urba legendo, fantomo, hantita aŭtovojo, prostituino ]

challenge:
  period: https://twitter.com/commudustylo/status/1489574874163130369
  link: 2022-02-06
  rules:
  - type: theme
    constraint: Urbaj legendoj
---

Li frapetis la levilon dekstre de la stirilo kaj la viŝiloj haltis.
Finfine!
Fine la pluvo estis malantaŭ li kaj li klare vidis antaŭ sia veturilo.
Veturadi nokte ne estis lia plej granda ĝojo.
Veturadi tra arbaro devigis lin atenti pri bestoj, kiuj povus kuri sur la vojon.
Kaj veturadi sole estis mortige enuiga.
Sed kiam pluvas super ĉio tio, ĝi vere fariĝas tasko.

La kilometroj pasis, tiel faris la arboj.
Ĉio estis senespere monotona.
Subite, li ekvidis palan formon de malproksime kaj levis sian piedon.
Kiam lia kamiono alproksimiĝis, li rekonis virinon, vestitan blanke, tenante siajn brakojn kontraŭ la nokta malvarmeto.

Aŭdinte lian motoron, aŭ eble vidante liajn lumilojn, ŝi turnis sin kaj etendis sian brakon, farante la universalan signon de petveturantoj.
Li denove malrapidiĝis kaj haltis ĉe ŝia nivelo, kaj apogis sin por malfermi la pasaĝeran pordon.
Li tiam rimarkis ke ŝia vesto pli similis al noktoĉemizo ol al robo.

"Vi sendube estas malvarma!" li diris.
"Kaj la pluvo venas.
Ĉu mi eble povas forkonduki vin ien?"

"Jes bonvolu!
Mi iros al la sekva vilaĝo."

"Bone, enveturu!"

Ŝi grimpis, tremante, kaj li vidis, ke ŝi estas nudpieda.
Dum li denove forveturis, li ekparolis, ĝoja pro havi iun kun kiu paroli.

"Kiel bela knabino kiel vi finiĝas preskaŭ nuda en la arbaro meze de la nokto?"

"Mi preferus ne paroli pri tio, se tio ne ĝenas vin."

"Nu, ĝi ne povas esti tiel malbona!
Mi havis mian parton de stultaj ŝercoj aŭ vetoj, vi scias.
Eĉ la eraroj, kiuj oni malkovras matene kaj postebrie.
Nu, mi diros al vi unu el miaj kaj vi diros al mi vian rakonton, ĉu?"

"Ne, vere, mi ne volas diskuti pri ĝi."

Ŝia tono ŝanĝiĝis.
Ŝi estis agrabla ĝis nun, sed ne plu, kaj li ne insistis.
Post kelkaj minutoj de tio, li trovis la kompanion pli peza ol la solecon sed, nekutima havi pasaĝeron, li ne trovis alian manieron ol ŝalti la radion por rompi la silencon.

"…tempon por paroli pri urbaj legendoj.
Ĉi-vespere, ni parolis pri klasikaĵo.
Fakte, vi probable konas ĝin: la malaperante petveturanton.
En la plej ofta versio de la rakonto, stranga petveturantino petas aŭtomobiliston de forkonduki ŝin hejmen sed, kiam ili alvenas, ŝi malaperas.
La viro tiam iom esploras la domon kaj malkovras bildon de la virino.
Malnovan bildon, ĉar la virino mortis antaŭ multaj jaroj.

"Kiel ĉiuj nuntempaj legendoj, ĉi tiu havas multajn variantojn.
En iuj regionoj, la virino malaperas alproksimiĝinte danĝeran paŝon.
En Francio, en Calvados, la ĝendarmoj kutimas al ripetaj raportoj.
La unua estas petveturantino, kiu panikas kaj krias kiam alproksimiĝas kurbiĝo.
Oni diras, ke ŝi estas la fantomo de juna virino, kiu hazarde mortis en tiu turniĝo en la 70-aj jaroj.
La dua temas pri alia petveturantino, kiu supozeble mortis en la 60-aj jaroj dum akcidento ĉe rimarkinde danĝera vojkruciĝo.

"Nu, vi scias ke la diferencoj inter la variantoj estas interesaj, sed la similecoj eĉ pli.
En la granda plimulto de kazoj, la petveturanto estas virino, blanke vestita, tiel aliĝanta al la larĝa kategorio de Blankaj Sinjorinoj.
Ne estas multaj ekzemploj de ĉi tiuj kontoj, kie la fantomo estas malbonvola…"

Li ne plu aŭskultis, ne povante ne rimarki, ke lia pasaĝerino ĉiel respondas al la priskribo ke li ĵus aŭdis.
Li provis koncentriĝi pri la vojo, sed daŭre ĵetis flankajn rigardojn por observi ŝin.
Post kelkaj minutoj, ŝi turnis sian kapon al li, larĝa rideto sur siaj lipoj.

"Kion?
Vi pensas, ke mi estas Blanka Sinjorino, ĉu?"

"N… Ne, tute ne," li diris, balbutante.

Ŝi eniris varman ridon, kaj li ne sciis, ĉu li devas zorgi aŭ trankviliĝi, ĝis ŝi finis ridi

"Dankon, mi bezonis ĝin post ĉi tiu nokto.
Bone, mi rakontos al vi mia rakonto, sed ne parolu pri ĝi.
Mi estas… virino kiu laboras nokte, se vi scias, kion mi volas diri.
Mia lasta kliento insistis foriri de la vilaĝo, por ke lia koramikino ne povus kapti lin, do ni iris en la arbaron.
Ne estis problemo ĝis li pagis min.
Li certe estis kontenta, aŭ almenaŭ distrita: li eniris la aŭton kaj forpelis, nur forgesante, ke ankaŭ mi dependis de li.
Ĉiuj miaj vestaĵoj kaj miaj posedaĵoj estas en la malantaŭo de lia kamiono.
Estos amuza kiam lia koramikino trovos ilin!"
