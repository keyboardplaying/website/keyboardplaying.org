---
date: 2022-01-15T22:25:00+01:00
title: La muzeo de eraraj sciencoj
slug: muzeo-eraraj-sciencoj
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Lerneja vojaĝo por malkovri la erarojn de niajn prauloj.

inspirations: [ weekly ]
keywords: [ mikrorakonto, muzeo, infanoj, scienco, historio, arkeologio, tempovojaĝado, fiziko, kvantuma fiziko, matematiko, Einstein ]

challenge:
  period: 2022-01-14
  link: https://twitter.com/commudustylo/status/1481945438160891912
  rules:
  - type: theme
    constraint: scienco
---

"Bonvenon al nia muzeo, infanoj!
Mi pensas, ke via instruisto jam diris al vi, kion ni trezoras ĉi tie kaj montros al vi hodiaŭ?
Jes, diru al mi!"

"La sciencaj eraroj de niaj prauloj!"

"Ĝuste!
Vi vidos, ke estas multaj.
Ni parolos pri multaj aferoj hodiaŭ.
Ĉu vi konas iun?
Antaŭen, ni aŭskultas."

"Mia paĉjo, li diras ke historio ne estas scienco, sed ke ĝi estis eĉ pli malbona antaŭe."

"Jes, via patro pravas.
Nu, en la pasinteco, arkeologoj serĉis historiajn restaĵojn, kiel konstruaĵojn aŭ skribajn spurojn.
Nur tiam, historiistoj provis kunmeti ĉion kaj interpreti ĝin, por konstrui koheran vizion pri tio, kio okazis antaŭ sia epoko.
Estis tre malfacila laboro, ĉefe ĉar la skribaĵoj ne estis objektivaj.
Kelkfoje, la verkantoj provus kaŝi, ke ili faris iom malbonan, same kiel kiam oni faras ion malbonan kaj diras etan mensogon por eviti esti punita."

"Sed panjo laboras por asekuroj, kaj ŝi uzas tempofenestrojn por vidi kio okazis antaŭe.
Ŝi diras, ke ĝi estas perfekta ĉar ŝi povas vidi ĉion, ie ajn kaj iam ajn.
Kial historiistoj ne faris tion?
Ĉar ĝi estas relugi… reĝim… malpermesita?"

"Tre bona demando!
La uzo de tempofenestroj ja estas reguligita, ĉar oni ne volas, ke tiuj, kiuj povas uzi ilin, fiŝu al siaj najbroj, ekzemple.
Sed vi pravas!
La hodiaŭaj historiistoj uzas ilin multe, same kiel ĉiuj, kiuj devas esplori pasintajn eventojn, kiel policanoj kaj asekuristoj."

"Kial do ne antaŭen?"

"Ĉar ĝi simple ne ekzistis!
Niaj prauloj estus tute nekapablaj elpensi ĉi tiujn fenestrojn.
Ĉu iu el vi havas ideon kial?
Ne?
Mi donas al vi indikon: ĝi rilatas al ĉi tiu muzeo."

"Pro scienca eraro?"

"Jes, jen!
Pasintaj scientistoj ne ĝuste komprenis fizikon.
Ekde Antikva Grekio, kaj eĉ antaŭe, niaj prapatroj volis observi la mondon kaj kompreni ĝiajn regulojn.
Ili kalkulis, kaj ili sukcesis trovi rezultojn, kiuj aspektis kiel tio, kion ili observis, do ili opiniis, ke ili estas ĝustaj, kaj ili daŭrigis.
Tiam, ili konstruis tre potencajn komputilojn por kalkuli por ili, kaj ili programis simuladojn kaj ĝi donis al ili rezultojn kiuj kongruis kun iliaj teorioj.
Ili estis feliĉaj!
Sed ili ne komprenis, ke ili enkondukis siajn erarojn en la programado."

"Kiel ili komprenis, ke ili eraris?"

"Kio estis la eraroj?"

"Kiel ili atingis tiom multe, se ilia tuta scienco estis malĝusta?"

"Ha ha, atendu momenton, tio estas multe da demandoj samtempe.
Do kiel ili komprenis, ke ili eraris?
Nu, ili rapide komprenis, ke iliaj ekvacioj ne validas por la tre grandaj: ili ne komprenis la universon kaj la movadon de la galaksioj.
Ili longe kredis, ke ĝi disvatiĝas kaj deduktis, ke ĝi iam aperis en granda eksplodo.
Iliaj regulaoj ankaŭ ne klarigis la tre malgranda, do ili kreis aliajn regulojn, kiujn ili nomis 'kvantuma fiziko.'"

"Mia fratino legis ke Einstenn verŝajne povus korekti ĉiujn iliajn erarojn, se li vivus sufiĉe longe."

"Jes, historiistoj, kun tempofenestroj, povis observi, ke Alberto Einstein komprenis aferojn intuicie, kaj ili opinas ke, donita unu aŭ du pliajn jardekojn, li povus kompreni la problemo de siaj samtempuloj.
Li ne povis tuj eltrovi ĝi nur pro la iloj, kiujn li estis instruista.
Ĉu vi scias, pri kio mi parolas?"

"Esperanto?"

"Ha ha, ne.
Einstein naskiĝis kaj kreskis en Germanio, do ĝi estus la germanan en lia kazo, sed la lingvo ne estis la problemo.
Estis la matematiko.
Fizikistoj uzis la matematiko _multe_.
Ili skribis multajn ekvaciojn, ĝi estis ilia universala lingvo.
Iun tagon, oni malkovris, ke la fundamentoj mem de la matematiko estas malĝustaj, kaj estis kvazaŭ karto estis forigita de la fundo de kartodomo: ĉio kolapsis.
Do ni rekonstruis, sed pli bone ĉi-foje, kaj ni faris multajn novajn malkovrojn survoje.
Nun vi komprenas, kial matematiko havas tiel gravan lokon en nia muzeo: ĝi estas la radika kaŭzo de la plej multaj sciencaj eraroj de niaj prauloj.
Kiel vi ŝatus komenci nian viziton per la matĉambro?"

"Jeeeeeeees!"
