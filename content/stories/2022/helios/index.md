---
date: 2022-01-30T12:20:20+01:00
title: Helios
slug: helios
author: chop
license: CC BY-NC-ND 4.0

description: |-
  The night was magical, but it was now time to leave.

inspirations: [ weekly ]
keywords: [ flash fiction, mythology, night, sun ]

challenge:
  period: 2022-01-30
  link: https://twitter.com/commudustylo/status/1487019371276951552
  rules:
  - type: image
---

This had been a magical night!
It had been so many years since he last had taken a night for himself, to walk among the humans, to talk with them, to see things from their point of view.
Of course, he observed them with curiousity every day from his post, but it just wasn't the same.

His rest was coming to an end however: it would not be long before daylight.
His absence would not go unnoticed if he didn't go back in time.
Fortunately, he had found a nice spot where no one would see him change back to his true form.
In a few seconds, he became sheer light, just in time to rise in the sky and light up the dawn with his clarity.
