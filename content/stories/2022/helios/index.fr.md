---
date: 2022-01-30T12:20:20+01:00
title: Hélios
slug: helios
author: chop
license: CC BY-NC-ND 4.0

description: |-
  La nuit était magique, mais il était maintenant temps de rentrer.

inspirations: [ weekly ]
keywords: [ micronouvelle, mythologie, nuit, soleil ]

challenge:
  period: 2022-01-30
  link: https://twitter.com/commudustylo/status/1487019371276951552
  rules:
  - type: image
---

Cette nuit avait été magique !
Cela faisait tant d'années qu'il n'avait pas pris une nuit pour lui, pour faire un tour au milieu des humains, échanger avec eux, voir les choses de leur point de vue.
Bien entendu, il les observait curieusement depuis son poste, tous les jours, mais ce n'était pas pareil.

Son repos arrivait cependant à son terme, le jour n'allait pas tarder à se lever.
Son absence serait remarquée s'il n'était pas rentré à temps.
Heureusement, il avait trouvé un sympathique emplacement où nul ne le verrait reprendre sa véritable forme.
En quelques secondes, il redevint pure lumière, juste à temps pour monter dans le ciel et éclairer l'aube de sa clarté.
