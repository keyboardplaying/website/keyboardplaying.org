---
title: Histoires
description: |-
  Quelques (micro)nouvelles et exercices d'écriture.
url: /histoires
aliases: [ /stories ]
classes: [ large-page ]

cascade:
  scripts: [ main ]
  classes: [ use-lettrine, use-asterisms ]
  toc: false
---
