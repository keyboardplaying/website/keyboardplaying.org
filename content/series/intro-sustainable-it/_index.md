---
title: Introduction to sustainable IT
description: |-
  Our digital world is far from virtual.
  It has a huge footprint on our world.
  It should be a part of software creator's jobs to make it more sustainable, but they should at least be aware of the topic.
  This series is a brief introduction to this vast topic.
slug: intro-sustainable-it
aliases: [ /series/intro-sustainable-digital ]
weight: 50
---
