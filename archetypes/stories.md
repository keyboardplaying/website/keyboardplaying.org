---
date: {{ .Date }}
title: {{ replace .Name "-" " " | title }}
slug: {{.Name }}
author: chop
license: CC BY-NC-ND 4.0

description: |-
  Summary or catchline for social networks

inspirations: [ weekly, bimonthly, songfic, daily-life ]
#keywords: [ key idea ]

challenge:
  period: #end date for challenge
  link: #Link to tweet on commudustylo profile
  rules:
  - type: general/theme/image/start/device/songfic/personal
    constraint: Describe the rule
draft: true
---

