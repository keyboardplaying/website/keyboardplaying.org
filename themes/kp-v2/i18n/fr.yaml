# Punctuation
':':
  other: ' :'

# Menu
menu.title:
  other: Menu

# Link titles
link.guest-articles:
  other: Publication d'articles invités

# Miniatures
## Variants for pages
miniature.link.read:
  other: Lire
miniature.link.more:
  other: Poursuivre la lecture
miniature.link.go:
  other: Y aller
## Variant for sections
miniature.link.all:
  one: Voir le billet
  other: Voir les billets

# Section info
section.stats.entries:
  one: Un billet
  other: '{{ .Count }} billets'
section.link.rss:
  other: Flux RSS

# Pages
## Stats
page.stats.time:
  one: 'Lecture : {{ .Count }} min'
  other: 'Lecture : {{ .Count }} min'
page.stats.time.lite:
  one: '{{ .Count }} min'
  other: '{{ .Count }} min'
page.stats.comments:
  one: '{{ .Count }} commentaire'
  other: '{{ .Count }} commentaires'
page.stats.comments.none:
  one: Pas encore commenté
page.stats.words:
  other: '{{ . }} mots'
page.stats.runes:
  other: '{{ . }} signes'
page.stats.runes.w.spaces:
  other: '{{ . }} sec'
## Git info
page.git.lastmod.date:
  other: 'MàJ : {{ . }}'
page.git.lastmod.summary:
  other: Modifié par {{ .AuthorName }} le {{ .AuthorDate.UTC.Format "2006-01-02" }}.
## In-page elements
page.info.series:
  other: Ce billet fait partie d'une série
page.toc.title:
  other: Sommaire
page.ref.title:
  one: Source
  other: Sources et références
page.ref.cite.translated:
  other: traduit de
page.ref.author:
  other: par
## General links
page.link.fork:
  other: Fourchez le repo pour proposer une amélioration.
page.link.share:
  other: 'Partager :'
page.link.issue:
  other: Signaler un problème
## Page navigation
page.nav.previous:
  other: 'Précédent : {{ . }}'
page.nav.next:
  other: 'Suivant : {{ . }}'
page.nav.related:
  other: Vous pourriez aussi aimer

# Guest article
page.guest.author:
  other: Cette page est un contenu invité écrit par {{ . }}.
page.guest.more:
  other: En savoir plus sur les contenus invités.

# Cover image
cover.author:
  other: <a href="{{ .link }}">Couverture</a> par {{ .author }}
cover.ack:
  other: en remerciant

# Comments
## Links
comments.link.reply.comment:
  other: Répondre au commentaire (démarrer un fil de discussion)
comments.link.reply.thread:
  other: Répondre au fil de discussion
## Form
comments.form.none:
  other: Soyez le premier à laisser un commentaire
comments.form.title:
  other: Écrivez votre commentaire
comments.form.replying:
  other: En réponse à
comments.form.comment:
  other: Votre commentaire
comments.form.author.name:
  other: Votre nom ou pseudo
comments.form.author.email:
  other: Votre email
comments.form.author.website:
  other: Votre site web
comments.form.notifications.email:
  other: Recevoir un courriel lorsque quelqu'un commente cette page
comments.form.submit:
  other: Soumettre le commentaire
comments.form.submission.inprogress:
  other: Votre commentaire est en cours d'enregistrement. Merci de patienter quelques secondes.
comments.form.submission.success:
  other: Votre commentaire a été enregistré. Il apparaîtra après validation par un modérateur.
comments.form.submission.failure:
  other: Votre commentaire n'a pas pu être enregistré. Merci de réessayer plus tard.
comments.form.submission.spam:
  other: Notre moteur pense qu'il s'agit de pourriel. Rien d'intéressant à dire ?

# Stories and challenges
story.challenge:
  other: Cette histoire a été écrite dans le cadre d'un défi.
story.challenge.link-label:
  other: Cliquez ici pour voir le défi
story.challenge.rule.theme:
  other: 'Thème :'
story.challenge.rule.image:
  other: "Que vous inspire l'image ?"
story.challenge.rule.start:
  other: 'Début imposé :'
story.challenge.rule.device:
  other: 'Figure de style :'
story.challenge.rule.songfic:
  other: "Songfic (texte inspiré d'une chanson) :"
story.challenge.rule.personal:
  other: 'Règle personnelle :'
story.challenge.rule.bonus:
  other: 'Contrainte facultative :'

# Tipping/support
tip.incentive.generic:
  other: Soutenir <a href="{{ .Tip }}">financièrement</a> ou <a href="{{ .Support }}">autrement</a>
tip.incentive.context.blog:
  other: Ça vous a aidé ?
tip.incentive.context.stories:
  other: Vous avez aimé ?
tip.incentive.context.projects:
  other: Ça vous a aidé ?
tip.incentive.cta.anon:
  one: 🥤 Offrez-moi un thé !
  other: 🥤 Offrez-nous un thé !
tip.incentive.cta.nomin:
  other: 🥤 Offrez un thé à {{ .authors }} ou {{ .lastAuthor }}!
tip.incentive.alternative:
  other: ou soutenez-nous autrement.

# Forms
forms.help.gdpr:
  other: |-
    Toutes les données saisies ici, à l'exception de votre email, seront affichées publiquement.
    Plus de détails dans notre {{ . }}.
forms.not.displayed:
  other: Il ne sera pas affiché.
forms.help.optional:
  other: facultatif
forms.help.markdown:
  other: "Vous pouvez utiliser Markdown : **\\*\\*gras\\*\\***, *\\*italique\\**, \\[mon super lien\\]\\(https://l-url-de-mon-super.lien\\)"
forms.action.reset:
  other: Réinitialiser

# Download proxy
dlp.form.url:
  other: URL du fichier à télécharger
dlp.form.submit:
  other: Télécharger
dlp.form.submitted:
  other: En cours…

# Icons
icon.alt.music:
  other: Musique

# Collapses
collapse.hide:
  other: Masquer
collapse.show:
  other: Afficher

# RSS
rss.item.author:
  other: Par {{ . }}

# 403 page
403.title:
  other: Accès interdit
403.quote:
  other: Le rejet ne signifie pas que vous n'êtes pas assez bon ; il signifie que l'autre personne n'a pas remarqué ce que vous avez à offrir.
403.link.home:
  other: Retourner à la page d'accueil

# 404 page
404.title:
  other: Cette page n'existe pas.
404.curse.1:
  other: Diantre !
404.curse.2:
  other: Fichtre !
404.curse.3:
  other: Diable !
404.curse.4:
  other: Saperlipopette !
404.link.home:
  other: Retourner à la page d'accueil
