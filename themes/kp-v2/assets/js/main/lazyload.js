/**
 * Gets a list of the nodes to be lazily loaded.
 *
 * @returns {NodeList} the list of nodes to be lazily loaded
 */
const getLazyNodes = () => document.querySelectorAll('.lazy');

/**
 * Performs the loading of a lazy element by replacing the src and srcset attributes with values
 * from the dataset when those are available.
 *
 * The {@code lazy} class is also removed.
 *
 * @param {*} element the element to load
 */
const loadLazyNode = element => {
    if (element.dataset.sizes) {
        element.sizes = element.dataset.sizes;
    }
    if (element.dataset.srcset) {
        element.srcset = element.dataset.srcset;
    }
    if (element.dataset.src) {
        element.src = element.dataset.src;
    }
    element.classList.remove('lazy');
};

/**
 * Initializes an IntersectionObserver.
 *
 * Relying on the IntersectionObserver API, we'll determine when a lazy elemnt should be loaded.
 */
const initLazyObserver = () => {
    // Second parameter possible: new IntersectionObserver((entries, observer) => {})
    const lazyObserver = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                const element = entry.target;
                loadLazyNode(element);
                lazyObserver.unobserve(element);
            }
        });
    });

    getLazyNodes().forEach(element => {
        lazyObserver.observe(element);
    });
};

export { initLazyObserver };
