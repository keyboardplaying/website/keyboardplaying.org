const toggleCollapsible = toggle => {
    toggle.closest('.collapse').classList.toggle('collapsed');
};

const initCollapsibles = () => {
    document.querySelectorAll('.collapse .toggle')
        .forEach(toggle => {
            toggle.addEventListener(
                'click',
                event => toggleCollapsible(event.target)
            );
        });
};

export { initCollapsibles };
