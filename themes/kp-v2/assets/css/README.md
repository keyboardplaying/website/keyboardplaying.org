# Work in progress

The styles are being reorganized for greater consistency and maintainability.

The future top-level directories should be `variables`, `mobile`, `desktop` and `wide` (and, later on, `print`).
All other directories should disappear as the reorganization progresses.

## Styles priority

### Display

1. `mobile` are the default styles, which should be applied to all components.
2. `desktop` are stylesheet adaptations to make the website more usable on a computer (at least 768px wide).
3. `wide` is dedicated to customizations for large screens (at least 1400px wide).

By default, when looking for a style, you should look in `mobile` first.

### Print

`print` will be dedicated to the print stylesheet.
